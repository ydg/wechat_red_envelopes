<?php $this->_extends('_layouts/bootstrap_layout'); ?>
<?php $this->_block('contents'); ?>
<div class="container">
  <fieldset>
    <legend><?=$title?></legend>
	<table class="table table-striped table-bordered table-condensed table-hover">
	<thead>
	<tr>
		<th>序号</th>
		<th>英文名称</th>
		<th>二字码</th>
		<th>中文名称</th>
	</tr>
	</thead>
	<tbody>
	<?foreach ($country as $k => $c):?>
	<tr>
		<td><?=$k+1?></td>
		<td><?=$c['en_name']?></td>
		<td><?=$c['code']?></td>
		<td><?=$c['cn_name']?></td>
	</tr>
	<?endforeach;?>
	</tbody>
	</table>
  </fieldset>
</div>
<?php $this->_endblock();?>