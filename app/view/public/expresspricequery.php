<?php $this->_extends('_layouts/bootstrap_layout'); ?>
<?php $this->_block('contents'); ?>
<div class="container">
<form method="get" action="">
<?include(Q::ini('custom_system/elements_dir') . 'searchform_element.php');?>
  <fieldset>
    <legend><?=$title?></legend>
    <?if (isset($result)):?>
	<table class="table table-bordered">
	<thead>
	<tr>
		<th>邮寄方式</th>
		<th>价格</th>
		<th>状态</th>
	</tr>
	</thead>
	<tbody>
	<?foreach ($result as $k => $r):?>
	<tr class="<?if ($r['ack'] == SUCCESS){echo $r['ack'];}else{echo 'error';}?>">
		<td><?=Shipping_Method::getShippingMethodByCode($k, 'name')?></td>
		<td><?=$r['postage_currency_symbol'] . $r['postage']?></td>
		<td><?=empty($r['message'])?'查询成功':$r['message']?></td>
	</tr>
	<?endforeach;?>
	</tbody>
	</table>
	<?endif;?>
    <label>目的国家</label>
    <select class="span2" name="country">
	<option>请选择..</option>
	<?foreach ($country as $c):?>
	<option value="<?=$c['code']?>" <?if (isset($_GET['country']) && $_GET['country'] == $c['code'])echo "selected='selected'"?>><?=$c['code'] .' '. $c['cn_name']?></option>
	<?endforeach;?>
	</select>
	<label>邮寄方式</label>
    <select class="span2" name="shipping_method[]" multiple="multiple">
	<?foreach ($shipping_method as $sm):?>
	<option value="<?=$sm['code']?>" <?if (isset($_GET['shipping_method']) && in_array($sm['code'], $_GET['shipping_method']))echo "selected='selected'"?>><?=$sm['name']?></option>
	<?endforeach;?>
	</select>
    <label class="input-prepend input-append">
	    <span class="add-on">重量</span>
	    <input class="input-mini" type="text" name="weight" value="<?=isset($_GET['weight']) ? $_GET['weight'] : ''?>" />
	    <span class="add-on">g</span>
    </label>
    <br/>
    <button type="submit" class="btn">马上查询</button>
  </fieldset>
</form>
</div>
<?php $this->_endblock();?>