<?php $this->_extends('_layouts/bootstrap_layout'); ?>
<?php $this->_block('contents'); ?>
<div class="container">
<form method="post" action="<?=url('Default::Public/ProductNumberFBA')?>">
<?include(Q::ini('custom_system/elements_dir') . 'searchform_element.php');?>
  <fieldset>
    <legend><?=$title?></legend>
    <label>国家</label>
    <select class="span2" name="country">
	<option value="US">美国</option>
	<option value="UK">英国</option>
	</select>
    <label>SKU</label>
    <label><input type="text" name="number" /></label>
    <label>打印数量</label>
	<label><input type="text" name="quantity" class="input-mini" /></label>
    <button type="submit" class="btn">打印</button>
  </fieldset>
</form>
</div>
<?php $this->_endblock();?>