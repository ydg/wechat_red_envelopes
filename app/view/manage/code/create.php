<!--_meta 作为公共模版分离出去-->
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="Bookmark" href="/favicon.ico" >
<link rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="./hui/lib/html5shiv.js"></script>
<script type="text/javascript" src="./hui/lib/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="./hui/static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="./hui/static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="./hui/lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="./hui/static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="./hui/static/h-ui.admin/css/style.css" />
<!--[if IE 6]>
<script type="text/javascript" src="./hui/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<!--/meta 作为公共模版分离出去-->

<title>导入防伪码</title>
<meta name="keywords" content="">
<meta name="description" content="">
<style>
@media (min-width: 768px)
{
	.col-sm-3 {
		width: 15%;
	}
	.col-sm-offset-3 {
		margin-left: 15%;
	}
}
</style>
</head>
<body>
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 防伪管理 <span class="c-gray en">&gt;</span> 防伪导入 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
	<form action="<?=url('Manage::Code/CreateSave')?>" method="post" class="form form-horizontal" id="form-member-add" enctype="multipart/form-data">
		<input type="hidden" value="1" name="type">
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3">选择文件：</label>
			<div class="formControls col-xs-8 col-sm-9"> <span class="btn-upload form-group">
				<input class="input-text upload-url" type="text" name="uploadfile" id="uploadfile" readonly nullmsg="请添加附件！" style="width:200px">
				<a href="javascript:void();" class="btn btn-primary radius upload-btn"><i class="Hui-iconfont">&#xe642;</i> 浏览文件</a>
				<input type="file" multiple name="file-2" class="input-file">
				</span> 
			</div>
		</div>
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
				<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
			</div>
		</div>
	</form>
</div>

<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="./hui/lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="./hui/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="./hui/static/h-ui/js/H-ui.min.js"></script> 
<script type="text/javascript" src="./hui/static/h-ui.admin/js/H-ui.admin.js"></script> 
<!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本--> 
<script type="text/javascript" src="./hui/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="./hui/lib/jquery.validation/1.14.0/jquery.validate.js"></script> 
<script type="text/javascript" src="./hui/lib/jquery.validation/1.14.0/validate-methods.js"></script> 
<script type="text/javascript" src="./hui/lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script type="text/javascript">
$(function(){
	$('.skin-minimal input').iCheck({
		checkboxClass: 'icheckbox-blue',
		radioClass: 'iradio-blue',
		increaseArea: '20%'
	});
	
	/*$("#form-member-add").validate({
		rules:{
			uploadfile:{
				required:false,
			},
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			//$(form).ajaxSubmit();
			var index = parent.layer.getFrameIndex(window.name);
			//parent.$('.btn-refresh').click();
			parent.layer.close(index);
		}
	});*/
});
</script> 
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>