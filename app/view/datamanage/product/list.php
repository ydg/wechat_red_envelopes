<?php $this->_extends('../_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<script type="text/javascript">
$(function(){
	$(".detail").colorbox({width:"40%", height:"65%", iframe:true});
	$(".del").click(function(){
		if ( ! confirm("确定清空？"))
		{
			return false;
		}
	});
});
</script>
<fieldset><legend>查询条件</legend>
<form method="get" action="">
<?include(Q::ini('custom_system/elements_dir') . 'searchform_element.php');?>
<table class="form_table">
<tr>
	<th>商品名称</th>
	<td><input type="text" name="name" value="<?=isset($_GET['name']) ? $_GET['name'] : ''?>" /></td>
</tr>
<tr>
	<td></td>
	<td><input type="submit" value="查询" class="button" /></td>
</tr>
</table>
</form>
</fieldset>
<table width="100%" class="list_table">
<tr>
	<th>商品编号</th>
	<th>商品名称</th>
	<th>商品规格</th>
	<th>商品单价</th>
	<th width="250">操作</th>
</tr>
<?foreach($product as $p):?>
<tr>
	<td scope="row"><?=$p['p_no'];?></td>
	<td><?=$p['p_name'];?></td>
	<td><?=$p['p_type']?></td>
	<td><?=$p['p_price'];?>元</td>
	<td>
		<a class="detail" href="<?=url('DataManage::Product/Detail', array('id'=>$p['id']))?>">详情</a>
		<a href="<?=url('DataManage::Product/Edit', array('id' => $p['id']))?>">编辑</a>
		<a class="del" href="<?=url('DataManage::Product/Delete', array('id'=>$p['id'])) . '#xxx'?>">删除</a>
	</td>
</tr>
<? endforeach;?>
</table>
<?=$page;?>
<?php $this->_endblock();?>