<?php $this->_extends('../_layouts/blank_layout'); ?>
<?php $this->_block('contents'); ?>
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/jq.ui.css" rel="stylesheet" />
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.ui.js"></script>
<script type="text/javascript">
$(function(){
	$(".datepicker").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true});
});
</script>

<fieldset><legend style="font-weight:bolder">产品详细</legend>
<table class="form_table">
<tr>
	<th style="font-weight:bolder">商品编号:</th>
	<td><?=$product['p_no'] ?></td>
</tr>
<tr>
	<th style="font-weight:bolder">商品名称:</th>
	<td><?=$product['p_name'] ?></td>
</tr>
<tr>
	<th style="font-weight:bolder">单价:</th>
	<td><?=$product['p_price'] ?></td>
</tr>
<tr>
	<th style="font-weight:bolder">规格:</th>
	<td><?=$product['p_type'] ?></td>
</tr>
<tr>
	<th style="font-weight:bolder">产品图片:</th>
	<td>
		<img src="<?=$product['p_pic1'] ?>" width="100" height="100" />
	</td>
</tr>
<tr>
	<th style="font-weight:bolder">产品描述:</th>
	<td><textarea id="prodetail" name="prodetail" cols="56" rows="6"><?=$product['p_details'] ?></textarea></td>
</tr>
</table>
</fieldset>
<?php $this->_endblock();?>