<?php $this->_extends('../_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/uploadPreview.js"></script>
<script type="text/javascript">
	$(function() {
		$("#up").uploadPreview({ Img: "ImgPr", Width: 120, Height: 120 });
		$("#submit").click(function(){
			if ($("#name").val()=='')
			{
				alert('商品名称不能为空!');return false;
			}
			if ($("#price").val() == '')
			{
				alert('商品价格不能为空!');return false;
			}
			if ($("#type").val() == '')
			{
				alert('商品规格不能为空!');return false;
			}
			if ($("#up").val() == '')
			{
				alert('商品图片不能为空!');return false;
			}
		});
	});  
</script>
<style type="text/css">
    img {
        border-style:none;
    }
    .box{
        position:relative;
        float:left;
        clear:both;
    }
    input.uploadFile{
        position:absolute;
        left:0px;
        top:36px;
        opacity:0;
        filter:alpha(opacity=0);
        cursor:pointer;
        width:276px;
        height:130px;
        overflow: hidden;
    }
    input.textbox{
        float:left;
        padding:5px;
        color:#999;
        height:24px;
        line-height:24px;
        border:1px #ccc solid;
        width:264px;
        margin-right:4px;
    }
       /* a.link{
        float:left;
        display:inline-block;
        padding:4px 16px;
        color:#fff;
        font:14px "Microsoft YaHei", Verdana, Geneva, sans-serif;
        cursor:pointer;
        background-color:#0099ff;
        line-height:28px;
        text-decoration:none;
    }*/
    
</style>
<form action="<?=url('DataManage::Product/CreateSave')?>" method="post"  enctype="multipart/form-data" >
	<table class="form_table">
	  <tr>
	    <th>编号</th>
	    <td><input type="text" id="number" size="42" style="color:red;font-size:16px;font-weight:bolder;border:none;" value="<?=$product_no?>" name="number" /></td>
	  </tr>
	  <tr>
	    <th>商品名称</th>
	    <td><input type="text" name="name" size="44" id="name" value="" /></td>
	  </tr>
	  <tr>
	    <th>单价</th>
	    <td><input type="text" name="price" size="44" id="price" value="" />元</td>
	  </tr>
	  <tr>
	    <th>规格</th>
	    <td><input type="text" name="type" size="44" id="type" value="" placeholder="规格类型" /></td>
	  </tr>
	  <tr>
	    <th>产品图片</th>
	    <td>
	    	<input type="text" name="copyFile" disabled="disabled" class="textbox" placeholder="点击下框进行上传产品图" />
	    	<div class="box">
	            <a href="javascript:void(0);"  class="link">
	            	<img id="ImgPr" width="276" height="130" />
	            </a>
	            <input type="file" class="uploadFile" id="up" name="upload" onchange="getFile(this,'copyFile')" />
			</div>
        </td>
	  </tr>
	  <tr>
	    <th>商品详细</th>
	    <td><textarea id="prodetail" name="prodetail" cols="56" rows="6"></textarea></td>
	  </tr>
	  <tr>
	    <th></th>
	    <td><input type="submit" name="submit" id="submit" class="button" value="保存"  /></td>
	  </tr>
	</table>
</form>
<script type="text/javascript">
	function getFile(obj,inputName){
		var file_name = $(obj).val();
		$("input[name='"+inputName+"']").val(file_name);
	}
</script>
<?php $this->_endblock();?>