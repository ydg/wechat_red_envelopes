<?php $this->_extends('../_layouts/blank_layout'); ?>
<?php $this->_block('contents'); ?>
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/jq.ui.css" rel="stylesheet" />
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.ui.js"></script>
<script type="text/javascript">
$(function(){
	getSelectVal(); 
    $("#province").change(function(){ 
        getSelectVal(); 
    });
    //上面为地区选择代码
	$("#c_no_p").change(function(){
		var c_no_p = $("#c_no_p").val();
		$('#agent_name').val(c_no_p);
	});
	$("#agent_name").change(function(){
		var c_no_p = $("#agent_name").val();
		$('#c_no_p').val(c_no_p);
	});
});
function getSelectVal(){ 
    $.getJSON("<?=url('DataManage::Agent/Province')?>",{province:$("#province").val()},function(json){ 
        var city = $("#city"); 
        var selectcity = "<?=$customer['c_city']?>";
        $("option",city).remove(); //清空原有的选项 
        $.each(json,function(index,array){
            if (array['cityid']==selectcity)
            {
            	var option = "<option value='"+array['cityid']+"' selected='selected'>"+array['city']+"</option>"; 
            }
            else
            { 
            	var option = "<option value='"+array['cityid']+"'>"+array['city']+"</option>"; 
            }
            city.append(option); 
        }); 
    }); 
}
</script>
	<table class="form_table">
	  <tr>
	    <th>代理编号</th>
	    <td><input type="text" id="number" size="24" value="<?=$customer['c_no']?>" name="number" /></td>
	    <th>密码</th>
	    <td><input type="text" name="password" size="24" id="password" value="<?=$customer['c_pwd']?>" /></td>
	  </tr>
	  <tr>
	    <th>代理名称</th>
	    <td><input type="text" name="name" size="24" id="name" value="<?=$customer['c_name']?>" /></td>
	    <th>代理类型</th>
	    <td>
	    	<select id="type" name="type" style="width:160px" msg="请选择">
				<option value="">请选择代理类型..</option>
				<?foreach ($rank as $r):?>
				<option value="<?=$r['rk_id']?>" <?if ($customer['c_no'] == $r['rk_id']):?>selected="selected"<?endif;?>><?=$r['rk_name']?></option>
				<?endforeach;?>
			</select>
	    </td>
	  </tr>
	  <tr>
	    <th>端口数量</th>
	    <td><input type="text" name="port" size="24" id="port" value="<?=$customer['c_port']?>" /></td>
	    <th>联系人</th>
	    <td><input type="text" name="contact" size="24" id="contact" value="<?=$customer['c_contact']?>" /></td>
	  </tr>
	  <tr>
	    <th>电话</th>
	    <td><input type="text" name="phone" size="24" id="phone" value="<?=$customer['c_phone']?>" placeholder="电话" /></td>
	    <th>传真</th>
	    <td><input type="text" name="fax" size="24" id="fax" value="<?=$customer['c_fax']?>" /></td>
	  </tr>
	  <tr>
	    <th>省份</th>
	    <td>
	    	<select id="province" style="width:160px" name="province">
	    		<?foreach ($provinces as $pv):?>
				<option value="<?=$pv['provinceid']?>" <?if ($pv['provinceid'] == $customer['c_province']):?>selected="selected"<?endif;?>><?=$pv['province']?></option>
				<?endforeach;?>
	    	</select>
	    </td>
	    <th>市</th>
	    <td>
	    	<select id="city" style="width:160px" name="city" >
	    		
	    	</select>
	    </td>
	  </tr>
	  <tr>
	    <th>地址</th>
	    <td colspan="3"><input type="text" name="adrr" size="61.6" id="addr" value="<?=$customer['c_addr']?>" placeholder="详细地址" /></td>
	  </tr>
	  <tr>
	    <th>开户银行</th>
	    <td>
	    	<select id="bank" name="bank" style="width:160px" msg="请选择">
				<option value="">请选择开户银行..</option>
				<?foreach ($bank as $b):?>
				<option value="<?=$b['bk_id']?>" <?if ($customer['c_bank'] == $b['bk_id']):?>selected="selected"<?endif;?>><?=$b['bk_name']?></option>
				<?endforeach;?>
			</select>
	    </td>
	    <th>具体支行</th>
	    <td><input type="text" name="branch" size="24" id="branch" value="<?=$customer['c_bankname']?>" /></td>
	  </tr>
	  <tr>
	    <th>银行账号</th>
	    <td><input type="text" name="bank_no" size="24" id="bank_no" value="<?=$customer['c_bankid']?>" placeholder="" /></td>
	    <th>开户户名</th>
	    <td><input type="text" name="bank_name" size="24" id="bank_name" value="<?=$customer['c_bankuser']?>" /></td>
	  </tr>
	  <tr>
	    <th>所属代理</th>
	    <td><input type="text" name="c_no_p" size="24" id="c_no_p" value="<?=$customer['c_no_p']?>" placeholder="" /></td>
	    <th>代理名称</th>
	    <td>
	    	<select id="agent_name" name="agent_name" style="width:160px" msg="请选择">
				<option value="">请选择代理名称..</option>
				<?foreach ($cust as $c):?>
				<option value="<?=$c['c_no']?>" <?if ($customer['c_no_p'] == $c['c_no']):?>selected="selected"<?endif;?>><?=$c['c_name']?></option>
				<?endforeach;?>
			</select>
	    </td>
	  </tr>
	</table>
<?php $this->_endblock();?>