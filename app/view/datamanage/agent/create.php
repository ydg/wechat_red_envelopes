<?php $this->_extends('../_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<script type="text/javascript">
$(function(){
 	getSelectVal(); 
    $("#province").change(function(){ 
        getSelectVal(); 
    }); 
    //上面为地区选择代码
	$("#c_no_p").change(function(){
		var c_no_p = $("#c_no_p").val();
		$('#agent_name').val(c_no_p);
	});
	$("#agent_name").change(function(){
		var c_no_p = $("#agent_name").val();
		$('#c_no_p').val(c_no_p);
	});
	$("#submit").click(function (){
		if ($("#number").val() == '')
		{
			alert("代理编号不能为空!");return false;
		}
		if ($("#password").val() == '')
		{
			alert("密码不能为空!");return false;
		}
		if ($("#name").val() == '')
		{
			alert("代理名称不能为空!");return false;
		}
		if ($("#type").val() == '')
		{
			alert("代理类型不能为空!");return false;
		}
		if ($("#phone").val() == '')
		{
			alert("电话号码不能为空!");return false;
		}
		if ($("#province").val() == '')
		{
			alert("请选择省份!");return false;
		}
		if ($("#city").val() == '')
		{
			alert("请选择城市!");return false;
		}
		if ($("#addr").val() == '')
		{
			alert("请填写详细地址!");return false;
		}
		if ($("#bank").val() == '')
		{
			alert("请选择开户银行!");return false;
		}
		if ($("#branch").val() == '')
		{
			alert("请填写支行信息!");return false;
		}
		if ($("#bank_no").val() == '')
		{
			alert("请填写银行卡账号!");return false;
		}
		if ($("#bank_name").val() == '')
		{
			alert("请填写开户人姓名!");return false;
		}
		if ($("#c_no_p").val() == '' || $("#agent_name").val() == '')
		{
			alert("请选择所属代理或者填写代理编号!");return false;
		}
	});
});
//除了这个还可以使用纯js，进行省份的选择
function getSelectVal(){ 
    $.getJSON("<?=url('DataManage::Agent/Province')?>",{province:$("#province").val()},function(json){ 
        var city = $("#city"); 
        $("option",city).remove(); //清空原有的选项 
        $.each(json,function(index,array){ 
            var option = "<option value='"+array['cityid']+"'>"+array['city']+"</option>"; 
            city.append(option); 
        }); 
    }); 
}
</script>
<form name="form2"  action="<?=url('DataManage::Agent/CreateSave')?>" method="post" >
	<table class="form_table">
	  <tr>
	    <th>代理编号</th>
	    <td><input type="text" id="number" size="24" value="" name="number" /><span style="color:red;">*</span></td>
	    <td width="68"></td>
	    <th>密码</th>
	    <td><input type="text" name="password" size="24" id="password" value="" /><span style="color:red;">*</span></td>
	  </tr>
	  <tr>
	    <th>代理名称</th>
	    <td><input type="text" name="name" size="24" id="name" value="" /><span style="color:red;">*</span></td>
	    <td width="68"></td>
	    <th>代理类型</th>
	    <td>
	    	<select id="type" name="type" style="width:160px" msg="请选择">
				<option value="">请选择代理类型..</option>
				<?foreach ($rank as $r):?>
				<option value="<?=$r['rk_id']?>"><?=$r['rk_name']?></option>
				<?endforeach;?>
			</select><span style="color:red;">*</span>
	    </td>
	  </tr>
	  <tr>
	    <th>端口数量</th>
	    <td><input type="text" name="port" size="24" id="port" value="" /></td>
	    <td width="68"></td>
	    <th>联系人</th>
	    <td><input type="text" name="contact" size="24" id="contact" value="" /></td>
	  </tr>
	  <tr>
	    <th>电话</th>
	    <td><input type="text" name="phone" size="24" id="phone" value="" placeholder="电话" /><span style="color:red;">*</span></td>
	    <td width="68"></td>
	    <th>传真</th>
	    <td><input type="text" name="fax" size="24" id="fax" value="" /></td>
	  </tr>
	  <tr>
	    <th>省份</th>
	    <td>
	    	<select id="province" style="width:160px" name="province">
				<?foreach ($provinces as $pv):?>
				<option value="<?=$pv['provinceid']?>"><?=$pv['province']?></option>
				<?endforeach;?>
	    	</select>
	    	<span style="color:red;">*</span>
	    </td>
	    <td width="68"></td>
	    <th>市</th>
	    <td>
	    	<select id="city" style="width:160px" name="city" >
	    	</select>
	    	<span style="color:red;">*</span>
	    </td>
	  </tr>
	  <tr>
	    <th>地址</th>
	    <td colspan="4">
	    	<input type="text" name="adrr" size="79" id="addr" value="" placeholder="详细地址" />
	    	<span style="color:red;">*</span>
	    </td>
	  </tr>
	  <tr>
	    <th>开户银行</th>
	    <td>
	    	<select id="bank" name="bank" style="width:160px" msg="请选择">
				<option value="">请选择开户银行..</option>
				<?foreach ($bank as $b):?>
				<option value="<?=$b['bk_id']?>"><?=$b['bk_name']?></option>
				<?endforeach;?>
			</select>
	    </td>
	    <td width="68"></td>
	    <th>具体支行</th>
	    <td><input type="text" name="branch" size="24" id="branch" value="" /></td>
	  </tr>
	  <tr>
	    <th>银行账号</th>
	    <td><input type="text" name="bank_no" size="24" id="bank_no" value="" placeholder="" /></td>
	    <td width="68"></td>
	    <th>开户户名</th>
	    <td><input type="text" name="bank_name" size="24" id="bank_name" value="" /></td>
	  </tr>
	  <tr>
	    <th>所属代理</th>
	    <td><input type="text" name="c_no_p" size="24" id="c_no_p" value="" placeholder="" /></td>
	    <td width="68"></td>
	    <th>所属代理名称</th>
	    <td>
	    	<select id="agent_name" name="agent_name" style="width:160px" msg="请选择">
				<option value="">请选择代理名称..</option>
				<?foreach ($cust as $c):?>
				<option value="<?=$c['c_no']?>"><?=$c['c_name']?></option>
				<?endforeach;?>
			</select>
	    </td>
	  </tr>
	  <tr>
	    <th></th>
	    <td colspan="3">
	    	<input type="submit" name="submit" id="submit" class="button" value="保存"  />
	    	<input type="reset" name="reset" id="reset" class="button" value="取消"  />
	    </td>
	  </tr>
	</table>
</form>
<?php $this->_endblock();?>