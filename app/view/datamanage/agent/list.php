<?php $this->_extends('../_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<script type="text/javascript">
$(function(){
	$(".detail").colorbox({width:"45%", height:"80%", iframe:true});
	$(".del").click(function(){
		if ( ! confirm("确定清空？"))
		{
			return false;
		}
	});
});
</script>
<fieldset><legend>查询条件</legend>
<form method="get" action="">
<?include(Q::ini('custom_system/elements_dir') . 'searchform_element.php');?>
<table class="form_table">
<tr>
	<th>代理编号</th>
	<td><input type="text" name="number" value="<?=isset($_GET['number']) ? $_GET['number'] : ''?>" /></td>
	<th>代理名称</th>
	<td><input type="text" name="name" value="<?=isset($_GET['name']) ? $_GET['name'] : ''?>" /></td>
	<th>所属代理</th>
	<td><input type="text" name="belong" value="<?=isset($_GET['belong']) ? $_GET['belong'] : ''?>" /></td>
</tr>
<tr>
	<td></td>
	<td><input type="submit" value="查询" class="button" /></td>
</tr>
</table>
</form>
</fieldset>
<table width="100%" class="list_table">
<tr>
	<th>代理编号</th>
	<th>代理名称</th>
	<th>代理类型</th>
	<th>电话</th>
	<th>省</th>
	<th>市</th>
	<th>状态</th>
	<th width="250">操作</th>
</tr>
<?foreach($agent as $a):?>
<tr>
	<td scope="row"><?=$a['c_no'];?></td>
	<td><?=$a['c_name'];?></td>
	<td>
		<?foreach($rank as $r):?>
		<?=($a['c_type'] == $r['rk_id'])? $r['rk_name'] : ''?>
		<?endforeach;?>
	</td>
	<td><?=$a['c_phone'];?></td>
	<td>
		<?foreach ($provinces as $p):?>
		<?if($a['c_province'] == $p['provinceid']):?><?=$p['province'];?><?endif;?>
		<?endforeach;?>
	</td>
	<td>
		<?foreach ($cities as $c):?>
		<?if($a['c_city'] == $c['cityid']):?><?=$c['city'];?><?endif;?>
		<?endforeach;?>
	</td>
	<td><?if ($a['c_status']):?><span style="color:green;"><?=$a['c_status'];?></span><?else:?><span style="color:red;">未审核</span><?endif;?></td>
	<td>
		<a class="detail" href="<?=url('DataManage::Agent/Detail', array('id'=>$a['id']))?>">详情</a>
		<a href="<?=url('DataManage::Agent/Edit', array('id' => $a['id']))?>">编辑</a>
		<a class="del" href="<?=url('DataManage::Agent/Delete', array('id'=>$a['id'])) . '#xxx'?>">删除</a>
		<?if (empty($a['c_status'])):?>
		<a href="<?=url('DataManage::Agent/Audit', array('id'=>$a['id'])) . '#xxx'?>">审核</a>
		<?endif;?>		
	</td>
</tr>
<? endforeach;?>
</table>
<?=$page;?>
<?php $this->_endblock();?>