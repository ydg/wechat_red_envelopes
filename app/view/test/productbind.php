<?php $this->_extends('./_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/prettyphoto.css" rel="stylesheet" />
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.prettyphoto.js"></script>
<script type="text/javascript">
$(function(){
	$("a[rel^='pretty_photo']").prettyPhoto({theme:'facebook'});
	$(".view").toggle(function(){
		$(this).parent().parent().next().show();
	},function(){
		$(this).parent().parent().next().hide();
	});
	$(".del").click(function(){
		if ( ! confirm("确定删除？"))
		{
			return false;
		}
	});
});
</script>
<table class="list_table" width="80%">
<tr>
	<th>ID</th>
	<th>捆绑号</th>
	<th>捆绑名称</th>
	<th>创建时间</th>
	<th width="100">操作</th>
</tr>
<?foreach ($product_bind as $pb):?>
<tr>
	<td><?=$pb['id']?></td>
	<td><?=$pb['number']?></td>
	<td><?=$pb['name']?></td>
	<td><?=$pb['create_time']?></td>
	<td>
		<a class="view" href="javascript:void(0);">查看</a>
		<a href="<?=url('Product::Bind/Edit', array('id'=>$pb['id']))?>">编辑</a>
		<input type="hidden" value="<?=$pb['id']?>" />
	</td>
</tr>
<tr style="display: none;">
	<td colspan="5">
		<table class="list_table" width="100%">
			<tr>
				<th>产品编号</th>
				<th>产品名称</th>
				<th>捆绑数量</th>
				<th>采购状态</th>
				<th>销售状态</th>
				<th>实际库存</th>
				<th>最近采购价</th>
				<th>资料成本</th>
				<th>周销</th>
				<th>月销</th>
				<th>重量</th>
			</tr>
			<?php $quantity_total = $last_purchase_price_total = $purchase_price_total = $weight_total = 0;?>
			<?foreach ($pb['item'] as $pbi):?>
			<?php $quantity_total += $pbi['quantity']?>
			<?php $last_purchase_price_total += $pbi['last_purchase_price'] * $pbi['quantity'];?>
			<?php $purchase_price_total += $pbi['purchase_price'] * $pbi['quantity'];?>
			<?php $weight_total += $pbi['weight'] * $pbi['quantity'];?>
			<tr>
				<td>
				<?if ($pbi['pic_url']):?>
					<?$pretty_photo_number = 0;?>
					<?foreach ($pbi['pic_url'] as $pbip):?>
					<a rel="pretty_photo[<?=$pbi['id']?>]" href="<?=Q::ini('custom_system/product_img_url_prefix').$pbip?>"><?if(!$pretty_photo_number):?><span class="img_icon"></span><?endif;?></a>
					<?$pretty_photo_number = 1;?>
					<?endforeach;?>
				<?endif;?>
				<?=$pbi['number']?>
				</td>
				<td><?=$pbi['name']?></td>
				<td><?=$pbi['quantity']?></td>
				<td>
					<font color="<?=Helper_BSS_Normal::getIniOne('product_purchase_status', 'value', $pbi['purchase_status'], 'color')?>">
					<?=Helper_BSS_Normal::getIniOne('product_purchase_status', 'value', $pbi['purchase_status'], 'name')?>
					</font>
				</td>
				<td>
					<font color="<?=Helper_BSS_Normal::getIniOne('product_sales_status', 'value', $pbi['sales_status'], 'color')?>">
					<?=Helper_BSS_Normal::getIniOne('product_sales_status', 'value', $pbi['sales_status'], 'name')?>
					</font>
				</td>
				<td><?=$pbi['actual_quantity']?></td>
				<td><?=sprintf('%.2f', $pbi['last_purchase_price'])?></td>
				<td><?=sprintf('%.2f', $pbi['purchase_price'])?></td>
				<td><?=$pbi['weekly_sales']?></td>
				<td><?=$pbi['monthly_sales']?></td>
				<td><?=sprintf('%d', $pbi['weight'])?>g</td>
			</tr>
			<?endforeach;?>
			<tr>
				<th colspan="2"><strong>绑定产品计总：</strong></th>
				<td><?=$quantity_total;?></td>
				<td colspan="3"></td>
				<td>￥<?=$last_purchase_price_total?></td>
				<td>￥<?=$purchase_price_total?></td>
				<td colspan="2"></td>
				<td><?=$weight_total?>g</td>
			</tr>
		</table>
	</td>
</tr>
<?endforeach;?>
</table>
<?=$page;?>
<?php $this->_endblock();?>