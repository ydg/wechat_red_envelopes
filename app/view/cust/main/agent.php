<?php $this->_extends('../_layouts/mobilea_layout'); ?>
<?php $this->_block('contents'); ?>
<link rel="stylesheet" href="<?=Q::ini('custom_system/base_url')?>inc/jquery.bxslider.css">
<script src="<?=Q::ini('custom_system/base_url')?>inc/jquery-1.9.0.min.js"></script>
<script src="<?=Q::ini('custom_system/base_url')?>inc/jquery.bxslider.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){$('.bxslider').bxSlider({captions:true, speed:300, pause:5000, mode:'horizontal', auto:true });})
</script>
<style>
body{background:#fff}
p{margin:0px;}
.panel_ul{list-style:none; font-size:12px;padding:0;padding:5px;position:relative;}
.panel_ul .title{color:#999; font-size:14px;margin:10px 0; height:30px;border-bottom:#ddd 2px solid}
.panel_ul a{border-bottom:#ddd 1px solid; height:50px; padding:10px; display:block}
.panel_ul .ltext{width:70%; float:left;font-size:16px;}
.panel_ul .rtext{ color:#999; float:right;text-align:right;width:30%}
</style>
<ul class="top-tool">
	<li class="title">代理商管理</li>
	<li class="back"></li><li class="home"><span disabled="disabled" onClick="window.location='<?=url('Cust::main/AgentDetail',array('c_no' => CURRENT_USER_NAME))?>&status=new'" style="float:right;margin:20px;font-size:14px">新建</span></li>
</ul>
<div style="height:50px"></div>
<div class="pz-css" style="margin:0px 5px">
<form  method="post">
	<div class="inputs" style=";width:100%; margin: 5px auto">
		<input type="text" style="width:85%" name="key" placeholder="快速查找代理名称">
		<button class="gray" type="submit" style="background:url(img/qry.png)  center no-repeat; width:15%">查找</button>
	</div>
</form>
</div>
<div class="panel_ul"><div class="title">全部代理商 <small>(<?=$count?>)</small></div>
<?foreach ($cust as $c):?>
<a href="<?=url('Cust::main/AgentDetail',array('id'=>$c['id']))?>">
	<div class="ltext"><?=$c['c_name']?><br>
	<small><?=$c['province'].$c['city'].$c['c_addr']?></small></div>
	<div class="rtext"><?=$c['rk_name']?><br><span><?=$c['c_status']?></span></div>
</a>
<?endforeach;?>
</div>

<div>
<div class="pz-box-mode" style="background:rgba(0, 0, 0, 0.7);" id="qrcode" onClick="this.style.display='none'">
<img id="imgqrcode" src="" style="width:100%"/>
</div>
<?include(Q::ini('custom_system/view_dir')."cust/main/bottom.php")?>
</div>
<?php $this->_endblock();?>