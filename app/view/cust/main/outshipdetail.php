<?php $this->_extends('../_layouts/mobilea_layout'); ?>
<?php $this->_block('contents'); ?>
<script src="<?=Q::ini('custom_system/base_url')?>inc/jquery-1.9.0.min.js"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<style>
body{background:#fff}
.lab{padding:0 10px; color:#999; font-size:14px;margin:10px 0 0; height:30px;border-bottom:#ddd 2px solid}
.panel_ul{list-style:none; font-size:12px;padding:0;padding:5px;position:relative;}
.panel_ul li{border-bottom:#ddd 1px solid;height:55px;padding:10px;}

.panel_ul .ltext{width:70%; float:left;font-size:16px;}
.panel_ul .rtext{ color:#999; float:right;text-align:right;width:30%}
h6{ width:50%; float:left}
button span{height:30px!important;width:30px!important;background-size:700%}
#btn_0,#btn_1,#btn_2,#btn_3{width:33.3%;padding:0px;border-bottom:1px solid #eee;border-radius:0px}
#btn_<%=t%>{border-bottom:2px solid #999999}
.group{border-bottom:1px solid #CCCCCC; height:60px;}
</style>
<?php
require_once Q::ini('custom_system/base_url')."jssdk/jssdk.php";
$jssdk = new JSSDK("wx934cb7f852f6c585", "94ea78cbbbd72c8a0ecbfc965a497afc");
$signPackage = $jssdk->GetSignPackage();
?>
<ul class="top-tool">
		<li class="title">单号明细</li>
		<li class="back"><a href="javascript:history.go(-1)"></a></li>
		<li class="home"><span onClick="del()" style="float:right;margin:20px;font-size:14px">删除</span></li>
	</ul>
	<div style="height:50px"></div>
		<div style="margin:5px 8px">
			<div class="inputs" style=";width:100%; margin: 5px auto">
				<input type="text" style="width:70%" onKeyDown="if(event.keyCode==13){addrow();}" id="barcode"  name="barcode" placeholder="添加条码">
				<button class="gray" type="button"  onClick="addrow()" style="width:15%">添加</button>
				<button class="green" type="button" style="width:15%" onClick="scanQRCode();">扫描</button>
			</div>
		</div>
	<div class="lab">全部记录 <small>(<?=$count?>)</small></div>
	
	<ul class="panel_ul" id="panel_ul">
	<?foreach ($ship as $s):?>
		<li barcode='<?=$s['barcode']?>' pno='<?=$s['p_no']?>'>
			<h6>条码：<?=$s['barcode']?></h6><h6>数量：<?=$s['qty']?></h6>
			<h6>产品：<?=$s['p_name']?></h6><h6 style="color:green;">金额：<?=$s['p_price']?></h6>
		</li>
	<?endforeach;?>
	</ul>

	<div style="clear:both;color: transparent; height:60px"></div>
	<div class="bottom-tool pz-css">
		<div class="group">
		<div class="span6"><button type="button" onClick="location.href='<?=url('Cust::main/Outship')?>'" id="btn_cancel">返回</button></div>
		<div class="span6"><button type="button" onClick="save()" class="red">保存</button></div>
		</div>
	</div>
	<?
		$no = isset($_GET['no'])?$_GET['no']:'0';
		$imode = isset($_GET['imode'])?$_GET['imode']:1;//有就是新建，没有就是添加
		$c_no = isset($_GET['c_no'])?$_GET['c_no']:'0';
	?>
<script type="text/javascript">
function del(){
	pz_box({title:"温馨提示：",yesno:true,
		yesfun:function(){
		//alert('nihao');return false;
			pz_post("<?=url('Cust::main/OutShipDelAjax')?>&mode=2",{s_no:'<?=$no?>'},
				function(data){
					if(data.success){
						pz_box({
							hide:function(){
								location.href="<?=url('Cust::main/Outship')?>";
							}
						}).show(data.msg);
					}else{pz_box().show(data.msg);}
			},'json');
		}
	}).show("确认删除该单号?");
}
$("#panel_ul").on("click","li",function(){
var $li=$(this);
pz_box({title:"温馨提示：",yesno:true,
	yesfun:function(){
		$li.remove();
		$("small",$(".lab")).html("("+($("li",$(".panel_ul")).length)+")");
		return true;
	}
}).show("确认删除该记录?");
});
function save(){
	var $li=$("li",$(".panel_ul"));
	var str="";
	for(var i=0;i<$li.length;i++){
		str=str+$($li[i]).attr("barcode")+","+$($li[i]).attr("pno")+"\r\n";
	}
	alert(str);
	pz_post("<?=url('Cust::Main/OutShipSaveAjax',array('mode'=> 4+$imode))?>",{s_no:"<?=$no?>",c_no:"<?=$c_no?>",barcode:str},
		function(data){//c_no是收货人,发货的对象
			if(data.success){
				pz_box({
					hide:function(){
						location.href="<?=url('Cust::main/Outship')?>";
					}
				}).show(data.msg);
			}else{pz_box().show(data.msg);}
	},'json');
}
function addrow(){
	barcode=document.getElementById("barcode");
	if(barcode.value==""){
		pz_box().show("请输入产品条码！");
		barcode.focus();
		return false;
	}
	var $li=$("li",$(".panel_ul"));
	for(var i=0;i<$li.length;i++){
		
		if (barcode.value==$($li[i]).attr("barcode")){
			pz_box().show("该条码已经存在！");
			barcode.focus();
			return false;
		}
	}
	pz_post("<?=url('Cust::Main/OutShipAjax')?>&mode=3",{rk_id:'<?=isset($_GET['rk_id'])?$_GET['rk_id']:0?>',barcode:barcode.value},
		function(data){
			if(data.success){
				var str="<li barcode='"+barcode.value+"' pno='"+data.p_no+"'>";
				str+="<h6>条码："+barcode.value+"</h6><h6>数量："+data.qty+"</h6>";
				str+="<h6>产品："+data.p_name+"</h6><h6 style='color:green;'>金额："+data.p_price+"</h6></li>";
				$(".panel_ul").append(str);
				$("small",$(".lab")).html("("+($li.length+1)+")");
			}else{pz_box().show(data.msg);}
	},'json');
	return false;	
}

</script>

<script>
wx.config({
    debug: false,
    appId: '<?=$signPackage["appId"];?>',
    timestamp: <?=$signPackage["timestamp"];?>,
    nonceStr: '<?=$signPackage["nonceStr"];?>',
    signature: '<?=$signPackage["signature"];?>',
    jsApiList: [
                'scanQRCode'
  ]
  });
function scanQRCode(){
	wx.scanQRCode({
		needResult: 1, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果，
		desc: 'scanQRCode desc',
		//scanType: ["qrCode","barCode"], // 可以指定扫二维码还是一维码，默认二者都有
		success: function (res) {
			//if(res.resultStr.length!=12){
			//	alert("数据不正确！");// 当needResult 为 1 时，扫码返回的结果
			//}else{
//				document.getElementById('barcode').value=res.resultStr.substr(9);
//				addrow();
			//}
			var arr = res.resultStr.split(",");
			//alert(arr[0]);
			//document.getElementById('barcode').value=res.resultStr.substr(9);
			//也可查找某个字符串是否包含特有的字符串进行判断
			if (arr.length == 2)
			{
				if (arr[0] == "CODE_128")
				{
					document.getElementById('barcode').value=arr[1];
				}
				else
				{
					if (arr[0] == "tel")
					{
						document.getElementById('barcode').value=arr[1];
					}
				}
			}
			else
			{
				document.getElementById('barcode').value=arr[0];
			}
			addrow();
	}
	});
}

// 9.1.2 扫描二维码并返回结果
//document.querySelector('#scanQRCode1').onclick = function () {
//  wx.scanQRCode({
//    needResult: 1,
//    desc: 'scanQRCode desc',
//    success: function (res) {
//      alert(JSON.stringify(res));
//    }
//  });
//};

//function decryptCode(code, callback) {
//  $.getJSON('/jssdk/decrypt_code.php?code=' + encodeURI(code), function (res) {
//    if (res.errcode == 0) {
//      codes.push(res.code);
//    }
//  });
//}

wx.error(function (res) {
  alert(res.errMsg);
});
</script>
<?php $this->_endblock();?>