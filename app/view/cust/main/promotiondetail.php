<?php $this->_extends('../_layouts/mobilea_layout'); ?>
<?php $this->_block('contents'); ?>
<link rel="stylesheet" href="<?=Q::ini('custom_system/base_url')?>inc/jquery.bxslider.css">
<script src="<?=Q::ini('custom_system/base_url')?>inc/jquery-1.9.0.min.js"></script>
<script src="<?=Q::ini('custom_system/base_url')?>inc/jquery.bxslider.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){$('.bxslider').bxSlider({captions:true, speed:300, pause:5000, mode:'horizontal', auto:true });})
</script>
<style>
body{background:#fff}
p{margin:0px;}
.panel_ul{list-style:none; font-size:12px;padding:0;padding:5px;position:relative;}
.panel_ul .title{color:#999; font-size:14px;margin:10px 0; height:50px;border-bottom:#ddd 2px solid}
.panel_ul div{font-size:16px;}
.panel_ul a{border-bottom:#ddd 1px solid; height:40px; padding:10px; display:block}
.panel_ul .ltext{ color:#333; display:inline-block; font-size:14px;width:80%}
.panel_ul .rtext{ color:#999; display:inline-block; text-align:right;width:20%}
</style>
<ul class="top-tool">
	<li class="title">通告详情</li>
	<li class="back"></li>
</ul>
<div style="height:50px"></div>

<div class="panel_ul">
	<div class="title">
		<span style="text-align:center;padding-left:auto;padding-right:auto;margin-bottom:10px;"><?=$result['title']?></span>&nbsp;&nbsp;<br/>
		<span class=""><?=$result['create_time']?></span>
		<br>
	</div>
	<p><?=$result['content']?></p>
</div>

<div>
<div class="pz-box-mode" style="background:rgba(0, 0, 0, 0.7);" id="qrcode" onClick="this.style.display='none'">
<img id="imgqrcode" src="" style="width:100%"/>
</div>
<?include(Q::ini('custom_system/view_dir')."cust/main/bottom.php")?>
</div>
<?php $this->_endblock();?>