<?php $this->_extends('../_layouts/mobilea_layout'); ?>
<?php $this->_block('contents'); ?>
<script src="<?=Q::ini('custom_system/base_url')?>inc/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>inc/PCASClass.js"></script>
<style>
.pz-panel{background:#F6F6F6;}
td{height:36px;}
</style>
<div class="pz-css">
	<ul class="top-tool">
		<li class="title">个人信息</li>
		<li class="back"><a href="javascript:history.go(-1)"></a>
		</li>
	</ul>
	<div style="height:50px"></div>
	<form action="<?=url('Cust::main/EditinfoSave')?>" id="submit_form" method="post">
		<div class="pz-panel" style="background-color:#fff">
			<table class="list" width="100%" border="0" cellspacing="0">
			<tr>
				<td width="90"></td>
				<td align="left"><span class="span12"><?=CURRENT_USER_NAME?></span></td>
			</tr>
			<tr>
				<td align="right">联系人：</td>
				<td align="left"><span class="span12"><input type="text" value="<?=$result['c_contact']?>" maxlength="20" id="c_contact" name="c_contact"></span></td>
			</tr>
			<tr>
				<td align="right">联系电话：</td>
				<td align="left"><span class="span12"><input type="text" value="<?=$result['c_phone']?>" maxlength="20" id="c_phone" name="c_phone"></span></td>
			</tr>
			<tr>
				<td align="right">所在省份：</td>
				<td align="left">
					<span class="span6">
						<!-- <select class="loginsel" id="c_province" name="c_province"/></select> -->
						<select id="province" class="loginsel" name="province">
							<?foreach ($provinces as $pv):?>
							<option value="<?=$pv['provinceid']?>" <?if ($pv['provinceid'] == $result['c_province']):?>selected="selected"<?endif;?>><?=$pv['province']?></option>
							<?endforeach;?>
				    	</select>
					</span>
					<span class="span6">
						<!--  <select class="loginsel" id="c_city" name="c_city"></select>-->
						<select id="city" class="loginsel" name="city" >
	    				</select>
					</span>
				</td>
			</tr>
			<tr>
				<td align="right">详细地址：</td>
				<td align="left"><span class="span12"><input type="text" placeholder="收货详细地址"  value="<?=$result['c_addr']?>"  id="c_addr" name="c_addr"></span></td>
			</tr>
			<tr>
				<td align="right">开户银行：</td>
				<td align="left"><span class="span12">
					<select id="c_bank" name="c_bank">
						<option value="">请选择开户银行..</option>
						<?foreach ($bank as $b):?>
						<option value="<?=$b['bk_id']?>" <?if ($result['c_bank'] == $b['bk_id']):?>selected="selected"<?endif;?>><?=$b['bk_name']?></option>
						<?endforeach;?>
					</select>
					</span>
				</td>
			</tr>
			<tr>
				<td align="right">开户支行：</td>
				<td align="left"><span class="span12"><input type="text"  placeholder="微信支付或淘宝请填“-”"  value="<?=$result['c_bankname']?>" maxlength="20" id="c_bankname" name="c_bankname"></span></td>
			</tr>
			<tr>
				<td align="right">开户户名：</td>
				<td align="left"><span class="span12"><input type="text"  placeholder="银行/微信支付/支付宝户名" value="<?=$result['c_bankuser']?>" maxlength="20" id="c_bankuser" name="c_bankuser"></span></td>
			</tr>
			<tr>
				<td align="right">银行帐号：</td>
				<td align="left"><span class="span12"><input type="text"  placeholder="银行/微信支付/支付宝帐号" value="<?=$result['c_bankid']?>" maxlength="20" id="c_bankid" name="c_bankid"></span></td>
			</tr>
		</table>
		</div>
		<div class="group">
			<div class="span6"><button  type="button" onClick="location.href='<?=url('Cust::main/Personcenter')?>'">返回</button></div><div class="span6"><button class="red">确认修改</button></div>
		</div>
		<input type="hidden" id="userID" name="userID" value="<?=$result['id']?>"/>
	</form>

</div>
<script type="text/javascript">
function getSelectVal(){ 
    $.getJSON("<?=url('DataManage::Agent/Province')?>",{province:$("#province").val()},function(json){ 
        var city = $("#city"); 
        var selectcity = "<?=$result['c_city']?>";
        $("option",city).remove(); //清空原有的选项 
        $.each(json,function(index,array){
            if (array['cityid']==selectcity)
            {
            	var option = "<option value='"+array['cityid']+"' selected='selected'>"+array['city']+"</option>"; 
            }
            else
            { 
            	var option = "<option value='"+array['cityid']+"'>"+array['city']+"</option>"; 
            }
            city.append(option); 
        }); 
    }); 
}
$("input,textarea,select").on("change",function(){
	$(this).removeClass("error");
});
$(function() {
	getSelectVal(); 
    $("#province").change(function(){ 
        getSelectVal(); 
    });
	$("#submit_form").submit(function(){
		var f=$("input,textarea,select")
		for(var i=0;i<f.length;i++){if($(f[i]).val().length==0){$(f[i]).addClass("error");$(f[i]).focus();return false}}
		pz_postform(submit_form,
			function(data,s,x){
				if (data.status == true) {
					pz_box().show(data.data)
				}else{
					pz_box().show(data.data)
				}
			},"json");
		return false
	})
})
</script>
<?php $this->_endblock();?>