<?php $this->_extends('../_layouts/mobilea_layout'); ?>
<?php $this->_block('contents'); ?>
<link rel="stylesheet" href="<?=Q::ini('custom_system/base_url')?>inc/jquery.bxslider.css">
<script src="<?=Q::ini('custom_system/base_url')?>inc/jquery-1.9.0.min.js"></script>
<script src="<?=Q::ini('custom_system/base_url')?>inc/jquery.bxslider.min.js"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">

$(document).ready(function(){$('.bxslider').bxSlider({captions:true, speed:300, pause:5000, mode:'horizontal', auto:true });});

function buy(mode){
	var spec1=0,spec2=0;
	if($("[name='spec1']").length){
		if($("[name='spec1']:checked").length==0){
			pz_box().show("请选择 "+$("#specname1").attr("data-name"));
			return false;
		}else{
			spec1=$("[name='spec1']:checked").val();
		}
	}
	if($("[name='spec2']").length){
		if($("[name='spec2']:checked").length==0){
			pz_box().show("请选择 "+$("#specname2").attr("data-name"));
			return false;
		}else{
			spec2=$("[name='spec2']:checked").val();
		}
	}

	var postdata = {
		mode:0,
		trade:"<?=$t_id ?>",
		spec1:spec1,
		spec2:spec2,
		num:$("#num").val();
	}
	var $btn = $(this);
	$.post("cookie.asp",postdata,
	function(data) {
		if (data.status == true){
			if(mode==1){location.href='cart.asp?us_id=<?=us_id?>';}
			pz_box().show(data.data);
		} else {
			pz_box().show(data.data);
		}
	},
	"json").error(function(x,t,e){ alert(t);});
	//return true;
}
</script>
<style>
.pz-panel img{max-width:100%}
</style>
<ul class="top-tool" style="top:0px; left:0px">
	<li class="title">商品详情</li>
	<li class="back"><a href="javascript:history.go(-1)"></a>
	</li><li class="home"><a href="cart.asp"></a></li>
</ul>
<div style="height:50px"></div>
<div style="-moz-box-shadow: 0px 0px 56px #999; -webkit-box-shadow: 0px 0px 6px #999; box-shadow: 0px 0px 6px #999; background:#fff;">
<ul class="bxslider" style="margin-top:0px">
	<?if ($product['p_pic1']):?><li><img  src='<?=$product['p_pic1']?>' width='100%'></li><?endif;?>
	<?if ($product['p_pic2']):?><li><img  src='<?=$product['p_pic2']?>' width='100%'></li><?endif;?>
	<?if ($product['p_pic3']):?><li><img  src='<?=$product['p_pic3']?>' width='100%'></li><?endif;?>
</ul>
	<div style="padding:0px 20px 2px">
		<p>
			<h5><?=$product['p_name']?></h5>
			<h4 style="color:#FF0000;">价格：<?=$product['p_price']?><br>
				<span style="font-size:13px;color:green;">代理价格: <?=$product['p_price']?></span>
			</h4>
			<h5>规格：<?=$product['p_spec']?></h5>
		</p>
	</div>
</div>

<div style="clear:left;height:10px"></div>


<div id="xq" class="pz-panel">
	<?=$product['p_details'] ?>
</div>
<div class="pz-box-mode" style="background:rgba(0, 0, 0, 0.7);" id="guanzhu" onClick="this.style.display='none'">
<img src="img/guanzhu.png" style="width:100%"/>
</div>
<div class="pz-box-mode" style="background:rgba(0, 0, 0, 0.7);" id="fenxiang" onClick="this.style.display='none'">
<img src="img/follow.png" style="width:100%"/>
<div class="bdsharebuttonbox" style="width:240px; margin:auto"><a href="#" class="bds_more" data-cmd="more"></a><a href="#" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间"></a><a href="#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a><a href="#" class="bds_tqq" data-cmd="tqq" title="分享到腾讯微博"></a><a href="#" class="bds_renren" data-cmd="renren" title="分享到人人网"></a><a href="#" class="bds_weixin" data-cmd="weixin" title="分享到微信"></a></div>
<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdUrl":"<%=bdurl%>","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"32"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
</div>

<div>
	<div class="pz-box-mode" style="background:rgba(0, 0, 0, 0.7);" id="qrcode" onClick="this.style.display='none'">
<img id="imgqrcode" src="" style="width:100%"/>
</div>
<?include(Q::ini('custom_system/view_dir')."cust/main/bottom.php")?>
</div>
<?php $this->_endblock();?>