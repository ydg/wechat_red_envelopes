<?php $this->_extends('../_layouts/mobilea_layout'); ?>
<?php $this->_block('contents'); ?>
<style>
body{background:#fff}
.lab{height:20px; width:80px; display:inline-block; margin:3px 6px;border-radius:15px;-moz-border-radius:15px;-webkit-border-radius:15px;background:#0075d9;font-weight:bold; color:#fff; text-align:center; }
</style>
<div class="pz-css">
<ul class="top-tool">
	<li class="title">产品追踪</li>
	<li class="back"><a href="javascript:history.go(-1)"></a>
</ul>
</div>
<div style="height:50px"></div>
<div class="pz-css">
	<div style="clear:both"></div>
	<form  method="post" action="<?=url('Cust::Main/Trackingproduct')?>" style="margin:5px 8px">
		<div class="inputs" style=";width:100%; margin: 5px auto">
			<input type="text" style="width:85%" name="barcode" value="<?=isset($_GET['barcode'])?$_GET['barcode']:''?>" placeholder="请输入物流码查询">
			<button class="gray" type="submit" style="width:15%">查询</button>
		</div>
	</form>
</div>
<?if (isset($result_header)):?>
<?foreach ($result_header as $rh):?>
<div class="pz-panel">
	<h5><span class="lab" style="display:block; text-align:center">总部出库</span></h5>
	<h5><span class="lab">出库单号</span><?=$rh['s_no']?></h5>
	<h5><span class="lab">出库日期</span><?=$rh['s_date']?></h5>
	<h5><span class="lab">产品名称</span><?=$rh['p_name']?></h5>
	<h5><span class="lab">出库条码</span><?=$rh['barcode']?></h5>
	<h5><span class="lab">客户名称</span><?=$rh['c_name']?></h5>
	<h5><span class="lab">客户地址</span><?=$rh['c_province'].$rh['c_city'].$rh['c_addr']?></h5>
	<h5>无出库记录!</h5>
</div>
<?endforeach;?>
<?endif;?>
<?if (isset($result_cust)):?>
<?foreach ($result_cust as $rc):?>
<div class="pz-panel">
	<h5><span class="lab" style="display:block;text-align:center">代理出库</span></h5>
	<h5><span class="lab">出库单号</span><?=$rc['s_no']?></h5>
	<h5><span class="lab">出库日期</span><?=$rc['s_date']?></h5>
	<h5><span class="lab">产品名称</span><?=$rc['p_name']?></h5>
	<h5><span class="lab">出库条码</span><?=$rc['barcode']?></h5>
	<h5><span class="lab">客户名称</span><?=$rc['c_name']?></h5>
	<h5><span class="lab">客户地址</span><?=$rc['c_province'].$rc['c_city'].$rc['c_addr']?></h5>
</div>
<?endforeach;?>
<?endif;?>
	
<div>
	<div class="pz-box-mode" style="background:rgba(0, 0, 0, 0.7);" id="qrcode" onClick="this.style.display='none'">
<img id="imgqrcode" src="" style="width:100%"/>
</div>
<?include(Q::ini('custom_system/view_dir')."cust/main/bottom.php")?>
</div>
<?php $this->_endblock();?>