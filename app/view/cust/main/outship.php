<?php $this->_extends('../_layouts/mobilea_layout'); ?>
<?php $this->_block('contents'); ?>
<script src="<?=Q::ini('custom_system/base_url')?>inc/jquery-1.9.0.min.js"></script>
<style>
body{background:#fff}
.panel_ul{list-style:none; font-size:12px;padding:0;padding:5px;position:relative;}
.panel_ul .title{color:#999; font-size:14px;margin:10px 0 0; height:30px;border-bottom:#ddd 2px solid}
.panel_ul a{border-bottom:#ddd 1px solid; height:70px; padding:10px; display:block}
.panel_ul .ltext{width:70%; float:left;font-size:16px;}
.panel_ul .rtext{ color:#999; float:right;text-align:right;width:30%}
h6{ width:50%; float:left}
button span{height:30px!important;width:30px!important;background-size:700%}
#btn_0,#btn_1,#btn_2,#btn_3{width:33.3%;padding:0px;border-bottom:1px solid #eee;border-radius:0px}
#btn_<?=isset($_GET['t'])?$_GET['t']:0?>{border-bottom:2px solid #999999}
.group{border-bottom:1px solid #CCCCCC; height:60px;}
</style>
<ul class="top-tool">
		<li class="title">出库管理</li>
		<li class="back"><a href="javascript:history.go(-1)"></a></li>
		<li class="back"></li><li class="home"><span onClick="newn()" style="float:right;margin:20px;font-size:14px">新建</span></li>
	</ul>
	<div style="height:50px"></div>
	<div class="pz-css">
		<div style="clear:both"></div>
		<form  method="post" style="margin:5px 8px">
			<div class="inputs" style=";width:100%; margin: 5px auto">
				<input type="text" style="width:85%" name="key" placeholder="快速查找单号">
				<button class="gray" type="submit" style="background:url(img/qry.png)  center no-repeat; width:15%">查找</button>
			</div>
		</form>
	</div>
	<div class="panel_ul"><div class="title">全部记录 <small>(<?=$count?>)</small></div>

	<?foreach ($ship as $s):?>
		<a href="<?=url('Cust::main/OutshipDetail',array('no'=>$s['s_no']))?>&rk_id=<?=$s['c_type']?>&c_no=<?=$s['c_no']?>">
		<ul class="panel_ul">
			<li style="height:60px">
				<h6>单号：<?=$s['s_no']?></h6><h6>日期：<?=$s['s_date']?></h6>
				<h6>客户：<?=$s['c_name']?></h6>
				<h6>数量：<?=$s['s_qty']?></h6><h6 style="color:green;">总额：<?=$s['p_price']?></h6>
			</li>
		</ul>
		</a>
	<?endforeach;?>
	</div>
	
	<div id="post_form" class="pz-css" style="display:none">
		<div class="span3"><label for="c_no">客户编号：</label></div>
		<div class="span9"><input  id="c_no_c" value="" name="c_no_c" type="text"onChange="document.getElementById('c_no').value=document.getElementById('c_no_c').value"></div>
		<div class="span3"><label for="c_no">客户名称：</label></div>
		<div class="span9"><select name="c_no" range="1-20" id="c_no" onChange="document.getElementById('c_no_c').value=document.getElementById('c_no').value">
		<option value=''>请选择客户...</option>
		<?foreach ($cust as $cu):?>
		<option value="<?=$cu['c_no']?>"><?=$cu['c_name']?></option>
		<?endforeach;?>
		</select>
		</div>
		<div class="clear"></div>
	</div>

<script>
function newn(){
	document.getElementById('c_no').value=document.getElementById('c_no_c').value;
	pz_box({title:"温馨提示：",yesno:true,el:"post_form",show:true,title:"请选择发货目标",
		yesfun:function(){
			obj=$("#c_no");if(obj.val().length==0||obj.val().length>20){obj.addClass("error");obj.focus();return false;}
			location.href='<?=url('Cust::main/OutshipDetail',array('imode'=>0))?>&c_no='+obj.val();
		}
	});
}
$("input,select").on("change",function(){$(this).removeClass("error");});
</script>

<div>
<div class="pz-box-mode" style="background:rgba(0, 0, 0, 0.7);" id="qrcode" onClick="this.style.display='none'">
<img id="imgqrcode" src="" style="width:100%"/>
</div>
<?include(Q::ini('custom_system/view_dir')."cust/main/bottom.php")?>
</div>
<?php $this->_endblock();?>