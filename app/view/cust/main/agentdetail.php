<?php $this->_extends('../_layouts/mobilea_layout'); ?>
<?php $this->_block('contents'); ?>
<script src="<?=Q::ini('custom_system/base_url')?>inc/jquery-1.9.0.min.js"></script>
<style>
.pz-panel{background:#F6F6F6;}
td{height:36px;}
</style>
<div class="pz-css">
	<ul class="top-tool">
		<li class="title">代理商详情</li>
		<li class="back"><a href="javascript:history.go(-1)"></a>
		</li><li class="home"><span onClick="formsubmit()" id="submit_form" style="float:right;margin:20px 20px;font-size:14px">保存</span></li>
	</ul>
	<?if (isset($_GET['status'])&&$_GET['status']=='new'):?>
	<div class="pz-panel" style="background-color:#fff">
	<div style="height:50px"></div>
	<span style="color:red;">为了方便管理,手机端暂时不开放添加代理！</span>
	<div style="height:50px"></div>
	</div>
	<?else:?>
	<div style="height:50px"></div>
	<form action="<?=url('Cust::main/Agent',array('c_no'=>$c['c_no']))?>" id="submit_form" method="post">
		<div class="pz-panel" style="background-color:#fff">
			<table class="list" width="100%" border="0" cellspacing="0">
			<tr>
				<td width="90" style="color:red;" align="right">代理编号：</td>
				<td align="left"><span class="span12"><input type="text" id="c_no" name="c_no" maxlength="15" value="<?=$result['c_no']?>"/></span></td>
			</tr>
			<tr>
				<td align="right">代理名称：</td>
				<td align="left"><span class="span12"><input type="text" value="<?=$result['c_name']?>" maxlength="20" id="c_name" name="c_name"></span></td>
			</tr>
			<tr>
				<td align="right">联系人：</td>
				<td align="left"><span class="span12"><input type="text" value="<?=$result['c_contact']?>" maxlength="20" id="c_contact" name="c_contact"></span></td>
			</tr>
			<tr>
				<td align="right">联系电话：</td>
				<td align="left"><span class="span12"><input type="text" value="<?=$result['c_phone']?>" maxlength="20" id="c_phone" name="c_phone"></span></td>
			</tr>
			<tr>
				<td align="right">所在省份：</td>
				<td align="left">
					<span class="span6">
						<!-- <select class="loginsel" id="c_province" name="c_province"/></select> -->
						<select id="province" class="loginsel" name="province">
							<?foreach ($provinces as $pv):?>
							<option value="<?=$pv['provinceid']?>" <?if ($pv['provinceid'] == $result['c_province']):?>selected="selected"<?endif;?>><?=$pv['province']?></option>
							<?endforeach;?>
				    	</select>
					</span>
					<span class="span6">
						<!--  <select class="loginsel" id="c_city" name="c_city"></select>-->
						<select id="city" class="loginsel" name="city" >
	    				</select>
					</span>
				</td>
			</tr>
			<tr>
				<td align="right">详细地址：</td>
				<td align="left"><span class="span12"><input type="text" placeholder="用于物流配送"  value="<?=$result['c_addr']?>"  id="c_addr" name="c_addr"></span></td>
			</tr>
		</table>
		</div>
		<div class="group">
			<div class="span12"><button onClick="window.location='<?=url('Cust::main/Agent',array('c_no'=>$result['c_no']))?>'" type="button" class="red">下属代理</button></div>
		</div>
	</form>
	<?endif;?>
</div>

<div>
<div class="pz-box-mode" style="background:rgba(0, 0, 0, 0.7);" id="qrcode" onClick="this.style.display='none'">
<img id="imgqrcode" src="" style="width:100%"/>
</div>
<?include(Q::ini('custom_system/view_dir')."cust/main/bottom.php")?>
</div>

<script type="text/javascript">
$("input,textarea,select").on("change",function(){
	$(this).removeClass("error");
});
$(function() {
	getSelectVal(); 
    $("#province").change(function(){ 
        getSelectVal(); 
    });
	$("#submit_form").submit(function(){
		var f=$("input,textarea,select");
		for(var i=0;i<f.length;i++){if($(f[i]).val().length==0){$(f[i]).addClass("error");$(f[i]).focus();return false;}}
		pz_postform(submit_form,
			function(data,s,x){
				if (data.status == true) {
					pz_box().show(data.data);
				}else{
					pz_box().show(data.data);
				}
			},"json");
		return false;
	})
})

function getSelectVal(){ 
    $.getJSON("<?=url('DataManage::Agent/Province')?>",{province:$("#province").val()},function(json){ 
        var city = $("#city"); 
        var selectcity = "<?=$result['c_city']?>";
        $("option",city).remove(); //清空原有的选项 
        $.each(json,function(index,array){
            if (array['cityid']==selectcity)
            {
            	var option = "<option value='"+array['cityid']+"' selected='selected'>"+array['city']+"</option>"; 
            }
            else
            { 
            	var option = "<option value='"+array['cityid']+"'>"+array['city']+"</option>"; 
            }
            city.append(option); 
        }); 
    }); 
}
</script>
<?php $this->_endblock();?>
