<?php $this->_extends('../_layouts/mobilea_layout'); ?>
<?php $this->_block('contents'); ?>
<script src="<?=Q::ini('custom_system/base_url')?>inc/jquery-1.9.0.min.js"></script>
<style>
body{background:#fff}
.panel_ul{list-style:none; font-size:12px;padding:0;padding:5px;position:relative;}
.panel_ul .title{color:#999; font-size:14px;margin:10px 0 0; height:30px;border-bottom:#ddd 2px solid}
.panel_ul a{border-bottom:#ddd 1px solid; height:70px; padding:10px; display:block}
.panel_ul .ltext{width:70%; float:left;font-size:16px;}
.panel_ul .rtext{ color:#999; float:right;text-align:right;width:30%}
h6{ width:50%; float:left}
button span{height:30px!important;width:30px!important;background-size:700%}
#btn_0,#btn_1,#btn_2,#btn_3{width:33.3%;padding:0px;border-bottom:1px solid #eee;border-radius:0px}
#btn_<%=t%>{border-bottom:2px solid #999999}
.group{border-bottom:1px solid #CCCCCC; height:60px;}
</style>
<ul class="top-tool">
		<li class="title">入库管理</li>
		<li class="back"><a href="javascript:history.go(-1)"></a>
		</li>
	</ul>
	<div style="height:50px"></div>
	<div class="pz-css">
		<div class="inputs span12" style="text-align:center">
			<div id="btn_0" onClick="javascript:location.href='<?=url('Cust::main/Inship',array('t'=>0))?>'">全部</div>
			<div id="btn_1" onClick="javascript:location.href='<?=url('Cust::main/Inship',array('t'=>1))?>'">总部发货</div>
			<div id="btn_2" onClick="javascript:location.href='<?=url('Cust::main/Inship',array('t'=>2))?>'">上级发货</div>
		</div>
		<div style="clear:both"></div>
		<form  method="post" style="margin:5px 8px">
			<div class="inputs" style=";width:100%; margin: 5px auto">
				<input type="text" style="width:85%" name="key" placeholder="快速查找单号">
				<button class="gray" type="submit" style="background:url(img/qry.png)  center no-repeat; width:15%">查找</button>
			</div>
		</form>
	</div>
	<div class="panel_ul"><div class="title">全部记录 <small>(<?=$count?>)</small></div>

	<?foreach ($inship as $i):?>
		<a href="<?=url('Cust::main/InshipDetail',array('no'=>$i['s_no']))?>&t=0">
		<ul class="panel_ul">
			<li style="height:60px">
				<h6>单号：<?=$i['s_no']?></h6><h6>日期：<?=$i['s_date']?></h6>
				<h6>客户：<?=$i['c_name']?></h6><h6>来源：<?='总部发货'?></h6>
				<h6>数量：<?=$i['s_qty']?></h6><h6 style="color:green;">总额：<?=$i['p_price']?></h6>
			</li>
		</ul>
		</a>
	<?endforeach;?>
	</div>

<div>
<div class="pz-box-mode" style="background:rgba(0, 0, 0, 0.7);" id="qrcode" onClick="this.style.display='none'">
<img id="imgqrcode" src="" style="width:100%"/>
</div>
<?include(Q::ini('custom_system/view_dir')."cust/main/bottom.php")?>
</div>
<?php $this->_endblock();?>