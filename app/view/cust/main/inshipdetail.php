<?php $this->_extends('../_layouts/mobilea_layout'); ?>
<?php $this->_block('contents'); ?>
<script src="<?=Q::ini('custom_system/base_url')?>inc/jquery-1.9.0.min.js"></script>
<style>
body{background:#fff}
.panel_ul{list-style:none; font-size:12px;padding:0;padding:5px;position:relative;}
.panel_ul .title{color:#999; font-size:14px;margin:10px 0 0; height:30px;border-bottom:#ddd 2px solid}
.panel_ul a{border-bottom:#ddd 1px solid; height:55px; padding:10px; display:block}
.panel_ul .ltext{width:70%; float:left;font-size:16px;}
.panel_ul .rtext{ color:#999; float:right;text-align:right;width:30%}
h6{ width:50%; float:left}
button span{height:30px!important;width:30px!important;background-size:700%}
#btn_0,#btn_1,#btn_2,#btn_3{width:33.3%;padding:0px;border-bottom:1px solid #eee;border-radius:0px}
#btn_<%=t%>{border-bottom:2px solid #999999}
.group{border-bottom:1px solid #CCCCCC; height:60px;}
</style>
<ul class="top-tool">
		<li class="title">单号明细</li>
		<li class="back"><a href="javascript:history.go(-1)"></a>
		</li>
	</ul>
	<div style="height:50px"></div>
	<div class="panel_ul"><div class="title">全部记录 <small>(<?=$count?>)</small></div>
	<?foreach ($inshipdetail as $i):?>
		<a href="">
		<ul class="panel_ul">
			<li style="height:60px">
				<h6>条码：<?=$i['barcode']?></h6><h6>数量：<?=$i['qty']?></h6>
				<h6>产品：<?=$i['p_name']?></h6><h6 style="color:green;">金额：<?=$i['p_price']?></h6>
			</li>
		</ul>
		</a>
	<?endforeach;?>
	</div>
<div>
<div class="pz-box-mode" style="background:rgba(0, 0, 0, 0.7);" id="qrcode" onClick="this.style.display='none'">
<img id="imgqrcode" src="" style="width:100%"/>
</div>
<?include(Q::ini('custom_system/view_dir')."cust/main/bottom.php")?>
</div>
<?php $this->_endblock();?>