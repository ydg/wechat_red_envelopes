<?php $this->_extends('../_layouts/mobilea_layout'); ?>
<?php $this->_block('contents'); ?>
<script src="<?=Q::ini('custom_system/base_url')?>inc/jquery-1.9.0.min.js"></script>
<style>
.pz-panel{background:#F6F6F6;}
td{height:36px;}
</style>
<div class="pz-css">
	<ul class="top-tool">
		<li class="title">密码修改</li>
		<li class="back"><a href="javascript:history.go(-1)"></a>
		</li><li class="home"><a href="index.asp"></a></li>
	</ul>
	<div style="height:50px"></div>
	<form action="<?=url('Cust::Main/EditPasswordSave')?>" id="submit_form" method="post">
		<div class="pz-panel">
			<h4>温馨提示：</h4>
				<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;请妥善保管好您的密码。</h5>
		</div>
		<div class="pz-panel" style="background-color:#fff">
			<table class="list" width="100%" border="0" cellspacing="0">
			<tr>
				<td width="100">旧&nbsp;&nbsp;密&nbsp;&nbsp;码：</td>
				<td width="100%" align="left"><span class="span12"><input type="password" maxlength="20" id="ouserPWD" name="ouserPWD"></span></td>
			</tr>
			<tr>
				<td width="100">输入密码：</td>
				<td width="100%" align="left"><span class="span12"><input type="password" maxlength="20" value="" id="userPWD" name="userPWD"></span></td>
			</tr>
			<tr>
				<td width="100">确定密码：</td>
				<td width="100%" align="left"><span class="span12"><input type="password" maxlength="20" value="" id="nuserPWD" name="nuserPWD"></span></td>
			</tr>
			</table>
		</div>
		<div class="group">
			<div class="span6"><button type="button" onClick="location.href='<?=url('Cust::main/Personcenter')?>'">取消</button></div><div class="span6"><button class="red">确认修改</button></div>
		</div>
	</form>
</div>
<script type="text/javascript">
$("input,textarea,select").on("change",function(){
	$(this).removeClass("error");
});
$(function() {	
	$("#submit_form").submit(function(){
		var f=$("input,textarea,select");
		for(var i=0;i<f.length;i++){if($(f[i]).val().length==0){$(f[i]).addClass("error");$(f[i]).focus();return false;}}
		var userPWD= document.getElementById("userPWD").value;
		if (userPWD.length<6){pz_box().show("密码长度需要至少6个字符！");return false;}
		var nuserPWD =document.getElementById("nuserPWD").value;
		if (nuserPWD!=userPWD){	pz_box().show("两次输入的密码不相同！");return false;}
//		pz_postform(submit_form,
//			function(data,s,x){
//				if (data.status == true) {
//					pz_box().show(data.data);
//				}else{
//					pz_box().show(data.data);
//				}
//			},"json");
//		return false;
	});
});
</script>
<?php $this->_endblock();?>