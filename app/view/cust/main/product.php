<?php $this->_extends('../_layouts/mobilea_layout'); ?>
<?php $this->_block('contents'); ?>
<link rel="stylesheet" href="<?=Q::ini('custom_system/base_url')?>inc/pz/pz-mobile.css">
<script src="<?=Q::ini('custom_system/base_url')?>inc/jquery-1.9.0.min.js"></script>
<link rel="stylesheet" href="<?=Q::ini('custom_system/base_url')?>inc/css.css">
<style>
.panel_ul{list-style:none; padding:0px}
.panel_ul *{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;box-sizing:border-box;}
.show_li{padding:5px;width:50%;float:left;}
.list_li{padding:5px;width:100%;}
.list_li .trade-panel{position:relative;height:70px;background:#fff;border: 0px solid transparent;border-radius: 5px;-webkit-border-radius: 5px;-webkit-box-shadow: 0px 2px 3px rgba(0,0,0,0.3);box-shadow: 0px 0px 3px rgba(0,0,0,0.3);}
.list_li img{height:70px; width:80px; border:5px #fff solid;}
.list_li .trade-ds{padding:5px; top:0; left:80px; position:absolute;background:none;border-radius:0 0 5px 5px;-webkit-border-radius:0 0 5px 5px;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;}
.list_li .show_name{ font-size:13px;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;display:block;}
.list_li b{font-size:14px;color:#FF0000;padding-right:5px}
.list_li del{font-size:12px;color:#666;}
.list_li .point{font-size:12px;color:green;display:block;}

.show_li img{width:100%;border-radius:5px 5px 0 0;-webkit-border-radius:5px 5px 0 0}
.show_li .trade-panel{background:#fff;border: 0px solid transparent;border-radius: 5px;-webkit-border-radius: 5px;-webkit-box-shadow: 0px 2px 3px rgba(0,0,0,0.3);box-shadow: 0px 0px 3px rgba(0,0,0,0.3);}
.show_li .trade-ds{padding:5px; height:65px;background:#fff;border-radius:0 0 5px 5px;-webkit-border-radius:0 0 5px 5px;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;}
.show_li .show_name{ font-size:13px;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;display:block;}
.show_li b{font-size:14px;color:#FF0000;padding-right:5px}
.show_li del{font-size:12px;color:#666;}
.show_li .point{font-size:12px;color:green;display:block;}
</style>
<script type="text/javascript"> 
$(function(){
	$(window).scroll(function() {
		if ($(window).scrollTop()>150){
			$(".bt-top").css("position", "fixed");
		}else{
			$(".bt-top").css("position", "relative");
		}
	});
	$(".tpbtn").click(function() {
		if ($(this).hasClass("on")){
			$("#J_Shade").removeClass("none");
			$(".circle").removeClass("hide").addClass("show");
			$(this).removeClass("on").addClass("off");
		}else{
			$("#J_Shade").addClass("none");
			$(".circle").removeClass("show").addClass("hide");
			$(this).removeClass("off").addClass("on");
		}
		
	});
});
</script>
	<ul class="top-tool">
		<li class="title">商品中心</li>
		<?if(isset($_GET['show'])&&$_GET['show']==1):?>
		<li class="title" style="font-size:14px;text-align:center;margin:20px;" onclick="location.href='<?=url('Cust::Main/Product')?>&key=<?if(isset($_POST['key'])):?><?=$_POST['key']?><?endif;?>&c_id=<?if(isset($_GET['c_id'])):?><?=$_GET['c_id']?><?endif;?>'">列表显示</li>
		<?else:?>
		<li class="title" style="font-size:14px;text-align:center;margin:20px;" onclick="location.href='<?=url('Cust::Main/Product',array('show'=>1))?>&key=<?if(isset($_POST['key'])):?><?=$_POST['key']?><?endif;?>&c_id=<?if(isset($_GET['c_id'])):?><?=$_GET['c_id']?><?endif;?>'">大图显示</li>
		<?endif;?>
		<li class="back"></li><li class="home"><span style="float:right;margin:20px;font-size:14px" onClick="window.location='<?=url('Cust::Main/Productlist')?>'">商品分类</span></li>
	</ul>
	<div style="height:50px"></div>
	<div class="pz-css" style="margin:0px 5px">
	<form  method="post">
		<div class="inputs" style=";width:100%; margin: 5px auto">
			<input type="text" style="width:85%" name="key" placeholder="快速查找商品">
			<button class="gray" type="submit" style="background:url(img/qry.png)  center no-repeat; width:15%">查找</button>
		</div>
	</form>
	</div>
	<ul class="panel_ul" style="background:#fff;">
	<?foreach ($product as $p):?>
	<li class="<?if(isset($_GET['show'])&&$_GET['show']==1):?>show_li<?else:?>list_li<?endif;?>">
		<div class="trade-panel">
		<a href="<?=url('Cust::Main/ProductDetail',array('id'=>$p['id']))?>">
			<img src="<?=$p['p_pic1']?>" width="100%" height="100px">
		</a>
		<div class="trade-ds">
			<span class="show_name"><?=$p['p_name']?></span>
			<b><?=$p['p_price']?></b>
			<span class="point">代理价格：<?=$p['p_price']?></span>
		</div>
	</li>
	<?endforeach;?>
	<div style="clear:both"></div>
	</ul>
<div>
	<div class="pz-box-mode" style="background:rgba(0, 0, 0, 0.7);" id="qrcode" onClick="this.style.display='none'">
<img id="imgqrcode" src="" style="width:100%"/>
</div>
<?include(Q::ini('custom_system/view_dir')."cust/main/bottom.php")?>
</div>
<?php $this->_endblock();?>