<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,minimum-scale=1,user-scalable=no,maximum-scale=1,initial-scale=1">
<link type="image/x-icon" href="<?=Q::ini('custom_system/base_url')?>favicon.ico" rel="icon"/>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.ui.js"></script>
<script src="<?=Q::ini('custom_system/base_url')?>js/jquery.slimscroll.min.js" type="text/javascript"></script>
<title>跳转</title>
<style type="text/css">
* {
	margin: 0;
	padding: 0;
	font-family: '微软雅黑',Verdana,arial,tahoma,sans-serif;
}
body {
	color: #444444;
	font-size: 12px;
}
.container {
	margin: 80px auto;
	text-align: left;
	width: 455px;
}
.graymodel{ float:left; width:100%; margin:5px 0;}
.grayh, .grayf, .grayhl, .grayhr, .grayfl, .grayfr{ background:url(<?=Q::ini('custom_system/base_url')?>img/rouGraybox.gif) }
.grayh{float:left; width:100%; background-position:0 -72px;  height:5px;}
.grayhl { float:left; width:5px; background-position:0 0;  height:5px;}
.grayhr { float:right; width:5px;  background-position:0 -36px;  height:5px;}
.grayhrc{float:right; margin-right:5px;height:28px; line-height:28px; text-align:right; cursor:pointer}
.grayf {width:100%; background-position:0 -180px; float:left; height:5px;}
.grayfl { float:left; width:5px; background-position:0 -108px; height:5px;}
.grayfr { float:right; width:5px; background-position:0 -144px;height:5px;}
.graybody { float:left;width:100%;height:100%;}
.graybodyin{border-left:1px solid #ccc;border-right:1px solid #ccc;height:100%;overflow:hidden; height: 250px;}
.hploginright{width:450px; }
.ssllogin{ width:100%; float:left;}
.ssllogin h1{ height:38px; line-height: 36px; border-bottom:1px solid #ccc; font-size: 17px; font-weight: normal;color: #444;}
.ssllogin h1 span{ margin-left:10px;}
.sslshadow{background:url(<?=Q::ini('custom_system/base_url')?>img/shadow.png) no-repeat; width:463px; height:31px; float:left; margin-top:2px; padding: 10px;}
</style>
</head>
<body>
<div class="container">
<div class="hploginright">
	<div class="graymodel">
		<div class="grayh">
			<div class="grayhl"></div>
			<div class="grayhr"></div>
		</div>
		<div class="graybody">
			<div class="graybodyin">
				<div class="ssllogin">
					<h1><span>温馨提示</span></h1>
					<div align="center" id="content">
					<h4 style="padding-top: 35px; font-size: 15px;"><?=$message_caption?></h4>
					<p><?=nl2br(h($message_body))?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="grayf">
			<div class="grayfl"></div><div class="grayfr"></div>
		</div>
	</div>
	<div class="sslshadow" align="center"><a href="<?=$redirect_url; ?>">如果您的浏览器没有自动跳转，请点击这里</a></div>
</div>
</div>
<script type="text/javascript">
<?if ($ack == SUCCESS):?>setTimeout("window.location.href ='<?=$redirect_url?>';", <?=$redirect_delay * 1000?>);<?endif;?>
$(function(){
	$("#content").slimScroll({height: '210px'});
});
</script>
<?=$hidden_script; ?>
</body>
</html>