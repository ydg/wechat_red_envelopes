<?php 
	if (CURRENT_USER_ID)
    {
    	if (isset($_POST['history']) && $_POST['history'])
		{
			header("Location:".$_POST['history']);
		}
    }
    else
    {
    	header("Location:".url('Default::Default/MobLogin'));
    }
?>
<!DOCTYPE html> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<meta name="viewport" content="width=device-width,minimum-scale=1,user-scalable=no,maximum-scale=1,initial-scale=1">
<title><?=$title?></title>
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>inc/css.css" rel="stylesheet" />
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>inc/pz/pz-mobile.css" rel="stylesheet" />
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>inc/pz/pz-mobile.js"></script>
</head>	
<body <?if (isset($_GET['controller'])&&isset($_GET['action'])&&$_GET['controller']=='main'&&$_GET['action']=='inship'):?>id="app-detail"<?else:?>id="top"<?endif;?> class="pz-css">
<?php $this->_block('contents');?><?php $this->_endblock();?>
</body>
</html>