<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
	<?=${Q::ini('app_config/APPID') . '_current_nca'}['namespace']?> - 
	<?=${Q::ini('app_config/APPID') . '_current_nca'}['controller']?> - 
	<?=${Q::ini('app_config/APPID') . '_current_nca'}['action']?> - 
	控价系统 </title>
<link href="<?=Q::ini('custom_system/base_url')?>img/favicon.ico" type="image/x-icon" rel="shortcut icon" /> 
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/colorbox.css" rel="stylesheet" />
<link href="<?=Q::ini('custom_system/base_url')?>css/main.css?" rel="stylesheet" type="text/css" />
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/jq.ui.css" rel="stylesheet" />
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.ui.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.colorbox.js"></script>
<script src="<?=Q::ini('custom_system/base_url')?>js/jquery.slimscroll.min.js" type="text/javascript"></script>
</head>
<body>
<div id="div_body">
	<div id="header" style="height:120px;">
		<div class="clear"></div>
		<div>
			<a href="<?=url('Setting::Permissions/RefreshSession')?>" id="header_system_logo"><img src="<?=Q::ini('custom_system/base_url')?>img/logo.png" title="刷新权限" width="90px" style="margin-bottom:6px;margin-left:10px;" /></a>
			<div style="color:#000;position:absolute;top:40px;left:120px;font-weight:bold;font-size:4rem;font-family:'华文隶书';">微商管理系统</div>
			<b id="header_logo_welcome" style="float:right;margin-top:55px;margin-right:60px;font-size:1.4rem;">您好，欢迎您<span style="font-size:2rem;color:#eb4000;"><?=CURRENT_USER_NAME?></span>！ <a href="<?=url('Default::Default/Logout')?>" style="width:120px;height:36px;margin-bottom:6px;border-radius:6px;background:orange;padding:5px 8px 5px 8px;color:#fff;">登出</a> | <a href="<?=url('Setting::Basic/NewsList');?>" id="news_unread_count"  style="width:130px;height:36px;margin-bottom:6px;border-radius:6px;background:#3b8c27;padding:5px 8px 5px 8px;color:#fff;">公告(<?=count(News_Unread::getNews());?>)</a> <!--| <a href="./headerScan.php" id="news_unread_count"  style="width:180px;height:36px;margin-bottom:6px;border-radius:6px;background:#eb4000;padding:5px 8px 5px 8px;color:#fff;"></a>--></b>
		</div>
		<div class="clear"></div>
		<div class="clear"></div>
		<div id="main_nav">
		<? include(Q::ini('custom_system/elements_dir') . 'navigation_element.php');?>
		</div>
	</div>
	<div id="content" style="margin-top:60px;">
		<div id="sidebar"><? include(Q::ini('custom_system/elements_dir') . 'sidebar_element.php');?></div>
		<div id="main_content">
			<div class="main_box">
				<!--<div class="main_box_h" style="border-radius:6px;border:0px;background:#ccc;">
					
					<div class="main_box_hc"><h1><?=${Q::ini('app_config/APPID') . '_current_nca'}['namespace']?> - <?=${Q::ini('app_config/APPID') . '_current_nca'}['controller']?> - <?=${Q::ini('app_config/APPID').'_current_nca'}['action']?></h1></div>
				</div>-->
				<div class="main_box_h">
					<div class="main_box_hl"></div>
					<div class="main_box_hr"></div>
					<div class="main_box_hc"><h1><?=${Q::ini('app_config/APPID') . '_current_nca'}['namespace']?> - <?=${Q::ini('app_config/APPID') . '_current_nca'}['controller']?> - <?=${Q::ini('app_config/APPID').'_current_nca'}['action']?></h1></div>
				</div>
				<div class="main_box_b">
					<div class="main_box_bi">
					<?php $this->_block('contents');?><?php $this->_endblock();?>
					</div>
				</div>
				<div class="main_box_f">
					<div class="main_box_fl"></div>
					<div class="main_box_fr"></div>
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<div id="footer_line"></div>
		<div>
			<p>
				<?=Q::ini('custom_system/system_name')?> <?=Q::ini('custom_system/system_secondary_name')?><?=Q::ini('custom_system/version')?> &nbsp;&nbsp;Copyright Reserved &copy;MICAI <?=Q::ini('custom_system/copyright')?>
			</p>
		</div>
	</div>
	<a href="#" class="gui-screen-top" style="display: block;" id="back_to_top"><s></s><b></b>返回顶部</a>
<script type="text/javascript">
$(function(){
	$("#dialog_browser").dialog({autoOpen:false, modal:true}); 
	$("#news_unread_count").colorbox({width:"50%", height:"60%", iframe:true});
	$(window).scroll(function(){
		var st = $(document).scrollTop();
		if (st > 0)
		{
			$("#back_to_top").show();
		}
		else
		{
			$("#back_to_top").hide();
		}
	});
	$(window).scroll();
	$("#back_to_top").click(function(){
		$("html body").animate({"scrollTop":0}, 200);
	});
	if(navigator.userAgent.indexOf("Win") != '-1')
	{
		if(!($.browser.webkit || $.browser.mozilla))
		{
			$("#dialog_browser").dialog("open");
		}
	}
});
</script>
</div>
<div id="dialog_browser" style="display:none;" align="center">温馨提示：请使用谷歌或者火狐浏览器！<br /><br />请点击下载链接：<a href='/Firefox Setup 13.0.1.exe' target='_blank' ><font color="red">火狐下载地址</font></a></div>
<!--<div class="remind" style="width:200px;height:100px;background:#fff;position:fixed;right:10px;bottom:20px;display:none;">新消息</div>
<audio id="mp3" src="./audio/remind.mp3"> </audio>
    <script>
        var remind = 1;
        var mp3 = $("#mp3")[0];
        var play= 0;
       
        if(sessionStorage.num){
            $(".remind").text(sessionStorage.num);
        }

        $.ajax({
            url:"<?=url('DataManage::Agent/sendOrderNotice')?>",
            success:function (data) {
                sessionStorage.num = data;
                $(".remind").text(data);
                remind = data;play=data;
                remind<=0?$(".remind").hide():$(".remind").show();

            }
        })


        setInterval(function () {
            $.ajax({
                url:"<?=url('DataManage::Agent/sendOrderNotice')?>",
                success:function (data) {
                    remind = data;
                    sessionStorage.num = data;
                    if(play==remind){
                        remind<=0?$(".remind").hide():$(".remind").show()
                    }else{
                        $(".remind").show().text(remind);
                        mp3.play();
                        play=remind;
                    }
                }
            })
        },10000)
</script>-->
</body>
</html>
