<!DOCTYPE html> 
<html> 
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link href="<?=Q::ini('custom_system/base_url')?>css/jq.mobile.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.js"></script>
	<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.mobile.js"></script>
</head>
<body>
<div id="content">
	<div data-role="page">
		<div data-role="header" data-theme="b" data-position="fixed">
			<h1>深圳信诚</h1>
		</div>
		<?php $this->_block('contents');?><?php $this->_endblock();?>
	</div>
</div>	
</body>
</html>