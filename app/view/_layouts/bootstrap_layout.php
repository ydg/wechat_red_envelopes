<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
	<?=isset(${Q::ini('app_config/APPID') . '_current_nca'}['namespace']) ? ${Q::ini('app_config/APPID') . '_current_nca'}['namespace'] : ''?> - 
	<?=isset(${Q::ini('app_config/APPID') . '_current_nca'}['controller']) ? ${Q::ini('app_config/APPID') . '_current_nca'}['controller'] : ''?> - 
	<?=isset(${Q::ini('app_config/APPID') . '_current_nca'}['action']) ? ${Q::ini('app_config/APPID') . '_current_nca'}['action'] : ''?> - 
	运营系统 
</title>
<link href="<?=Q::ini('custom_system/base_url')?>css/bootstrap.css?<?=time();?>" rel="stylesheet" />
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/bootstrap.js"></script>
</head>
<body>
<?php $this->_block('contents');?><?php $this->_endblock();?>
</body>
</html>
