<?php $this->_extends('../_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<form action="<?=url('Setting::Task/CreateSave')?>" method="post" >
<table class="form_table">
	<tr>
		<th>任务名称</th>
		<td><input type="text" name="name" size="30" /></td>
	</tr>
	<tr>
		<th>执行模块</th>
		<td><input type="text" name="model" size="30" /></td>
	</tr>
	<tr>
		<th>执行时间段</th>
  		<td><textarea name="regex" cols="29" rows="5"></textarea></td>
	</tr>
	<tr>
		<th>执行时长</th>
		<td><input type="text" name="seconds" size="10" /></td>
	</tr>
	<tr>
		<th>开启状态</th>
		<td>
			<?foreach (Q::ini('custom_flag/task_status') as $ts):?>
			<label><input type="radio" name="status" value="<?=$ts['value']?>" /> <?=$ts['name']?></label>
			<?endforeach;?>
		</td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" class="button" value="添加任务" /></td>
	</tr>
</table>
</form>
<?php $this->_endblock(); ?>