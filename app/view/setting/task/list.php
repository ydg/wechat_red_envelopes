<?php $this->_extends('../_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<script type="text/javascript">
$(function(){
	$(".view").toggle(function(){
		$(this).parent().parent().next().show();
	},function(){
		$(this).parent().parent().next().hide();
	});
	$(".detail").colorbox({width:"60%", height:"80%", iframe:true});
});
</script>
<fieldset><legend>查询条件</legend>
<form method="get" action="">
<?include(Q::ini('custom_system/elements_dir') . 'searchform_element.php');?>
<table class="form_table">
<tr>
	<th>执行模块</th>
	<td><input type="text" name="model" value="<?=isset($_GET['model']) ? $_GET['model'] : ''?>" /></td>
</tr>
<tr>
	<th>状态</th>
	<td>
		<?foreach (Q::ini('custom_flag/task_status') as $ts):?>
		<label><input type="checkbox" name="status[<?=$ts['value']?>]" value="<?=$ts['value']?>" <?if(isset($_GET['status']) && array_key_exists($ts['value'], $_GET['status']))echo "checked='checked'"?> /> <?=$ts['name']?></label>
		<?endforeach;?>
	</td>
</tr>
<tr>
	<td></td>
	<td><input type="submit" value="查询" class="button" /></td>
</tr>
</table>
</form>
</fieldset>
<table width="100%" class="list_table">
<tr>
	<th>任务ID</th>
	<th>任务名</th>
	<th>执行模块</th>
	<th>执行时长（秒）</th>
	<th>执行时间段</th>
	<th>状态</th>
	<th>创建时间</th>
	<th width="120">操作</th>
</tr>
<? foreach($task as $t):?>
<tr <? foreach ($clash as $c)
{
	if (in_array($t['name'], $c['name']))
	{
		$name = $title = '';
		foreach ($c['name'] as $cn)
		{
			$name .= $cn . "\n";
		}
		foreach ($c['repeat_time'] as $crt)
		{
			$title .= $crt . "\n"; 
		}
		echo 'bgcolor="red" title="冲突任务：' . "\n" . $name . "冲突时间：\n" . $title . '"'; 
	}
}?>>
	<td><?=$t['id'];?></td>
	<td><?=$t['name'];?></td>
	<td><?=$t['model']?></td>
	<td><?=$t['seconds']?></td>
	<td><?=$t['regex']?></td>
	<td><?=Helper_BSS_Normal::getIniOne('task_status', 'value', $t['status'], 'name')?></td>
	<td><?=$t['create_time'];?></td>
	<td>
		<a href="<?=url('Setting::Task/Edit', array('id'=>$t['id']))?>">编辑</a>
		<a href="<?=url('Setting::Task/Detail', array('id'=>$t['id']))?>" class="detail">查看日志</a>
	</td>
</tr>
<? endforeach;?>
</table>
<?=$page;?>
<?php $this->_endblock();?>