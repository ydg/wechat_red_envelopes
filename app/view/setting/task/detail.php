<?php $this->_extends('../_layouts/blank_layout'); ?>
<?php $this->_block('contents'); ?>
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/jq.ui.css" rel="stylesheet" />
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.ui.js"></script>
<script type="text/javascript">
$(function(){
	$(".datepicker").datepicker({dateFormat: 'yy-mm-dd', changeMonth: true});
});
</script>
<fieldset><legend>搜索条件</legend>
<form method="get">
<?php include(Q::ini('custom_system/elements_dir') . 'searchform_element.php');?>
<input type="hidden" name="id" value="<?=isset($_GET['id']) ? $_GET['id'] : '' ?>" />
<table class="form_table">
<tr>
	<th>日志时间</th>
	<td>
		<input type="text" name="begin_time" class="datepicker" value="<?=isset($_GET['begin_time']) ? $_GET['begin_time'] : ''?>" /> to 
		<input type="text" name="end_time" class="datepicker" value="<?=isset($_GET['end_time']) ? $_GET['end_time'] : ''?>" />
	</td>
</tr>
<tr>
	<th>执行结果</th>
	<td>
		<?foreach(Q::ini('custom_flag/task_log_ack') as $a):?>
			<label><input type="checkbox" name="task_log_ack[<?=$a['value']?>]" value="<?=$a['value']?>" <?if (isset($_GET['task_log_ack']) && array_key_exists($a['value'], $_GET['task_log_ack']))echo "checked='checked'";?> />&nbsp;<?=$a['name']?></label>&nbsp;&nbsp;
		<?endforeach;?>
	</td>
</tr>
<tr>
	<th></th>
	<td><input type="submit" class="button" value="查询" /></td>
</tr>
</table>
</form>
</fieldset>
<table width="100%" class="list_table">
<tr>
	<th>ID</th>
	<th>对应任务ID</th>
	<th>ACK</th>
	<th>Message</th>
	<th>日志时间</th>
</tr>
<?foreach ($task_log as $tl):?>
<tr>
	<td><?=$tl['id']?></td>
	<td><?=$tl['task_id']?></td>
	<td><?=Helper_BSS_Normal::getIniOne('task_log_ack', 'value', $tl['ack'], 'name')?></td>
	<td><?=$tl['message']?></td>
	<td><?=$tl['time']?></td>
</tr>
<?endforeach;?>
</table>
<?=$page;?>
<?php $this->_endblock();?>