<?php $this->_extends('../_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<form action="<?=url('Setting::Task/EditSave')?>" method="post" >
<table class="form_table">
	<tr>
		<th>任务名称</th>
		<td><input type="text" name="name" size="30" value="<?=$task['name']?>"/></td>
	</tr>
	<tr>
		<th>执行模块</th>
		<td><input type="text" name="model" size="30" value="<?=$task['model']?>" /></td>
	</tr>
	<tr>
		<th>执行时间段</th>
  		<td><textarea name="regex" cols="29" rows="5"><?=$task['regex']?></textarea></td>
	</tr>
	<tr>
		<th>执行时长</th>
		<td><input type="text" name="seconds" size="10" value="<?=$task['seconds']?>" /></td>
	</tr>
	<tr>
		<th>开启状态</th>
		<td>
			<?foreach (Q::ini('custom_flag/task_status') as $ts):?>
			<label><input type="radio" name="status" value="<?=$ts['value']?>" <?if ($task['status'] == $ts['value'])echo 'checked="checked"'?>/> <?=$ts['name']?></label>
			<?endforeach;?>
		</td>
	</tr>
	<tr>
		<td><input type="hidden" name="task_id" value="<?=$task['id']?>" /></td>
		<td><input type="submit" class="button" value="保存" /></td>
	</tr>
</table>
</form>
<?php $this->_endblock(); ?>