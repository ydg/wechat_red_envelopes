<?php $this->_extends('../_layouts/bootstrap_layout'); ?>
<?php $this->_block('contents'); ?>
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/style.css" rel="stylesheet" />
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/chosen.css?<?=time()?>" rel="stylesheet" />
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/app.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/form-components.js"></script>
<script type="text/javascript">
jQuery(document).ready(function()
{
	   App.init();
	   FormComponents.init();
});
</script>
<div class="span6">
<form method="post" action="<?=url('Setting::User/CopySave')?>">
  <fieldset>
    <legend>复制权限[角色]</legend>
    <div class="control-group">
	<label class="control-label">被复制人[单选]======>需要复制人员[多选]</label>
	<div class="controls">
		<select class="chosen span2" name="user_id">
			<option value="">选择被复制人员</option>
			<?foreach ($user as $u):?>
			<option value="<?=$u['id']?>"><?=$u['name']?></option>
			<?endforeach;?>
		</select>
		<select class="chosen span3" name="copy_user_ids[]" multiple="multiple" tabindex="4">
			<?foreach ($user as $u):?>
			<option value="<?=$u['id']?>" <?if ($u['id'] == $_GET['user_id'])echo 'selected';?>><?=$u['name']?></option>
			<?endforeach;?>
		</select>
	</div>
	</div>
  </fieldset>
  <button type="submit" class="btn">确认复制</button>
</form>
</div>
<?php $this->_endblock();?>