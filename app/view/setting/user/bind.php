<?php $this->_extends('../_layouts/bootstrap_layout'); ?>
<?php $this->_block('contents'); ?>
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/multi-select-metro.css" rel="stylesheet" />
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/style-metro.css.css" rel="stylesheet" />
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/style.css" rel="stylesheet" />
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/chosen.css" rel="stylesheet" />
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jquery.multi-select.js"></script>   
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/select2.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/date.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/app.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/form-components.js"></script>
<script type="text/javascript">
jQuery(document).ready(function()
{
	   App.init();
	   FormComponents.init();
});
$(function(){
	$("#check_all").click(function(){
		if ($(this).is(":checked"))
		{
			$(".list_table input[type='checkbox']:enabled").attr("checked", true);
		}
		else
		{
			$(".list_table input[type='checkbox']").attr("checked", false);
		}
	});
	$("input[type='checkbox']").click(function(){
		var id = $(this).val();
		if ($(this).is(":checked"))
		{
			$("#"+ id +" input[type='checkbox']:enabled").attr("checked", true);
		}
		else
		{
			$("#"+ id +" input[type='checkbox']:enabled").attr("checked", false);
		}
	});
	$("#user_id").change(function(){
		location.href = "<?=url('Setting::User/Bind')?>&user_id=" + $(this).val();
	});
});
</script>
<style>
.list_table {border-top: 1px solid #ccc; border-left: 1px solid #ccc; text-align: center;border-collapse: separate;border-spacing: 0px;}
.list_table th,.list_table td {border-right: 1px solid #ccc;border-bottom: 1px solid #ccc;font-weight: normal;height: 23px;}
.list_table tr th {line-height:28px; height:27px; text-align: center;}
.list_table tr td {padding: 4px;}
input {margin-bottom: 4px;}
</style>
<div class="span9">
<form method="post" action="<?=url('Setting::User/BindSave')?>">
  <fieldset>
  	<div class="control-group" style="position: fixed; right: 10px; top: 25px;">
	<select class="chosen span2" name="user_id" id="user_id">
		<option value="">请选择..</option>
		<?foreach ($user as $u):?>
		<option value="<?=$u['id']?>" <?if ($mapping['id'] == $u['id'])echo "selected='selected'"?>><?=$u['name']?></option>
		<?endforeach;?>
	</select><br />
	<button type="submit" class="btn">确认分配</button>
	</div>
    <legend>分配用户权限</legend>
	
    <div class="control-group">
    <label class="control-label">※ 分配角色 <input type="checkbox" id="check_all"/></label>
	<table class="list_table" width="100%">
	<tr>
		<th><strong>父级角色</strong></th>
		<th><strong>子级角色</strong></th>
	</tr>
	<?foreach ($roles as $r):?>
	<?if (!in_array($r['id'], $current) && !isset($r['children'])) continue;?>
	<tr align="left">
		<td><label><?if(in_array($r['id'], $current)):?><input type="checkbox" name="roles_ids[]" value="<?=$r['id']?>" <?=(in_array($r['id'], $mapping['roles_ids'])) ? 'checked="checked"' : ''?> /><?endif;?> <?=$r['desc']?></label></td>
		<?if (isset($r['children'])):?>
		<td>
			<table id="<?=$r['id']?>" width="100%" height="100%" class="list_table">
				<?foreach ($r['children'] as $rc):?>
				<tr align="left">
					<td><label><?if(in_array($rc['id'], $current)):?><input type="checkbox" name="roles_ids[]" value="<?=$rc['id']?>" <?=(in_array($rc['id'], $mapping['roles_ids'])) ? 'checked="checked"' : ''?>/><?endif;?> <?=$rc['desc'];?></label></td>
					<?if (isset($rc['children'])):?>
					<td>
						<table id="<?=$rc['id']?>" width="100%" class="list_table">
						<?foreach ($rc['children'] as $rcc):?>
							<tr align="left">
								<td><label><?if(in_array($rcc['id'], $current)):?><input type="checkbox" name="roles_ids[]" value="<?=$rcc['id']?>" <?=(in_array($rcc['id'], $mapping['roles_ids'])) ? 'checked="checked"' : ''?> /><?endif;?> <?=$rcc['desc'];?></label></td>
							</tr>
						<?endforeach;?>
						</table>
					</td>
					<?else:?>
					<td></td>
					<?endif;?>
				</tr>
				<?endforeach;?>
			</table>
		</td>
		<?else:?>
		<td></td>
		<?endif;?>
	</tr>
	<?endforeach;?>
	</table>
    </div>
  </fieldset>
</form>
</div>
<?php $this->_endblock();?>