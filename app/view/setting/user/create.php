<?php $this->_extends('../_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url');?>js/validator.js"></script>
<form action="<?=url('Setting::User/CreateSave')?>" method="post" onSubmit="return Validator.Validate(this,3)" >
<table class="form_table">
	<tr>
		<th>用户名</th>
		<td><input type="text" name="username" id="username" size="15" dataType="Username" msg="请填写正确的用户名"/> <span class="red">*</span></td>
	</tr>
	<tr>
		<th>密码</th>
  		<td><input type="password" name="password" id="password" size="15" dataType="Password" msg="请填写正确的密码" /> <span class="red">*</span></td>
	</tr>
	<tr>
		<th>确认密码</th>
		<td><input type="password" name="conf_password" id="conf_password" size="15" dataType="Repeat" to="password" msg="两次输入的密码不一致" /> <span class="red">*</span></td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" class="button" value="添加用户" /></td>
	</tr>
</table>
</form>
<?php $this->_endblock(); ?>