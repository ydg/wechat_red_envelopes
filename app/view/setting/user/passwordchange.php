<?php $this->_extends('../_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url');?>js/validator.js"></script>
<form action="<?=url('Setting::User/PasswordChangeSave')?>" method="post" onSubmit="return Validator.Validate(this,3)">
<table class="form_table">
	<tr>
		<th>原始密码</th>
		<td><input type="password" name="old_password" size="15" dataType="Password" msg="请填写正确的用户名" /> <span class="red">*</span></td>
	</tr>
	<tr>
		<th>新密码</th>
		<td><input type="password" name="new_password" size="15" dataType="Password" msg="请填写正确的新密码" /> <span class="red">*</span></td>
	</tr>
	<tr>
		<th>确认密码</th>
		<td><input type="password" name="new_password_again" size="15" dataType="Repeat" to="new_password" msg="两次输入的密码不一致" /> <span class="red">*</span></td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" class="button" value="修改密码" /></td>
	</tr>
</table>
</form>
<?php $this->_endblock();?>