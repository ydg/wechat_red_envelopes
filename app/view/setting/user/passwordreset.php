<?php $this->_extends('../_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url');?>js/validator.js"></script>
<form action="<?=url('Setting::User/PasswordResetSave')?>" method="post" onSubmit="return Validator.Validate(this,3)" >
<table cellspacing="0" class="form_table">
	<tr>
		<th>用户名</th>
		<td><?=$user['name'];?> <input name="user_id" id="user_id" type="hidden" value="<?=$user['id'];?>"></td>
	</tr>
	<tr>
		<th>修改密码</th>
		<td><input type="password" name="password" id="password" size="15" dataType="Password" msg="请填写正确的密码" /> <span class="red">*</span> </td>
	</tr>
	<tr>
		<th>确认密码</th>
		<td><input type="password" name="conf_password" id="conf_password" size="15" dataType="Repeat" to="password" msg="两次输入的密码不一致" /> <span class="red">*</span> </td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" class="button" value="重置用户" /></td>
	</tr>
</table>
</form>
<?php $this->_endblock(); ?>