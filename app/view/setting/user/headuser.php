<?php $this->_extends('../_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<script type="text/javascript">
$(function(){
	$(".bind").colorbox({width:"65%", height:"90%", iframe:true});
	$(".copy").colorbox({width:"40%", height:"60%", iframe:true});
	$(".del").click(function(){
		if ( ! confirm("确定清空？"))
		{
			return false;
		}
	});
});
</script>
<fieldset><legend>查询条件</legend>
<form method="get" action="">
<?include(Q::ini('custom_system/elements_dir') . 'searchform_element.php');?>
<table class="form_table">
<tr>
	<th>用户名</th>
	<td><input type="text" name="name" value="<?=isset($_GET['name']) ? $_GET['name'] : ''?>" /></td>
</tr>
<tr>
	<td></td>
	<td><input type="submit" value="查询" class="button" /></td>
</tr>
</table>
</form>
</fieldset>
<table width="100%" class="list_table">
<tr>
	<th>ID</th>
	<th>用户名</th>
	<th>状态</th>
	<th>最后登录时间</th>
	<th>登录次数</th>
	<th width="280">操作</th>
</tr>
<?foreach($user as $u):?>
<tr>
	<td scope="row"><?=$u['id'];?></td>
	<td><?=$u['name'];?></td>
	<td><?=Helper_BSS_Normal::getIniOne('user_status', 'value', $u['status'], 'name')?></td>
	<td><?=$u['last_login_time'];?></td>
	<td><?=$u['login_count'];?></td>
	<td>
		<a href="<?=url('Setting::User/PasswordReset', array('user_id'=>$u['id']))?>">重置密码</a>
		<a href="<?=url('Setting::User/SetOffDutyCleanMapping', array('user_id' => $u['id']))?>" class="del">离职清权限</a>
		<a class="bind" href="<?=url('Setting::User/Bind', array('user_id'=>$u['id'])) . '#xxx'?>">分配权限</a>
		<a class="copy" href="<?=url('Setting::User/Copy', array('user_id'=>$u['id']))?>">复制权限</a>
		<?if ($u['id'] == 1):?>
		<font color="gray">删除</font>
		<?else:?>
		<a href="<?=url('Setting::User/HeadDelete', array('user_id'=>$u['id']))?>">删除</a>
		<?endif;?>
	</td>
</tr>
<? endforeach;?>
</table>
<?=$page;?>
<?php $this->_endblock();?>