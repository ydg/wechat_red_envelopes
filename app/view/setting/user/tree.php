<?php $this->_extends('../_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/multi-select-metro.css" rel="stylesheet" />
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url');?>js/validator.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/bootstrap.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jquery.multi-select.js"></script>   
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/select2.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/date.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/app.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/form-components.js"></script>
<script type="text/javascript">
$(function(){
	App.init();
	FormComponents.init();
	$("#user_id").change(function(){
		location.href = "<?=url('Setting::User/Tree')?>&id=" + $(this).val();
	});
});
</script>
<table width="50%" class="list_table">
<tr>
	<th>ID</th>
	<th>父ID</th>
	<th>用户名</th>
	<th>最后登录时间</th>
	<th>登录次数</th>
</tr>
<? foreach($tree as $t):?>
<?
	$flag = '';
	for ($i=1; $i<=$t['level'] ;$i++)
	{
		$flag .= '|-';
	}
?>
<tr>
	<td scope="row"><?=$t['id'];?></td>
	<td scope="row"><?=$t['parent_id'];?></td>
	<td align="left"><?=$flag . $t['name'];?></td>
	<td><?=$t['last_login_time'];?></td>
	<td><?=$t['login_count'];?></td>
</tr>
<? endforeach;?>
</table>
<div class="span7" style="position: fixed; top: 155px; right: 30px;">
<form action="<?=url('Setting::User/TreeSave')?>" method="post" onSubmit="return Validator.Validate(this,3)" >
<fieldset style="padding: 25px;">
<legend>
分配下级管理人员
<select name="user_id" id="user_id" dataType="Require" msg="请选择需要分配下级人员">
	<option value="">请选择..</option>
	<?foreach ($tree as $t):?>
	<?
		$flag = '';
		for ($i=1; $i<=$t['level'] ;$i++)
		{
			$flag .= '|-';
		}
	?>
	<option value="<?=$t['id'];?>" <?if (isset($_GET['id']) && ($t['id'] == $_GET['id'])) echo "selected='selected'"?>><?=$flag . $t['name']?></option>
	<?endforeach;?>
</select>
<span class="red">*</span>
</legend>
<div class="control-group">
<div class="controls">
<select id="my_multi_select2" name="child_user_ids[]" multiple="multiple">
	<?foreach ($tree as $t):?>
	<?
		if (isset($_GET['id']) && $_GET['id'] == $t['id'])
		{
			continue;
		}
		$flag = '';
		for ($i=1; $i<=$t['level'] ;$i++)
		{
			$flag .= '|-';
		}
	?>
	<option value="<?=$t['id'];?>" <?if (in_array($t['id'], $child_tree_ids)) echo "selected='selected'"?>><?=$flag . $t['name']?></option>
	<?endforeach;?>
</select>
</div>
</div>
</fieldset>
<div align="center"><input type="submit" class="button" value="确认分配"/></div>
</form>
</div>
<?php $this->_endblock();?>