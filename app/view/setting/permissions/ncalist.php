<?php $this->_extends('../_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<script type="text/javascript">
$(function(){
	$(".allot").colorbox({width:"60%", height:"90%", iframe:true});
});
</script>
<?if (isset($parent_action_id)):?>
<a href="<?=url('Setting::Permissions/NCAList', array('action_id' => $parent_action_id))?>">返回上一级</a>
<?endif;?>
<form action="<?=url('Setting::Permissions/NCASave')?>" method="post">
<table width="100%" class="list_table">
<tr>
	<th width="110">Namespace</th>
	<th width="130">Controller</th>
	<th>Action</th>
	<th width="150">名称</th>
	<th width="150">描述</th>
	<th width="130">系统角色</th>
	<th width="60">显示</th>
	<th width="60">排序↑</th>
	<th width="100">操作</th>
</tr>
<?foreach ($nca as $n):?>
<tr>
	<td><?=$n['namespace']?></td>
	<td><?=$n['controller']?></td>
	<td><?=$n['action']?></td>
	<td><input type="text" name="name[<?=$n['id']?>]" value="<?=$n['name']?>" /></td>
	<td><input type="text" name="desc[<?=$n['id']?>]" value="<?=$n['desc']?>" /></td>
	<td>
		<select name="roles_access_control[<?=$n['id']?>]">
			<option value="ACL_NULL" <?if ($n['roles_access_control'] == 'ACL_NULL'):?>selected="selected"<?endif;?>>ACL_NULL</option>
			<option value="ACL_EVERYONE" <?if ($n['roles_access_control'] == 'ACL_EVERYONE'):?>selected="selected"<?endif;?>>ACL_EVERYONE</option>
			<option value="ACL_HAS_ROLE" <?if ($n['roles_access_control'] == 'ACL_HAS_ROLE'):?>selected="selected"<?endif;?>>ACL_HAS_ROLE</option>
			<option value="ACL_NO_ROLE" <?if ($n['roles_access_control'] == 'ACL_NO_ROLE'):?>selected="selected"<?endif;?>>ACL_NO_ROLE</option>
		</select>
	</td>
	<td>
		<select name="display_flag[<?=$n['id']?>]">
			<?foreach (Q::ini('custom_flag/permissions_actions_display_flag') as $padf):?>
			<option value="<?=$padf['value']?>" <?if ($padf['value'] == $n['display_flag']):?>selected="selected"<?endif;?>><?=$padf['name']?></option>
			<?endforeach;?>
		</select>
	</td>
	<td><input type="text" size="3" name="sort[<?=$n['id']?>]" value="<?=$n['sort']?>" /></td>
	<td>
		<?if ( ! $n['action']):?>
		<a href="<?=url('Setting::Permissions/NCAList', array('action_id' => $n['id']))?>">详细</a>
		<?endif;?>
	</td>
</tr>
<?endforeach;?>
</table>
<div align="center"><input type="submit" value="保存" class="button" /></div>
</form>
<?php $this->_endblock();?>