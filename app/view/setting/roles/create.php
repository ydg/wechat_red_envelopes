<?php $this->_extends('../_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<form action="<?=url('Setting::Roles/CreateSave')?>" method="post">
<table class="form_table">
<tr>
	<th>选择父级</th>
	<td>
		<select name="parent_id">
			<option value="">请选择..</option>
			<?foreach ($roles as $r):?>
			<?
				$flag = '';
				for ($i=1; $i<=$r['level'] ;$i++)
				{
					$flag .= '|-';
				}
			?>
			<option value="<?=$r['id']?>" title="<?=$r['desc']?>" <?if (isset($current_roles['parent_id']) && ($current_roles['parent_id'] == $r['id']))echo 'selected="selected"'?>><?=$flag . $r['desc'];?></option>
			<?endforeach;?>
		</select>
	</td>
</tr>
<tr>
	<th>角色说明</th>
	<td><textarea rows="4" cols="20" name="desc"><?=isset($current_roles['desc']) ? $current_roles['desc'] : '';?></textarea></td>
</tr>
<tr>
	<th>排序</th>
  	<td><input type="text" name="sort" value="<?=isset($current_roles['sort']) ? $current_roles['sort'] : '';?>" /></td>
</tr>
<tr>
	<th><?if (isset($current_roles['id'])):?><input type="hidden" name="id" value="<?=$current_roles['id'];?>" /><?endif;?></th>
	<td><input type="submit" value="保存" class="button" /></td>
</tr>
</table>
</form>
<?php $this->_endblock(); ?>