<?php $this->_extends('../_layouts/bootstrap_layout'); ?>
<?php $this->_block('contents'); ?>
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/chosen.css" rel="stylesheet" />
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/app.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {       
	   App.init();
});
$(function(){
	$("input[type='checkbox']").click(function(){
		var id = $(this).attr("id");
		if ($(this).is(":checked"))
		{
			$("input[id^='"+id+"_']").attr("checked", true);
		}
		else
		{
			$("input[id^='"+id+"_']").attr("checked", false);
		}
	});
	$("#roles_id").change(function(){
		location.href = "<?=url('Setting::Roles/AllotAction')?>&id=" + $(this).val();
	});
});
</script>
<div class="span11">
<form action="<?=url('Setting::Roles/AllotActionSave')?>" method="post">
<fieldset>
	<div class="control-group" style="position: fixed; right: 60px; top: 20px;">
	<select id="roles_id" name="roles_id" class="chosen">
		<option value="">请选择..</option>
		<?foreach ($roles as $r):?>
		<?
			$flag = '';
			for ($i=1; $i<=$r['level'] ;$i++)
			{
				$flag .= '|-';
			}
		?>
		<option value="<?=$r['id']?>" title="<?=$r['desc']?>" <?if (isset($_GET['id']) && $_GET['id'] == $r['id'])echo "selected='selected'"?> ><?=$flag . $r['desc'];?></option>
		<?endforeach;?>
	</select>
	</div>
    <legend>分配-Action</legend>
	<div class="control-group">
	<table class="table table-hover table-bordered">
	<thead>
	<tr>
		<th width="160">Namespace</th>
		<th width="200">Controller</th>
		<th width="250">Action</th>
		<th>描述</th>
	</tr>
	</thead>
	<tbody>
	<?foreach ($nca as $n):?>
	<?$i=0;?>
	<?foreach ($n['children'] as $c):?>
	<?$j=0;?>
	<?foreach ($c['children'] as $a):?>
	<tr>
		<td align="left"><?if ($i == 0):?><label><input type="checkbox" id="<?=$n['id']?>" /> <?=$n['name']?></label><?endif;?></td>
		<td align="left"><?if ($j == 0):?><label><input type="checkbox" id="<?=$n['id']?>_<?=$c['id']?>" /> <?=$c['name']?></label><?endif;?></td>
		<td align="left" title="<?=$a['namespace']?>_<?=$a['controller']?>_<?=$a['action']?>"><label><input type="checkbox" id="<?=$n['id']?>_<?=$c['id']?>_<?=$a['id']?>" name="nca_id[]" value="<?=$a['id']?>" <?=($a['permissions_actions_id']) ? "checked='checked'" : ''?> /> <?=$a['name']?> <i class="<?=Helper_BSS_Normal::getIniOne('permissions_actions_display_flag', 'value', $a['display_flag'], 'icon_class')?>"></i></label></td>
		<td align="left"><?=$a['desc']?></td>
	</tr>
	<?$i++;$j++;?>
	<?endforeach;?>
	<?endforeach;?>
	<?endforeach;?>
	</tbody>
	</table>
	<button type="submit" class="btn">确认分配</button>
    </div>
</fieldset>
</form>
</div>
<?php $this->_endblock();?>