<?php $this->_extends('../_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/multi-select-metro.css" rel="stylesheet" />
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url');?>js/validator.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/bootstrap.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jquery.multi-select.js"></script>   
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/select2.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/date.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/app.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/form-components.js"></script>
<script type="text/javascript">
$(function(){
	App.init();
	FormComponents.init();
	$(".bind").colorbox({width:"43%", height:"80%", iframe:true});
	$(".allot").colorbox({width: "70%", height: "95%", iframe:true});
	$("#roles_id").change(function(){
		location.href = "<?=url('Setting::Roles/list')?>&id=" + $(this).val();
	});
});
</script>
<div class="span7" style="position: fixed; top: 155px; right: 30px;">
<form action="<?=url('Setting::Roles/TreeSave')?>" method="post" onSubmit="return Validator.Validate(this,3)" >
<fieldset style="padding: 25px;">
<legend>
捆绑子角色
<select name="roles_id" id="roles_id" dataType="Require" msg="请选择父角色">
	<option value="">请选择..</option>
	<?foreach ($roles as $r):?>
	<?
		$flag = '';
		for ($i=1; $i<=$r['level'] ;$i++)
		{
			$flag .= '|-';
		}
	?>
	<option value="<?=$r['id'];?>" <?if (isset($_GET['id']) && ($r['id'] == $_GET['id'])) echo "selected='selected'"?> title="<?=$flag . $r['desc']?>"><?=$flag . mb_substr($r['desc'], 0, 9, 'utf-8')?></option>
	<?endforeach;?>
</select>
<span class="red">*</span>
</legend>
<div class="control-group">
<div class="controls">
<select id="my_multi_select2" name="child_roles_ids[]" multiple="multiple">
	<?foreach ($roles as $r):?>
	<?
		if (isset($_GET['id']) && $_GET['id'] == $r['id'])
		{
			continue;
		}
		$flag = '';
		for ($i=1; $i<=$r['level'] ;$i++)
		{
			$flag .= '|-';
		}
	?>
	<option value="<?=$r['id'];?>" <?if (in_array($r['id'], $child_tree_ids)) echo "selected='selected'"?>><?=$flag . $r['desc']?></option>
	<?endforeach;?>
</select>
</div>
</div>
</fieldset>
<div align="center"><input type="submit" class="button" value="确认分配"/></div>
</form>
</div>
<table width="60%" class="list_table">
<tr>
	<th>ID</th>
	<th>角色</th>
	<th width="200">操作</th>
</tr>
<? foreach($roles as $r):?>
<?
	$flag = '';
	for ($i=1; $i<=$r['level']; $i++)
	{
		$flag .= '|-';
	}
?>
<tr align="left">
	<td><?=$r['id'];?></td>
	<td><?=$flag . $r['desc'];?></td>
	<td>
		<a href="<?=url('Setting::Roles/Create', array('id'=>$r['id']))?>">编辑</a>
		<a href="<?=url('Setting::Roles/Delete', array('id'=>$r['id']))?>" onclick="if (!confirm('确定删除？'))return false;">删除</a>
		<a class="allot" href="<?=url('Setting::Roles/AllotAction', array('id'=>$r['id']))?>">分配-Action</a>
		<a class="bind"  href="<?=url('Setting::Roles/AllotUser', array('id'=>$r['id']))?>">分配用户</a>
	</td>
</tr>
<? endforeach;?>
</table>
<?php $this->_endblock();?>