<?php $this->_extends('../_layouts/bootstrap_layout'); ?>
<?php $this->_block('contents'); ?>
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/multi-select-metro.css" rel="stylesheet" />
<link type="text/css" href="<?=Q::ini('custom_system/base_url')?>css/chosen.css" rel="stylesheet" />
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jquery.multi-select.js"></script>   
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/select2.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jquery.input-ip-address-control-1.0.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/date.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/app.js"></script>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/form-components.js"></script>
<script type="text/javascript">
jQuery(document).ready(function()
{
	   App.init();
	   FormComponents.init();
});
$(function(){
	$("#roles_id").change(function(){
		location.href = "<?=url('Setting::Roles/AllotUser')?>&id=" + $(this).val();
	});
});
</script>
<div class="span7">
<form method="post" action="<?=url('Setting::Roles/AllotUserSave')?>">
<fieldset>
	<div class="control-group" style="float: right; margin-top: -60px;">
	<select class="chosen" name="roles_id" id="roles_id">
		<option value="">请选择..</option>
		<?foreach ($roles as $r):?>
		<?
			$flag = '';
			for ($i=1; $i<=$r['level'] ;$i++)
			{
				$flag .= '|-';
			}
		?>
		<option value="<?=$r['id']?>" title="<?=$r['desc']?>" <?if (isset($_GET['id']) && $_GET['id'] == $r['id'])echo "selected='selected'"?> ><?=$flag . $r['desc'];?></option>
		<?endforeach;?>
	</select>
	</div>
    <legend>分配用户角色</legend>
    <div class="control-group">
	<div class="controls">
		<select id="my_multi_select2" name="user_ids[]" multiple="multiple">
		<?foreach ($users as $user):?>
			<option value="<?=$user['id'];?>" <?if (in_array($user['id'], $roles_user_ids)) echo "selected='selected'"?>><?=$user['name']?></option>
		<?endforeach;?>
		</select>
	</div>
	</div>
	<button type="submit" class="btn">确认分配</button>
</fieldset>
</form>
</div>
<?php $this->_endblock();?>