<?php $this->_extends('../_layouts/blank_layout'); ?>
<?php $this->_block('contents'); ?>
<h3>发布新公告</h3>
<form method="post" action="<?=url('Setting::Basic/NewsCreateSave')?>">
<table class="form_table" width="100%">
<tr>
	<th>标题</th>
	<td><input type="text" name="title" size="30" /></td>
</tr>
<tr>
	<th>内容</th>
	<td>
		<textarea name="content" rows="5" cols="30"></textarea>
	</td>
</tr>
<tr>
	<th></th>
	<td><input type="submit" class="button" value="发布" /></td>
</tr>
</table>
<a href="<?=$_SERVER['HTTP_REFERER']?>">返回上级</a>
</form>
<?php $this->_endblock();?>