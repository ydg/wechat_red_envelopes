<?php $this->_extends('../_layouts/default_layout'); ?>
<?php $this->_block('contents'); ?>
<form action="<?=url('Setting::Basic/CompanyInfoSave')?>" method="post" >
<table class="form_table">
	<tr>
		<th>企业名称</th>
		<td><input type="text" name="company_name" size="80" value="<?=$info['company_name']?>"/></td>
	</tr>
	<tr>
		<th>企业网址</th>
		<td><input type="text" name="company_url" size="80" value="<?=$info['company_url']?>" /><a href="<?=$info['company_url']?>">进入网站</a></td>
	</tr>
	<tr>
		<th>联系方式</th>
  		<td><input type="text" name="company_contact" size="80" value="<?=$info['company_contact']?>" /></td>
	</tr>
	<tr>
		<th>冻结回复</th>
		<td><textarea name="freeze_reply" cols="79.5" rows="5"><?=$info['freeze_reply']?></textarea></td>
	</tr>
	<tr>
		<td><input type="hidden" name="id" value="<?=isset($info['id'])? $info['id'] : '' ?>" /></td>
		<td><input type="submit" class="button" value="修改" /></td>
	</tr>
</table>
</form>
<?php $this->_endblock(); ?>