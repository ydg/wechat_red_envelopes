<?php $this->_extends('../_layouts/blank_layout'); ?>
<?php $this->_block('contents'); ?>
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.js"></script>
<script type="text/javascript">
$(function(){
	$("#news_create").click(function(){
		location.href="<?=url('Setting::Basic/NewsCreate')?>";
	});
	$(".view").toggle(function(){
		$(this).parent().parent().next().show();
		$(this).parent().parent().addClass("newsshow");
		$(this).parent().parent().next().addClass("newsshow");
	}, function(){
		$(this).parent().parent().removeClass("newsshow");
		$(this).parent().parent().next().hide();
	});
	$(".view").click(function(){
		$(this).children().addClass("news_read_icon");
		var news_id = $(this).prev().val();
		$.ajax({
			url: "<?=url('Setting::Basic/NewsDetail')?>&news_id=" + news_id,
			type: "GET",
			dataType: "json",
			success:
				function(data)
				{
					
				}
		});
	});
});
</script>
<style type="text/css">
.newsshow{background: #f5f6f7;}
</style>
<h3>公告信息</h3>
<table class="list_table" width="90%">
<tr>
	<th>标题</th>
	<th width="150">时间</th>
	<th width="60">发布者</th>
</tr>
<?foreach ($news as $n):?>
<tr>
	<td align="left"><input type="hidden" value="<?=$n['id']?>" /><a class="view"><span class="<?=($n['unread']) ? 'news_unread_icon' : 'news_read_icon';?>"></span><?=$n['title']?></a></td>
	<td><?=$n['create_time']?></td>
	<td><?=$n['user_name']?></td>
</tr>
<tr style="display: none;">
	<td colspan="3" align="left"><?=$n['content']?></td>
</tr>
<?endforeach;?>
</table>
<a href="<?=url('Setting::Basic/NewsCreate')?>"></a>
<?=$page;?>
<input type="button" class="button" id="news_create" value="发布新公告" />
<?php $this->_endblock();?>