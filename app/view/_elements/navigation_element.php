<div style="float:left; width:100%; height:6px; background:url(<?=Q::ini('custom_system/base_url')?>img/red_line.png) bottom repeat-x;">
	<div style="float:left; width:5px; height:6px; font-size:0px; background:url(<?=Q::ini('custom_system/base_url')?>img/red_line.png) top left no-repeat;"></div>
	<div style="float:right; width:5px; height:6px; font-size:0px; background:url(<?=Q::ini('custom_system/base_url')?>img/red_line.png) top right no-repeat;"></div>
</div>
<div style="float: left;width: 100%;height: 41px;background: url(<?=Q::ini('custom_system/base_url')?>img/nav_bottom.png) top left repeat-x;">
	<div style="float: left;width: 5px;height: 41px;background: url(<?=Q::ini('custom_system/base_url')?>img/nav_left.png) top left no-repeat;"></div>
	<div style="float: right;width: 5px;height: 41px;background: url(<?=Q::ini('custom_system/base_url')?>img/nav_right.png) top right no-repeat;"></div>
	<div style="float: left;position: relative;text-align: left;margin-left: 10px;margin-right: 0px;width: 98%;">
		<?foreach (${Q::ini('app_config/APPID') . '_nca'} as $k => $n):?>
		<?if((isset($_GET['namespace']) && $_GET['namespace'] == $k) || ($k == 'default')):?>
		<a class="nav_a nav_a_selected"><?=$n['name']?></a>
		<?else:?>
		<a class="nav_a nav_a_select" href="<?=$n['first_action_url']?>"><?=$n['name']?></a>
		<?endif;?>
		<?endforeach;?>
		<div style="float: right;position: relative;padding-top:6px;">
			<span><?=date('Y年m月d日')?></span>
			<!--<a class="nav_a_more">工具</a>
			<ul  style="position: absolute; display: none;background: white;border: 1px solid #CFCFCF;top: 38px; right:3px;width: 100px;padding: 5px;">
				<li><a href="./headerScan.php" target="_blank"> - 总部扫码出库</a></li>
			</ul>-->
		</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
	$(".nav_a_more").hover(function(){
		$(this).next().show();
		$(this).next().find("ul").show();
	}, function(){
		$(this).next().hide();
		$(this).next().find("ul").hide();
	});
	$(".nav_a_more").next().hover(function(){
		$(this).prev().addClass("nav_a_more_hover");
		$(this).show();
		$(this).find("ul").show();
	}, function(){
		$(this).prev().removeClass("nav_a_more_hover");
		$(this).hide();
		$(this).find("ul").hide();
	});
});
</script>