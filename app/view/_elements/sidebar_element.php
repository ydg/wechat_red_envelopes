<script type="text/javascript">
$(function(){
	$(".sidebar_list h2.select").click(function(){
		$(this).next().slideToggle("fast");
		$(this).toggleClass("selected");
	});
	$(".sidebar_list").slimScroll({height: $(window).height() - 150 + 'px'});
});
</script>
<?php if (isset($_GET['namespace']) && isset(${Q::ini('app_config/APPID') . '_nca'}[$_GET['namespace']])):?>
<div class="sidebar_h">
	<div class="sidebar_hl"></div>
	<div class="sidebar_hc"><h1><?if (isset($_GET['namespace'])):?><?=${Q::ini('app_config/APPID') . '_nca'}[$_GET['namespace']]['name']?><?else:?><?endif;?></h1></div>
	<div class="sidebar_hr"></div>
</div>
<div class="sidebar_b">
	<div class="sidebar_bodyin">
	<ul class="sidebar_list">
		<?foreach (${Q::ini('app_config/APPID') . '_nca'}[$_GET['namespace']]['children'] as $kc => $c):?>
		<li>
			<?if ($kc == $_GET['controller']):?>
			<h2 class="select selected"><b></b><?=$c['name']?></h2>
			<ul style="display: block;">
			<?else:?>
			<h2 class="select"><b></b><?=$c['name']?></h2>
			<ul>
			<?endif;?>
			<?foreach ($c['children'] as $ka => $a):?>
				<li><a class="select <?=($kc == $_GET['controller'] && $ka == $_GET['action']) ? 'selected' : '' ?>" href="<?=url($_GET['namespace'] . '::' . $kc . '/' . $ka)?>"><b><?=$a['name']?></b></a></li>
			<?endforeach;?>
			</ul>
		</li>
		<?endforeach;?>
		<div class="sidebar_f">
			<div class="sidebar_fl"></div>
			<div class="sidebar_fr"></div>
		</div>
	</ul>
	</div>
</div>
<?php endif;?>

<!-- 
<div class="sidebar_h">
	<div class="sidebar_hl"></div>
	<div class="sidebar_hc"><h1>采购</h1></div>
	<div class="sidebar_hr"></div>
</div>
<div class="sidebar_b">
	<div class="sidebar_bodyin">
	<ul class="sidebar_list">
		<li>
			<h2 class="select"><b></b>分类管理</h2>
			<ul>
				<li><a class="select"><b>列表</b></a></li>
				<li><a class="select"><b>添加</b></a></li>
			</ul>
		</li>
		<li>
			<h2 class="select selected"><b></b>产品信息管理</h2>
			<ul style="display: block;">
				<li><a class="select selected"><b>列表</b></a></li>
				<li><a class="select"><b>编辑</b></a></li>
			</ul>
		</li>
		<li>
			<h2 class="select"><b></b>供应商管理</h2>
			<ul>
				<li><a class="select"><b>列表</b></a></li>
				<li><a class="select"><b>添加</b></a></li>
			</ul>
		</li>
	</ul>
	</div>
</div>
<div class="sidebar_f">
	<div class="sidebar_fl"></div>
	<div class="sidebar_fr"></div>
</div>
-->