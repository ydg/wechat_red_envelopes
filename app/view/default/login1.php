<?php
		$is_mobile = Helper_BSS_Normal::is_mob();
    	if ($is_mobile)
    	{
			header("Location:".url('Default::Default/MobLogin'));
    	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.js"></script>
<title>用户登录</title>
<style type="text/css">
* {
	margin: 0;
	padding: 0;
	font-family: '微软雅黑',Verdana,arial,tahoma,sans-serif;
}
body {
	color: #444444;
	font-size: 12px;
}
.container {
	margin: 80px auto;
	text-align: left;
	width: 455px;
}
.graymodel{ float:left; width:100%; margin:5px 0;}
.grayh, .grayf, .grayhl, .grayhr, .grayfl, .grayfr{ background:url(<?=Q::ini('custom_system/base_url')?>img/rouGraybox.gif) }
.grayh{float:left; width:100%; background-position:0 -72px;  height:5px;}
.grayhl { float:left; width:5px; background-position:0 0;  height:5px;}
.grayhr { float:right; width:5px;  background-position:0 -36px;  height:5px;}
.grayhrc{float:right; margin-right:5px;height:28px; line-height:28px; text-align:right; cursor:pointer}
.grayf {width:100%; background-position:0 -180px; float:left; height:5px;}
.grayfl { float:left; width:5px; background-position:0 -108px; height:5px;}
.grayfr { float:right; width:5px; background-position:0 -144px;height:5px;}
.graybody { float:left;width:100%;height:100%;}
.graybodyin{border-left:1px solid #ccc;border-right:1px solid #ccc;height:100%;overflow:hidden;}
.hploginright{width:450px; position: absolute;}
.ssllogin{ width:100%; float:left;}
.ssllogin h1{ height:38px; line-height:36px;border-bottom:1px solid #ccc; margin-bottom:10px;font-size: 17px;font-weight: normal;color: #444;}
.ssllogin h1 span{ margin-left:10px;}
.ssllogin dl{ float:left;margin-top:30px;  padding-bottom:25px; width:100%;margin-left: 40px;}
.ssllogin dt, .ssllogin dd{ float:left;}
.ssllogin dt{ width:120px; text-align:right; margin-right:10px;}
.ssllogin dd{ width:250px; margin-bottom:20px; vertical-align:middle}
.ssllogin dd input{ vertical-align:middle}
.ssllogin dd .logintext{border:1px solid #B0CED9;padding:3px;}
.hpbtnlogin {width:112px; height:33px; padding-bottom:8px; padding-right:10px; border:none; color:#fff; font-size:13px; font-weight:bold; cursor:pointer;background:url(<?=Q::ini('custom_system/base_url')?>img/loginbtn.png) no-repeat;}
.sslshadow{background:url(<?=Q::ini('custom_system/base_url')?>img/shadow.png) no-repeat; width:463px; height:31px; float:left; margin-top:2px;}
#login_error {
background-color: #FFEBE8;
border:1px solid #C00;
border-radius: 3px;
padding: 10px;
width: 428px;
}
</style>
</head>
<body>
<div class="container">
<form method="post" action="<?=url('Default::Default/Login')?>">
<input type="hidden" name="history" value="<?=isset($_GET['history']) ? $_GET['history'] : ''?>" />
<div class="hploginright">
<?if (isset($_GET['msg'])):?>
<div id="login_error">
	<span>
		<strong>Error : </strong><?=isset($_GET['msg'])? $_GET['msg'] : "";?> is wrong！
		<a href="tencent://message/?uin=1028251782&Menu=yes" title="联系运维组">ask for help！</a>
	</span>
</div>
<?endif;?>
	<div class="graymodel">
		<div class="grayh">
			<div class="grayhl"></div>
			<div class="grayhr"></div>
		</div>
		<div class="graybody">
			<div class="graybodyin">
				<div class="ssllogin">
					<h1><span>深圳信诚 v<font size="0.8px" color="#66ccff">1.0</font></span></h1>
					<dl id="dllogin">
						<dt>Uername</dt>
						<dd><input type="text" name="name" /></dd>
						<dt>Password</dt>
						<dd><input type="password" name="password" /></dd>
						<dt>&nbsp;</dt>
						<dd><input type="submit" value="Login" class="hpbtnlogin" /></dd>
					</dl>
				</div>
			</div>
		</div>
		<div class="grayf">
			<div class="grayfl"></div><div class="grayfr"></div>
		</div>
	</div>
	<div class="sslshadow"></div>
</div>
</form>
</div>
</body>
</html>
<?=isset($_GET['msg']) ? "<script type='text/javascript'>function shake(){box_left = ($(window).width() -  $('#login_error').width()) / 50; $('#login_error').css({'position':'relative'});for(var i=1; 5>=i; i++){ $('#login_error').animate({left:box_left-2*(40-10*i)},50);$('#login_error').animate({left:box_left+2*(40-10*i)},50);}}; shake();</script>" : ''?>