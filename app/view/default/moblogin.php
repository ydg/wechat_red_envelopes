<html>
<!DOCTYPE html>
<html lang="en" class="no-js">

    <head>

        <meta charset="utf-8">
        <title>登录(Login)--防窜货系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- CSS -->
        <link rel="stylesheet" href="<?=Q::ini('custom_system/base_url')?>assets/css/reset.css">
        <link rel="stylesheet" href="<?=Q::ini('custom_system/base_url')?>assets/css/supersized.css">
        <link rel="stylesheet" href="<?=Q::ini('custom_system/base_url')?>assets/css/style.css">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="assets/js/html5.js"></script>
        <![endif]-->

    </head>

    <body>

        <div class="page-container">
            <h1>登录(深圳信诚)</h1>
            <?if (isset($_GET['msg'])):?>
			<p><strong>Error : </strong><?=$_GET['msg'];?> is wrong！</p>
			<?endif;?>
            <form action="<?=url('Default::Default/MobLogin')?>" method="post">
            	<input type="hidden" name="history" value="<?=isset($_GET['history']) ? $_GET['history'] : ''?>" />
                <input type="text" name="name" class="username" placeholder="请输入您的用户名！">
                <input type="password" name="password" class="password" placeholder="请输入您的用户密码！">
                <!--<input type="Captcha" class="Captcha" name="Captcha" placeholder="请输入验证码！">-->
                <button type="submit" class="submit_button">登录</button>
                <div class="error"><span>+</span></div>
            </form>
            <!--<div class="connect">
                <p>快捷</p>
                <p>
                    <a class="facebook" href=""></a>
                    <a class="twitter" href=""></a>
                </p>
            </div>-->
        </div>
		
        <!-- Javascript -->
        <script src="assets/js/jquery-1.8.2.min.js" ></script>
        <script src="assets/js/supersized.3.2.7.min.js" ></script>
        <script src="assets/js/supersized-init.js" ></script>
        <script src="assets/js/scripts.js" ></script>

    </body>

</html>

