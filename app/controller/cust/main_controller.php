<?php
Class Controller_Cust_Main extends Controller_Abstract
{
	function actionPersonCenter()
	{
		$this->_view['title'] = '个人中心';
	}
	
	function actionEditinfo()
	{
		$this->_view['title'] = '修改个人信息';
		$bank = Bank::find()->asArray()->getAll();
		$provinces = Provinces::find()->asArray()->getAll();
		$cities = Cities::find()->asArray()->getAll();
//		dump($provinces);exit;
		$this->_view['bank'] =  $bank;
		$this->_view['provinces'] =  $provinces;
		$this->_view['cities'] =  $cities;
		if (CURRENT_USER_NAME)
		{
			$result = Customer::find('c_no=?',CURRENT_USER_NAME)->asArray()->getOne();
			//$result = Customer::find('c_no=?','qwert')->asArray()->getOne();
			//dump($result);exit;
			$this->_view['result'] = $result;
		}
	}
	
	function actionEditinfoSave()
	{
		$this->_view['title'] = '个人信息保存';
		//dump($_POST);exit;
		$edit = Customer::find('id=?', $_POST['userID'])->getOne();
		if (!$edit['id'])
		{
			return $this->_redirectMessage('编辑失败', '请检查提交的数据是否正确', $_SERVER['HTTP_REFERER'], FAILURE);
		}
		else
		{
			$customer = $_POST;
			$edit->c_contact = $customer['c_contact'];
			$edit->c_phone =$customer['c_phone'];
			$edit->c_province =$customer['province'];
			$edit->c_city =$customer['city'];
			$edit->c_addr =$customer['c_addr'];
			$edit->c_bank =$customer['c_bank'];
			$edit->c_bankid =$customer['c_bankid'];
			$edit->c_bankname =$customer['c_bankname'];
			$edit->c_bankuser =$customer['c_bankuser'];
			$edit->save();
			return $this->_redirectMessage('编辑成功', '正在返回..', $_SERVER['HTTP_REFERER'], SUCCESS);
		}
	}
	
	function actionEditPassword()
	{
		$this->_view['title'] = '修改密码';
	}
	
	function actionEditPasswordSave()
	{
		$this->_view['title'] = '修改密码保存';
		if ($this->_context->isPOST())
		{
			$user = $this->_app->currentUser();
			if (empty($_POST['ouserPWD']))
			{
				return $this->_redirectMessage('旧密码为空！', '请输入您的旧密码！', $_SERVER['HTTP_REFERER'], FAILURE);
			}
			if (empty($_POST['nuserPWD']))
			{
				return $this->_redirectMessage('新密码为空！', '请输入您的新密码！', $_SERVER['HTTP_REFERER'], FAILURE);
			}
			
			$result = User::passwordChange($_POST['ouserPWD'], $_POST['nuserPWD']);
			if ($result['ack'] == SUCCESS)
			{
				//在下面修改customer表的密码
				QDB::getConn()->execute("update customer set c_pwd='" . $_POST['nuserPWD'] . "'");
				return $this->_redirectMessage('密码修改成功！', '更新用户密码成功。', $_SERVER['HTTP_REFERER'], $result['ack']);
			}
			else
			{
				return $this->_redirectMessage('密码错误！', $result['message'], $_SERVER['HTTP_REFERER'], $result['ack']);
			}
		}
		else
		{
			return $this->_redirect(url('Cust::Main/EditPassword'));
		}
	}
	
	function actionTrackingProduct()
	{
		$this->_view['title'] = '产品追踪';
		$columns = array(
			'ship.s_date',
			'shipdetail.*',
			'product.p_no,product.p_name,product.p_price',
			'customer.c_no,customer.c_name,customer.c_addr,customer.c_province,customer.c_city'
		);
		$columns1 = array(
			'ship1.s_date',
			'shipdetail1.*',
			'product.p_no,product.p_name,product.p_price',
			'customer.c_no,customer.c_name,customer.c_addr,customer.c_province,customer.c_city'
		);
		if (isset($_POST['barcode']))
		{
			$k = explode(',', Helper_BSS_API::getk(trim($_POST['barcode'])));
			if ($k[1] > 1)
			{
//					$result = Shipdetail::find('shipdetail.barcode=?', trim($_GET['barcode']))
//						->joinLeft('ship', '', 'ship.s_no=shipdetail.s_no')
//						->joinLeft('customer', '', 'customer.c_no=ship.c_no')
//						->joinLeft('product', '', 'product.p_no=shipdetail.p_no')
//						->setColumns($columns)->asArray()->getAll();
//					$this->_view['result_header'] = $result;
					
					//dump($result);exit;
					//$result = Shipdetail::find($k[2] . ' and shipdetail.barcode=\'' . trim($_GET['barcode']) . '\'')
				$result = Shipdetail::find('k=\'' . $k[0] . '\' and shipdetail.barcode=\'' . trim($_POST['barcode']) . '\'')
					->joinLeft('ship', '', 'ship.s_no=shipdetail.s_no')
					->joinLeft('customer', '', 'customer.c_no=ship.c_no')
					->joinLeft('product', '', 'product.p_no=shipdetail.p_no')
					->setColumns($columns)->asArray()->getAll();
				$result_cust = Shipdetail1::find('shipdetail1.k=\'' . $k[0] . '\' and shipdetail1.barcode=\'' . trim($_GET['barcode']) . '\'')
					->joinLeft('ship1', '', 'ship1.s_no=shipdetail1.s_no')
					->joinLeft('customer', '', 'customer.c_no=ship1.c_no')
					->joinLeft('product', '', 'product.p_no=shipdetail1.p_no')
					->setColumns($columns1)->asArray()->getAll();
			}
			else 
			{
				$result = Shipdetail::find($k[2])
					->joinLeft('ship', '', 'ship.s_no=shipdetail.s_no')
					->joinLeft('customer', '', 'customer.c_no=ship.c_no')
					->joinLeft('product', '', 'product.p_no=shipdetail.p_no')
					->setColumns($columns)->asArray()->getAll();
				$result_cust = Shipdetail1::find($k[2])
					->joinLeft('ship1', '', 'ship1.s_no=shipdetail1.s_no')
					->joinLeft('customer', '', 'customer.c_no=ship1.c_no')
					->joinLeft('product', '', 'product.p_no=shipdetail1.p_no')
					->setColumns($columns1)->asArray()->getAll();
			}
			//dump($result);exit;
			if ($result_cust)
			{
				$this->_view['result_cust'] = $result_cust;
			}
			if ($result)
			{
				$this->_view['result_header'] = $result;
			}
//			else 
//			{
//				return $this->_redirectMessage('查询失败', '没有产品信息', $_SERVER['HTTP_REFERER'], FAILURE);
//			}
		}
		else 
		{
			$result_cust = Shipdetail1::find('ship1.c_no=\'' . CURRENT_USER_NAME . '\'')
				->joinLeft('ship1', '', 'ship1.s_no=shipdetail1.s_no')
				->joinLeft('customer', '', 'customer.c_no=ship1.c_no')
				->joinLeft('product', '', 'product.p_no=shipdetail1.p_no')
				->setColumns($columns1)->asArray()->getAll();
			if ($result_cust)
			{
				$this->_view['result_cust'] = $result_cust;
			}
		}
		
	}
	
	function actionPromotion()
	{
		$this->_view['title'] = '政策通告';
		$result = News::find()->asArray()->getAll();
		$this->_view['result'] = $result;
	}
	
	function actionPromotionDetail()
	{
		$this->_view['title'] = '政策通告详细';
		if (isset($_GET['id'])&&$_GET['id'])
		{
			$result = News::find('id=?',$_GET['id'])->asArray()->getOne();
			$this->_view['result'] = $result;
		}
		else 
		{
			echo "<script> alert('没有该通告！');history.go(-2);return false;</script>";
		}
	}
	
	function actionProduct()
	{
		$this->_view['title'] = '商品';
		if (isset($_POST['key'])&&$_POST['key'])
		{
			$where = "product.p_name like '%" . $_POST['key'] . "%'";
			$result = Product::find($where)->asArray()->getAll();
		}
		else 
		{
			if (isset($_GET['c_id'])&&$_GET['c_id'])
			{
				$result = Product::find('ma_c_id=?', $_GET['c_id'])->asArray()->getAll();
			}
			else 
			{
				$result = Product::find()->asArray()->getAll();//dump($result);exit;
			}
		}
		$this->_view['product'] = $result;
	}
	
	function actionProductDetail()
	{
		$this->_view['title'] = '商品详情';
		if (isset($_GET['id'])&&$_GET['id'])
		{
			$result = Product::find('id=?',$_GET['id'])->asArray()->getOne();//dump($result);exit;
			$this->_view['product'] = $result;
		}
		else 
		{
			echo "<script> alert('没有该产品！');history.go(-1);return false;</script>";
		}
	}
	
	function actionProductList()
	{
		$this->_view['title'] = '商品分类';
		$category = Mall_Category::find()->asArray()->getAll();
		$this->_view['category'] = $category;
	}
	
	function actionAgent()
	{
		$this->_view['title'] = '代理管理';
		if (isset($_GET['c_no'])&&$_GET['c_no'])
		{
			$user_id = User::find('user.name=?', $_GET['c_no'])->asArray()->getOne();
		}
		else 
		{
			$user_id = User::find('user.name=?', CURRENT_USER_NAME)->asArray()->getOne();
		}
		if ($user_id)
		{
			$cust = User::find('user.parent_id=?',$user_id['id'])
				->joinLeft('customer', 'customer.*', 'user.name=customer.c_no')
				->joinLeft('provinces', 'provinces.province', 'customer.c_province=provinces.provinceid')
				->joinLeft('cities', 'cities.city', 'customer.c_city=cities.cityid')
				->joinLeft('rank', 'rank.*', 'customer.c_type=rank.rk_id')
				->asArray()->getAll();
			$record = array();
			foreach ($cust as $c)
			{
				if ($c['id'])
				{
					$record[] = $c;
				}
			}
		}
//		dump($record);exit;
		$this->_view['cust'] = empty($record)?array():$record;
		$this->_view['count'] = empty($record)?0:count($record);
	}
	
	function actionAgentDetail()
	{
		$this->_view['title'] = '代理商详情';
		$bank = Bank::find()->asArray()->getAll();
		$provinces = Provinces::find()->asArray()->getAll();
		$cities = Cities::find()->asArray()->getAll();
		$this->_view['bank'] =  $bank;
		$this->_view['provinces'] =  $provinces;
		$this->_view['cities'] =  $cities;
		$id = isset($_GET['id'])?$_GET['id']:0;
		if (isset($_GET['id'])&&$_GET['id'])
		{
			$result = Customer::find('id=?',$id)->asArray()->getOne();
			//$result = Customer::find('c_no=?','qwert')->asArray()->getOne();
			//dump($result);exit;
			$this->_view['result'] = $result;
		}
	}
	
	function actionInship()
	{
		$this->_view['title']='入库管理';
//		$inship = Inship::find('inship.c_no=?','226235')
//			->joinLeft('customer', 'customer.c_name', 'customer.c_no=inship.c_no')
//			->joinLeft('inshipdetail', '', 'inshipdetail.i_no=inship.i_no')
//			->asArray()->getAll();6
		$t = isset($_GET['t'])?$_GET['t']:'';
		$key = isset($_POST['key'])?$_POST['key']:'';
//		$result = InShip::search(0, 0, array('c_no'=>CURRENT_USER_NAME,'t '=>$t,'number' => $key));
//		$result = Ship::search(0, 0, array('c_no'=>CURRENT_USER_NAME,'t '=>$t,'number' => $key));
		
		//============================================================================//
		//t=1为总部发货 t=2为代理发货 t=0为所有发货记录（就搜索发货或者收货都是这个人就可以了（但是这样就看不到所有的子类））
		$column = array(
			'ship.*',
			'customer.c_name'
		);
		$column1 = array(
			'ship1.*',
			'customer.c_name'
		);
		if ($t==0)
		{
			$result = array();
			$user = User::getUsername();//获取当前用户的下级
			$where = '1';
			$where .= ' and customer.c_no in('.$user.')';
			//如果使用分页只需要合并数组
			if ($key)
			{
				$ship = Ship::find($where. ' and ship.s_no=\'' .$key. '\'')
					->joinLeft('customer', '', 'customer.c_no=ship.c_no')
					->setColumns($column)
					->asArray()->getAll();
				$ship1 = Ship1::find($where. ' and ship1.s_no=\'' .$key. '\'')
					->joinLeft('customer', '', 'customer.c_no=ship1.c_no')
					->setColumns($column1)
					->asArray()->getAll();
			}
			else 
			{
				$ship = Ship::find($where)
					->joinLeft('customer', '', 'customer.c_no=ship.c_no')
					->setColumns($column)
					->asArray()->getAll();
				$ship1 = Ship1::find($where)
					->joinLeft('customer', '', 'customer.c_no=ship1.c_no')
					->setColumns($column1)
					->asArray()->getAll();
			}
			$result = array_merge($ship,$ship1);
		}
		else 
		{
			if ($t==1)
			{
				$user = User::getUsername();//获取当前用户的下级
				$where = '1';
				$where .= ' and customer.c_no in('.$user.')';
				//如果使用分页只需要合并数组
				if ($key)
				{
					$result = Ship::find($where. ' and ship.s_no=\'' .$key. '\'')
						->joinLeft('customer', '', 'customer.c_no=ship.c_no')
						->setColumns($column)
						->asArray()->getAll();
				}
				else 
				{
					$result = Ship::find($where)
						->joinLeft('customer', '', 'customer.c_no=ship.c_no')
						->setColumns($column)
						->asArray()->getAll();
				}
			}
			else 
			{
				$user = User::getUsername();//获取当前用户的下级
				$where = '1';
				$where .= ' and customer.c_no in('.$user.')';
				//如果使用分页只需要合并数组
				if ($key)
				{
					$result = Ship1::find($where. ' and ship1.s_no=\'' .$key. '\'')
						->joinLeft('customer', '', 'customer.c_no=ship1.c_no')
						->setColumns($column1)
						->asArray()->getAll();
				}
				else 
				{
					$result = Ship1::find($where)
						->joinLeft('customer', '', 'customer.c_no=ship1.c_no')
						->setColumns($column1)
						->asArray()->getAll();
				}
			}
		}
		//============================================================================//
		//暂停使用的代码
//		if ($result['ack'] == SUCCESS)
//		{
//			if ($t==1)
//			{
//				$user = User::find('is_agent=?',0)->setColumns('user.name')->asArray()->getAll();
//				$admin = array();
//				foreach ($user as $u)
//				{
//					$admin[] = $u['name'];
//				}
//				//dump($admin);exit;
//				$record = array();
//				foreach ($result['data'] as $r)
//				{
//					if (in_array($r['username'], $admin))
//					{
//						$inshipdetail = Inshipdetail::find('i_no=?',$r['i_no'])
//							->joinLeft('product', 'product.p_price', 'inshipdetail.p_no=product.p_no')
//							->asArray()->getAll();
//							//dump($inshipdetail);
//						 if ($inshipdetail)
//						 {
//						 	$price = 0;
//						 	foreach ($inshipdetail as $i)
//						 	{
//						 		 $price += $i['qty']*$i['p_price'];
//						 	}
//						 	$r['p_price'] = $price;
//						 }
//						 $record[] = $r;
//					}
//				}
//			}
//			else 
//			{
//				$record = array();
//				foreach ($result['data'] as $r)
//				{
//					$inshipdetail = Inshipdetail::find('i_no=?',$r['i_no'])
//						->joinLeft('product', 'product.p_price', 'inshipdetail.p_no=product.p_no')
//						->asArray()->getAll();
//						//dump($inshipdetail);
//					 if ($inshipdetail)
//					 {
//					 	$price = 0;
//					 	foreach ($inshipdetail as $i)
//					 	{
//					 		 $price += $i['qty']*$i['p_price'];
//					 	}
//					 	$r['p_price'] = $price;
//					 }
//					 $record[] = $r;
//				}
//			}
//		}
		$this->_view['count'] = empty($result)? 0 : count($result);
		$this->_view['inship'] = empty($result)? array():$result;
		
	}
	
	function actionInshipDetail()
	{
		$this->_view['title'] = '入库详细';
		if (isset($_GET['no'])&&$_GET['no'])
		{
			$inshipdetail = Inshipdetail::find('i_no=?',$_GET['no'])
				->joinLeft('product', 'product.p_name,product.p_price', 'product.p_no=inshipdetail.p_no')
				->asArray()->getAll();
			$this->_view['count'] = empty($inshipdetail)? 0 : count($inshipdetail);
			$this->_view['inshipdetail'] = empty($inshipdetail)? array():$inshipdetail;
			//dump($inshipdetail);exit;
		}
		else 
		{
			echo "<script> alert('没有该入库单！');history.go(-1);return false;</script>";
		}
	}
	
	function actionOutship()
	{
		$this->_view['title'] = '出库管理';
		$cust = Customer::find()->asArray()->getAll();
		$this->_view['cust'] = $cust;
		$key = isset($_POST['key'])?$_POST['key']:'';
//		$result = Ship1::search(0, 0, array('username'=>CURRENT_USER_NAME,'number'=>$key));//username是发货的人
		$result = Ship1::search(0, 0, array('number'=>$key));
		if ($result['ack'] == SUCCESS)
		{
			//dump($result['data']);exit;
			$this->_view['ship'] = $result['data'];
			$this->_view['count'] = empty($result['data'])?0:count($result['data']);
		}
	}
	
	function actionOutshipDetail()
	{
//		$columns = array(
//			'ship.s_date',
//			'shipdetail.*',
//			'product.p_no,product.p_name,product.p_price',
//			'customer.c_no,customer.c_name'
//		);
//		$columns1 = array(
//			'ship1.s_date',
//			'shipdetail1.*',
//			'product.p_no,product.p_name,product.p_price',
//			'customer.c_no,customer.c_name'
//		);
//		$barcode = '601518709152';
//	$k = explode(',', Helper_BSS_API::getk($barcode));
//			if ($k[1] > 1)
//			{
//				$result = Shipdetail::find('k=\'' . $k[0] . '\' and shipdetail.barcode=\'' . $barcode . '\'')
//					->joinLeft('ship', '', 'ship.s_no=shipdetail.s_no')
//					->joinLeft('customer', '', 'customer.c_no=ship.c_no')
//					->joinLeft('product', '', 'product.p_no=shipdetail.p_no')
//					->setColumns($columns)->asArray()->getAll();
//			}
//	else 
//			{
//				$result = Shipdetail::find($k[2])
//					->joinLeft('ship', '', 'ship.s_no=shipdetail.s_no')
//					->joinLeft('customer', '', 'customer.c_no=ship.c_no')
//					->joinLeft('product', '', 'product.p_no=shipdetail.p_no')
//					->setColumns($columns)->asArray()->getAll();
//			}
//			dump($result);exit;
		$this->_view['title'] = '出库详细';
		$no = isset($_GET['no'])?$_GET['no']:'';
		$c_no = isset($_GET['c_no'])?$_GET['c_no']:'';
		if ($no)
		{
			$ship = Shipdetail1::find('s_no=?', $_GET['no'])
				->joinLeft('product', 'product.p_name', 'product.p_no=shipdetail1.p_no')
				->asArray()->getAll();
			//dump($ship);exit;
			$this->_view['ship'] = $ship;
			$this->_view['count'] = empty($ship)?0:count($ship);
			$this->_view['no'] = $_GET['no'];
		}
		else 
		{
			if ($c_no)
			{
				$this->_view['ship'] = array();
				$this->_view['count'] = 0;
				$this->_view['c_no'] = $_GET['c_no'];
			}
			else 
			{
				echo "<script> alert('没有该入出库单！');history.go(-1);return false;</script>";
			}
		}
	}
	
	function actionOutShipDelAjax()
	{
		$this->_view['title'] = '出货删除Ajax';
		$mode = isset($_REQUEST['mode'])?$_REQUEST['mode']:'';
		$no = isset($_REQUEST['s_no'])?$_REQUEST['s_no']:'';
		if($mode==2)
		{
			QDB::getConn()->execute("delete from ship1 where s_no='". $no . "'");
			QDB::getConn()->execute("delete from shipDetail1 where s_no='". $no . "'");
			echo json_encode(array('success'=>'success','msg'=>'success'),JSON_UNESCAPED_UNICODE);exit;
		}
		else 
		{
			echo json_encode(array('msg'=>'not mode,it is null!'),JSON_UNESCAPED_UNICODE);exit;
		}
	}
	
	function actionOutShipAjax()
	{
		$this->_view['title'] = '出库Ajax';
		$mode = isset($_REQUEST['mode'])?$_REQUEST['mode']:'';
		$no = isset($_REQUEST['rk_id'])?$_REQUEST['rk_id']:'';
		$barcode ='351001821009';//isset($_REQUEST['barcode'])?$_REQUEST['barcode']:'';
		if($mode==3)
		{
			$columns = array(
				'ship.s_date',
				'shipdetail.*',
				'product.p_no,product.p_name,product.p_price',
				'customer.c_no,customer.c_name'
			);
			$k = explode(',', Helper_BSS_API::getk($barcode));
			if ($k[1] > 1)//只是为了查询该产品是否有总部发出
			{
				$result = Shipdetail::find('k=\'' . $k[0] . '\' and shipdetail.barcode=\'' . $barcode . '\'')
					->joinLeft('ship', '', 'ship.s_no=shipdetail.s_no')
					->joinLeft('customer', '', 'customer.c_no=ship.c_no')
					->joinLeft('product', '', 'product.p_no=shipdetail.p_no')
					->setColumns($columns)->asArray()->getOne();
			}
			else 
			{
				$result = Shipdetail::find($k[2])
					->joinLeft('ship', '', 'ship.s_no=shipdetail.s_no')
					->joinLeft('customer', '', 'customer.c_no=ship.c_no')
					->joinLeft('product', '', 'product.p_no=shipdetail.p_no')
					->setColumns($columns)->asArray()->getOne();
			}
			//dump($result);exit;//current($result)取第一条的值
			if ($result)
			{
//				$record = array();
//				foreach ($result as $r)
//				{
//					$record[] = $r;
//				}
//				$record['msg'] = 'success';
//				$record['success'] = 'success';
				$result['msg'] = 'success';
				$result['success'] = 'success';
				echo json_encode($result);exit;
			}
			else
			{
				echo json_encode(array('msg'=>'the code is void!'),JSON_UNESCAPED_UNICODE);exit;
			}
		}
		else 
		{
			echo json_encode(array('msg'=>'not mode,it is null!'),JSON_UNESCAPED_UNICODE);exit;
		}
		//Response.Write("{success:true,qty:"&lngQTY&",p_no:"""&oRs("p_no")&""",p_name:"""&oRs("p_name")&""",p_price:"""&FormatCurrency(oRs("p_price"&rk_id)*lngQTY,2,true)&"""}")
//		$product_basic = Product::find('p_name like \'%' . $_REQUEST['q'] . '%\'')->limitPage(0, 10)->asArray()->getAll();
//		$result = array();
//		if ($product_basic)
//		{
//			foreach ($product_basic as $pb)
//			{
//				$result[] = array(
//					0 => '('.$pb['p_no'] . ') ' . $pb['p_name'],
//					'id' => $pb['id'],
//					'number' => $pb['p_no'],
//					'price' => $pb['p_price']
//				);
//			}
//		}
//		echo json_encode($result);
//		exit;
	}
	
	function actionOutShipSaveAjax()
	{
		$this->_view['title'] ='保存出库信息ajax';
		$mode = isset($_REQUEST['mode'])?$_REQUEST['mode']:'';
		$s_no = isset($_REQUEST['s_no'])?$_REQUEST['s_no']:'';
		$c_no = isset($_REQUEST['c_no'])?$_REQUEST['c_no']:'';
		$barcode = isset($_REQUEST['barcode']) ? $_REQUEST['barcode']:'';
		if ($mode==4)//新建
		{
			if($c_no==CURRENT_USER_NAME)
			{
				echo json_encode(array('msg'=>'不能给自己发货!'),JSON_UNESCAPED_UNICODE);exit;
			}
			else 
			{
				$order_no = Ship1::find()->order('id desc')->asArray()->getOne();
				$set_no='';
				if(substr($order_no['s_no'], 0,6) == substr(date('Ymd',time()), 2))
				{
					$set_no = $order_no['s_no']+1;
				}
				else
				{
					$set_no = substr(date('Ymd',time()), 2).'00001';//如果是代理发货就直接使用倒过来的方式，防止订单好重复
				}
				$cust = Customer::find('c_no=?', $c_no)->asArray()->getOne();
				if (empty($cust))
				{
					echo json_encode(array('msg'=>'客户不存在!'),JSON_UNESCAPED_UNICODE);exit;
				}
				else//传过来的数据有条形码，产品编号以换行结束
				{
					$data = explode("\r\n", trim($barcode));//要去掉空格
					$pcs = 0;//件数
					$quantity = 0;//总数量
					$total_price = 0;
					foreach ($data as $d)
					{
						$dc = explode(',', $d);
						
						$k = explode(',', Helper_BSS_API::getk($dc[0]));//验证条形码的正确性，返回的格式（k, quantity，where）
						$product = Product::find('p_no=\''.$dc[1].'\'')->asArray()->getOne();
						//$is_exsit = QDB::getConn()->execute('select id from product where p_no=\''. trim($dc[1]) .'\'');
						//$abc = $product['id'];
						
						if (!$product)
						{
							echo json_encode(array('msg'=>'产品不存在'),JSON_UNESCAPED_UNICODE);exit;
						}
						else 
						{
							//echo json_encode(array('msg'=>'hello my name is ydg'),JSON_UNESCAPED_UNICODE);exit;
							$price = $product['p_price']*$k[1];
							QDB::getConn()->execute('insert into shipdetail1(s_no, barcode, k, qty, qty_over, p_no, p_price) values(\''.$set_no.'\',\''.$dc[0].'\',\''.$k[0].'\','.$k[1].','.$k[1].',\''.$dc[1].'\','.$price.')');
							$pcs ++;
							$quantity += $k[1];
							$total_price += $price;
						}
					}
					//详细信息添加完
					QDB::getConn()->execute("INSERT INTO ship1(s_no,c_no,c_no_p,username,s_pcs,s_qty,s_qty_over,p_price,s_date) values('".$set_no."','".$c_no."','".CURRENT_USER_NAME."','".CURRENT_USER_NAME."',".$pcs.",".$quantity.",".$quantity.",".$total_price.",'".date('Y-m-d H:i:s',time())."')");
					echo json_encode(array('success'=>'success','msg'=>'success'),JSON_UNESCAPED_UNICODE);exit;
				}
			}
		}
		else
		{
			if ($mode==5)//添加产品
			{
				if($c_no==CURRENT_USER_NAME)
				{
					echo json_encode(array('msg'=>'不能给自己发货!'));exit;
				}
				else 
				{
					QDB::getConn()->execute('delete from shipdetail1 where s_no=\''.$s_no.'\'');
					$data = explode("\r\n", trim($barcode));
					$pcs = 0;//件数
					$quantity = 0;//总数量
					$total_price = 0;
					foreach ($data as $d)
					{
						$dc = explode(',', $d);
						$k = explode(',', Helper_BSS_API::getk($dc[0]));//验证条形码的正确性，返回的格式（k, quantity，where）
						$product = Product::find('p_no=\''.$dc[1].'\'')->asArray()->getOne();
						if (empty($product))
						{
							echo json_encode(array('msg'=>'产品不存在!'),JSON_UNESCAPED_UNICODE);exit;
						}
						else 
						{
							$price = $product['p_price']*$k[1];
							QDB::getConn()->execute('insert into shipdetail1(s_no,barcode,k,qty,qty_over,p_no,p_price) values(\''.$s_no.'\',\''.$dc[0].'\',\''.$k[0].'\','.$k[1].','.$k[1].',\''.$dc[1].'\','.$price.')');
							$pcs ++;
							$quantity += $k[1];
							$total_price += $price;
						}
					}
					//详细信息添加完
					QDB::getConn()->execute("update ship1 set s_pcs =".$pcs.",s_qty=".$quantity.",s_qty_over=".$quantity.",p_price=".$total_price." where s_no='".$s_no."'");
					//QDB::getConn()->execute("INSERT INTO ship1(s_no,c_no,c_no_p,username,s_pcs,s_qty,s_qty_over,p_price,s_date)values('".$set_no."','".$c_no."','".CURRENT_USER_NAME."','".CURRENT_USER_NAME."',".$pcs.",".$quantity.",".$quantity.",".$total_price.",'".date('Y-m_d H:i:s',time())."')");
					echo json_encode(array('success'=>'success','msg'=>'success'),JSON_UNESCAPED_UNICODE);exit;
				}
				
			}
			else 
			{
				echo json_encode(array('msg'=>'无效操作，你的操作已被记录，请不要胡乱操作'),JSON_UNESCAPED_UNICODE);exit;	
			}
		}
	}
	//暂时用不上
	function actionCustomer()
	{
		$this->_view['title'] = '用户Ajax';
		$cust = Customer::find()->asArray()->getAll();
		
	}
}