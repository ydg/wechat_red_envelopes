<?php
class Controller_Default extends Controller_Abstract
{
    function actionIndex()
    {
    	$this->_view['title'] = 'Index';
    	//echo 'nihao1';exit;//测试句子
    	$is_mobile = Helper_BSS_Normal::isMobile();
    	if ($is_mobile)
    	{
    		if (CURRENT_USER_ID)
	    	{
	    		return $this->_redirect(url('Cust::Main/PersonCenter'));
	    	}
	    	else
	    	{
	    		return $this->_redirect(url('Default::Default/MobLogin'));
	    	}
    	}
    	else
    	{
	    	if (CURRENT_USER_ID)
	    	{
	    		return $this->_redirect(url('Default::Default/Admin'));
	    	}
	    	else
	    	{
	    		return $this->_redirect(url('Default::Default/Login'));
	    	}
    	}
    }
	
	function actionAdmin()
	{
		$this->_view['title'] = '管理后台';
	}
	
	function actionWelcome()
	{
		$this->_view['title'] = '欢迎页面';
	}
    
	function actionMobLogin()
    {
    	$this->_view['title'] = '手机登陆';
    	if ($this->_context->isPOST())
		{
			try
			{
				$user = User::meta()->validateLogin($_POST['name'], $_POST['password']);
				$roles = Roles_Users_Mapping::getUserRolesId($user['id']);
				$this->_app->changeCurrentUser($user->aclData(), $roles);
				if ($_POST['history'])
				{
					return $this->_redirect($_POST['history']);
				}
				else
    			{
    				return $this->_redirect(url('Cust::Main/PersonCenter'));
    			}
			}
			catch (AclUser_UsernameNotFoundException $ex)
			{
				return $this->_redirect(url('Default::Default/MobLogin', array('msg' => 'username', 'history' => $_POST['history'])));
			}
			catch (AclUser_WrongPasswordException $ex)
			{
				return $this->_redirect(url('Default::Default/MobLogin', array('msg' => 'password', 'history' => $_POST['history'])));
			}
		}
    }

    function actionLogin()
    {
    	$this->_view['title'] = '用户登入';
    	$is_mobile = Helper_BSS_Normal::isMobile();
    	if ($is_mobile)
    	{
    		if (CURRENT_USER_ID && CURRENT_USER_NAME)
	    	{
	    		return $this->_redirect(url('Cust::Main/PersonCenter'));
	    	}
	    	else
	    	{
	    		return $this->_redirect(url('Default::Default/MobLogin'));
	    	}
    	}
    	else 
    	{
	    	if ($this->_context->isPOST())
	    	{
	    		try
	    		{
	    			$user = User::meta()->validateLogin($_POST['name'], $_POST['password']);
	    			$this->_app->changeCurrentUser($user->aclData(), $user['id']);
	    			if ($_POST['history'])
	    			{
	    				return $this->_redirect($_POST['history']);
	    			}
	    			else
	    			{
	    				return $this->_redirect(url('default::default/index'));
	    			}
	    		}
	    		catch (AclUser_UsernameNotFoundException $ex)
	    		{
	    			return $this->_redirect(url('default::default/login', array('msg'=>'username')));
	    		}
	    		catch (AclUser_WrongPasswordException $ex)
	    		{
	    			return $this->_redirect(url('default::default/login', array('msg'=>'password')));
	    		}
	    	}
    	}
    }
    
    function actionLogout()
    {
    	$this->_view['title'] = '用户登出';
    	$this->_app->cleanCurrentUser();
    	return $this->_redirect(url('default::default/login'));
    }
    
    function actionLoginExternal()
    {
    	$this->_view['title'] = '外部登录';
    	$headers = apache_request_headers();
    	$user = User::find('name=? and password=?', $headers['bss_username'], $headers['bss_password'])->asArray()->getOne();
    	if ($user)
    	{
    		$roles = Roles_Users_Mapping::getUserRolesId($user['id']);;
    		$this->_app->changeCurrentUser(array('id' => $user['id'], 'name' => $user['name']), $roles);
    		echo json_encode(array('ack' => SUCCESS, 'message' => '', 'session_id' => session_id()));
    	}
    	else
    	{
    		echo json_encode(array('ack' => FAILURE, 'message' => 'login failure', 'session_id' => ''));
    	}
    	exit;
    }
    
	function actionMobileLogin()
	{
		$this->_view['title'] = '手机登录';
		if ($this->_context->isPOST())
		{
			try
			{
				$user = User::meta()->validateLogin($_POST['name'], $_POST['password']);
				$roles = Roles_Users_Mapping::getUserRolesId($user['id']);
				$this->_app->changeCurrentUser($user->aclData(), $roles);
				if ($_POST['history'])
				{
					return $this->_redirect($_POST['history']);
				}
			}
			catch (AclUser_UsernameNotFoundException $ex)
			{
				return $this->_redirect(url('Default::Default/MobileLogin', array('msg' => 'username', 'history' => $_POST['history'])));
			}
			catch (AclUser_WrongPasswordException $ex)
			{
				return $this->_redirect(url('Default::Default/MobileLogin', array('msg' => 'password', 'history' => $_POST['history'])));
			}
		}
	}
}