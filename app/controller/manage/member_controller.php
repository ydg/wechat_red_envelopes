<?php
Class Controller_Manage_Member extends Controller_Abstract
{
	function actionCreate()
	{
		$this->_view['title'] = '创建会员';
	}
	
	function actionCreateSave()
	{
		$this->_view['title'] = '创建会员保存';
		set_time_limit(0);
		$type = isset($_POST['type'])?$_POST['type']:1;
		$codefile = $_FILES['file-2'];
		$time = time();
		if($codefile['size'] < 1024*4096)
		{
			$codefile['name'] = iconv('utf-8','gb2312',$codefile['name']);
			if($codefile['error'] == 0)
			{
				$data = Helper_BSS_Normal::uploadonlytxt($codefile, Q::ini('custom_system/base_url').'upload/txt');
				
				if($data['ack'] == SUCCESS)
				{
					$datastr = file_get_contents($data['data']);
					unlink($data['data']);
					$dataline = explode("\n", $datastr);
					
					if($type == '1')
					{
// 						$start_time = microtime(true);
						foreach ($dataline as $v)
						{
							$sql = '';
							$v = trim($v);
							if($v != '')
							{
								$code = explode(',', $v);
								$sql .= "insert into code (code,batch,status,time) values('".$code[0]."','".$time."',1,'".$time."');";
								QDB::getConn()->execute($sql);
							}
						}
// 						$end_time = microtime(true);
// 						echo ($end_time-$start_time).' s';
						return $this->_redirectMessage('上传成功', "已成功将上传的txt转换成码！正在返回...", url('Manage::Member/Create'),SUCCESS);
					}
					else
					{
						return $this->_redirectMessage('上传失败', "请选择拖法类型！", url('Manage::Member/Create'),FAILURE);
					}
				}
				else 
				{
					return $this->_redirectMessage('上传失败', $data['message'], url('Manage::Member/Create'),FAILURE);
				}
// 				unlink($data['data']);
			}
		}
		else 
		{
			return $this->_redirectMessage('上传失败', "上传的文件大于2024KB,请分包上传！", url('Manage::Member/Create'),FAILURE);
		}
	}
	
	function actionList()
	{
		$this->_view['title'] = '会员列表';
		$cp = isset($_GET['cp']) ? $_GET['cp'] : 1;
		$ps = isset($_GET['ps']) ? $_GET['ps'] : 20;
		$url_arr = Helper_BSS_Normal::buildCondition();
		$result = Member::search($url_arr,$cp, $ps);
		if ($result['ack'] == SUCCESS)
		{
			$page = new Helper_BSS_Pagec($result['pagination'], url('Manage::Member/List', $url_arr));
			$this->_view['page'] = $page->getPage();
            $this->_view['pagecount'] = $result['pagination'];
			$this->_view['member'] = $result['data'];
		}
	}

    function actionShow()
    {
        $this->_view['title'] = '显示详情';
        $id = isset($_GET['id']) ? $_GET['id'] : 0;
        $code = Code::find('id=?', $id)->asArray()->getOne();
        if ($code)
        {
            $this->_view['code'] =  $code['code'];
        }
        else
        {
            return $this->_redirectMessage('查询失败', '提交参数错误', url('Manage::Member/List'), FAILURE);
        }
    }
	
	function actionDetail()
	{
		$this->_view['title'] = '用户信息详情';
		$id = isset($_GET['id']) ? $_GET['id'] : 0;
		$product = Member::find('id=?', $id)->asArray()->getOne();
		if ($product)
		{
			$this->_view['product'] =  $product;
		}
		else
		{
			return $this->_redirectMessage('查询失败', '提交参数错误', url('Manage::Member/List'), FAILURE);
		}
	}
	
	function actionEdit()
	{
		$this->_view['title'] = '会员编辑';
		$id = isset($_GET['id']) ? $_GET['id'] : 0;
		$member = Member::find('id=?', $id)->asArray()->getOne();
		if ($product)
		{
			$this->_view['member'] = $member;
		}
		else
		{
			return $this->_redirectMessage('编辑失败', '没有该产品存在', url('Manage::Member/List'), FAILURE);
		}
	}
	
	function actionEditSave()
	{
		$this->_view['title'] = '会员编辑保存';
		if (isset($_POST)&&$_POST)
		{
			$member = $_POST;
			if (isset($_POST)&&$_FILES['upload']['name']&&$_FILES['upload']['tmp_name'])
			{
				$img = Helper_BSS_Normal::upload($_FILES['upload'], Q::ini('custom_system/base_url').'productimg');
			}
			if (isset($img)&&$img['ack'] == SUCCESS)
			{
				$product['file'] = $img['data'];
				$result = Member::edit($member);
				if ($result['ack'] == SUCCESS)
				{
					return $this->_redirectMessage('编辑成功', '正在返回..', url('Manage::Member/List'), $result['ack']);
				}
				else
				{
					return $this->_redirectMessage('编辑失败', '请检查提交的数据是否正确', url('Manage::Member/List'), FAILURE);
				}
			}
			else
			{
				$result = Member::edit($member);
				if ($result['ack'] == SUCCESS)
				{
					return $this->_redirectMessage('编辑成功', '正在返回..', url('Manage::Member/List'), $result['ack']);
				}
				else
				{
					return $this->_redirectMessage('编辑失败', '请检查提交的数据是否正确', url('Manage::Member/List'), FAILURE);
				}
				return $this->_redirectMessage('上传图片失败', '请检查图片的格式是否正确', url('Manage::Member/List'), FAILURE);
			}
		}
		else
		{
			return $this->_redirectMessage('编辑失败', '请检查提交的数据是否正确', url('Manage::Member/List'), FAILURE);
		}
	}
	
	function actionDelete()
	{
		$this->_view['title'] = '删除';
		$result = Member::meta()->deleteWhere(array('id'=>$_GET['id']));
		if (!$result)
		{
            echo 1;exit;
		}
		else
		{
            echo 0;exit;
		}
	}

    function actionStop()
    {
        $this->_view['title'] = '停用';
        //echo  $_GET['id'];exit;
        $result = Code::find(array('id'=>$_GET['id']))->getOne();
        if ($result)
        {
            $result->status = 2;
            $result->save();
            echo 1;exit;
        }
        else
        {
            echo 0;exit;
        }
    }
}