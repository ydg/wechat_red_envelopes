<?php
Class Controller_Manage_FeedBack extends Controller_Abstract
{
	function actionList()
	{
		$this->_view['title'] = '反馈列表';
		$cp = isset($_GET['cp']) ? $_GET['cp'] : 1;
		$ps = isset($_GET['ps']) ? $_GET['ps'] : 20;
		$url_arr = Helper_BSS_Normal::buildCondition();
		$result = FeedBack::search($url_arr,$cp, $ps);
		if ($result['ack'] == SUCCESS)
		{
			$page = new Helper_BSS_Pagec($result['pagination'], url('Manage::FeedBack/List', $url_arr));
			$this->_view['page'] = $page->getPage();
            $this->_view['pagecount'] = $result['pagination'];
			$this->_view['feedback'] = $result['data'];
		}
	}

    function actionShow()
    {
        $this->_view['title'] = '显示详情';
        $id = isset($_GET['id']) ? $_GET['id'] : 0;
        $code = FeedBack::find('id=?', $id)->asArray()->getOne();
        if ($code)
        {
            $this->_view['code'] =  $code['code'];
        }
        else
        {
            return $this->_redirectMessage('查询失败', '提交参数错误', url('Manage::FeedBack/List'), FAILURE);
        }
    }
	
	function actionDetail()
	{
		$this->_view['title'] = '反馈详细';
		$id = isset($_GET['id']) ? $_GET['id'] : 0;
		$product = FeedBack::find('id=?', $id)->asArray()->getOne();
		if ($product)
		{
			$this->_view['product'] =  $product;
		}
		else
		{
			return $this->_redirectMessage('查询失败', '提交参数错误', url('Manage::FeedBack/List'), FAILURE);
		}
	}
	
	function actionEdit()
	{
		$this->_view['title'] = '反馈编辑';
		$id = isset($_GET['id']) ? $_GET['id'] : 0;
		$product = FeedBack::find('id=?', $id)->asArray()->getOne();
		if ($product)
		{
			$this->_view['product'] = $product;
		}
		else
		{
			return $this->_redirectMessage('编辑失败', '没有该产品存在', url('Manage::FeedBack/List'), FAILURE);
		}
	}
	
	function actionEditSave()
	{
		$this->_view['title'] = '反馈编辑保存';
		if (isset($_POST)&&$_POST)
		{
			$product = $_POST;
			if (isset($_POST)&&$_FILES['upload']['name']&&$_FILES['upload']['tmp_name'])
			{
				$img = Helper_BSS_Normal::upload($_FILES['upload'], Q::ini('custom_system/base_url').'productimg');
			}
			if (isset($img)&&$img['ack'] == SUCCESS)
			{
				$product['file'] = $img['data'];
				$result = Product::edit($product);
				if ($result['ack'] == SUCCESS)
				{
					return $this->_redirectMessage('编辑成功', '正在返回..', url('Manage::FeedBack/List'), $result['ack']);
				}
				else
				{
					return $this->_redirectMessage('编辑失败', '请检查提交的数据是否正确', url('Manage::FeedBack/List'), FAILURE);
				}
			}
			else
			{
				$result = Product::edit($product);
				if ($result['ack'] == SUCCESS)
				{
					return $this->_redirectMessage('编辑成功', '正在返回..', url('Manage::FeedBack/List'), $result['ack']);
				}
				else
				{
					return $this->_redirectMessage('编辑失败', '请检查提交的数据是否正确', url('Manage::FeedBack/List'), FAILURE);
				}
				return $this->_redirectMessage('上传图片失败', '请检查图片的格式是否正确', url('Manage::FeedBack/List'), FAILURE);
			}
		}
		else
		{
			return $this->_redirectMessage('编辑失败', '请检查提交的数据是否正确', url('Manage::FeedBack/List'), FAILURE);
		}
	}
	
	function actionDelete()
	{
		$this->_view['title'] = '删除';
		$result = FeedBack::meta()->deleteWhere(array('id'=>$_GET['id']));
		if (!$result)
		{
            echo 1;exit;
		}
		else
		{
            echo 0;exit;
		}
	}

    function actionStop()
    {
        $this->_view['title'] = '停用';
        //echo  $_GET['id'];exit;
        $result = Code::find(array('id'=>$_GET['id']))->getOne();
        if ($result)
        {
            $result->status = 2;
            $result->save();
            echo 1;exit;
        }
        else
        {
            echo 0;exit;
        }
    }
}