<?php
Class Controller_Setting_Roles extends Controller_Abstract
{
	function actionCreate()
	{
		$this->_view['title'] = '新增';
		$roles_id = isset($_GET['id']) ? $_GET['id'] : '';
		$check_in_db = Roles::find('id=?', $roles_id)->asArray()->getOne();
		$this->_view['current_roles'] = $check_in_db;
		$this->_view['roles'] = Roles::getOffspringById();
	}
	
	function actionCreateSave()
	{
		$this->_view['title'] = '新增（保存）';
		$method = (isset($_POST['id'])) ? 'edit' : 'create';
		$result = Roles::$method();
		if ($result['ack'] == SUCCESS)
		{
			return $this->_redirectMessage($result['message'], '正在返回..', url('Setting::Roles/List'), $result['ack']);
		}
		else
		{
			return $this->_redirectMessage($result['message'], '正在返回..', $_SERVER['HTTP_REFERER'], $result['ack']);
		}
	}
	
	function actionDelete()
	{
		$this->_view['title'] = '删除';
		$roles = Roles::find('id=?', $_GET['id'])->asArray()->getOne();
		if ($roles)
		{
			$child_roles = Roles::find('parent_id=?', $roles['id'])->getAll();
			foreach ($child_roles as $cr)
			{
				$cr->parent_id = 0;
				$cr->save();
			}
			Roles::meta()->deleteWhere('id=?', $roles['id']);
			Roles_Users_Mapping::meta()->deleteWhere('roles_id=?', $roles['id']);
			Roles_Actions_Mapping::meta()->deleteWhere('roles_id=?', $roles['id']);
			OLM_Permissions::refreshACLFile();
			return $this->_redirectMessage('删除成功！', '正在返回..', $_SERVER['HTTP_REFERER'], SUCCESS);
		}
		else
		{
			return $this->_redirectMessage('删除失败！', '系统不存在该角色', $_SERVER['HTTP_REFERER'], FAILURE);
		}
	}
	
	function actionList()
	{
		$this->_view['title'] = '列表';
		$this->_view['roles'] = Roles::getOffspringById();
		$tree = Roles::getOffspringById();
		$child_tree_ids = array();
		if (isset($_GET['id'])) 
		{
			$child_tree = Roles::getOffspringById($_GET['id']);
			foreach ($child_tree as $ct)
			{
				$child_tree_ids[] = $ct['id']; 
			}
		}
		$this->_view['child_tree_ids'] = $child_tree_ids;
	}
	
	function actionTreeSave()
	{
		$this->_view['title'] = '树形结构（保存）';
		$result = Roles::updateMapping($_POST['roles_id'], $_POST['child_roles_ids']);
		if ($result['ack'] == SUCCESS)
		{
			return $this->_redirectMessage('分配成功', '正在返回..', $_SERVER['HTTP_REFERER'], $result['ack']);
		}
		else
		{
			return $this->_redirectMessage('分配失败', '正在返回..', $_SERVER['HTTP_REFERER'], $result['ack']);
		}
	}
	
	function actionAllotAction()
	{
		$this->_view['title'] = '分配action';
		$this->_view['roles'] = Roles::getOffspringById();
		$roles_id = isset($_GET['id']) ? $_GET['id'] : 0;
		$namespace = Permissions_Actions::find('namespace!=? and controller=? and action=?', '', '', '')->order('sort asc')->asArray()->getAll();
		foreach ($namespace as $kn => $n)
		{
			$controller = Permissions_Actions::find('namespace=? and controller!=? and action=?', $n['namespace'], '', '')->order('sort asc')->asArray()->getAll();
			foreach ($controller as $kc => $c)
			{
				$action = Permissions_Actions::find('permissions_actions.namespace=? and permissions_actions.controller=? and permissions_actions.action!=?', $n['namespace'], $c['controller'], '')
					->joinLeft('roles_actions_mapping', 'permissions_actions_id', 'permissions_actions.id=roles_actions_mapping.permissions_actions_id and roles_actions_mapping.roles_id=' . $roles_id)
					->asArray()->getAll();
				$controller[$kc]['children'] = $action;
			}
			$namespace[$kn]['children'] = $controller;
		}
		$this->_view['nca'] = $namespace;
	}
	
	function actionAllotActionSave()
	{
		$this->_view['title'] = '分配action（保存）';
		$roles = Roles::find('id=?', $_POST['roles_id'])->asArray()->getOne();
		if ($roles)
		{
			$nca_id = isset($_POST['nca_id']) ? $_POST['nca_id'] : array();
			Roles_Actions_Mapping::updateMapping(array($roles['id']), $nca_id);
			OLM_Permissions::refreshACLFile();
			return $this->_redirectMessage('分配成功', '返回', $_SERVER['HTTP_REFERER'], SUCCESS);
		}
		else
		{
			return $this->_redirectMessage('角色错误', '返回列表', $_SERVER['HTTP_REFERER'], FAILURE);
		}
	}
	
	function actionAllotUser()
	{
		$this->_view['title'] = '分配用户';
		$users = User::find('type!=1 and status=? and parent_id=?', Q::ini('custom_flag/user_status/on_duty/value'), CURRENT_USER_ID)->asArray()->getAll();
		$this->_view['users'] = $users;
		$check_roles = Roles::find('id=?', $_GET['id'])->asArray()->getOne();
		if ($check_roles['id'])
		{
			$roles_user_ids = array();
			$roles_user = Roles_Users_Mapping::find('roles_users_mapping.user_id=user.id and roles_users_mapping.roles_id=?', $check_roles['id'])
				->joinLeft('user', '', 'user.parent_id=' . CURRENT_USER_ID)
				->asArray()->getAll();
			foreach ($roles_user as $ru)
			{
				$roles_user_ids[] = $ru['user_id'];
			}
			$this->_view['roles_user_ids'] = $roles_user_ids;
			$this->_view['roles_user'] = $roles_user;
			$this->_view['roles'] = Roles::getOffspringById();
		}
		else
		{
			return $this->_redirectMessage('角色不存在', '正在返回..', $_SERVER['HTTP_REFERER'], FAILURE);
		}
	}
	
	function actionAllotUserSave()
	{
		$this->_view['title'] = '分配用户（保存）';
		$roles = Roles::find('id=?', $_POST['roles_id'])->asArray()->getOne();
		if ($roles)
		{
			$user_ids = isset($_POST['user_ids']) ? $_POST['user_ids'] : array();
			Roles_Users_Mapping::updateMapping($roles['id'], $user_ids, 'by_roles');
			return $this->_redirectMessage('分配成功', '正在返回..', $_SERVER['HTTP_REFERER'], SUCCESS);
		}
		
	}
}