<?php
Class Controller_Setting_Task extends Controller_Abstract
{
	function actionCreate()
	{
		$this->_view['title'] = '新增';
	}
	
	function actionCreateSave()
	{
		$this->_view['title'] = '新增（保存）';
		if ($this->_context->isPOST())
		{
			$task = array();
			$task['name'] = isset($_POST['name']) ? trim($_POST['name']) : '';
			$task['model'] = isset($_POST['model']) ? trim($_POST['model']) : '';
			$task['regex'] = isset($_POST['regex']) ? trim($_POST['regex']) : '';
			$task['seconds'] = isset($_POST['seconds']) ? trim($_POST['seconds']) : 0;
			$status = (isset($_POST['status'])) ? $_POST['status'] : 0;
			$result = Task::create($task, $status);
			if ($result['ack'] == FAILURE)
			{
				return $this->_redirectMessage('添加任务失败', $result['message'], $_SERVER['HTTP_REFERER'], $result['ack']);
			}
			else
			{
				return $this->_redirectMessage('添加任务成功', '正在返回..', url('Setting::Task/List'), $result['ack']);
			}
		}
	}
	
	function actionEdit()
	{
		$this->_view['title'] = '编辑';
		if ($_GET['id'])
		{
			$task = Task::find('id=?', $_GET['id'])->asArray()->getOne();
			if ($task['id'])
			{
				$this->_view['task'] = $task;
			}
		}
	}
	
	function actionEditSave()
	{
		$this->_view['title'] = '编辑（保存）';
		if ($this->_context->isPOST())
		{
			$task = Task::find('id=?', $_POST['task_id'])->getOne();
			if ($task['id'])
			{
				$task->name = isset($_POST['name']) ? trim($_POST['name']) : '';
				$task->model = isset($_POST['model']) ? trim($_POST['model']) : '';
				$task->regex = isset($_POST['regex']) ? trim($_POST['regex']) : '';
				$task->seconds = isset($_POST['seconds']) ? trim($_POST['seconds']) : 0;
				$task->status = $_POST['status'];
				$task->save();
				return $this->_redirectMessage('编辑成功', '正在返回..', url('Setting::Task/List'), SUCCESS);
			}
			else
			{
				return $this->_redirectMessage('编辑失败', '该订单不存在', $_SERVER['HTTP_REFERER'], FAILURE);
			}
		}
	}
	
	function actionList()
	{
		$this->_view['title'] = '列表';
		$cp = isset($_GET['cp']) ? $_GET['cp'] : 0;
		$ps = isset($_GET['ps']) ? $_GET['ps'] : 20;
		$url_arr = Helper_BSS_Normal::buildCondition();
		$result = Task::search($url_arr, $cp ,$ps);
		if ($result['ack'] == SUCCESS)
		{
			$page = new Helper_BSS_Page($result['pagination'], url('Setting::Task/List', $url_arr));
			$this->_view['page'] = $page->getPage();
			$clash = Task::checkCronRepeat(date('Y-m-d 00:00:00'), date('Y-m-d H:i:s', strtotime('+1 days')));
			$this->_view['clash'] = $clash['repeat_task'];
			$this->_view['task'] = $result['data'];
		}
	}
	
	function actionDetail()
	{
		$this->_view['title'] = '查看详细';
		if ($_GET['id'])
		{
			$task = Task::find('id=?', $_GET['id'])->asArray()->getOne();
			if ($task['id'])
			{
				$cp = isset($_GET['cp']) ? $_GET['cp'] : 0;
				$ps = isset($_GET['ps']) ? $_GET['ps'] : 10;
				$url_arr = Helper_BSS_Normal::buildCondition();
				$url_arr['task_id'] = $task['id'];
				$result = Task_Log::search($url_arr, $cp, $ps);
				if ($result['ack'] == SUCCESS)
				{
					$page = new Helper_BSS_Page($result['pagination'], url('setting::task/detail', $url_arr));
					$this->_view['page'] = $page->getPage();
					$this->_view['task_log'] = $result['data'];
				}
			}
		}
	}
}