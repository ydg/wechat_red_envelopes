<?php
class Controller_Setting_Permissions extends Controller_Abstract
{
	function actionRefreshNCA()
	{
		$this->_view['title'] = 'NCA刷新';
		Permissions_Actions::updateNCA();
		Roles_Actions_Mapping::SuperAdminAddMapping();
		Roles_Actions_Mapping::cleanMapping();
		Q::cleanCache(Q::ini('app_config/APPID').'_all_nca');
		return $this->_redirectMessage('更新成功', '返回NCA列表', url('Setting::Permissions/NCAList'), SUCCESS);
	}
	
	function actionNCAList()
	{
		$this->_view['title'] = 'NCA列表';
		if (empty($_GET['action_id']))
		{
			$nca = Permissions_Actions::find('controller=? and action=?', '', '')->order('sort asc')->asArray()->getAll();
		}
		else
		{
			$action = Permissions_Actions::find('id=?', $_GET['action_id'])->asArray()->getOne();
			if ($action['namespace'] &&  ! $action['controller'])
			{
				$nca = Permissions_Actions::find('namespace=? and controller!=? and action=?', $action['namespace'], '', '')->order('sort asc')->asArray()->getAll();
				$this->_view['parent_action_id'] = '';
			}
			if ($action['namespace'] && $action['controller'])
			{
				$nca = Permissions_Actions::find('namespace=? and controller=? and action!=?', $action['namespace'], $action['controller'], '')->order('sort asc')->asArray()->getAll();
				$parent_action = Permissions_Actions::find('namespace=? and controller=? and action=?', $action['namespace'], '', '')->asArray()->getOne();
				$this->_view['parent_action_id'] = $parent_action['id'];
			}
		}
		$this->_view['nca'] = $nca;
	}
	
	function actionNCASave()
	{
		$this->_view['title'] = 'NCA编辑（保存）';
		foreach ($_POST['roles_access_control'] as $key => $val)
		{
			$action = Permissions_Actions::find('id=?', $key)->getOne();
			$action->roles_access_control = $_POST['roles_access_control'][$key];
			$action->name = $_POST['name'][$key];
			$action->desc = $_POST['desc'][$key];
			$action->display_flag = $_POST['display_flag'][$key];
			$action->sort = $_POST['sort'][$key];
			$action->param = isset($_POST['param'][$key]) ? $_POST['param'][$key] : '';
			$action->save();
		}
		Q::cleanCache(Q::ini('app_config/APPID').'_all_nca');
		return $this->_redirectMessage('保存成功', '返回列表', $_SERVER['HTTP_REFERER'], SUCCESS);
	}
	
	function actionRefreshACLFile()
	{
		$this->_view['title'] = 'ACL刷新';
		OLM_Permissions::refreshACLFile();
		return $this->_redirectMessage('ACL刷新成功', '返回', $_SERVER['HTTP_REFERER'], SUCCESS);
	}
	
	function actionRefreshSession()
	{
		$this->_view['title'] = '刷新session';
		$roles = implode(',', Q::normalize(Roles_Users_Mapping::getUserRolesId(CURRENT_USER_ID)));
        session_start();
        $_SESSION[Q::ini('acl_session_key')]['roles'] = $roles;
        session_write_close();
        return $this->_redirectMessage('权限已刷新', '正在返回..', $_SERVER['HTTP_REFERER'], SUCCESS);
	}
}