<?php
class Controller_Setting_Basic extends Controller_Abstract
{
	function actionIndex()
	{
		$this->_view['title'] = '测试';
	}
	
	function actionNewsList()
	{
		$this->_view['title'] = '公告列表';
		$cp = isset($_GET['cp']) ? $_GET['cp'] : 0;
		$ps = isset($_GET['ps']) ? $_GET['ps'] : 10;
		$url_arr = Helper_BSS_Normal::buildCondition();
		$select = News::find()->joinLeft('user', 'user.name as user_name', 'user.id=news.user_id')->limitPage($cp, $ps);
		$news = $select->order('create_time desc')->asArray()->getAll();
		foreach ($news as $k => $n)
		{
			$news[$k]['unread'] = News_Unread::find('news_id=? and user_id=?', $n['id'], CURRENT_USER_ID)->asArray()->getOne();
		}
		$pagination = $select->getPagination();
		$page = new Helper_BSS_Page($pagination, url('Setting::Basic/NewsList'));
		$this->_view['page'] = $page->getPage();
		$this->_view['news'] = $news;
	}
	
	function actionNewsCreate()
	{
		$this->_view['title'] = '创建公告';
	}
	
	function actionNewsCreateSave()
	{
		$this->_view['title'] = '创建公告（保存）';
		if ($this->_context->isPOST())
		{
			$news = array();
			$news['title'] = trim($_POST['title']);
			$news['content'] = trim($_POST['content']);
			if ($news['title'] && $news['content'])
			{
				$result = News::create($news);
				if ($result['ack'] == SUCCESS)
				{
					return $this->_redirectMessage('发布成功', '正在返回..', url('Setting::Basic/NewsList'), $result['ack']);
				}
				else
				{
					return $this->_redirectMessage('发布失败', $result['message'], $_SERVER['HTTP_REFERER'], $result['ack']);
				}
			}
			else
			{
				return $this->_redirectMessage('发布失败', '标题或内容不能为空！', $_SERVER['HTTP_REFERER'], $result['ack']);
			}
		}
	}
	
	function actionNewsDetail()
	{
		$this->_view['title'] = '公告详细';
		if (isset($_REQUEST['news_id']))
		{
			$news = News_Unread::find('news_id=? and user_id=?', $_REQUEST['news_id'], CURRENT_USER_ID)->asArray()->getOne();
			if ($news)
			{
				News_Unread::meta()->deleteWhere('news_id=? and user_id=?', $news['news_id'], CURRENT_USER_ID);
				echo 1;exit;
			}
		}
		echo 0;exit;
	}
	
	function actionCompanyInfo()
	{
		$this->_view['title'] = '企业信息';
		$info = Setting::find()->asArray()->getOne();
		$this->_view['info'] = $info;
	}
	
	function actionCompanyInfoSave()
	{
		$this->_view['title'] = '企业信息保存';
		if ($_POST)
		{
			$info = Setting::find('id=?', $_POST['id'])->getOne();
			if ($info['id'])
			{
				$info->company_name = isset($_POST['company_name']) ? trim($_POST['company_name']) : '';
				$info->company_url = isset($_POST['company_url']) ? trim($_POST['company_url']) : '';
				$info->company_contact = isset($_POST['company_contact']) ? trim($_POST['company_contact']) : '';
				$info->freeze_reply = isset($_POST['freeze_reply']) ? trim($_POST['freeze_reply']) : '';
				$info->create_time = CURRENT_DATETIME;
				$info->save();
				return $this->_redirectMessage('修改成功', '正在返回', $_SERVER['HTTP_REFERER'], SUCCESS);
			}
			else
			{
				$return = Setting::create($_POST);
				if ($return['ack'] == SUCCESS)
				{
					return $this->_redirectMessage('添加成功', '正在返回', $_SERVER['HTTP_REFERER'], SUCCESS);
				}
				else
				{
					return $this->_redirectMessage('添加失败', '正在返回', $_SERVER['HTTP_REFERER'], FAILURE);
				}
			}
		}
		else
		{
			return $this->_redirectMessage('添加或修改失败', '正在返回', $_SERVER['HTTP_REFERER'], FAILURE);
		}
	}
	
	function actionDeleteCache()
	{
		$this->_view['title'] = '清空缓存';
		$cache_path = Q::ini('app_config/CONFIG_CACHE_SETTINGS/QCache_File/cache_dir');
		$handle = opendir($cache_path); 
		if ($handle)
		{
			while (($file = readdir($handle)) != false)
			{
				if ($file != '.' && $file != '..' && $file != '.svn')
				{
					if (is_file($cache_path . '/' . $file) && substr($file, strrpos($file, '.') + 1) == 'php')
					{
						unlink($cache_path . '/' . $file);
					}
				}
			}
			closedir($handle);
			clearstatcache();
			return $this->_redirectMessage('清空缓存成功', '正在返回', $_SERVER['HTTP_REFERER'], SUCCESS);
		}
	}
}