<?php
Class Controller_Setting_User extends Controller_Abstract
{
	function actionPasswordChange()
	{
		$this->_view['title'] = '密码修改';
	}
	
	function actionPasswordChangeSave()
	{
		$this->_view['title'] = '密码修改（保存）';
		if ($this->_context->isPOST())
		{
			$user = $this->_app->currentUser();
			if (empty($_POST['old_password']))
			{
				return $this->_redirectMessage('旧密码为空！', '请输入您的旧密码！', url('Setting::User/PasswordChange'), FAILURE);
			}
			if (empty($_POST['new_password']))
			{
				return $this->_redirectMessage('新密码为空！', '请输入您的新密码！', url('Setting::User/PasswordChange'), FAILURE);
			}
			
			$result = User::passwordChange($_POST['old_password'], $_POST['new_password']);
			if ($result['ack'] == SUCCESS)
			{
				QDB::getConn()->execute('update customer set c_pwd=\''.$_POST['new_password'].'\' where c_no=\''.CURRENT_USER_NAME.'\'');
				return $this->_redirectMessage('密码修改成功！', '更新用户密码成功。', url('Setting::User/PasswordChange'), $result['ack']);
			}
			else
			{
				return $this->_redirectMessage('密码错误！', $result['message'], url('Setting::User/PasswordChange'), $result['ack']);
			}
		}
		else
		{
			return $this->_redirect(url('Setting::User/PasswordChange'));
		}
	}
	
	function actionList()
	{
		$this->_view['title'] = '列表';
		$cp = isset($_GET['cp']) ? $_GET['cp'] : 1;
		$ps = isset($_GET['ps']) ? $_GET['ps'] : 20;
		$url_arr = Helper_BSS_Normal::buildCondition();
		$url_arr['status'][Q::ini('custom_flag/user_status/on_duty/value')] = Q::ini('custom_flag/user_status/on_duty/value');
		$result = User::search($cp, $ps, $url_arr);
		if ($result['ack'] == SUCCESS)
		{
			$page = new Helper_BSS_Page($result['pagination'], url('Setting::User/List', $url_arr));
			$this->_view['page'] = $page->getPage();
			$this->_view['user'] = $result['data'];
		}
	}
	
	function actionTree()
	{
		$this->_view['title'] = '树形列表';
		$child_tree_ids = array();
		$tree = User::getOffspringById(CURRENT_USER_ID);
		if (isset($_GET['id'])) 
		{
			$child_tree = User::getOffspringById($_GET['id']);
			foreach ($child_tree as $ct)
			{
				$child_tree_ids[] = $ct['id']; 
			}
		}
		$this->_view['child_tree_ids'] = $child_tree_ids;
		$this->_view['tree'] = $tree;
	}
	
	function actionTreeSave()
	{
		$this->_view['title'] = '树形分配（保存）';
		$result = User::updateMapping($_POST['user_id'], $_POST['child_user_ids']);
		if ($result['ack'] == SUCCESS)
		{
			return $this->_redirectMessage('分配成功', '正在返回...', $_SERVER['HTTP_REFERER'], $result['ack']);
		}
		else
		{
			return $this->_redirectMessage('分配失败', $result['message'], $_SERVER['HTTP_REFERER'], $result['ack']);
		}
	}
	
	function actionPasswordReset()
	{
		$this->_view['title'] = '密码重置';
		$user = User::find('id=?', intval($this->_context->user_id))->asArray()->getOne();
		if ($user['parent_id'] != CURRENT_USER_ID && $user['id'] != CURRENT_USER_ID)
		{
			return $this->_redirectMessage('没有重置该用户密码权限!', '正在返回..', $_SERVER['HTTP_REFERER'], FAILURE);
		}
		$this->_view['user'] = $user;
	}
	
	function actionPasswordResetSave()
	{
		$this->_view['title'] = '密码重置（保存）';
		if ($this->_context->isPOST())
		{
			$result = User::passwordReset($_POST['user_id'], $_POST['password']);
			if ($result['ack'] == SUCCESS)
			{
				return $this->_redirectMessage('修改成功！', '正在返回..', url('Setting::User/List'), $result['ack']);
			}
			else
			{
				return $this->_redirectMessage('修改失败！', $result['message'], url('Setting::User/list'), $result['ack']);
			}
		}
		else 
		{
			return $this->_redirect(url('Setting::User/List'));
		}
		
	}
	
	function actionHeadUser()
	{
		$this->_view['title'] = '总部用户';
		$cp = isset($_GET['cp']) ? $_GET['cp'] : 1;
		$ps = isset($_GET['ps']) ? $_GET['ps'] : 20;
		$url_arr = Helper_BSS_Normal::buildCondition();
		$url_arr['status'][Q::ini('custom_flag/user_status/on_duty/value')] = Q::ini('custom_flag/user_status/on_duty/value');
		$result = User::headsearch($cp, $ps, $url_arr);
		if ($result['ack'] == SUCCESS)
		{
			$page = new Helper_BSS_Page($result['pagination'], url('Setting::User/HeadUser', $url_arr));
			$this->_view['page'] = $page->getPage();
			$this->_view['user'] = $result['data'];
		}
	}
	
	function actionHeadDelete()
	{
		$this->_view['title'] = '总部用户删除';
		$result = QDB::getConn()->execute("delete from user where id=" . $_GET['user_id']);
		if ($result)
		{			
			return $this->_redirectMessage('删除成功', '返回列表', $_SERVER['HTTP_REFERER'], SUCCESS);
		}
		else
		{
			return $this->_redirectMessage('删除失败：' . $result['message'], '返回列表', $_SERVER['HTTP_REFERER'], FAILURE);
		}
	}
	
	function actionSetOffDutyCleanMapping()
	{
		$this->_view['title'] = '设置离职&清空权限';
		$result = User::cleanMapping($_GET['user_id']);
		if ($result['ack'] == SUCCESS)
		{			
			return $this->_redirectMessage('操作成功', '返回列表', $_SERVER['HTTP_REFERER'], $result['ack']);
		}
		else
		{
			return $this->_redirectMessage('操作失败：' . $result['message'], '返回列表', $_SERVER['HTTP_REFERER'], $result['ack']);
		}
	}
	
	function actionCreate()
	{
		$this->_view['title'] = '创建';
	}
	
	function actionCreateSave()
	{
		$this->_view['title'] = '创建（保存）';
		if ($this->_context->isPOST())
		{
			$user_exists = User::create(trim($_POST['username']), trim($_POST['password']));
			if ($user_exists['ack'] == FAILURE)
			{
				return $this->_redirectMessage('添加用户失败', '用户名已存在', url('Setting::User/Create'), $user_exists['ack']);
			}
			else
			{
				return $this->_redirectMessage('添加用户成功', '返回用户列表', url('Setting::User/list'), $user_exists['ack']);
			}
		}
		else
		{
			return $this->_redirect(url('Setting::User/Create'));
		}
	}

	function actionBind()
	{
		$this->_view['title'] = '分配权限';
		$check_user = User::find('id=?', $_GET['user_id'])->asArray()->getOne();
		if ($check_user['id'])
		{
			$roles = Roles::find()->asArray()->getAll();
			$arr = $current = array();
			foreach ($roles as $r)
			{
				$arr[$r['id']] = array(
					'id' => $r['id'],
					'desc' => $r['desc'],
					'pid' => $r['parent_id']
				);
				$current[] = $r['id'];
			}
			$this->_view['roles'] = Helper_BSS_Array::getTree($arr);
//			$shop = Shop::getActiveShop();
			if (CURRENT_USER_ROLES != 1)
			{
//				$shop = Shop::find('shop.id=shop_user_mapping.shop_id')
//					->joinLeft('shop_user_mapping', '', 'shop_user_mapping.user_id=' . CURRENT_USER_ID)
//					->asArray()->getAll();				
				$current = QDB::getConn()->execute('select roles_id from roles_users_mapping where user_id=' . CURRENT_USER_ID)->fetchCol(); 
			}
//			$shop_group = array();
//			foreach ($shop as $s)
//			{
//				$shop_group[$s['platform']][] = $s;
//			}
//			$this->_view['shop_group'] = $shop_group;
			$this->_view['current'] = $current;
		  	$check_user['roles_ids'] = QDB::getConn()->execute('select roles_id from roles_users_mapping where user_id=' . $check_user['id'])->fetchCol(); 
//		  	$check_user['shop_ids'] = QDB::getConn()->execute('select shop_id from shop_user_mapping where user_id=' . $check_user['id'])->fetchCol();
			$this->_view['mapping'] = $check_user;
			$this->_view['user'] = User::find('parent_id=?', CURRENT_USER_ID)->asArray()->getAll();
		}
		else
		{
			return $this->_redirectMessage('分配用户不存在', '正在返回..', $_SERVER['HTTP_REFERER'], FAILURE);
		}
	}
	
	function actionBindSave()
	{
		$this->_view['title'] = '分配权限（保存）';
		$check_user = User::find('id=? and status=?', $_POST['user_id'], Q::ini('custom_flag/user_status/on_duty/value'))->asArray()->getOne();
		if ($check_user)
		{
			$roles_ids = isset($_POST['roles_ids']) ? $_POST['roles_ids'] : array();
//			$shop_ids = isset($_POST['shop_ids']) ? $_POST['shop_ids'] : array();
			Roles_Users_Mapping::updateMapping($roles_ids, $check_user['id']);
//			Shop_User_Mapping::updateMapping($shop_ids, $check_user['id']);
			OLM_Permissions::refreshACLFile();
			return $this->_redirectMessage('分配权限成功', '正在返回..', $_SERVER['HTTP_REFERER'], SUCCESS);
		}
		else
		{
			return $this->_redirectMessage('分配权限失败', '正在返回..', $_SERVER['HTTP_REFERER'], FAILURE);
		}
	}
	
	function actionCopy()
	{
		$this->_view['title'] = '复制权限';
		$user = User::find('parent_id=?', CURRENT_USER_ID)->asArray()->getAll();
		$this->_view['user'] = $user;
	}
	
	function actionCopySave()
	{
		$this->_view['title'] = '复制权限（保存）';
		$result = Roles_Users_Mapping::copy($_POST['user_id'], $_POST['copy_user_ids']);
		if ($result['ack'] == SUCCESS)
		{			
			return $this->_redirectMessage('复制成功', '正在返回..', $_SERVER['HTTP_REFERER'], $result['ack']);
		}
		else
		{
			return $this->_redirectMessage('复制失败：' . $result['message'], '正在返回..', $_SERVER['HTTP_REFERER'], $result['ack']);
		}
	}
}