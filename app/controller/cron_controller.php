<?php
class Controller_Cron extends Controller_Abstract
{
	function actionExecute()
	{
		$this->_view['title'] = '执行定期任务';
		if (isset($_GET['key']) && $_GET['key'] == Q::ini('custom_system/cron_key'))
		{
			$task = Task::find('status=?', Q::ini('custom_flag/task_status/opened/value'))->asArray()->getAll();
			$time = date('Y-m-d H:i w');
			foreach ($task as $k => $t)
			{
				if (preg_match($t['regex'], $time))
				{
					$task_log_result = Task_Log::create($t['id']);
					if ($task_log_result['ack'] == SUCCESS)
					{
						set_time_limit($t['seconds']);
						$result = OLM_Cron::$t['model']();
						QDB::getConn()->execute('update task_log set ack=' . Q::ini('custom_flag/task_log_ack/' . strtolower($result['ack']) . '/value') . ',message=\'' . $result['message'] . '\', execute_time=\'' . sprintf('%.4f', microtime(true) - $GLOBALS['g_boot_time']) . '\' where id=' . $task_log_result['task_log_id']);
					}
				}
			}
		}
	}
}