<?php
Class Controller_DataManage_Product extends Controller_Abstract
{
	function actionCreate()
	{
		$this->_view['title'] = '新建';
		$product_no = Product::find()->order('id desc')->asArray()->getOne();
		$set_no='';
		if(isset($product_no)&&$product_no['p_no'])
		{
			$set_no = str_pad($product_no['p_no']+1,6,"0",STR_PAD_LEFT);
		}
		else
		{
			$set_no = '000001';//如果是代理发货就直接使用倒过来的方式，防止订单好重复
		}
		$this->_view['product_no'] = $set_no;
	}
	
	function actionCreateSave()
	{
		$this->_view['title'] = '新建保存';
		$img = Helper_BSS_Normal::upload($_FILES['upload'], Q::ini('custom_system/base_url').'productimg');
		if ($img['ack'] == SUCCESS)
		{
			$product = $_POST;
			$product['file'] = $img['data'];
			$result = Product::create($product);
			if ($result['ack'] == SUCCESS)
			{
				return $this->_redirectMessage('添加成功', '正在返回..', url('DataManage::Product/List'), $result['ack']);
			}
			else
			{
				return $this->_redirectMessage('添加失败', '请检查提交的数据是否正确', url('DataManage::Product/List'), FAILURE);
			}
		}
		else
		{
			return $this->_redirectMessage('上传图片失败', '请检查图片的格式是否正确', url('DataManage::Product/List'), FAILURE);
		}
	}
	
	function actionList()
	{
		$this->_view['title'] = '产品列表';
		$cp = isset($_GET['cp']) ? $_GET['cp'] : 1;
		$ps = isset($_GET['ps']) ? $_GET['ps'] : 20;
		$url_arr = Helper_BSS_Normal::buildCondition();
		$result = Product::search($cp, $ps, $url_arr);
		if ($result['ack'] == SUCCESS)
		{
			$page = new Helper_BSS_Page($result['pagination'], url('DataManage::Product/List', $url_arr));
			$this->_view['page'] = $page->getPage();
			$this->_view['product'] = $result['data'];
		}
	}
	
	function actionDetail()
	{
		$this->_view['title'] = '产品详细';
		$id = isset($_GET['id']) ? $_GET['id'] : 0;
		$product = Product::find('id=?', $id)->asArray()->getOne();
		if ($product)
		{
			$this->_view['product'] =  $product;
		}
		else
		{
			return $this->_redirectMessage('查询失败', '提交参数错误', url('DataManage::Product/List'), FAILURE);
		}
	}
	
	function actionEdit()
	{
		$this->_view['title'] = '产品编辑';
		$id = isset($_GET['id']) ? $_GET['id'] : 0;
		$product = Product::find('id=?', $id)->asArray()->getOne();
		if ($product)
		{
			$this->_view['product'] = $product;
		}
		else
		{
			return $this->_redirectMessage('编辑失败', '没有该产品存在', url('DataManage::Product/List'), FAILURE);
		}
	}
	
	function actionEditSave()
	{
		$this->_view['title'] = '编辑保存';
		if (isset($_POST)&&$_POST)
		{
			$product = $_POST;
			if (isset($_POST)&&$_FILES['upload']['name']&&$_FILES['upload']['tmp_name'])
			{
				$img = Helper_BSS_Normal::upload($_FILES['upload'], Q::ini('custom_system/base_url').'productimg');
			}
			if (isset($img)&&$img['ack'] == SUCCESS)
			{
				$product['file'] = $img['data'];
				$result = Product::edit($product);
				if ($result['ack'] == SUCCESS)
				{
					return $this->_redirectMessage('编辑成功', '正在返回..', url('DataManage::Product/List'), $result['ack']);
				}
				else
				{
					return $this->_redirectMessage('编辑失败', '请检查提交的数据是否正确', url('DataManage::Product/List'), FAILURE);
				}
			}
			else
			{
				$result = Product::edit($product);
				if ($result['ack'] == SUCCESS)
				{
					return $this->_redirectMessage('编辑成功', '正在返回..', url('DataManage::Product/List'), $result['ack']);
				}
				else
				{
					return $this->_redirectMessage('编辑失败', '请检查提交的数据是否正确', url('DataManage::Product/List'), FAILURE);
				}
				return $this->_redirectMessage('上传图片失败', '请检查图片的格式是否正确', url('DataManage::Product/List'), FAILURE);
			}
		}
		else
		{
			return $this->_redirectMessage('编辑失败', '请检查提交的数据是否正确', url('DataManage::Product/List'), FAILURE);
		}
	}
	
	function actionDelete()
	{
		$this->_view['title'] = '删除产品';
		$result = Product::meta()->deleteWhere(array('id'=>$_GET['id']));
		if (!$result)
		{
			return $this->_redirectMessage('删除成功', '正在返回..', url('DataManage::Agent/List'), SUCCESS);
		}
		else
		{
			return $this->_redirectMessage('删除失败', '请检查提交的数据是否存在', url('DataManage::Agent/List'), FAILURE);
		}
	}
}