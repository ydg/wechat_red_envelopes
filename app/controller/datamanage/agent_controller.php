<?php
Class Controller_DataManage_Agent extends Controller_Abstract
{
	function actionCreate()
	{
		$this->_view['title'] = '新建(代理资料)';
		//$rank = Rank::find()->asArray()->getAll();
		//$bank = Bank::find()->asArray()->getAll();
		//$provinces = Provinces::find()->asArray()->getAll();
		//$cities = Cities::find()->asArray()->getAll();
		$cust = Customer::find()->asArray()->getAll();
		$this->_view['rank'] =  $rank;
		$this->_view['bank'] =  $bank;
		$this->_view['provinces'] =  $provinces;
		$this->_view['cities'] =  $cities;
		$this->_view['cust'] =  $cust;
	}
	
	function actionProvince()
	{
		$this->_view['title'] = '新建(代理资料)';
		$province = $_REQUEST["province"];
		if(isset($province))
		{ 
			$cities = Cities::find('provinceid=?',$province)->asArray()->getAll();
		    echo json_encode($cities); exit;
		} 
	}
	
	function actionCreateSave()
	{
		$this->_view['title'] = '代理资料保存';
		$customer = $_POST;
		//dump($customer);exit;
		$result = Customer::create($customer);
		if ($result['ack'] == SUCCESS)
		{
			$user_exists = User::create(trim($customer['number']), trim($customer['password']));
			if ($user_exists['ack'] == FAILURE)
			{
				return $this->_redirectMessage('添加用户失败', '用户名已存在', url('DataManage::Agent/List'), $user_exists['ack']);
			}
			else
			{
				return $this->_redirectMessage('添加成功', '正在返回..', url('DataManage::Agent/List'), $result['ack']);
			}
		}
		else
		{
			return $this->_redirectMessage('添加失败', '请检查提交数据是否正确', url('DataManage::Agent/List'), FAILURE);
		}
	}
	
	function actionEdit()
	{
		$this->_view['title'] = '代理资料编辑';
		$rank = Rank::find()->asArray()->getAll();
		$bank = Bank::find()->asArray()->getAll();
		$provinces = Provinces::find()->asArray()->getAll();
		$cities = Cities::find()->asArray()->getAll();
		$cust = Customer::find()->asArray()->getAll();
		$this->_view['rank'] =  $rank;
		$this->_view['bank'] =  $bank;
		$this->_view['provinces'] =  $provinces;
		$this->_view['cities'] =  $cities;
		$this->_view['cust'] =  $cust;
		$id = isset($_GET['id']) ? $_GET['id'] : 0;
		$customer = Customer::find('id=?', $id)->asArray()->getOne();
		if ($customer)
		{
			$this->_view['customer'] = $customer;
		}
		else
		{
			return $this->_redirectMessage('编辑失败', '没有代理存在', url('DataManage::Agent/List'), FAILURE);
		}
	}
	
	function actionEditSave()
	{
		$this->_view['title'] = '代理资料编辑（保存）'; 
		$customer = $_POST;
		$c_pwd = Customer::find('id=?',$customer['id'])->asArray()->getOne();
		$result = Customer::edit($customer);
		if ($result['ack'] == SUCCESS)
		{
			if (trim($customer['password']) != $c_pwd['c_pwd'])
			{
//				$user = User::find('name=? and password=?', trim($customer['user_name']), md5($c_pwd['c_pwd']))->getOne();
//				if ($user['id'])
//				{
//					$user->password = $new_password;
//					$user->save();
//					return array('ack' => SUCCESS, 'message' => '');
//				}
//				else
//				{
//					return array('ack' => FAILURE, 'message' => '用户不存在..');
//				}
				QDB::getConn()->execute('update user set password=\''.md5(trim($customer['password'])).'\' where name=\''.trim($customer['user_name']).'\'');
			}
			return $this->_redirectMessage('编辑成功', '正在返回..', url('DataManage::Agent/List'), $result['ack']);
		}
		else
		{
			return $this->_redirectMessage('编辑失败', '请检查提交的数据是否正确', url('DataManage::Agentt/List'), FAILURE);
		}
//		$user = User::find('id=?', $user_id)->getOne();
//		$user->password = $new_password;
//		$user->save();
	}
	
	function actionDetail()
	{
		$this->_view['title'] = '代理资料详细'; 
		$rank = Rank::find()->asArray()->getAll();
		$bank = Bank::find()->asArray()->getAll();
		$provinces = Provinces::find()->asArray()->getAll();
		$cust = Customer::find()->asArray()->getAll();
		$this->_view['rank'] =  $rank;
		$this->_view['bank'] =  $bank;
		$this->_view['provinces'] =  $provinces;
		$this->_view['cust'] =  $cust;
		$id = isset($_GET['id']) ? $_GET['id'] : 0;
		$customer = Customer::find('id=?', $id)->asArray()->getOne();
		if ($customer)
		{
			$this->_view['customer'] = $customer;
		}
		else
		{
			return $this->_redirectMessage('查看失败', '没有该代理存在', url('DataManage::Agent/List'), FAILURE);
		}
	}
	
	function actionList()
	{
		$this->_view['title'] = '代理资料列表';
		$cp = isset($_GET['cp']) ? $_GET['cp'] : 1;
		$ps = isset($_GET['ps']) ? $_GET['ps'] : 20;
		$url_arr = Helper_BSS_Normal::buildCondition();
		$result = Customer::search($cp, $ps, $url_arr);
		if ($result['ack'] == SUCCESS)
		{
			$page = new Helper_BSS_Page($result['pagination'], url('DataManage::Agent/List', $url_arr));
			$this->_view['page'] = $page->getPage();
			$this->_view['agent'] = $result['data'];
			$rank = Rank::find()->asArray()->getAll();
			$provinces = Provinces::find()->asArray()->getAll();
			$cities = Cities::find()->asArray()->getAll();
			$this->_view['rank'] =  $rank;
			$this->_view['provinces'] =  $provinces;
			$this->_view['cities'] =  $cities;
		}
	}
	
	function actionDelete()
	{
		$this->_view['title'] = '删除代理资料';
		$cust = Customer::find('id=?',$_GET['id'])->asArray()->getOne();
		$result = Customer::meta()->deleteWhere(array('id'=>$_GET['id']));
		$user = User::meta()->deleteWhere(array('name'=>$cust['c_no']));
		if (!$result)
		{
			if (!$user)
			{
				return $this->_redirectMessage('删除成功', '正在返回..', url('DataManage::Agent/List'), SUCCESS);
			}
			else 
			{
				return $this->_redirectMessage('删除失败', '请检查提交的数据是否存在', url('DataManage::Agent/List'), FAILURE);
			}
		}
		else
		{
			return $this->_redirectMessage('删除失败', '请检查提交的数据是否存在', url('DataManage::Agent/List'), FAILURE);
		}
	}
	
	function actionAudit()
	{
		$this->_view['title'] = '审核';
		$audit = Customer::find('id=?', $_GET['id'])->getOne();
		if (!$audit['id'])
		{
			return $this->_redirectMessage('审核失败', '该代理不存在，请检查提交数据的正确性', url('DataManage::Agent/List'), FAILURE);
		}
		else
		{
			$audit->c_status = '已审核';
			$audit->save();
			return $this->_redirectMessage('审核成功', '正在返回..', url('DataManage::Agent/List'), FAILURE);
		}
	}
}