<?php
class Helper_BSS_XLS
{
	static function exportXls($data, $output_filename = 'export.xls')
	{
		$data = iconv("UTF-8", "UTF-16LE", $data);
		$data = "\xFF\xFE" . $data;
		header( "Content-Type: application/vnd.ms-excel; charset=u");
		header( "Content-Type: application/octet-stream" );
		header( "Content-Type: application/download" );
		header( 'Content-Disposition:inline; filename="' . $output_filename . '"' );
		header( "Content-Transfer-Encoding: binary" );
		header( "Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header( "Pragma: no-cache");
		echo $data;
		exit;
	}
	
	static function explorerdir($sDir)
	{
	    static $aTempArr=array();
	    $dp=opendir($sDir);
	    while ($sFileName = readdir($dp)){
	        
	        if ($sFileName !='.' && $sFileName !='..'){  
	            
	            $sPath=$sDir."/" . $sFileName; 
	            if ( is_dir($sPath)){
	                
	                explorerdir($sPath);   
	            } else  {
	                
	//                $filetime=date("Y-m-d H:i:s",filectime("$path")); 
	//                $fp=$path.",".$filetime;
	                $fp=$sPath;
	                $aTempArr[]=$fp;
	            }
	       }
	    }
	    closedir($dp);
	    return $aTempArr;
	}
	
	static function downloads($name)
	{
        $file_dir = Q::ini('custom_system/base_url')."dbf/";    
       
        if (!file_exists($file_dir.$name)){
            header("Content-type: text/html; charset=GB2312");
            echo "File not found!";
            exit; 
        }
        else 
        {
            $file = fopen($file_dir.$name,"r");  
            @Header("Content-type: application/octet-stream");
            @Header("Accept-Ranges: bytes");
            @Header("Accept-Length: ".filesize($file_dir . $name));
            @Header("Content-Disposition: attachment; filename=".$name);
            echo fread($file, filesize($file_dir.$name));
            fclose($file);
        }
    }
	
	static function xlsFileToArray($filename, $file_id)
	{
		require_once(Q::ini('custom_system/lib_dir') . 'PHPExcel.php');
		$reader = PHPExcel_IOFactory::createReader('Excel5');
		$phpexcel = $reader->load($filename);
		$worksheet = $phpexcel->getActiveSheet();
		$highest_row = $worksheet->getHighestRow();
		$highest_column = PHPExcel_Cell::columnIndexFromString($worksheet->getHighestColumn());
		$key = array();
		for($j = 0; $j < $highest_column; $j++)
		{
			$key[] = (string)$worksheet->getCellByColumnAndRow($j, 1)->getValue();
		}
		$data = array();
		for ($i = 2; $i <= $highest_row; $i++)
		{
			$row = array();
			for ($j = 0; $j < $highest_column; $j++)
			{
				$row[$key[$j]] = (string)$worksheet->getCellByColumnAndRow($j, $i)->getValue();
			}
			$row['file_id'] = $file_id;
			array_push($data, $row);
		}
		return $data;
	}
	
	static function ebayProductInfoExport($product_info)
	{
		include Q::ini('custom_system/lib_dir') . 'PHPExcel.php';
		$phpexcel = new PHPExcel();
		$phpexcel->getProperties()->setCreator(Q::ini('custom_system/system_name') . Q::ini('custom_system/version'));
		$phpexcel->getProperties()->setLastModifiedBy(Q::ini('custom_system/system_name') . Q::ini('custom_system/version'));
		$phpexcel->getProperties()->setTitle('ebay产品描述模板表');
		$phpexcel->getProperties()->setSubject('ebay产品描述模板表');
		$phpexcel->getProperties()->setDescription('ebay产品描述模板表-运营系统支撑系统');
		$phpexcel->getProperties()->setKeywords('ebay产品描述模板表');
		$phpexcel->getProperties()->setCategory('ebay产品描述模板表');
		$phpexcel->setActiveSheetIndex(0);
		$phpexcel->getActiveSheet()->setTitle('ebay产品描述模板表');
		$phpexcel->getActiveSheet()->setCellValue('A1', 'language');
		$phpexcel->getActiveSheet()->setCellValue('B1', 'SKU');
		$phpexcel->getActiveSheet()->setCellValue('C1', 'Item_Name');
		$phpexcel->getActiveSheet()->setCellValue('D1', 'Item_Title');
		$phpexcel->getActiveSheet()->setCellValue('E1', 'Description');
		$i = 2;
		foreach ($product_info as $pi)
		{
			$phpexcel->getActiveSheet()->setCellValue('A' . $i, Helper_BSS_Normal::getIniOne('online_ebay_product_language', 'value', $pi['language'], 'text'));
			$phpexcel->getActiveSheet()->setCellValue('B' . $i, $pi['number']);
			$phpexcel->getActiveSheet()->setCellValue('C' . $i, $pi['name']);
			$phpexcel->getActiveSheet()->setCellValue('D' . $i, $pi['title']);
			$phpexcel->getActiveSheet()->setCellValue('E' . $i, $pi['description']);
			++$i;
		}
		$outputFileName = "ebay产品描述模板.xls";
		$objWriter = new PHPExcel_Writer_Excel5($phpexcel);
		header("Content-Type: application/vnd.ms-excel");
		header("Content-type: application/octet-stream");
		header("Content-type: application/download");
		header('Content-Disposition:inline;filename="' . $outputFileName . '"');
		header("Content-Transfer-Encoding:binary");
		header("Cache-Control: must-revalidate, post-check=0, pri-check=0");
		header("Pragma: no-cache");
		$objWriter->Save('php://output');
		exit;
	}
	
	static function onlineEbayListingWrongSaveToXls($wrong_listings, $file_name)
	{
		if (! is_file(Q::ini('custom_system/wrong_listing_dir') . $file_name))
		{
			include Q::ini('custom_system/lib_dir') . 'PHPExcel.php';
			$phpexcel_title = new PHPExcel();
			$phpexcel_title->getProperties()->setCreator(Q::ini('custom_system/system_name') . Q::ini('custom_system/version'));
			$phpexcel_title->getProperties()->setLastModifiedBy(Q::ini('custom_system/system_name') . Q::ini('custom_system/version'));
			$phpexcel_title->getProperties()->setTitle('ebay上架产品出错表');
			$phpexcel_title->getProperties()->setSubject('ebay产品描述模板表');
			$phpexcel_title->getProperties()->setDescription('ebay上架产品出错表-运营系统支撑系统');
			$phpexcel_title->getProperties()->setKeywords('ebay上架产品出错表');
			$phpexcel_title->getProperties()->setCategory('ebay上架产品出错表');
			$phpexcel_title->setActiveSheetIndex(0);
			$phpexcel_title->getActiveSheet()->setTitle('ebay上架产品出错表');
			$phpexcel_title->getActiveSheet()->setCellValue('A1', 'shop_name');
			$phpexcel_title->getActiveSheet()->setCellValue('B1', 'SKU');
			$phpexcel_title->getActiveSheet()->setCellValue('C1', 'shop_item_id');
			$phpexcel_title->getActiveSheet()->setCellValue('D1', 'site_id');
			$phpexcel_title->getActiveSheet()->setCellValue('E1', 'item_url');
			$objWriter = new PHPExcel_Writer_Excel5($phpexcel_title);
			$objWriter->Save(Q::ini('custom_system/wrong_listing_dir') . $file_name);
		}
		require_once(Q::ini('custom_system/lib_dir') . 'PHPExcel.php');
		$reader = PHPExcel_IOFactory::createReader('Excel5');
		$phpexcel = $reader->load(Q::ini('custom_system/wrong_listing_dir') . $file_name);
		$row = $phpexcel->getActiveSheet()->getHighestRow();
		$row++;
		foreach ($wrong_listings as $wl)
		{
			$shop = Shop::find('id=?', $wl['shop_id'])->asArray()->getOne();
			$phpexcel->getActiveSheet()->setCellValue('A' . $row, $shop['name']);
			$phpexcel->getActiveSheet()->getCell('B' . $row)->setValueExplicit($wl['sku'], PHPExcel_Cell_DataType::TYPE_STRING);
			$phpexcel->getActiveSheet()->getCell('C' . $row)->setValueExplicit($wl['shop_item_id'], PHPExcel_Cell_DataType::TYPE_STRING);
			$phpexcel->getActiveSheet()->setCellValue('D' . $row, $wl['site_id']);
			$phpexcel->getActiveSheet()->setCellValue('E' . $row, $wl['item_url']);
			$row++;
		}
		$objWriter = new PHPExcel_Writer_Excel5($phpexcel);
		$objWriter->Save(Q::ini('custom_system/wrong_listing_dir') . $file_name);
	}
	
	static function productBasicExport($data)
	{
		include Q::ini('custom_system/lib_dir') . 'PHPExcel.php';
		$phpexcel = new PHPExcel();
		$phpexcel->getProperties()->setCreator(Q::ini('custom_system/system_name') . Q::ini('custom_system/version'));
		$phpexcel->getProperties()->setLastModifiedBy(Q::ini('custom_system/system_name') . Q::ini('custom_system/version'));
		$phpexcel->getProperties()->setTitle('产品基本信息表');
		$phpexcel->getProperties()->setSubject('产品基本信息表');
		$phpexcel->getProperties()->setDescription('产品基本信息表-运营系统支撑系统');
		$phpexcel->getProperties()->setKeywords('产品基本信息表');
		$phpexcel->getProperties()->setCategory('产品基本信息表');
		$phpexcel->setActiveSheetIndex(0);
		$phpexcel->getActiveSheet()->setTitle('产品基本信息表');
		$phpexcel->getActiveSheet()->setCellValue('A1', 'number');
		$phpexcel->getActiveSheet()->setCellValue('B1', 'name');
		$phpexcel->getActiveSheet()->setCellValue('C1', 'weight');
		$phpexcel->getActiveSheet()->setCellValue('D1', 'purchase_price');
		$phpexcel->getActiveSheet()->setCellValue('E1', 'preferred_provider_id');
		$phpexcel->getActiveSheet()->setCellValue('F1', 'product_categories_id');
		$phpexcel->getActiveSheet()->setCellValue('G1', 'declared_value');
		$phpexcel->getActiveSheet()->setCellValue('H1', 'declared_name_cn');
		$phpexcel->getActiveSheet()->setCellValue('I1', 'declared_name_en');
		$phpexcel->getActiveSheet()->setCellValue('J1', 'notes');
		$i = 2;
		foreach ($data as $d)
		{
			$phpexcel->getActiveSheet()->getCell('A' . $i)->setValueExplicit($d['number'], PHPExcel_Cell_DataType::TYPE_STRING);
			$phpexcel->getActiveSheet()->setCellValue('B' . $i, $d['name']);
			$phpexcel->getActiveSheet()->setCellValue('C' . $i, $d['weight']);
			$phpexcel->getActiveSheet()->setCellValue('D' . $i, $d['purchase_price']);
			$phpexcel->getActiveSheet()->setCellValue('E' . $i, $d['preferred_provider_id']);
			$phpexcel->getActiveSheet()->setCellValue('F' . $i, $d['product_categories_id']);
			$phpexcel->getActiveSheet()->setCellValue('G' . $i, $d['declared_value']);
			$phpexcel->getActiveSheet()->setCellValue('H' . $i, $d['declared_name_cn']);
			$phpexcel->getActiveSheet()->setCellValue('I' . $i, $d['declared_name_en']);
			$phpexcel->getActiveSheet()->setCellValue('J' . $i, $d['notes']);
			$i++;
		}
		$objWriter = new PHPExcel_Writer_Excel5($phpexcel);
		$outputFileName = "产品基本信息表-" . date('YmdHis', CURRENT_TIMESTAMP) . '.xls';
		header("Content-Type: application/vnd.ms-excel");
		header("Content-type: application/octet-stream");
		header("Content-type: application/download");
		header('Content-Disposition:inline;filename="' . $outputFileName . '"');
		header("Content-Transfer-Encoding:binary");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Pragma: no-cache");
		$objWriter->save('php://output');
		exit;
	}
}