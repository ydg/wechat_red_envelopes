<?php
class Helper_BSS_Ebtpl
{
	static function parseToHtml($content, $variables)
	{
		$fp = tmpfile();
		$data = stream_get_meta_data($fp);
		$tmp_file_name = $data['uri'];
		file_put_contents($tmp_file_name, $content);
		ob_start();
		extract($variables);
		include($tmp_file_name);
		$response = ob_get_clean();
		fclose($fp);
		return $response;
	}
}