<?php
class Helper_BSS_Normal
{
	static function buildCondition()
	{
		$ret = $_GET;
		unset($ret['namespace']);
		unset($ret['controller']);
		unset($ret['action']);
		unset($ret['ps']);
		unset($ret['cp']);
		return Helper_BSS_Normal::mysqlEscape($ret);
	}
	
	static function mysqlEscape($arr)
	{
		foreach ($arr as &$a)
		{
			if (is_array($a))
			{
				$a = Helper_BSS_Normal::mysqlEscape($a);
			}
			else
			{
				$a = mysql_escape_string(trim($a));
			}
		}
		return $arr;
	}
	
	static function getProductImgUrl(&$product, $key = 'number')
	{
		foreach ($product as &$p)
		{
			if ($p[$key])
			{
				$p['pic_url'] = Helper_BSS_API::getProductImageUrl($p[$key]);
			}
			else
			{
				$p['pic_url'] = array();
			}
		}
	}
	
	static function getIniOne($flag, $search_key, $search_value, $result_key, $ini='custom_flag')
	{
		foreach (Q::ini($ini.'/'.$flag) as  $key => $value)
		{
			if ($value[$search_key] == $search_value)
			{
				return $value[$result_key];
			}
		}
		return '';
	}
	
	static function getIniAll($flag, $search, $ini='custom_flag')
	{
		$match = array();
		foreach (Q::ini($ini . '/' . $flag) as $key => $value)
		{
			$match_flag = true;
			foreach ($search as $search_key => $search_value)
			{
				if ($value[$search_key] != $search_value)
				{
					$match_flag = false;
				}
			}
			if ($match_flag)
			{
				$match[] = $value;
			}
		}
		return $match;
	}
	
	static function getPeriod($begin_date, $end_date, $period)
	{
		$begin_time_stamp = strtotime($begin_date);
		$end_time_stamp = strtotime($end_date);
		$period_arr = array();
		if ($period == 'daily')
		{
			while ($begin_time_stamp <= $end_time_stamp)
			{
				$arr['begin'] = date('Y-m-d H:i:s', $begin_time_stamp);
				$current_end_stamp = strtotime(date('Y-m-d 23:59:59', $begin_time_stamp));
				if ($current_end_stamp > $end_time_stamp)
				{
					$arr['end'] = date('Y-m-d H:i:s', $end_time_stamp);
				}
				else
				{
					$arr['end'] = date('Y-m-d H:i:s', $current_end_stamp);
				}
				$period_arr[] = $arr;
				$begin_time_stamp = $current_end_stamp + 1;
			}
		}
		if ($period == 'weekly')
		{
			while ($begin_time_stamp <= $end_time_stamp)
			{
				$arr['begin'] = date('Y-m-d H:i:s', $begin_time_stamp);
				$current_end_stamp = strtotime(date('Y-m-d 23:59:59', strtotime(date('Y-m-d H:i:s', $begin_time_stamp) . '+ ' . (7 - date('N', $begin_time_stamp)) . ' day')));
				if ($current_end_stamp > $end_time_stamp)
				{
					$arr['end'] = date('Y-m-d H:i:s', $end_time_stamp);
				}
				else
				{
					$arr['end'] = date('Y-m-d H:i:s', $current_end_stamp);
				}
				$period_arr[] = $arr;
				$begin_time_stamp = $current_end_stamp + 1;
			}
		}
		if ($period == 'monthly')
		{
			while ($begin_time_stamp <= $end_time_stamp)
			{
				$arr['begin'] = date('Y-m-d H:i:s', $begin_time_stamp);
				$current_end_stamp = strtotime(date('Y-m-t 23:59:59', $begin_time_stamp));
				if ($current_end_stamp > $end_time_stamp)
				{
					$arr['end'] = date('Y-m-d H:i:s', $end_time_stamp);
				}
				else
				{
					$arr['end'] = date('Y-m-d H:i:s', $current_end_stamp);
				}
				$period_arr[] = $arr;
				$begin_time_stamp = $current_end_stamp + 1;
			}
		}
		return $period_arr;
	}
	
	static function discount(&$data)
	{
		foreach ($data as &$d)
		{
			$total = $d['s_fee'];
			foreach ($d['item'] as $di)
			{
				$total += $di['quantity'] * $di['price'];
				if (isset($di['attr']) && $di['attr'])
				{
					foreach ($di['attr'] as $dia)
					{
						if ($dia['price'])
						{
							if ($dia['prefix'] == '+')
							{
								$total += $di['quantity'] * $dia['price'];
							}
							if ($dia['prefix'] == '-')
							{
								$total -= $di['quantity'] * $dia['price'];
							}
						}
					}
				}
			}
			$d['discount'] = $total - $d['amount'];
		}
	}
	
	static function time_difference($time)
	{
		$timezone = date_default_timezone_get();
		date_default_timezone_set("UTC");
		$ret = '';
		$years = (date('Y', $time) - 1970);
		if ($years)
		{
			$ret .= $years . '年';
		}
		$months =(date('n',$time) - 1);
		if ($months)
		{
			$ret .= $months . '月';
		}
		$days = (date('j', $time) - 1);
		if ($days)
		{
			$ret .= $days . '天';
		}
		$hours = (date('G', $time));
		if ($hours)
		{
			$ret .= $hours . '时';
		}
		$mins = (date('i', $time));
		if ($mins)
		{
			$ret .= $mins . '分';
		}
		$secs = (date('s', $time));
		if ($secs)
		{
			$ret .= $secs . '秒';
		}
		date_default_timezone_set($timezone);
		return $ret;
	}
	
	static function assembledAmazonOrdersToArray($orders, $shop_id)
	{
		$sales_order = array();
		foreach ($orders as $k => $order)
		{
			if ($order->isSetAmazonOrderId())
			{
				$sales_order[$k]['shop_order_number'] = $order->getAmazonOrderId();
				$sales_order[$k]['transaction_id'] = $order->getAmazonOrderId();
			}
			if ($order->isSetFulfillmentChannel())
			{
				$sales_order[$k]['param']['channel'] = $order->getFulfillmentChannel();
			}
			if ($order->isSetShippingAddress())
			{
				$shippingAddress = $order->getShippingAddress();
				$sales_order[$k]['fullname'] = $shippingAddress->getName();
				if ($shippingAddress->isSetAddressLine1())
				{
					$sales_order[$k]['street1'] = $shippingAddress->getAddressLine1();
					if ($shippingAddress->isSetAddressLine2())
					{
						$sales_order[$k]['street2'] = $shippingAddress->getAddressLine2();
					}
					else
					{
						$sales_order[$k]['street2'] = NULL;
					}
				}
				elseif ($shippingAddress->isSetAddressLine2())
				{
					$sales_order[$k]['street1'] = $shippingAddress->getAddressLine2();
				}
				if ($shippingAddress->isSetStateOrRegion())
				{
					$sales_order[$k]['state'] = $shippingAddress->getStateOrRegion();
				}
				if ($shippingAddress->isSetCity())
				{
					$sales_order[$k]['city'] = $shippingAddress->getCity();
				}
				if (! isset($sales_order[$k]['city']))
				{
					$sales_order[$k]['city'] = $shippingAddress->getStateOrRegion();
				}
				if (! isset($sales_order[$k]['state']))
				{
					$sales_order[$k]['state'] = $shippingAddress->getCity();
				}
				if ($shippingAddress->isSetCountryCode())
				{
					$country = Country::find('code=?', $shippingAddress->getCountryCode())->asArray()->getOne();
					$sales_order[$k]['country'] = $country ? $country['en_name'] : $shippingAddress->getCountryCode();
					$sales_order[$k]['country_cn'] = $country ? $country['cn_name'] : '';
				}
				$sales_order[$k]['zip'] = $shippingAddress->getPostalCode();
				if ($shippingAddress->isSetPhone())
				{
					$sales_order[$k]['phone'] = $shippingAddress->getPhone();
				}
			}
			if ($order->isSetBuyerEmail())
			{
				$sales_order[$k]['email'] = $order->getBuyerEmail();
			}
			if ($order->isSetBuyerName())
			{
				$sales_order[$k]['shop_buyer_id'] = $order->getBuyerName();
			}
			if ($order->isSetOrderTotal())
			{
				$orderTotal = $order->getOrderTotal();
				if ($orderTotal->isSetCurrencyCode())
				{
					$currency = Currency::find('code=?', $orderTotal->getCurrencyCode())->asArray()->getOne();
					$sales_order[$k]['currency_id'] = $currency['id'];
					$sales_order[$k]['currency_rate'] = $currency['rate'];
				}
				if ($orderTotal->isSetAmount())
				{
					$sales_order[$k]['amount'] = $orderTotal->getAmount();
				}
			}
			if ($order->isSetPurchaseDate())
			{
				$sales_order[$k]['sales_time'] = date('Y-m-d H:i:s', strtotime($order->getPurchaseDate()));
			}
			$sales_order[$k]['shop_id'] = $shop_id;
			$sales_order[$k]['shipping_fee'] = 0;
			$sales_order[$k]['discount'] = 0;
			$sales_order[$k]['shipping_method'] = Shipping_Method::getShippingMethodByCode('GZ_R_SG_Package');
			$sales_order[$k]['payment_method'] = Q::ini('custom_flag/sales_order_payment_method/amazon/value');
			$sales_order[$k]['paid_time'] = NULL;
		}
		return $sales_order;
	}
	
	static function assembledAmazonItemsToArray($orderItemList)
	{
		$items = array();
		foreach ($orderItemList as $key => $orderItem)
		{
			$new_item = array();
			$new_item['OrderItemQuantity'] = ($orderItem->getQuantityOrdered()) ? $orderItem->getQuantityOrdered() : 0;
			if ($orderItem->isSetSellerSKU())
			{
				$new_item['product_basic_number'] = substr($orderItem->getSellerSKU(), 0, 6);
				$new_item['quantity'] = $new_item['OrderItemQuantity'];
				if ($orderItem->isSetOrderItemId())
				{
					$new_item['shop_item_id'] = $orderItem->getOrderItemId();
				}
				if ($orderItem->isSetItemPrice())
				{
					$itemPrice = $orderItem->getItemPrice();
					if ($itemPrice->isSetAmount())
					{
						$new_item['sales_price'] = $itemPrice->getAmount();
					}
				}
				if ($orderItem->isSetShippingPrice())
				{
					$shippingPrice = $orderItem->getShippingPrice();
					if ($shippingPrice->isSetAmount())
					{
						$new_item['sales_price'] += $shippingPrice->getAmount();
					}
				}
				if ($orderItem->isSetShippingDiscount())
				{
					$shippingDiscount = $orderItem->getShippingDiscount();
					if ($shippingDiscount->isSetAmount())
					{
						$new_item['sales_price'] -= $shippingDiscount->getAmount();
					}
				}
				if ($orderItem->isSetPromotionDiscount())
				{
					$promotionDiscount = $orderItem->getPromotionDiscount();
					if ($promotionDiscount->isSetAmount())
					{
						$new_item['sales_price'] -= $promotionDiscount->getAmount();
					}
				}
				if (empty($new_item['quantity']))
				{
					unset($new_item);
					continue;
				}
				$new_item['sales_price'] = $new_item['sales_price']/$new_item['quantity'];
				$items[] = $new_item;
			}
		}
		return $items;
	}
	
	static function dimensionResend($condition, $resend_where, $where)
	{
		$current = array_shift($condition);
		$resend_count = QDB::getConn()->execute('select ' . $current . ',count(' . $current . ') as count from sales_order left join sales_order_customer_info on sales_order_customer_info.sales_order_id=sales_order.id where ' . $resend_where . ' group by ' . $current)->fetchAll();
		$all_count = QDB::getConn()->execute('select ' . $current . ',count(' . $current . ') as count from sales_order left join sales_order_customer_info on sales_order_customer_info.sales_order_id=sales_order.id where ' . $where . ' group by ' . $current)->fetchAll();
		$resend = array();
		if (empty($condition))
		{
			foreach ($resend_count as $r)
			{
				$resend[$current][$r[$current]]['count'] = $r['count'];
			}
			foreach ($all_count as $a)
			{
				if ($a[$current])
				{
					$count = isset($resend[$current][$a[$current]]['count']) ? $resend[$current][$a[$current]]['count'] : 0;
					$resend[$current][$a[$current]]['count'] = $count;
					$resend[$current][$a[$current]]['all_count'] = $a['count'];
				}
			}
			return $resend;
		}
		foreach ($resend_count as $r)
		{
			$resend[$current][$r[$current]]['count'] = $r['count'];
			foreach ($all_count as $a)
			{
				if ($a[$current])
				{
					if ($a[$current] == $r[$current])
					{
						$resend[$current][$r[$current]]['all_count'] = $a['count'];
					}
				}
			}
			$resend[$current][$r[$current]]['detail'] = Helper_BSS_Normal::dimensionResend($condition, $resend_where . ' and ' . $current . '=\'' . $r[$current] . '\'', $where . ' and ' . $current . '=\'' . $r[$current] . '\'');
		}
		return $resend;
	}
	
	static function getResendHtml($result)
	{
		if (isset($result['shipping_method']))
		{
			foreach ($result['shipping_method'] as $k => $rs)
			{
				$shipping_method = Shipping_Method::find('id=?', $k)->asArray()->getOne();
				echo '<li style="margin: 5px 10px; color: red;" class="parent">' . $shipping_method['name'] . '：（'.$rs['count'].'/'.$rs['all_count'].'）<ul style="list-style: circle;">';
				if (isset($rs['detail']))
				{
					 Helper_BSS_Normal::getResendHtml($rs['detail']);
				}
				echo '</ul>';
			}
		}
		elseif (isset($result['resend_reason']))
		{
			foreach ($result['resend_reason'] as $rrk => $rr)
			{
				echo '<li style="margin: 5px 10px; color: black;" class="parent">'.Helper_BSS_Normal::getIniOne('sales_order_resend_reason', 'value', $rrk, 'name').'：（'.$rr['count'].'/'.$rr['all_count'].'）<ul style="list-style: circle;">';
				if (isset($rr['detail']))
				{
					Helper_BSS_Normal::getResendHtml($rr['detail']);
				}
				echo '</ul>';
			}
		}
		elseif (isset($result['country']))
		{
			foreach ($result['country'] as $rck => $rc)
			{
				echo '<li style="margin: 5px 10px; color: green;" class="parent">'.$rck.'：（'.$rc['count'].'/'.$rc['all_count'].'）<ul style="list-style: circle;">';
				if (isset($rc['detail']))
				{
					Helper_BSS_Normal::getResendHtml($rc['detail']);
				}
				echo '</ul>';
			}
		}
	}
	
	/**
	 * 获取数据表字段数据
	 * @param 表名 $table_name
	 * @param 字段名 $column_name
	 */
	static function getTableColunmData($table_name, $column_name = 'path')
	{
		$data = QDB::getConn()->execute('select * from ' . $table_name)->fetchCol($column_name);
		return $data;
	}
	
	/**
	 * 生成随机字符串
	 * @param 字符串长度 $length
	 * @param 对比数组 $exists_array
	 * @param 生成规则 $rule
	 */
	static function generate($length, $exists_array = array(), $rule = 'md5')
	{
		if ($rule == 'md5')
		{
			$str = substr(str_shuffle(md5(CURRENT_USER_NAME . CURRENT_TIMESTAMP)), 0, $length);
			while (in_array($str, $exists_array))
			{
				$str = substr(str_shuffle(md5(CURRENT_USER_NAME . CURRENT_TIMESTAMP)), 0, $length);
			}
			return $str;
		}
	}
	
	static function createFolder($path, $mode = 0777)
	{
		return is_dir($path) or (Helper_BSS_Normal::createFolder(dirname($path)) and mkdir($path, $mode));
	}
	
	static function getDirlist($base_dir)
	{
		$fso = opendir($base_dir);
		$files = array();
		while($flist = readdir($fso))
		{
			if ($flist != '.' && $flist != '..' && $flist != '.svn')
			{
		     	 $files[]= $flist;
			}
		}
		closedir($fso);
		return $files;
	}
	
	static function upload($file, $path)
	{
		if ( ! empty($file))
		{
			if ($file['error'] > 0)
			{
				switch ($file['error'])
				{
					  case 1:  return array('ack' => FAILURE, 'message' => 'File exceeded upload_max_filesize'); break;
				      case 2:  return array('ack' => FAILURE, 'message' => '不能超过800M'); break; 
				      case 3:  return array('ack' => FAILURE, 'message' => 'File only partially uploaded'); break;
				      case 4:  return array('ack' => FAILURE, 'message' => 'No file uploaded'); break;
				}
			}
			else
			{
				$fileTypes = array('gif', 'png', 'jpg', 'jpeg', 'txt');
				$fileParts = pathinfo($file['name']);
				if ( ! in_array($fileParts['extension'] , $fileTypes))
				{
				 	 return array('ack' => FAILURE, 'message' => '文件类型错误，请重新选择文件!<br>只允许gif,png,jpg,jpeg,txt类型的文件');
				}
				if(file_exists($path . '/' . $file['name']))
				{
					unlink($path . '/' . $file['name']);
				}
				$result = move_uploaded_file($file["tmp_name"], $path . '/' . $file['name']);
				if ($result)
				{
					//修改返回数据
					return array('ack' => SUCCESS, 'data'=> $path . '/' . $file['name'], 'message' => '文件已成功上传！');
				}
			}
		}
		else
		{
			return array('ack' => FAILURE, 'message' => '文件名称不存在!');
		}
	}
	
	static function uploadonlytxt($file, $path)
	{
		if ( ! empty($file))
		{
			if ($file['error'] > 0)
			{
				switch ($file['error'])
				{
					case 1:  return array('ack' => FAILURE, 'message' => 'File exceeded upload_max_filesize'); break;
					case 2:  return array('ack' => FAILURE, 'message' => '不能超过800M'); break;
					case 3:  return array('ack' => FAILURE, 'message' => 'File only partially uploaded'); break;
					case 4:  return array('ack' => FAILURE, 'message' => 'No file uploaded'); break;
				}
			}
			else
			{
				$fileTypes = array('txt');
				$fileParts = pathinfo($file['name']);
				if ( ! in_array($fileParts['extension'] , $fileTypes))
				{
					return array('ack' => FAILURE, 'message' => '文件类型错误，请重新选择文件!只允许txt类型的文件');
				}
				$file['name'] = time().mt_rand(0, 10).$file['name'];
				if(file_exists($path . '/' . $file['name']))
				{
					unlink($path . '/' . $file['name']);
				}
				$result = move_uploaded_file($file["tmp_name"], $path . '/' . $file['name']);
				if ($result)
				{
					//修改返回数据
					return array('ack' => SUCCESS, 'data'=> $path . '/' . $file['name'], 'message' => '文件已成功上传！');
				}
			}
		}
		else
		{
			return array('ack' => FAILURE, 'message' => '文件名称不存在!');
		}
	}
	
	static function xcopy($source, $destination)
	{
		if (! is_dir($source))
		{
			return false;
		}
		if (! is_dir($destination))
		{
			Helper_BSS_Normal::createFolder($destination);
		}
		$handle = opendir($source);
		@mkdir($destination);
		while (($entry= readdir($handle)) !== false)
		{
			if (($entry != '.') && ($entry != '..'))
			{
				if (is_dir($source . '/' . $entry))
				{
					Helper_BSS_Normal::xcopy($source. '/' . $entry, $destination . '/' . $entry);
				}
				else
				{
					copy($source . '/' . $entry, $destination . '/' . $entry);
				}
			}
		}
		return true;
	}
	
	static function AliExpressDateTransfer($aliexpress_date)
	{
		return date('Y-m-d H:i:s', strtotime(substr($aliexpress_date, 0, 14) . substr($aliexpress_date, -5)));
	}
	
	//测试
	static function is_mob(){

	     //正则表达式,批配不同手机浏览器UA关键词。

	     $regex_match="/(nokia|iphone|android|motorola|^mot\-|softbank|foma|docomo|kddi|up\.browser|up\.link|";

	     $regex_match.="htc|dopod|blazer|netfront|helio|hosin|huawei|novarra|CoolPad|webos|techfaith|palmsource|";

	     $regex_match.="blackberry|alcatel|amoi|ktouch|nexian|samsung|^sam\-|s[cg]h|^lge|ericsson|philips|sagem|wellcom|bunjalloo|maui|";

	     $regex_match.="symbian|smartphone|midp|wap|phone|windows ce|iemobile|^spice|^bird|^zte\-|longcos|pantech|gionee|^sie\-|portalmmm|";

	     $regex_match.="jig\s browser|hiptop|^ucweb|^benq|haier|^lct|opera\s*mobi|opera\*mini|320x320|240x320|176x220";

	     $regex_match.=")/i";

	    

	     return isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE']) or preg_match($regex_match, strtolower($_SERVER['HTTP_USER_AGENT'])); //如果UA中存在上面的关键词则返回真。

	}
	//测试
	
	static function isMobile()
	{
		if (isset($_SERVER['HTTP_X_WAP_PROFILE']))
		{
			return true;
		}
		if (isset($_SERVER['HTTP_VIA']))
		{
			return stristr($_SERVER['HTTP_VIA'], 'wap') ? true : false;
		}
		if (isset($_SERVER['HTTP_USER_AGENT']))
		{
			$clientkeywords = array(
				'nokia',
				'coolpad',
				'sony',
				'ericsson',
				'mot',
				'samsung',
				'htc',
				'sgh',
				'lg',
				'sharp',
				'sie-',
				'philips',
				'panasonic',
				'alcatel',
				'lenovo',
				'iphone',
				'ipod',
				'blackberry',
				'meizu',
				'android',
				'netfront',
				'symbian',
				'ucweb',
				'windowsce',
				'palm',
				'operamini',
				'operamobi',
				'openwave',
				'nexusone',
				'cldc',
				'midp',
				'wap',
				'mobile'
			);
			if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT'])))
			{
				return true;
			}
		}
		if (isset($_SERVER['HTTP_ACCEPT']))
		{
			if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false) || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))
			{
				return true;
			}
		}
	}
	
	static function WishSKUMapping($sku)
	{
		$array = array(
		       '039879' => '000001',
			    '040107' => '000002',
			    '040109' => '000003',
			    '040110' => '000004',
			    '040111' => '000005',
			    '040112' => '000006',
			    '040113' => '000007',
			    '040114' => '000008',
			    '040115' => '000009',
			    '040116' => '000010',
			    '040117' => '000011',
			    '040118' => '000012',
			    '040119' => '000013',
			    '040120' => '000014',
			    '040121' => '000015',
			    '040122' => '000016',
			    '040123' => '000017',
			    '040124' => '000018',
			    '040125' => '000019',
			    '040126' => '000020',
			    '040127' => '000021',
			    '040128' => '000022',
			    '040129' => '000023',
			    '040130' => '000024',
			    '040131' => '000025',
			    '040132' => '000026',
			    '040133' => '000027',
			    '040134' => '000028',
			    '040135' => '000029',
			    '040136' => '000030',
			    '040137' => '000031',
			    '040138' => '000032',
			    '040140' => '000034',
			    '040141' => '000035',
			    '040142' => '000036',
			    '040143' => '000037',
			    '040144' => '000038',
			    '040145' => '000039',
			    'watch-40146' => '000040',
			    '040147' => '000041',
			    '040148' => '000042',
			    '040149' => '000043',
			    '040150' => '000044',
			    '040151' => '000045',
			    '040152' => '000046',
			    '040153' => '000047',
			    '040155' => '000048',
			    '040156' => '000049',
			    '040158' => '000050',
			    '040159' => '000052',
			    '040160' => '000053',
			    '040161' => '000054',
			    '040162' => '000055',
			    '040163' => '000056',
			    '040164' => '000057',
			    '040165' => '000058',
			    '040166' => '000059',
			    '048440-to-1' => '000101',
			    '048441-to-2' => '000102',
			    '048442-to-3' => '000103',
			    '048443-to-4' => '000104',
			    '048444-to-5' => '000105',
			    '048445-to-6' => '000106',
			    '048446-to-7' => '000107',
			    '048447-to-8' => '000108',
			    '048449-to-10' => '000109',
			    '048450-to-11' => '000110',
			    '048454-to-15' => '000111',
			    '048455-to-16' => '000112',
			    '048458-CK11' => '000113',
			    '048459-CK12' => '000114',
			    '048460-CK13' => '000115',
			    '048462-CK15' => '000116',
			    '048463-CK16' => '000117',
			    '048464-CK17' => '000118',
			    '048465-CK18' => '000119',
			    'AA0051' => '000301',
			    'AA0052' => '000302',
			    'AA0053' => '000303',
			    'AA0054' => '000304',
			    'AA0055' => '000305',
			    'AA0056' => '000306',
			    'AA0057' => '000307',
			    'AA0058' => '000308',
			    'AA0059' => '000309',
			    'AA0060' => '000310',
			    'AA0061' => '000311',
			    'AA0062' => '000312',
			    'AA0063' => '000313',
			    'AA0064' => '000314',
			    'AA0065' => '000315',
			    'AA0066' => '000316',
			    'AA0067' => '000317',
			    'AA0068' => '000318',
			    'AA0069' => '000319',
			    'AA0070' => '000320',
			    'AA0071' => '000321',
			    'AA0072' => '000322',
			    'AA0073' => '000323',
			    'AA0074' => '000324',
			    'AA0075' => '000325',
			    'AA0076' => '000326',
			    'AA0077' => '000327',
			    'AA0078' => '000328',
			    'AA0079' => '000329',
			    'AA0080' => '000330',
			    'AA0081' => '000331',
			    'AA0082' => '000332',
			    'AA0083' => '000333',
			    'AA0084' => '000334',
			    'AA0085' => '000335',
			    'AA0086' => '000336',
			    'AA0087' => '000337',
			    'AA0088' => '000338',
			    'AA0089' => '000339',
			    'AA0090' => '000340',
			    'AA0091' => '000341',
			    'AA0092' => '000342',
			    'AA0093' => '000343',
			    'AA0094' => '000344',
			    'AA0095' => '000345',
			    'AA0096' => '000346',
			    'AA0097' => '000347',
			    'AA0098' => '000348',
			    'AA0099' => '000349',
			    'AA0100' => '000350',
			    'AA0101' => '000351',
			    'AA0102' => '000352',
			    'AA0103' => '000353',
			    'AA0104' => '000354',
			    'AA0105' => '000355',
			    'AA0106' => '000356',
			    'AA0107' => '000357',
			    'AA0108' => '000358',
			    'AA0109' => '000359',
			    'AA0110' => '000360',
			    'AA0111' => '000361',
			    'AA0112' => '000362',
			    'AA0113' => '000363',
			    'AA0114' => '000364',
			    'AA0115' => '000365',
			    'AA0116' => '000366',
			    'AA0117' => '000367',
			    'AA0118' => '000368',
			    'AA0119' => '000369',
			    'AA0120' => '000370',
			    'AA0121' => '000371',
			    'AA0122' => '000372',
			    'AA0123' => '000373',
			    'AA0124' => '000374',
			    'AA0125' => '000375',
			    'AA0126' => '000376',
			    'AA0127' => '000377',
			    'AA0128' => '000378',
			    'AA0129' => '000379',
			    'AA0130' => '000380',
			    'AA0131' => '000381',
			    'AA0132' => '000382',
			    'AA0133' => '000383',
			    'AA0134' => '000384',
			    'AA0135' => '000385',
			    'AA0136' => '000386',
			    'AA0137' => '000387',
			    'AA0138' => '000388',
			    'AA0139' => '000389',
			    'AA0140' => '000390',
			    'AA0141' => '000391',
			    'AA0142' => '000392',
			    'AA0143' => '000393',
			    'AA0144' => '000394',
			    'AA0145' => '000395',
			    'AA0146' => '000396',
			    'AA0147' => '000397',
			    'AA0148' => '000398',
			    'AA0149' => '000399',
			    'AA0150' => '000400',
			    'AA0151' => '000401',
			    'AA0152' => '000402',
			    'AA0153' => '000403',
			    'AA0154' => '000404',
			    'AA0155' => '000405',
			    'AA0156' => '000406',
			    'AA0157' => '000407',
			    'AA0158' => '000408',
			    'AA0159' => '000409',
			    'AA0160' => '000410',
			    'AA0161' => '000411',
			    'AA0162' => '000412',
			    'AA0163' => '000413',
			    'AA0164' => '000414',
			    'AA0165' => '000415',
			    'AA0166' => '000416',
			    'AA0167' => '000417',
			    'AA0168' => '000418',
			    'AA0169' => '000419',
			    'AA0170' => '000420',
			    'AA0171' => '000421',
			    'AA0172' => '000422',
			    'AA0173' => '000423',
			    'AA0174' => '000424',
			    'AA0175' => '000425',
			    'AA0176' => '000426',
			    'AA0177' => '000427',
			    'AA0178' => '000428',
			    'AA0179' => '000429',
			    'AA0180' => '000430',
			    'AA0181' => '000431',
			    'AA0182' => '000432',
			    'AA0183' => '000433',
			    'AA0184' => '000434',
			    'AA0185' => '000435',
			    'AA0186' => '000436',
			    'AA0187' => '000437',
			    'AA0188' => '000438',
			    'AA0189' => '000439',
			    'AA0190' => '000440',
			    'AA0191' => '000441',
			    'AA0192' => '000442',
			    'AA0193' => '000443',
			    'AA0194' => '000444',
			    'AA0195' => '000445',
			    'AA0196' => '000446',
			    'AA0197' => '000447',
			    'AA0198' => '000448',
			    'AA0199' => '000449',
			    'AA0200' => '000450',
			    'AA0201' => '000451',
			    'AA0202' => '000452',
			    'AA0203' => '000453',
			    'AA0204' => '000454',
			    'AA0205' => '000455',
			    'AA0206' => '000456',
			    'AA0207' => '000457',
			    'AA0208' => '000458',
			    'AA0209' => '000459',
			    'AA0210' => '000460',
			    'AA0211' => '000461',
			    'AA0212' => '000462',
			    'AA0213' => '000463',
			    'AA0214' => '000464',
			    'AA0215' => '000465',
			    'AA0216' => '000466',
			    'AA0217' => '000467',
			    'AA0218' => '000468',
			    'AA0219' => '000469',
			    'AA0220' => '000470',
			    'AA0221' => '000471',
			    'AA0222' => '000472',
			    'AA0223' => '000473',
			    'AA0224' => '000474',
			    'AA0225' => '000475',
			    'AA0226' => '000476',
			    'AA0227' => '000477',
			    'AA0228' => '000478',
			    'AA0229' => '000479',
			    'AA0230' => '000480',
			    'AA0231' => '000481',
			    'AA0232' => '000482',
			    'AA0233' => '000483',
			    'AA0234' => '000484',
			    'AA0235' => '000485',
			    'AA0236' => '000486',
			    'AA0237' => '000487',
			    'AA0238' => '000488',
			    'AA0239' => '000489',
			    'AA0240' => '000490',
			    'AA0241' => '000491',
			    'AA0242' => '000492',
			    'AA0243' => '000493',
			    'AA0244' => '000494',
			    'AA0245' => '000495',
			    'AA0246' => '000496',
			    'AA0247' => '000497',
			    'AA0248' => '000498',
			    'AA0249' => '000499',
			    'AA0250' => '000500',
			    'AA0251' => '000501',
			    'AA0252' => '000502',
			    'AA0253' => '000503',
			    'AA0254' => '000504',
			    'AA0255' => '000505',
			    'AA0256' => '000506',
			    'AA0257' => '000507',
			    'AA0258' => '000508',
			    'AA0259' => '000509',
			    'AA0260' => '000510',
			    'AA0261' => '000511',
			    'AA0262' => '000512',
			    'AA0263' => '000513',
			    'AA0264' => '000514',
			    'AA0265' => '000515',
			    'AA0266' => '000516',
			    'AA0267' => '000517',
			    'AA0268' => '000518',
			    'AA0269' => '000519',
			    'AA0270' => '000520',
			    'AA0271' => '000521',
			    'AA0272' => '000522',
			    'AA0273' => '000523',
			    'AA0274' => '000524',
			    'AA0275' => '000525',
			    'AA0276' => '000526',
			    'AA0277' => '000527',
			    'AA0278' => '000528',
			    'AA0279' => '000529',
			    'AA0280' => '000530',
			    'AA0281' => '000531',
			    'AA0282' => '000532',
			    'AA0283' => '000533',
			    'AA0284' => '000534',
			    'AA0285' => '000535',
			    'AA0286' => '000536',
			    'AA0287' => '000537',
			    'AA0288' => '000538',
			    'AA0289' => '000539',
			    'AA0290' => '000540',
			    'AA0291' => '000541',
			    'CCAMERA001' => '000542',
			    'CCAMERA002' => '000543',
			    'CCAMERA003' => '000544',
			    'CCAMERA004' => '000545',
			    'CCAMERA005' => '000546',
			    'CCAMERA006' => '000547',
			    'CCAMERA007' => '000548',
			    'CCAMERA008' => '000549',
			    'CCAMERA009' => '000550',
			    'CCAMERA010' => '000551',
			    'CCAMERA011' => '000552',
			    'CCAMERA012' => '000553',
			    'CCAMERA013' => '000554',
			    'CCAMERA014' => '000555',
			    'CCAMERA015' => '000556',
			    'CCAMERA016' => '000557',
			    'CCAMERA017' => '000558',
			    'CCAMERA018' => '000559',
			    'CCAMERA019' => '000560',
			    'CCAMERA020' => '000561',
			    'CCAMERA021' => '000562',
			    'CCAMERA022' => '000563',
			    'CCAMERA023' => '000564',
			    'CCAMERA024' => '000565',
			    'CCAMERA025' => '000566',
			    'CCAMERA026' => '000567',
			    'CCAMERA027' => '000568',
			    'CCAMERA028' => '000569',
			    'CCAMERA029' => '000570',
			    'CCAMERA030' => '000571',
			    'CCAMERA031' => '000572',
			    'CCAMERA032' => '000573',
			    'CCAMERA033' => '000574',
			    'CCAMERA034' => '000575',
			    'Follow backcam0002' => '000576',
			    'Follow backcam001' => '000577',
			    'Follow backcam002' => '000578',
			    'Follow backcam003(pz304' => '000579',
			    'Follow backcam004(pz70d' => '000580',
			    'Follow backcam005(pz70a' => '000581',
			    'Follow backcam006(pz415' => '000582',
			    'Follow backcam007(pz413' => '000583',
			    'Follow backcam008(pz420' => '000584',
			    'Follow backcam009(pz403' => '000585',
			    'Follow backcam04(pz70d' => '000586',
			    'Follow DV0001' => '000587',
			    'Follow DV001' => '000588',
			    'Follow DV01' => '000589',
			    'Follow002' => '000590',
			    'OBD001' => '000591',
			    'OBD002' => '000592',
			    'OBD003' => '000593',
			    'OBD004' => '000594',
			    'OBD005' => '000595',
			    'OBD006' => '000596',
			    '12 color A1' => '000639',
			    '12 color A12' => '000640',
			    '12 color A3' => '000641',
			    '12 color A4' => '000642',
			    '24 color B1' => '000643',
			    '24 color B2' => '000644',
			    '24 color B3' => '000645',
			    '24 color B4' => '000646',
			    '36 color C1' => '000647',
			    '36 color C2' => '000648',
			    '36 color C3' => '000649',
			    '36 color C4' => '000650',
			    'bluesky 40501' => '000651',
			    'bluesky 40502' => '000652',
			    'bluesky 40503' => '000653',
			    'bluesky 40504' => '000654',
			    'bluesky 40505' => '000655',
			    'bluesky 40506' => '000656',
			    'bluesky 40507' => '000657',
			    'bluesky 40508' => '000658',
			    'bluesky 40509' => '000659',
			    'bluesky 40510' => '000660',
			    'bluesky 40511' => '000661',
			    'bluesky 40512' => '000662',
			    'bluesky 40513' => '000663',
			    'bluesky 40514' => '000664',
			    'bluesky 40515' => '000665',
			    'bluesky 40517' => '000666',
			    'bluesky 40518' => '000667',
			    'bluesky 40519' => '000668',
			    'bluesky 40520' => '000669',
			    'bluesky 40521' => '000670',
			    'bluesky 40522' => '000671',
			    'bluesky 40523' => '000672',
			    'bluesky 40524' => '000673',
			    'bluesky 40525' => '000674',
			    'bluesky 40526' => '000675',
			    'bluesky 40527' => '000676',
			    'bluesky 40528' => '000677',
			    'bluesky 40529' => '000678',
			    'bluesky 40530' => '000679',
			    'bluesky 40531' => '000680',
			    'bluesky 40532' => '000681',
			    'bluesky 40533' => '000682',
			    'bluesky 40534' => '000683',
			    'bluesky 40535' => '000684',
			    'bluesky 40536' => '000685',
			    'bluesky 40537' => '000686',
			    'bluesky 40538' => '000687',
			    'bluesky 40539' => '000688',
			    'bluesky 40540' => '000689',
			    'bluesky 40541' => '000690',
			    'bluesky 40542' => '000691',
			    'bluesky 40543' => '000692',
			    'bluesky 40544' => '000693',
			    'bluesky 40545' => '000694',
			    'gd010' => '000695',
			    'gd061' => '000696',
			    'gd062' => '000697',
			    'gd063' => '000698',
			    'GD064' => '000699',
			    'GD065' => '000700',
			    'GD066' => '000701',
			    'GD067' => '000702',
			    'GD068' => '000703',
			    'GD069' => '000704',
			    'GD070' => '000705',
			    'GD071' => '000706',
			    'GD072' => '000707',
			    'GD073' => '000708',
			    'GD074' => '000709',
			    'GD075' => '000710',
			    'GD076' => '000711',
			    'GD077' => '000712',
			    'GD078' => '000713',
			    'GD079' => '000714',
			    'GD080' => '000715',
			    'GD081' => '000716',
			    'GD082' => '000717',
			    'GD083' => '000718',
			    'GD084' => '000719',
			    'GD085' => '000720',
			    'GD086' => '000721',
			    'GD087' => '000722',
			    'GD088' => '000723',
			    'GD089' => '000724',
			    '105' => '000939',
			    '106' => '000940',
			    '107' => '000941',
			    '108' => '000942',
			    '109' => '000943',
			    '110' => '000944',
			    '111' => '000945',
			    '112' => '000946',
			    '113' => '000947',
			    '114' => '000948',
			    '115' => '000949',
			    '116' => '000950',
			    '117' => '000951',
			    '118' => '000952',
			    '119' => '000953',
			    '120' => '000954',
			    '121' => '000955',
			    '122' => '000956',
			    '123' => '000957',
			    '13401' => '000958',
			    '13402' => '000959',
			    '13501' => '000960',
			    '13502' => '000961',
			    '13601' => '000962',
			    '13603' => '000963',
			    '13701' => '000964',
			    '13702' => '000965',
			    '13801' => '000966',
			    '13802' => '000967',
			    '13901' => '000968',
			    '13902' => '000969',
			    '14001' => '000970',
			    '14002' => '000971',
			    '14101' => '000972',
			    '14102' => '000973',
			    '14103' => '000974',
			    '14104' => '000975',
			    '14105' => '000976',
			    '14106' => '000977',
			    '142' => '000978',
			    '143' => '000979',
			    '144' => '000980',
			    '145' => '000981',
			    '146' => '000982',
			    '14701' => '000983',
			    '14702' => '000984',
			    '14801' => '000985',
			    '14802' => '000986',
			    '14803' => '000987',
			    '14804' => '000988',
			    '14805' => '000989',
			    '14806' => '000990',
			    '14901' => '000991',
			    '14902' => '000992',
			    '14903' => '000993',
			    '14904' => '000994',
			    '14905' => '000995',
			    '14906' => '000996',
			    '150' => '000997',
			    '151' => '000998',
			    '152' => '000999',
			    '153' => '001000',
			    '154' => '001001',
			    '155' => '001002',
			    '156' => '001003',
			    '157' => '001004',
			    '158' => '001005',
			    '159' => '001006',
			    '160' => '001007',
			    '161' => '001008',
			    '162' => '001009',
			    '163' => '001010',
			    '164' => '001011',
			    '165' => '001012',
			    '16601' => '001013',
			    '16602' => '001014',
			    '167' => '001015',
			    '168' => '001016',
			    '169' => '001017',
			    '170' => '001018',
			    '0005-1' => '001239',
			    '0005-2' => '001240',
			    '0005-3' => '001241',
			    '0005-4' => '001242',
			    '0005-5' => '001243',
			    '0005-6' => '001244',
			    '0005-7' => '001245',
			    '0005-8' => '001246',
			    '0007-1' => '001247',
			    '0009-1' => '001248',
			    '0009-2' => '001249',
			    '0009-3' => '001250',
			    '0009-5' => '001251',
			    '0009-6' => '001252',
			    '0009-8' => '001253',
			    '0010-1' => '001254',
			    '0010-3' => '001255',
			    '0010-4' => '001256',
			    '0010-5' => '001257',
			    '0010-6' => '001258',
			    '0010-7' => '001259',
			    '0043-1' => '001260',
			    '0043-2' => '001261',
			    '0043-3' => '001262',
			    '0043-4' => '001263',
			    '0043-5' => '001264',
			    '0043-6' => '001265',
			    '0043-7' => '001266',
			    '0043-8' => '001267',
			    '0134-1' => '001268',
			    '0134-2' => '001269',
			    '0134-3' => '001270',
			    '0134-4' => '001271',
			    'W0001-1' => '001272',
			    'W-0001-1' => '001273',
			    'W0001-2' => '001274',
			    'W-0001-2' => '001275',
			    'W0001-3' => '001276',
			    'W-0001-3' => '001277',
			    'W0002-1' => '001278',
			    'W-0002-1' => '001279',
			    'W0002-2' => '001280',
			    'W-0002-2' => '001281',
			    'W0003-1' => '001282',
			    'W0003-2' => '001283',
			    'W0003-3' => '001284',
			    'W0004-1' => '001285',
			    'W0004-2' => '001286',
			    'W0004-3' => '001287',
			    'W0004-4' => '001288',
			    'W0004-5' => '001289',
			    'W0004-6' => '001290',
			    'W0005-1' => '001291',
			    'W0005-10' => '001292',
			    'W0005-11' => '001293',
			    'W0005-12' => '001294',
			    'W0005-13' => '001295',
			    'W0005-14' => '001296',
			    'W0005-15' => '001297',
			    'W0005-16' => '001298',
			    'W0005-17' => '001299',
			    'W0005-18' => '001300',
			    'W0005-19' => '001301',
			    'W0005-2' => '001302',
			    'W0005-20' => '001303',
			    'W0005-21' => '001304',
			    'W0005-22' => '001305',
			    'W0005-23' => '001306',
			    'W0005-24' => '001307',
			    'W0005-25' => '001308',
			    'W0005-26' => '001309',
			    'W0005-27' => '001310',
			    'W0005-28' => '001311',
			    'W0005-3' => '001312',
			    'W0005-4' => '001313',
			    'W0005-5' => '001314',
			    'W0005-6' => '001315',
			    'W0005-7' => '001316',
			    'W0005-8' => '001317',
			    'W0005-9' => '001318',
			    'W0006-1' => '001319',
			    'W0006-2' => '001320',
			    'W0006-3' => '001321',
			    'W0006-4' => '001322',
			    'W0006-5' => '001323',
			    'W0006-6' => '001324',
			    'W0006-7' => '001325',
			    'W0006-8' => '001326',
			    'W0007-1' => '001327',
			    'W0007-2' => '001328',
			    'W0007-3' => '001329',
			    'W0007-4' => '001330',
			    'W0007-5' => '001331',
			    'W0007-6' => '001332',
			    'W0007-7' => '001333',
			    'W0007-8' => '001334',
			    'W0008-1' => '001335',
			    'W0008-2' => '001336',
			    'W0008-3' => '001337',
			    'W0008-4' => '001338',
			    'W0008-5' => '001339',
			    'W0008-6' => '001340',
			    'W0008-7' => '001341',
			    'W0008-8' => '001342',
			    'W0008-9' => '001343',
			    'W0009-1' => '001344',
			    'W0009-10' => '001345',
			    'W0009-11' => '001346',
			    'W0009-12' => '001347',
			    'W0009-13' => '001348',
			    'W0009-2' => '001349',
			    'W0009-3' => '001350',
			    'W0009-4' => '001351',
			    'W0009-5' => '001352',
			    'W0009-6' => '001353',
			    'W0009-7' => '001354',
			    'W0009-8' => '001355',
			    'W0009-9' => '001356',
			    'W0010-1' => '001357',
			    'W0010-10' => '001358',
			    'W0010-2' => '001359',
			    'W0010-3' => '001360',
			    'W0010-4' => '001361',
			    'W0010-5' => '001362',
			    'W0010-6' => '001363',
			    'W0010-7' => '001364',
			    'W0010-8' => '001365',
			    'W0010-9' => '001366',
			    'W0011-1' => '001367',
			    'W0011-10' => '001368',
			    'W0011-11' => '001369',
			    'W0011-12' => '001370',
			    'W0011-13' => '001371',
			    'W0011-2' => '001372',
			    'W0011-3' => '001373',
			    'W0011-4' => '001374',
			    'W0011-5' => '001375',
			    'W0011-6' => '001376',
			    'W0011-7' => '001377',
			    'W0011-8' => '001378',
			    'W0011-9' => '001379',
			    'W0012-1' => '001380',
			    'W0012-2' => '001381',
			    'W0013-1' => '001382',
			    'W0013-10' => '001383',
			    'W0013-2' => '001384',
			    'W0013-3' => '001385',
			    'W0013-4' => '001386',
			    'W0013-5' => '001387',
			    'W0013-6' => '001388',
			    'W0013-7' => '001389',
			    'W0013-8' => '001390',
			    'W0013-9' => '001391',
			    'W0014-01' => '001392',
			    'W0014-02' => '001393',
			    'W0014-03' => '001394',
			    'W0014-1' => '001395',
			    'W0014-2' => '001396',
			    'W0014-3' => '001397',
			    'W0015-01' => '001398',
			    'W0015-02' => '001399',
			    'W0015-03' => '001400',
			    'W0016-01' => '001401',
			    'W0016-02' => '001402',
			    'W0016-0202' => '001403',
			    'W0017-01' => '001404',
			    'W0017-02' => '001405',
			    'W0018-0101' => '001406',
			    'W0018-0102' => '001407',
			    'W0018-0103' => '001408',
			    'W0018-0104' => '001409',
			    'W0018-0201' => '001410',
			    'W0018-0202' => '001411',
			    'W0018-0203' => '001412',
			    'W0018-0204' => '001413',
			    'W0018-0205' => '001414',
			    'W0018-0301' => '001415',
			    'W0018-0302' => '001416',
			    'W0018-0303' => '001417',
			    'W0018-0304' => '001418',
			    'W0018-0401' => '001419',
			    'W0018-0402' => '001420',
			    'W0018-0403' => '001421',
			    'W0018-0404' => '001422',
			    'W0018-0405' => '001423',
			    'W0019-01' => '001424',
			    'W0019-02' => '001425',
			    'W0019-03' => '001426',
			    'W0019-0301' => '001427',
			    'W0019-0401' => '001428',
			    'W0019-0402' => '001429',
			    'W0019-0403' => '001430',
			    'W0019-0404' => '001431',
			    'W0019-0405' => '001432',
			    'W0019-0406' => '001433',
			    'W0020-0101' => '001434',
			    'W0020-0102' => '001435',
			    'W0020-0103' => '001436',
			    'W0020-0201' => '001437',
			    'W0020-0202' => '001438',
			    'W0020-0203' => '001439',
			    'W0020-0204' => '001440',
			    'W0020-0205' => '001441',
			    'W0021-01' => '001442',
			    'W0021-02' => '001443',
			    'W0022-01' => '001444',
			    'W0022-02' => '001445',
			    'W0022-03' => '001446',
			    'W0022-0401' => '001447',
			    'W0022-0402' => '001448',
			    'W0022-0403' => '001449',
			    'W0022-0404' => '001450',
			    'W0022-0405' => '001451',
			    'W0022-0406' => '001452',
			    'W0022-0407' => '001453',
			    'W0022-0408' => '001454',
			    'W0022-0409' => '001455',
			    'W0022-0410' => '001456',
			    'W0022-0411' => '001457',
			    'W0022-0412' => '001458',
			    'W0022-0413' => '001459',
			    'W0022-0414' => '001460',
			    'W0023-1' => '001461',
			    'W0023-10' => '001462',
			    'W0023-2' => '001463',
			    'W0023-3' => '001464',
			    'W0023-4' => '001465',
			    'W0023-5' => '001466',
			    'W0023-6' => '001467',
			    'W0023-7' => '001468',
			    'W0023-8' => '001469',
			    'W0023-9' => '001470',
			    'W0024-01' => '001471',
			    'W0024-0201' => '001472',
			    'W0024-0202' => '001473',
			    'W0024-0203' => '001474',
			    'W0024-0204' => '001475',
			    'W0024-0301' => '001476',
			    'W0024-0302' => '001477',
			    'W0024-0303' => '001478',
			    'W0024-0304' => '001479',
			    'W0024-0305' => '001480',
			    'W0024-0306' => '001481',
			    'W0024-0307' => '001482',
			    'W0025-01' => '001483',
			    'W0025-02' => '001484',
			    'W0025-03' => '001485',
			    'W0025-04' => '001486',
			    'W0025-05' => '001487',
			    'W0025-06' => '001488',
			    'W0026-01' => '001489',
			    'W0026-02' => '001490',
			    'W0026-03' => '001491',
			    'W0027-0101' => '001492',
			    'W0027-0102' => '001493',
			    'W0027-0103' => '001494',
			    'W0027-0104' => '001495',
			    'W0027-0105' => '001496',
			    'W0027-0106' => '001497',
			    'W0027-0107' => '001498',
			    'W0027-0108' => '001499',
			    'W0027-0109' => '001500',
			    'W0027-0110' => '001501',
			    'W0027-0201' => '001502',
			    'W0027-0202' => '001503',
			    'W0027-0203' => '001504',
			    'W0027-0204' => '001505',
			    'W0027-0205' => '001506',
			    'W0027-0206' => '001507',
			    'W0028-01' => '001508',
			    'W0028-02' => '001509',
			    'W0028-03' => '001510',
			    'W0028-04' => '001511',
			    'W0028-05' => '001512',
			    'W0028-06' => '001513',
			    'W0028-07' => '001514',
			    'W0028-08' => '001515',
			    'W0028-09' => '001516',
			    'W0029-0101' => '001517',
			    'W0029-0102' => '001518',
			    'W0029-0201' => '001519',
			    'W0029-0202' => '001520',
			    'W0029-0203' => '001521',
			    'W0029-0301' => '001522',
			    'W0029-0302' => '001523',
			    'W0029-0303' => '001524',
			    'W0029-0401' => '001525',
			    'W0029-0402' => '001526',
			    'W0029-0403' => '001527',
			    'W0029-0404' => '001528',
			    'W0029-0405' => '001529',
			    'W0029-0501' => '001530',
			    'W0029-0502' => '001531',
			    'W0029-0503' => '001532',
			    'W0029-0504' => '001533',
			    'W0029-0505' => '001534',
			    'W0029-0601' => '001535',
			    'W0029-0602' => '001536',
			    'W0029-0603' => '001537',
			    'W0029-0604' => '001538',
			    'W0029-0605' => '001539',
			    'W0029-0701' => '001540',
			    'W0029-0702' => '001541',
			    'W0029-0703' => '001542',
			    'W0029-0704' => '001543',
			    'W0029-0705' => '001544',
			    'W0029-0801' => '001545',
			    'W0029-0802' => '001546',
			    'W0029-0803' => '001547',
			    'W0030-1' => '001548',
			    'W0030-2' => '001549',
			    'W0030-3' => '001550',
			    'W0030-4' => '001551',
			    'W0030-5' => '001552',
			    'W0030-6' => '001553',
			    'W0031-0101' => '001554',
			    'W0031-0102' => '001555',
			    'W0031-0103' => '001556',
			    'W0031-0104' => '001557',
			    'W0031-0105' => '001558',
			    'W0031-0201' => '001559',
			    'W0031-0202' => '001560',
			    'W0031-0203' => '001561',
			    'W0031-0204' => '001562',
			    'W0031-0205' => '001563',
			    'W0032-01' => '001564',
			    'W0032-02' => '001565',
			    'W0032-03' => '001566',
			    'W0032-04' => '001567',
			    'W0032-05' => '001568',
			    'W0032-06' => '001569',
			    'W0032-07' => '001570',
			    'W0032-08' => '001571',
			    'W0032-09' => '001572',
			    'W0032-10' => '001573',
			    'W0032-11' => '001574',
			    'W0032-12' => '001575',
			    'W0032-13' => '001576',
			    'W0032-14' => '001577',
			    'W0032-15' => '001578',
			    'W0032-16' => '001579',
			    'W0032-1701' => '001580',
			    'W0032-1702' => '001581',
			    'W0032-1703' => '001582',
			    'W0032-1704' => '001583',
			    'W0032-1705' => '001584',
			    'W0033-0101' => '001585',
			    'W0033-0102' => '001586',
			    'W0033-0103' => '001587',
			    'W0033-0104' => '001588',
			    'W0033-0201' => '001589',
			    'W0033-0202' => '001590',
			    'W0033-0203' => '001591',
			    'W0033-0204' => '001592',
			    'W0033-0301' => '001593',
			    'W0033-0302' => '001594',
			    'W0033-0303' => '001595',
			    'W0033-0304' => '001596',
			    'W0033-0305' => '001597',
			    'W0033-0306' => '001598',
			    'W0033-0401' => '001599',
			    'W0033-0402' => '001600',
			    'W0033-0403' => '001601',
			    'W0033-0404' => '001602',
			    'W0033-0501' => '001603',
			    'W0033-0502' => '001604',
			    'W0033-0503' => '001605',
			    'W0033-0504' => '001606',
			    'W0033-0601' => '001607',
			    'W0033-0602' => '001608',
			    'W0033-0603' => '001609',
			    'W0033-0604' => '001610',
			    'W0033-0605' => '001611',
			    'W0033-0606' => '001612',
			    'W0034-01' => '001613',
			    'W0034-02' => '001614',
			    'W0034-03' => '001615',
			    'W0034-04' => '001616',
			    'W0034-05' => '001617',
			    'W0034-06' => '001618',
			    'W0034-07' => '001619',
			    'W0034-08' => '001620',
			    'W0035-0101' => '001621',
			    'W0035-0102' => '001622',
			    'W0035-0103' => '001623',
			    'W0035-0104' => '001624',
			    'W0035-0105' => '001625',
			    'W0035-0106' => '001626',
			    'W0035-0201' => '001627',
			    'W0035-0202' => '001628',
			    'W0035-0203' => '001629',
			    'W0035-0301' => '001630',
			    'W0035-0302' => '001631',
			    'W0035-0401' => '001632',
			    'W0035-0402' => '001633',
			    'W0035-0501' => '001634',
			    'W0035-0502' => '001635',
			    'W0035-0601' => '001636',
			    'W0035-0602' => '001637',
			    'W0035-0603' => '001638',
			    'W0035-0701' => '001639',
			    'W0035-0702' => '001640',
			    'W0035-0703' => '001641',
			    'W0035-0801' => '001642',
			    'W0035-0802' => '001643',
			    'W0035-0803' => '001644',
			    'W0035-0901' => '001645',
			    'W0035-0902' => '001646',
			    'W0035-0903' => '001647',
			    'W0036-01' => '001648',
			    'W0036-02' => '001649',
			    'W0036-03' => '001650',
			    'W0036-04' => '001651',
			    'W0037-0101' => '001652',
			    'W0037-0102' => '001653',
			    'W0037-0103' => '001654',
			    'W0037-0104' => '001655',
			    'W0037-0105' => '001656',
			    'W0037-0106' => '001657',
			    'W0037-0201' => '001658',
			    'W0037-0202' => '001659',
			    'W0037-0203' => '001660',
			    'W0037-0204' => '001661',
			    'W0037-0205' => '001662',
			    'W0037-0206' => '001663',
			    'W0037-0301' => '001664',
			    'W0037-0302' => '001665',
			    'W0037-0303' => '001666',
			    'W0037-0304' => '001667',
			    'W0037-0305' => '001668',
			    'W0037-0306' => '001669',
			    'W0037-0401' => '001670',
			    'W0037-0402' => '001671',
			    'W0037-0403' => '001672',
			    'W0037-0404' => '001673',
			    'W0037-0405' => '001674',
			    'W0037-0406' => '001675',
			    'W0037-0501' => '001676',
			    'W0037-0502' => '001677',
			    'W0037-0503' => '001678',
			    'W0037-0504' => '001679',
			    'W0037-0505' => '001680',
			    'W0037-0506' => '001681',
			    'W0037-0601' => '001682',
			    'W0037-0602' => '001683',
			    'W0037-0603' => '001684',
			    'W0037-0604' => '001685',
			    'W0037-0605' => '001686',
			    'W0037-0606' => '001687',
			    'W0037-0701' => '001688',
			    'W0037-0702' => '001689',
			    'W0037-0703' => '001690',
			    'W0038-01' => '001691',
			    'W0038-02' => '001692',
			    'W0038-03' => '001693',
			    'W0038-04' => '001694',
			    'W0038-05' => '001695',
			    'W0038-06' => '001696',
			    'W0038-07' => '001697',
			    'W0038-08' => '001698',
			    'W0039-01' => '001699',
			    'W0039-02' => '001700',
			    'W0039-03' => '001701',
			    'W0039-04' => '001702',
			    'W0039-05' => '001703',
			    'W0039-06' => '001704',
			    'W0039-07' => '001705',
			    'W0039-08' => '001706',
			    'W0039-09' => '001707',
			    'W0039-10' => '001708',
			    'W0039-1101' => '001709',
			    'W0039-1102' => '001710',
			    'W0039-1103' => '001711',
			    'W0039-1104' => '001712',
			    'W0039-1105' => '001713',
			    'W0039-1106' => '001714',
			    'W0040-01' => '001715',
			    'W0040-02' => '001716',
			    'W0040-03' => '001717',
			    'W0040-04' => '001718',
			    'W0040-05' => '001719',
			    'W0040-06' => '001720',
			    'W0040-07' => '001721',
			    'W0040-08' => '001722',
			    'W0040-09' => '001723',
			    'W0040-10' => '001724',
			    'W0040-11' => '001725',
			    'W0040-12' => '001726',
			    'W0040-13' => '001727',
			    'W0040-14' => '001728',
			    'W0040-15' => '001729',
			    'W0040-16' => '001730',
			    'W0040-17' => '001731',
			    'W0040-18' => '001732',
			    'W0040-19' => '001733',
			    'W0101-1' => '001734',
			    'W0101-2' => '001735',
			    'W0101-3' => '001736',
			    'W0103-1' => '001737',
			    'W0103-2' => '001738',
			    'W0103-3' => '001739',
			    'W0103-4' => '001740',
			    'W0103-5' => '001741',
			    'W0104-1' => '001742',
			    'W0104-2' => '001743',
			    'W0104-3' => '001744',
			    'W0104-4' => '001745',
			    'W0104-5' => '001746',
			    'W0104-6' => '001747',
			    'W0104-7' => '001748',
			    'W0104-8' => '001749',
			    'W0104-9' => '001750',
			    'W0104-10' => '001751',
			    'W0104-11' => '001752',
			    'W0104-12' => '001753',
			    'W0104-13' => '001754',
			    'W0104-14' => '001755',
			    'W0105-1' => '001756',
			    'W0105-2' => '001757',
			    'W0106-1' => '001758',
			    'W0106-2' => '001759',
			    'W0106-3' => '001760',
			    'W0106-4' => '001761',
			    'W0106-5' => '001762',
			    'W0107-1' => '001763',
			    'W0107-2' => '001764',
			    'W0107-3' => '001765',
			    'W0107-4' => '001766',
			    'W0107-5' => '001767',
			    'W0107-6' => '001768',
			    'W0107-7' => '001769',
			    'W0107-8' => '001770',
			    'W0107-9' => '001771',
			    'W0107-10' => '001772',
			    'W0109-1' => '001773',
			    'W0109-2' => '001774',
			    'W0109-3' => '001775',
			    'W0109-4' => '001776',
			    'W0109-5' => '001777',
			    'W0109-6' => '001778',
			    'W0109-7' => '001779',
			    'W0111-1' => '001780',
			    'W0111-2' => '001781',
			    'W0111-3' => '001782',
			    'W0112-1' => '001783',
			    'W0112-2' => '001784',
			    'W0113-1' => '001785',
			    'W0113-2' => '001786',
			    'W0113-3' => '001787',
			    'W0114-1' => '001788',
			    'W0114-2' => '001789',
			    'W0114-3' => '001790',
			    'W0114-4' => '001791',
			    'W0114-5' => '001792',
			    'W0115-1' => '001793',
			    'W0115-2' => '001794',
			    'W0115-3' => '001795',
			    'W0115-4' => '001796',
			    'W0115-5' => '001797',
			    'W0116-1' => '001798',
			    'W0116-2' => '001799',
			    'W0116-3' => '001800',
			    'W0116-4' => '001801',
			    'W0116-5' => '001802',
			    'W0116-6' => '001803',
			    'W0116-7' => '001804',
			    'W0116-8' => '001805',
			    'W0116-9' => '001806',
			    'W0117-1' => '001807',
			    'W0117-2' => '001808',
			    'W0117-3' => '001809',
			    'W0117-4' => '001810',
			    'W0117-5' => '001811',
			    'W0117-6' => '001812',
			    'W0117-7' => '001813',
			    'W0117-8' => '001814',
			    'W0117-9' => '001815',
			    'W0117-10' => '001816',
			    'W0117-11' => '001817',
			    'W0117-12' => '001818',
			    'W0117-13' => '001819',
			    'W0117-14' => '001820',
			    'W0117-15' => '001821',
			    'W0117-16' => '001822',
			    'W0117-17' => '001823',
			    'W0118-1' => '001824',
			    'W0118-2' => '001825',
			    'W0118-3' => '001826',
			    'W0120-1' => '001827',
			    'W0120-2' => '001828',
			    'W0120-3' => '001829',
			    'W0120-4' => '001830',
			    'W0120-5' => '001831',
			    'W0120-6' => '001832',
			    'W0121-1' => '001833',
			    'W0121-2' => '001834',
			    'W0122-1' => '001835',
			    'W0122-2' => '001836',
			    'W0122-3' => '001837',
			    'W0122-4' => '001838',
			    'W0122-5' => '001839',
			    'W0123-1' => '001840',
			    'W0123-2' => '001841',
			    'W0123-3' => '001842',
			    'W0123-4' => '001843',
			    'W0123-5' => '001844',
			    'W0123-6' => '001845',
			    'W0123-7' => '001846',
			    'W0124-1' => '001847',
			    'W0124-2' => '001848',
			    'W0124-3' => '001849',
			    'W0124-4' => '001850',
			    'W0124-5' => '001851',
			    'W0124-6' => '001852',
			    'W0124-7' => '001853',
			    'W0124-8' => '001854',
			    'W0126-1' => '001855',
			    'W0126-2' => '001856',
			    'W0126-3' => '001857',
			    'W0126-4' => '001858',
			    'W0126-5' => '001859',
			    'W0126-6' => '001860',
			    'W0126-7' => '001861',
			    'W0126-8' => '001862',
			    'W0127-1' => '001863',
			    'W0127-2' => '001864',
			    'W0127-3' => '001865',
			    'W0127-4' => '001866',
			    'W0127-5' => '001867',
			    'W0127-6' => '001868',
			    'W0127-7' => '001869',
			    'W0129' => '001870',
			    'W0130-1' => '001871',
			    'W0130-2' => '001872',
			    'W0130-3' => '001873',
			    'W0130-4' => '001874',
			    'W0130-5' => '001875',
			    'W0130-6' => '001876',
			    'W0130-7' => '001877',
			    'W0130-8' => '001878',
			    'W0131-1' => '001879',
			    'W0131-2' => '001880',
			    'W0131-3' => '001881',
			    'W0131-4' => '001882',
			    'W0131-5' => '001883',
			    'W0131-6' => '001884',
			    'W0132-1' => '001885',
			    'W0132-2' => '001886',
			    'W0132-3' => '001887',
			    'W0132-4' => '001888',
			    'W0133-1' => '001889',
			    'W0133-2' => '001890',
			    'W0133-3' => '001891',
			    'W0133-4' => '001892',
			    'W0133-5' => '001893',
			    'W0133-6' => '001894',
			    'W0134-1' => '001895',
			    'W0134-2' => '001896',
			    'W0134-3' => '001897',
			    'W0134-4' => '001898',
			    'W0134-5' => '001899',
			    'W0135-1' => '001900',
			    'W0135-2' => '001901',
			    'W0135-3' => '001902',
			    'W0135-4' => '001903',
			    'W0136-1' => '001904',
			    'W0136-2' => '001905',
			    'W0136-3' => '001906',
			    'W0136-4' => '001907',
			    'W0137-1' => '001908',
			    'W0137-2' => '001909',
			    'W0137-3' => '001910',
			    'W0137-4' => '001911',
			    'W0138-1' => '001912',
			    'W0138-2' => '001913',
			    'W0138-3' => '001914',
			    'W0138-4' => '001915',
			    'W0138-5' => '001916',
			    'W0138-6' => '001917',
			    'W0139-1' => '001918',
			    'W0139-2' => '001919',
			    'W0139-3' => '001920',
			    'W0139-4' => '001921',
			    'W0140-1' => '001922',
			    'W0140-2' => '001923',
			    'W0140-3' => '001924',
			    'W0140-4' => '001925',
			    'W0140-5' => '001926',
			    'W0140-6' => '001927',
			    'W0140-7' => '001928',
			    'W0140-8' => '001929',
			    'W0140-9' => '001930',
			    'W0140-10' => '001931',
			    'W0140-11' => '001932',
			    'W0140-12' => '001933',
			    'W0140-13' => '001934',
			    'W0140-14' => '001935',
			    'W0140-15' => '001936',
			    'W0140-16' => '001937',
			    'W0140-17' => '001938',
			    'W0140-18' => '001939',
			    'W0140-19' => '001940',
			    'W0141-1' => '001941',
			    'W0141-2' => '001942',
			    'W0141-3' => '001943',
			    'W0141-4' => '001944',
			    'W0142-1' => '001945',
			    'W0142-2' => '001946',
			    'W0143-1' => '001947',
			    'W0143-2' => '001948',
			    'W0143-3' => '001949',
			    'W0143-4' => '001950',
			    'W0143-5' => '001951',
			    'W0143-6' => '001952',
			    'W0143-7' => '001953',
			    'W0143-8' => '001954',
			    'W0143-9' => '001955',
			    'W0143-10' => '001956',
			    'W0143-11' => '001957',
			    'W0143-12' => '001958',
			    'W0143-13' => '001959',
			    'W0145-1' => '001960',
			    'W0145-2' => '001961',
			    'W0145-3' => '001962',
			    'W0145-4' => '001963',
			    'W0145-5' => '001964',
			    'W0145-6' => '001965',
			    'W0145-7' => '001966',
			    'W0146-1' => '001967',
			    'W0146-2' => '001968',
			    'W0146-3' => '001969',
			    'W0146-4' => '001970',
			    'W0146-5' => '001971',
			    'W0146-6' => '001972',
			    'W0147-1' => '001973',
			    'W0147-2' => '001974',
			    'W0147-3' => '001975',
			    'W0147-4' => '001976',
			    'W0148-1' => '001977',
			    'W0148-2' => '001978',
			    'W0148-3' => '001979',
			    'W0149-1' => '001980',
			    'W0149-2' => '001981',
			    'W0149-3' => '001982',
			    'W0149-4' => '001983',
			    'W0149-5' => '001984',
			    'W0150-1' => '001985',
			    'W0150-2' => '001986',
			    'W0152-1' => '001987',
			    'W0152-2' => '001988',
			    'W0152-3' => '001989',
			    'W0153-1' => '001990',
			    'W0153-2' => '001991',
			    'W0153-3' => '001992',
			    'W0153-4' => '001993',
			    'W0153-5' => '001994',
			    'W0153-6' => '001995',
			    'W0153-7' => '001996',
			    'W0153-8' => '001997',
			    'W0153-9' => '001998',
			    'W0153-10' => '001999',
			    'W0153-11' => '002000',
			    'W0153-12' => '002001',
			    'W0154-1' => '002002',
			    'W0154-2' => '002003',
			    'W0155-1' => '002004',
			    'W0155-2' => '002005',
			    'W0155-3' => '002006',
			    'W0155-4' => '002007',
			    'W0155-5' => '002008',
			    'W0156-1' => '002009',
			    'W0156-2' => '002010',
			    'W0156-3' => '002011',
			    'W0156-4' => '002012',
			    'W0156-5' => '002013',
			    'W0156-6' => '002014',
			    'W0156-7' => '002015',
			    'W0156-8' => '002016',
			    'W0156-9' => '002017',
			    'W0156-10' => '002018',
			    'W0156-11' => '002019',
			    'W0156-12' => '002020',
			    'W0156-13' => '002021',
			    'W0156-14' => '002022',
			    'W0156-15' => '002023',
			    'W0156-16' => '002024',
			    'W0157-1' => '002025',
			    'W0157-2' => '002026',
			    'W0157-3' => '002027',
			    'W0157-4' => '002028',
			    'W0157-5' => '002029',
			    'W0157-6' => '002030',
			    'W0157-7' => '002031',
			    'W0157-8' => '002032',
			    'W0157-9' => '002033',
			    'W0157-10' => '002034',
			    'W0158-1' => '002035',
			    'W0158-2' => '002036',
			    'W0158-3' => '002037',
			    'W0158-4' => '002038',
			    'W0158-5' => '002039',
			    'W0158-6' => '002040',
			    'W0158-7' => '002041',
			    'W0158-8' => '002042',
			    'W0158-10' => '002043',
			    'W0158-11' => '002044',
			    'W0158-12' => '002045',
			    'W0158-13' => '002046',
			    'W0158-14' => '002047',
			    'W0158-15' => '002048',
			    'W0159-1' => '002049',
			    'W0159-2' => '002050',
			    'W0159-3' => '002051',
			    'W0159-4' => '002052',
			    'W0159-5' => '002053',
			    'W0159-6' => '002054',
			    'W0159-7' => '002055',
			    'W0159-8' => '002056',
			    'W0159-9' => '002057',
			    'W0159-10' => '002058',
			    'W0159-11' => '002059',
			    'W0159-12' => '002060',
			    'W0159-13' => '002061',
			    'W0159-14' => '002062',
			    'W0159-15' => '002063',
			    'W0159-16' => '002064',
			    'W0159-17' => '002065',
			    'W0159-18' => '002066',
			    'W0159-19' => '002067',
			    'W0159-20' => '002068',
			    'W0159-21' => '002069',
			    'W0160-1' => '002070',
			    'W0160-2' => '002071',
			    'W0161-1' => '002072',
			    'W0161-2' => '002073',
			    'W0161-3' => '002074',
			    'W0162-1' => '002075',
			    'W0162-2' => '002076',
			    'W0162-3' => '002077',
			    'W0163-1' => '002078',
			    'W0163-2' => '002079',
			    'W0163-3' => '002080',
			    'W0163-4' => '002081',
			    'W0163-5' => '002082',
			    'W0164-1' => '002083',
			    'W0164-2' => '002084',
			    'W0164-3' => '002085',
			    'W0164-4' => '002086',
			    'W0164-5' => '002087',
			    'W0164-6' => '002088',
			    'W0164-7' => '002089',
			    'W0164-8' => '002090',
			    'W0164-9' => '002091',
			    'W0164-10' => '002092',
			    'W0164-11' => '002093',
			    'W0164-12' => '002094',
			    'W0164-13' => '002095',
			    'W0164-14' => '002096',
			    'W0165-1' => '002097',
			    'W0165-2' => '002098',
			    'W0165-3' => '002099',
			    'W0165-4' => '002100',
			    'W0165-5' => '002101',
			    'W0165-6' => '002102',
			    'W0165-7' => '002103',
			    '00001' => '002239',
			    '00002' => '002240',
			    '00003' => '002241',
			    '00004' => '002242',
			    '00005' => '002243',
			    '00006' => '002244',
			    '00007' => '002245',
			    '00008' => '002246',
			    '00009' => '002247',
			    '00010' => '002248',
			    '00011' => '002249',
			    '00012' => '002250',
			    '00013' => '002251',
			    '00014' => '002252',
			    '00015' => '002253',
			    '00016' => '002254',
			    '00017' => '002255',
			    '00018' => '002256',
			    '00019' => '002257',
			    '00020' => '002258',
			    '00021' => '002259',
			    '00022' => '002260',
			    '00023' => '002261',
			    '00024' => '002262',
			    '00025' => '002263',
			    '00026' => '002264',
			    '00027' => '002265',
			    '00028' => '002266',
			    '00029' => '002267',
			    '00030' => '002268',
			    '00031' => '002269',
			    '00032' => '002270',
			    '00033' => '002271',
			    '00034' => '002272',
			    '00035' => '002273',
			    '00036' => '002274',
			    '00037' => '002275',
			    '00038' => '002276',
			    '00039' => '002277',
			    '00040' => '002278',
			    '00041' => '002279',
			    '00042' => '002280',
			    '00043' => '002281',
			    '00044' => '002282',
			    '00045' => '002283',
			    '00046' => '002284',
			    '00047' => '002285',
			    '00048' => '002286',
			    '00049' => '002287',
			    '00050' => '002288',
			    '00051' => '002289',
			    '00052' => '002290',
			    '00053' => '002291',
			    '00054' => '002292',
			    '00055' => '002293',
			    '00056' => '002294',
			    '00057' => '002295',
			    '00058' => '002296',
			    '00059' => '002297',
			    '00060' => '002298',
			    '00061' => '002299',
			    '00062' => '002300',
			    '00063' => '002301',
			    '00064' => '002302',
			    '00065' => '002303',
			    '00066' => '002304',
			    '00067' => '002305',
			    '00068' => '002306',
			    '00069' => '002307',
			    '00070' => '002308',
			    '00071' => '002309',
			    '00072' => '002310',
			    '00073' => '002311',
			    '00074' => '002312',
			    '00075' => '002313',
			    '00076' => '002314',
			    '00077' => '002315',
			    '00078' => '002316',
			    '00079' => '002317',
			    '00080' => '002318',
			    '00081' => '002319',
			    '00082' => '002320',
			    '00083' => '002321',
			    '00084' => '002322',
			    '00085' => '002323',
			    '00086' => '002324',
			    '00087' => '002325',
			    '00088' => '002326',
			    '00089' => '002327',
			    '00090' => '002328',
			    '00091' => '002329',
			    '00092' => '002330',
			    '00093' => '002331',
			    '00094' => '002332',
			    '00095' => '002333',
			    '00096' => '002334',
			    '00097' => '002335',
			    '00098' => '002336',
			    '00099' => '002337',
			    '00100' => '002338',
			    '00101' => '002339',
			    '00102' => '002340',
			    '00103' => '002341',
			    '00104' => '002342',
			    '00171' => '002343',
			    '00172' => '002344',
			    '00173' => '002345',
			    '00174' => '002346', 
			    'watch-039879' => '000001',
			    'watch-040107' => '000002',
			    'watch-040109' => '000003',
			    'watch-040110' => '000004',
			    'watch-040111' => '000005',
			    'watch-040112' => '000006',
			    'watch-040113' => '000007',
			    'watch-040114' => '000008',
			    'watch-040115' => '000009',
			    'watch-040116' => '000010',
			    'watch-040117' => '000011',
			    'watch-040118' => '000012',
			    'watch-040119' => '000013',
			    'watch-040120' => '000014',
			    'watch-040121' => '000015',
			    'watch-040122' => '000016',
			    'watch-040123' => '000017',
			    'watch-040124' => '000018',
			    'watch-040125' => '000019',
			    'watch-040126' => '000020',
			    'watch-040127' => '000021',
			    'watch-040128' => '000022',
			    'watch-040129' => '000023',
			    'watch-040130' => '000024',
			    'watch-040131' => '000025',
			    'watch-040132' => '000026',
			    'watch-040133' => '000027',
			    'watch-040134' => '000028',
			    'watch-040135' => '000029',
			    'watch-040136' => '000030',
			    'watch-040137' => '000031',
			    'watch-040138' => '000032',
			    'watch-40139' => '000033',
			    'watch-040140' => '000034',
			    'watch-040141' => '000035',
			    'watch-040142' => '000036',
			    'watch-040143' => '000037',
			    'watch-040144' => '000038',
			    'watch-040145' => '000039',
			    'watch-040146' => '000040',
			    'watch-040148' => '000042',
			    'watch-040149' => '000043',
			    'watch-040150' => '000044',
			    'watch-040151' => '000045',
			    'watch-040152' => '000046',
			    'watch-040153' => '000047',
			    'watch-040155' => '000048',
			    'watch-040156' => '000049',
			    'watch-040158' => '000050',
			    'watch-040159' => '000052',
			    'watch-040160' => '000053',
			    'watch-040161' => '000054',
			    'watch-040162' => '000055',
			    'watch-040163' => '000056',
			    'watch-040164' => '000057',
			    'watch-040165' => '000058',
			    'watch-040166' => '000059',
			    'gd011' => '000642',
			    'gd012' => '000645',
			    'gd013' => '000649',
			    'gd017' => '000651',
			    'gd018' => '000652',
			    'gd019' => '000653',
			    'gd020' => '000654',
			    'gd021' => '000655',
			    'gd022' => '000656',
			    'gd023' => '000657',
			    'gd024' => '000658',
			    'gd025' => '000659',
			    'gd026' => '000660',
			    'gd027' => '000661',
			    'gd028' => '000662',
			    'gd029' => '000663',
			    'gd030' => '000664',
			    'gd031' => '000665',
			    'gd032' => '000666',
			    'gd033' => '000667',
			    'gd034' => '000668',
			    'gd035' => '000669',
			    'gd036' => '000670',
			    'gd037' => '000671',
			    'gd038' => '000672',
			    'gd039' => '000673',
			    'gd040' => '000674',
			    'gd041' => '000675',
			    'gd042' => '000676',
			    'gd043' => '000677',
			    'gd044' => '000678',
			    'gd045' => '000679',
			    'gd046' => '000680',
			    'gd047' => '000681',
			    'gd048' => '000682',
			    'gd049' => '000683',
			    'gd050' => '000684',
			    'gd051' => '000685',
			    'gd052' => '000686',
			    'gd053' => '000687',
			    'gd054' => '000688',
			    'gd055' => '000689',
			    'gd056' => '000690',
			    'gd057' => '000691',
			    'gd058' => '000692',
			    'gd059' => '000693',
			    'gd060' => '000694',
			    'M0001-46' => '000696',
			    'M0001-47' => '000697',
			    'M0001-48' => '000698',
			    'M0002-01' => '000699',
			    'M0002-02' => '000700',
			    'M0002-03' => '000701',
			    'M0002-04' => '000702',
			    'M0002-05' => '000703',
			    'M0002-06' => '000704',
			    'M0002-07' => '000705',
			    'M0002-08' => '000706',
			    'M0002-09' => '000707',
			    'M0002-10' => '000708',
			    'M0002-11' => '000709',
			    'M0002-12' => '000710',
			    'M0002-13' => '000711',
			    'M0002-14' => '000712',
			    'M0002-15' => '000713',
			    'M0002-16' => '000714',
			    'M0002-17' => '000715',
			    'M0002-18' => '000716',
			    'M0002-19' => '000717',
			    'M0002-20' => '000718',
			    'M0002-21' => '000719',
			    'M0002-22' => '000720',
			    'M0002-23' => '000721',
			    'M0002-24' => '000722',
			    '10501' => '000939',
			    '12101' => '000955',
			    '13602' => '000962',
			    '141' => '000970',
			    '147' => '000983',
			    '148' => '000985',
			    '149' => '000991',
			    '15301' => '001000',
			    '15801' => '001005',
			    '16101' => '001008',
			    '166' => '001013',
			    '1' => '002239',
			    '2' => '002240',
			    '3' => '002241',
			    '4' => '002242',
			    'W0100' => '000001',
			    'W0099' => '000002',
			    'W0098' => '000003',
			    'W0097' => '000004',
			    'W0096' => '000005',
			    'W0095' => '000006',
			    'W0094' => '000007',
			    'W0093' => '000008',
			    'W0092' => '000009',
			    'W0091' => '000010',
			    'W0090' => '000011',
			    'W0089' => '000012',
			    'W0088' => '000013',
			    'W0087' => '000014',
			    'W0086' => '000015',
			    'W0085' => '000016',
			    'W0084' => '000017',
			    'W0083' => '000018',
			    'W0082' => '000019',
			    'W0081' => '000020',
			    'W0080' => '000021',
			    'W0079' => '000022',
			    'W0078' => '000023',
			    'W0076' => '000025',
			    'W0075' => '000026',
			    'W0074' => '000027',
			    'W0073' => '000028',
			    'W0072' => '000029',
			    'W0071' => '000030',
			    'W0070' => '000031',
			    'W0069' => '000032',
			    'W0068' => '000033',
			    'W0067' => '000034',
			    'W0066' => '000035',
			    'W0065' => '000036',
			    'W0064' => '000037',
			    'W0063' => '000038',
			    'W0062' => '000039',
			    'W0061' => '000040',
			    'W0060' => '000041',
			    'W0059' => '000042',
			    'W0058' => '000043',
			    'W0057' => '000044',
			    'W0056' => '000045',
			    'W0055' => '000046',
			    'W0054' => '000047',
			    'W0052' => '000048',
			    'W0051' => '000049',
			    'W0050' => '000050',
			    'W0049' => '000051',
			    'W0048' => '000052',
			    'W0047' => '000053',
			    'W0046' => '000054',
			    'W0045' => '000055',
			    'W0044' => '000056',
			    'W0043' => '000057',
			    'W0042' => '000058',
			    'W0041' => '000059',
			    'gd014' => '000649',
			    'M0001-01' => '000651',
			    'M0001-02' => '000652',
			    'M0001-03' => '000653',
			    'M0001-04' => '000654',
			    'M0001-05' => '000655',
			    'M0001-06' => '000656',
			    'M0001-07' => '000657',
			    'M0001-08' => '000658',
			    'M0001-09' => '000659',
			    'M0001-10' => '000660',
			    'M0001-11' => '000661',
			    'M0001-12' => '000662',
			    'M0001-13' => '000663',
			    'M0001-14' => '000664',
			    'M0001-15' => '000665',
			    'M0001-17' => '000666',
			    'M0001-18' => '000667',
			    'M0001-19' => '000668',
			    'M0001-20' => '000669',
			    'M0001-21' => '000670',
			    'M0001-22' => '000671',
			    'M0001-23' => '000672',
			    'M0001-24' => '000673',
			    'M0001-25' => '000674',
			    'M0001-26' => '000675',
			    'M0001-27' => '000676',
			    'M0001-28' => '000677',
			    'M0001-29' => '000678',
			    'M0001-30' => '000679',
			    'M0001-31' => '000680',
			    'M0001-32' => '000681',
			    'M0001-33' => '000682',
			    'M0001-34' => '000683',
			    'M0001-35' => '000684',
			    'M0001-36' => '000685',
			    'M0001-37' => '000686',
			    'M0001-38' => '000687',
			    'M0001-39' => '000688',
			    'M0001-40' => '000689',
			    'M0001-41' => '000690',
			    'M0001-42' => '000691',
			    'M0001-43' => '000692',
			    'M0001-44' => '000693',
			    'M0001-45' => '000694',
			    'gd064' => '000698',
			    'Temperature Change 01' => '000699',
			    'Temperature Change 02' => '000700',
			    'Temperature Change 03' => '000701',
			    'Temperature Change 04' => '000702',
			    'Temperature Change 05' => '000703',
			    'Temperature Change 06' => '000704',
			    'Temperature Change 07' => '000705',
			    'Temperature Change 08' => '000706',
			    'Temperature Change 09' => '000707',
			    'Temperature Change 10' => '000708',
			    'Temperature Change 11' => '000709',
			    'Temperature Change 12' => '000710',
			    'Temperature Change 13' => '000711',
			    'Temperature Change 14' => '000712',
			    'Temperature Change 15' => '000713',
			    'Temperature Change 16' => '000714',
			    'Temperature Change 17' => '000715',
			    'Temperature Change 18' => '000716',
			    'Temperature Change 19' => '000717',
			    'Temperature Change 20' => '000718',
			    'Temperature Change 21' => '000719',
			    'Temperature Change 22' => '000720',
			    'Temperature Change 23' => '000721',
			    'Temperature Change 24' => '000722',
			    'Temperature Change base' => '000723',
			    'Temperature Change top' => '000724',
				'M0004-01' => '000639',
			    'M0004-02' => '000640',
			    'M0004-03' => '000641',
			    'M0004-04' => '000642',
			    'M0004-05' => '000643',
			    'M0004-06' => '000644',
			    'M0004-07' => '000645',
			    'M0004-08' => '000646',
			    'M0004-09' => '000647',
			    'M0004-10' => '000648',
			    'M0004-11' => '000649',
			    'M0004-12' => '000650',		
		);
		if (array_key_exists($sku, $array))
		{
			$sku = $array[$sku];
		}
		return $sku;
	}
	
	static function downfile($fileurl, $name)
	{
		$filename=$fileurl;
		$file  =  fopen($filename, "rb"); 
		Header( "Content-type:  application/octet-stream "); 
		Header( "Accept-Ranges:  bytes "); 
		Header( "Content-Disposition:  attachment;  filename= ".$name.".dbf"); 
		$contents = "";
		while (!feof($file)) {
			$contents .= fread($file, 8192);
		}
		echo $contents;
		fclose($file);
		exit;
//		ob_start(); 
//		$filename=$fileurl;
//		$date=$name;
//		header( "Content-type:  application/octet-stream "); 
//		header( "Accept-Ranges:  bytes "); 
//		header( "Content-Disposition:  attachment;  filename= {$date}.dbf"); 
//		$size=readfile($filename);
//		header( "Accept-Length: " .$size);
	}
	
	static function bin2bstr($input)
	// Convert a binary expression_r(e.g., "100111") into a binary-string
	{
		if (!is_string($input)) return null; // Sanity check
		// Pack into a string
		$input = str_split($input, 4);
		$str = '';
		foreach ($input as $v)
		{
			$str .= base_convert($v, 2, 16);
		}
		$str = pack('H*', $str);
		return $str;
	}
	
	static function bstr2bin($input)
	// Binary representation of a binary-string
	{
		if (!is_string($input)) return null; // Sanity check
		
		// Unpack as a hexadecimal string
		$value = unpack('H*', $input);
		// Output binary representation
		$value = str_split($value[1], 1);
		$bin = '';
		foreach ($value as $v)
		{
			$b = str_pad(base_convert($v, 16, 2), 4, '0', STR_PAD_LEFT);
			$bin .= $b;
		}
		return $bin;
	}
	
 	static function binary_to_file($file)
 	{
       // $content = $GLOBALS['HTTP_RAW_POST_DATA'];  // 需要php.ini设置
        if(empty($content)){
            $content = file_get_contents('php://input');    // 不需要php.ini设置，内存压力小
        }
        $ret = file_put_contents($file, $content, true);
        return $ret;
    }
}