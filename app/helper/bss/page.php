<?php
/**
 * 分页类
 *
 * $Id:$
 */

class Helper_BSS_Page
{
	private $_pagination = null;
	private $_url = null;
	
	public function __construct($pagination = null, $url=null)
	{
		if (is_array($pagination)) 
		{
			$this->_pagination = $pagination;
		}
		if (isset($url)) 
		{
			$this->_url = $url;
		}
	}
	
	public function getPage($intOffsetNum=5,$join='&',$pext='=')
	{
		if($this->_pagination['page_count'] <= $intOffsetNum*2)
		{
			$i = 1;
			$intOffsetNum = $this->_pagination['page_count']-$this->_pagination['current'];
		} else if($this->_pagination['current'] <= $intOffsetNum){
			$i = 1;
			$intOffsetNum = $intOffsetNum*2+1-$this->_pagination['current'];
		} else {
			$i = $this->_pagination['current']-$intOffsetNum;
		}
		$pageList = "<div class=\"viciao\">";
		if ($this->_pagination['current'] == 1)
		{
			$pageList .= '<span class="disabled">首页</span>';
			$pageList .= '<span class="disabled">< 上一页</span>';
		}
		else 
		{
			$pageList .= "<a href=\"{$this->_url}{$join}cp{$pext}1{$join}ps{$pext}{$this->_pagination['page_size']}\">首页</a>";
			$pageList .= "<a href=\"{$this->_url}{$join}cp{$pext}{$this->_pagination['prev']}{$join}ps{$pext}{$this->_pagination['page_size']}\">< 上一页</a>";
		}
		$to = (($this->_pagination['current']+$intOffsetNum) >= $this->_pagination['page_count'])?($this->_pagination['page_count']):($this->_pagination['current']+$intOffsetNum);
		for ($i;$i<=$to;$i++) 
		{
			if($i == $this->_pagination['current'])
			{
				$pageList .= '<span class="current">'.$i.'</span>';
			} else {
				$pageList .= "<a href=\"{$this->_url}{$join}cp{$pext}{$i}{$join}ps{$pext}{$this->_pagination['page_size']}\">{$i}</a>";
			}
		}
		if ($this->_pagination['next'] == $this->_pagination['current']) 
		{
			$pageList .= '<span class="disabled">下一页  ></span>';
			$pageList .= '<span class="disabled">末页</span>';
		}
		else 
		{
			$pageList .= "<a href=\"{$this->_url}{$join}cp{$pext}{$this->_pagination['next']}{$join}ps{$pext}{$this->_pagination['page_size']}\">下一页  ></a>";
			$pageList .= "<a href=\"{$this->_url}{$join}cp{$pext}{$this->_pagination['page_count']}{$join}ps{$pext}{$this->_pagination['page_size']}\">末页</a>";
		}
		$pageList .= "&nbsp;&nbsp;到第 <input name=\"cp\" type=\"text\" value=\"{$this->_pagination['current']}\" onchange=\"location.href='{$this->_url}{$join}cp{$pext}'+this.value+'{$join}ps{$pext}{$this->_pagination['page_size']}';return false;\" /> 页";
		$pageList .= "&nbsp;&nbsp;共 <b>".$this->_pagination['record_count']."</b> 条记录&nbsp;&nbsp;每页显示 ";
		$pageList .= "<input name=\"ps\" type=\"text\" value=\"{$this->_pagination['page_size']}\" onchange=\"location.href='{$this->_url}{$join}ps{$pext}'+this.value;return false;\" /> 条记录</div>";
		
		return $pageList;
	}
	
	public function __destruct()
	{
		
	}
}
?>