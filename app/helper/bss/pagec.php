<?php
/**
 * 分页类
 *
 * $Id:$
 */

class Helper_BSS_Pagec
{
	private $_pagination = null;
	private $_url = null;
	
	public function __construct($pagination = null, $url=null)
	{
		if (is_array($pagination)) 
		{
			$this->_pagination = $pagination;
		}
		if (isset($url)) 
		{
			$this->_url = $url;
		}
	}
	
	public function getPage($intOffsetNum=5,$join='&',$pext='=')
	{
		if($this->_pagination['page_count'] <= $intOffsetNum*2)
		{
			$i = 1;
			$intOffsetNum = $this->_pagination['page_count']-$this->_pagination['current'];
		} else if($this->_pagination['current'] <= $intOffsetNum){
			$i = 1;
			$intOffsetNum = $intOffsetNum*2+1-$this->_pagination['current'];
		} else {
			$i = $this->_pagination['current']-$intOffsetNum;
		}
		$pageList = "<div id=\"page\"><div class=\"layui-box layui-laypage layui-laypage-default\" id=\"layui-laypage-10\">";
		$pageList .= "<span class=\"layui-laypage-count\">共".$this->_pagination['record_count']."条</span>";
		if ($this->_pagination['current'] == 1)
		{
			//$pageList .= '<span class="layui-laypage-curr"><em class="layui-laypage-em"></em><em>1</em></span>';
			$pageList .= "<a href=\"javascript:;\" class=\"layui-laypage-prev layui-disabled\" data-page=\"0\">上一页</a>";//上一页
			//$pageList .= '<span class="layui-laypage-curr"><em class="layui-laypage-em"></em><em>1</em></span>';
		}
		else 
		{
			$pageList .= "<a href=\"{$this->_url}{$join}cp{$pext}1{$join}ps{$pext}{$this->_pagination['page_size']}\">首页</a>";
			$pageList .= "<a href=\"{$this->_url}{$join}cp{$pext}1{$join}ps{$pext}{$this->_pagination['page_size']}\" class=\"layui-laypage-prev\" data-page=\"1\"><em>上一页</em></a>";//上一页
		}
		$to = (($this->_pagination['current']+$intOffsetNum) >= $this->_pagination['page_count'])?($this->_pagination['page_count']):($this->_pagination['current']+$intOffsetNum);
		for ($i;$i<=$to;$i++) 
		{
			if($i == $this->_pagination['current'])
			{
				$pageList .= "<span class=\"layui-laypage-curr\"><em class=\"layui-laypage-em\"></em><em>{$i}</em></span>";
			} 
			else 
			{
				$pageList .= "<a href=\"{$this->_url}{$join}cp{$pext}{$i}{$join}ps{$pext}{$this->_pagination['page_size']}\" data-page=\"{$i}\">{$i}</a>";
			}
		}
		if ($this->_pagination['next'] == $this->_pagination['current']) 
		{
			//$pageList .= 'a href="javascript:;" class="layui-laypage-next" data-page="3"><em>&gt;</em></a>';
			$pageList .= "<a href=\"javascript:;\" class=\"layui-laypage-next layui-disabled\" data-page=\"{$this->_pagination['current']}\">下一页</a>";
		}
		else 
		{
			$pageList .= "<a href=\"{$this->_url}{$join}cp{$pext}{$this->_pagination['next']}{$join}ps{$pext}{$this->_pagination['page_size']}\" class=\"layui-laypage-next\" data-page=\"{$this->_pagination['next']}\"><em>下一页</em></a>";
			$pageList .= "<a href=\"{$this->_url}{$join}cp{$pext}{$this->_pagination['page_count']}{$join}ps{$pext}{$this->_pagination['page_size']}\"  class=\"layui-laypage-next\" >末页</a>";
		}
		$pageList .= "<span class=\"layui-laypage-skip\">每页显示<input name=\"ps\" type=\"text\"  value=\"{$this->_pagination['page_size']}\" onchange=\"location.href='{$this->_url}{$join}ps{$pext}'+this.value;return false;\" />条记录</span>";
		$pageList .= "<span class=\"layui-laypage-skip\" style=\"padding:0;\">到第<input type=\"text\" name=\"cp\" min=\"1\" value=\"{$this->_pagination['current']}\"  onchange=\"location.href='{$this->_url}{$join}cp{$pext}'+this.value+'{$join}ps{$pext}{$this->_pagination['page_size']}';return false;\"/>页<button type=\"button\"  onchange=\"location.href='{$this->_url}{$join}cp{$pext}'+this.value+'{$join}ps{$pext}{$this->_pagination['page_size']}';return false;\" class=\"layui-laypage-btn\" style=\"background-color: #009688;color:#fff;border:none;border-radius:2px;\">确定</button></span>";
		
		$pageList .= "</div></div>";
		
		return $pageList;
	}
	
	public function __destruct()
	{
		
	}
}
?>