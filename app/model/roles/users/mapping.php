<?php
class Roles_Users_Mapping extends QDB_ActiveRecord_Abstract
{
	/**
	 * 返回对象的定义
	 *
	 * @static
	 *
	 * @return array
	 */
	static function __define()
	{
		return array
		(
			// 指定该 ActiveRecord 要使用的行为插件
			'behaviors' => '',

			// 指定行为插件的配置
			'behaviors_settings' => array
			(
			    # '插件名' => array('选项' => 设置),
			),

			// 用什么数据表保存对象
			'table_name' => 'roles_users_mapping',

			// 指定数据表记录字段与对象属性之间的映射关系
			// 没有在此处指定的属性，QeePHP 会自动设置将属性映射为对象的可读写属性
			'props' => array
			(
				// 主键应该是只读，确保领域对象的“不变量”
				'id' => array('readonly' => true),

				/**
				 *  可以在此添加其他属性的设置
				 */
				# 'other_prop' => array('readonly' => true),

				/**
				 * 添加对象间的关联
				 */
				# 'other' => array('has_one' => 'Class'),

			),

			/**
			 * 允许使用 mass-assignment 方式赋值的属性
			 *
			 * 如果指定了 attr_accessible，则忽略 attr_protected 的设置。
			 */
			'attr_accessible' => '',

			/**
			 * 拒绝使用 mass-assignment 方式赋值的属性
			 */
			'attr_protected' => 'id',

			/**
			 * 指定在数据库中创建对象时，哪些属性的值不允许由外部提供
			 *
			 * 这里指定的属性会在创建记录时被过滤掉，从而让数据库自行填充值。
			 */
			'create_reject' => '',

			/**
			 * 指定更新数据库中的对象时，哪些属性的值不允许由外部提供
			 */
			'update_reject' => '',

			/**
			 * 指定在数据库中创建对象时，哪些属性的值由下面指定的内容进行覆盖
			 *
			 * 如果填充值为 self::AUTOFILL_TIMESTAMP 或 self::AUTOFILL_DATETIME，
			 * 则会根据属性的类型来自动填充当前时间（整数或字符串）。
			 *
			 * 如果填充值为一个数组，则假定为 callback 方法。
			 */
			'create_autofill' => array
			(
				# 属性名 => 填充值
				# 'is_locked' => 0,
			),

			/**
			 * 指定更新数据库中的对象时，哪些属性的值由下面指定的内容进行覆盖
			 *
			 * 填充值的指定规则同 create_autofill
			 */
			'update_autofill' => array
			(
			),

			/**
			 * 在保存对象时，会按照下面指定的验证规则进行验证。验证失败会抛出异常。
			 *
			 * 除了在保存时自动验证，还可以通过对象的 ::meta()->validate() 方法对数组数据进行验证。
			 *
			 * 如果需要添加一个自定义验证，应该写成
			 *
			 * 'title' => array(
			 *	array(array(__CLASS__, 'checkTitle'), '标题不能为空'),
			 * )
			 *
			 * 然后在该类中添加 checkTitle() 方法。函数原型如下：
			 *
			 * static function checkTitle($title)
			 *
			 * 该方法返回 true 表示通过验证。
			 */
			'validations' => array
			(
			),
		);
	}


/* ------------------ 以下是自动生成的代码，不能修改 ------------------ */

	/**
	 * 开启一个查询，查找符合条件的对象或对象集合
	 *
	 * @static
	 *
	 * @return QDB_Select
	 */
	static function find()
	{
		$args = func_get_args();
		return QDB_ActiveRecord_Meta::instance(__CLASS__)->findByArgs($args);
	}

	/**
	 * 返回当前 ActiveRecord 类的元数据对象
	 *
	 * @static
	 *
	 * @return QDB_ActiveRecord_Meta
	 */
	static function meta()
	{
		return QDB_ActiveRecord_Meta::instance(__CLASS__);
	}


/* ------------------ 以上是自动生成的代码，不能修改 ------------------ */
	
	static function updateMapping($roles_ids, $user_ids, $type='by_user')
	{
		if ($type == 'by_user')
		{
			$mapping = Roles_Users_Mapping::find('user_id=?', $user_ids)->asArray()->getAll();
			$roles_old = array();
			foreach ($mapping as $m)
			{
				$roles_old[] = $m['roles_id'];
			}
			$roles_delete = array_diff($roles_old, $roles_ids);
			$roles_create = array_diff($roles_ids, $roles_old);
			if ($roles_delete)
			{
				Roles_Users_Mapping::meta()->deleteWhere('user_id=? and roles_id in (?)', $user_ids, $roles_delete);
			}
			foreach ($roles_create as $rc)
			{
				$new_mapping = new Roles_Users_Mapping();
				$new_mapping->roles_id = $rc;
				$new_mapping->user_id = $user_ids;
				$new_mapping->save();
			}
		}
		if ($type == 'by_roles')
		{
			$mapping = Roles_Users_Mapping::find('roles_id=?', $roles_ids)->asArray()->getAll();
			$current_users_child_ids = QDB::getConn()->execute('select id from user where status=' . Q::ini('custom_flag/user_status/on_duty/value') . ' and parent_id=' . CURRENT_USER_ID)->fetchCol();
			$users_old = array();
			foreach ($mapping as  $m)
			{
				if (in_array($m['user_id'], $current_users_child_ids))
				{
					$users_old[] = $m['user_id'];
				}
			}
			$users_delete = array_diff($users_old, $user_ids);
			$users_create = array_diff($user_ids, $users_old);
			foreach ($users_delete as $ud)
			{
				Roles_Users_Mapping::meta()->deleteWhere('roles_id=? and user_id=?', $roles_ids, $ud);
				self::updateNextMapping($ud);
			}
			foreach ($users_create as $uc)
			{
				$new_mapping = new Roles_Users_Mapping();
				$new_mapping->roles_id = $roles_ids;
				$new_mapping->user_id = $uc;
				$new_mapping->save();
			}
		}
		return array('ack' => SUCCESS, 'message' => '');
	}
	
	static function updateNextMapping($user_id)
	{
		$user = User::find('id=?', $user_id)->asArray()->getOne();
		if ($user)
		{
			$child = User::find('parent_id=?', $user_id)->asArray()->getAll();
			$mapping = Roles_Users_Mapping::getUserRolesId($user_id);
			foreach ($child as $c)
			{
				$child_mapping = Roles_Users_Mapping::getUserRolesId($c['id']);
				foreach ($child_mapping as $cm)
				{
					if (! in_array($cm, $mapping))
					{
						Roles_Users_Mapping::meta()->deleteWhere('user_id=? and roles_id=?', $c['id'], $cm);
					}
				}
				self::updateNextMapping($c['id']);
			}
		}
	}
	
	static function getUserRolesId($user_id)
	{
		return QDB::getConn()->execute('select roles_id from roles_users_mapping where user_id=' . $user_id)->fetchCol();
	}
	
	/**
	 * 复制用户角色权限
	 * @param 被复制用户ID	$user_id
	 * @param 需复制用户ids	$copy_user_ids
	 */
	static function copy($user_id, $copy_user_ids)
	{
		$current_user_child_ids = QDB::getConn()->execute('select id from user where parent_id=' . CURRENT_USER_ID)->fetchCol();
		if (!in_array($user_id, $current_user_child_ids))
		{
			return array('ack' => FAILURE, 'message' => '用户ID=' . $user_id .'不属于当前操作人的下级');
		}
		$copy_roles_ids = self::getUserRolesId($user_id);
		foreach ($copy_user_ids as $cui)
		{
			if (!in_array($cui, $current_user_child_ids))
			{
				return array('ack' => FAILURE, 'message' => '用户ID=' . $cui .'不属于当前操作人的下级');
			}
			else
			{
				self::updateMapping($copy_roles_ids, $cui);
			}
		}
		return array('ack' => SUCCESS, 'message' => '');
	}
}