<?php
class Task_Log extends QDB_ActiveRecord_Abstract
{
	    /**
	 * 返回对象的定义
	 *
	 * @static
	 *
	 * @return array
	 */
	static function __define()
	{
		return array
		(
			// 指定该 ActiveRecord 要使用的行为插件
			'behaviors' => '',

			// 指定行为插件的配置
			'behaviors_settings' => array
			(
				# '插件名' => array('选项' => 设置),
			),

			// 用什么数据表保存对象
			'table_name' => 'task_log',

			// 指定数据表记录字段与对象属性之间的映射关系
			// 没有在此处指定的属性，QeePHP 会自动设置将属性映射为对象的可读写属性
			'props' => array
			(
				// 主键应该是只读，确保领域对象的“不变量”
				'id' => array('readonly' => true),

				/**
				 *  可以在此添加其他属性的设置
				 */
				# 'other_prop' => array('readonly' => true),

				/**
				 * 添加对象间的关联
				 */
				# 'other' => array('has_one' => 'Class'),

			),

			/**
			 * 允许使用 mass-assignment 方式赋值的属性
			 *
			 * 如果指定了 attr_accessible，则忽略 attr_protected 的设置。
			 */
			'attr_accessible' => '',

			/**
			 * 拒绝使用 mass-assignment 方式赋值的属性
			 */
			'attr_protected' => 'id',

			/**
			 * 指定在数据库中创建对象时，哪些属性的值不允许由外部提供
			 *
			 * 这里指定的属性会在创建记录时被过滤掉，从而让数据库自行填充值。
			 */
			'create_reject' => '',

			/**
			 * 指定更新数据库中的对象时，哪些属性的值不允许由外部提供
			 */
			'update_reject' => '',

			/**
			 * 指定在数据库中创建对象时，哪些属性的值由下面指定的内容进行覆盖
			 *
			 * 如果填充值为 self::AUTOFILL_TIMESTAMP 或 self::AUTOFILL_DATETIME，
			 * 则会根据属性的类型来自动填充当前时间（整数或字符串）。
			 *
			 * 如果填充值为一个数组，则假定为 callback 方法。
			 */
			'create_autofill' => array
			(
				# 属性名 => 填充值
				# 'is_locked' => 0,
			),

			/**
			 * 指定更新数据库中的对象时，哪些属性的值由下面指定的内容进行覆盖
			 *
			 * 填充值的指定规则同 create_autofill
			 */
			'update_autofill' => array
			(
			),

			/**
			 * 在保存对象时，会按照下面指定的验证规则进行验证。验证失败会抛出异常。
			 *
			 * 除了在保存时自动验证，还可以通过对象的 ::meta()->validate() 方法对数组数据进行验证。
			 *
			 * 如果需要添加一个自定义验证，应该写成
			 *
			 * 'title' => array(
			 *        array(array(__CLASS__, 'checkTitle'), '标题不能为空'),
			 * )
			 *
			 * 然后在该类中添加 checkTitle() 方法。函数原型如下：
			 *
			 * static function checkTitle($title)
			 *
			 * 该方法返回 true 表示通过验证。
			 */
			'validations' => array
			(
			),
		);
	}


/* ------------------ 以下是自动生成的代码，不能修改 ------------------ */

	/**
	 * 开启一个查询，查找符合条件的对象或对象集合
	 *
	 * @static
	 *
	 * @return QDB_Select
	 */
	static function find()
	{
		$args = func_get_args();
		return QDB_ActiveRecord_Meta::instance(__CLASS__)->findByArgs($args);
	}

	/**
	 * 返回当前 ActiveRecord 类的元数据对象
	 *
	 * @static
	 *
	 * @return QDB_ActiveRecord_Meta
	 */
	static function meta()
	{
		return QDB_ActiveRecord_Meta::instance(__CLASS__);
	}


/* ------------------ 以上是自动生成的代码，不能修改 ------------------ */
	/**
	 * 记录任务日志
	 * @param $task 任务详细
	 * @return 记录成功(失败)
	 */
	static function create($task_id)
	{
		$exist_task = Task::find('id=?', $task_id)->asArray()->getOne();
		if ($exist_task['id'])
		{
			$new_task_log = new Task_Log();
			$new_task_log->task_id = $exist_task['id'];
			$new_task_log->ack = Q::ini('custom_flag/task_log_ack/hit/value');
			$new_task_log->time = CURRENT_DATETIME;
			$new_task_log->save();
			return array('ack' => SUCCESS, 'message' => '', 'task_log_id' => mysql_insert_id());
		}
		else
		{
			return array('ack' => FAILURE, 'message' => 'task is not exists!');
		}
	}
	
	/**
	 * 按条件搜索任务日志记录
	 * @param $condition 条件
	 * @param $cp 当前页
	 * @param $ps 每页大小
	 */
	static function search($condition, $cp, $ps)
	{
		$columns = array(
			'task_log.*'
		);
		$where = Task_Log::buildSearchWhere($condition);
		if ($cp == 0 && $ps == 0)
		{
			$select = Task_Log::find($where)->order('task_log.time desc')->setColumns($columns);
			$pagination = array();
		}
		else
		{
			$select = Task_Log::find($where)->order('task_log.time desc')->setColumns($columns)->limitPage($cp, $ps);
			$pagination = $select->getPagination();
		}
		$task_log = $select->asArray()->getAll();
		return array('ack' => SUCCESS, 'message' => '', 'data' => $task_log, 'pagination' => $pagination);
	}
	
	/**
	 * 组装搜索条件
	 * @param $condition 条件数组
	 */
	static function buildSearchWhere($condition)
	{
		$where = '1';
		if(! empty($condition['task_id']))
		{
			$where .= ' and task_log.task_id=' . $condition['task_id'];
		}
		if (! empty($condition['begin_time']))
		{
			$where .= ' and task_log.time>=\'' . $condition['begin_time'] . '\'';
		}
		if (! empty($condition['end_time']))
		{
			$where .= ' and task_log.time<=\'' . $condition['end_time'] . ' 23:59:59\'';
		}
		if (isset($condition['task_log_ack']))
		{
			$task_log_ack_filter = '0';
			foreach (Q::ini('custom_flag/task_log_ack') as $a)
			{
				if (isset($condition['task_log_ack'][$a['value']]) && $condition['task_log_ack'][$a['value']] == $a['value'])
				{
					$task_log_ack_filter .= ' or task_log.ack=' . $a['value'];
				}
			}
			$where .= " and ($task_log_ack_filter)";
		}
		return $where;
	}	
}