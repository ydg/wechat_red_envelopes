<?php
class OLM_ChkCode
{
	static function ChkBarCode($barcode)
	{
		$ingk=0;
		$chkbarcode=0;
		if (strlen($barcode) == 10)
		{
			$chkbarcode = OLM_ChkCode::ChkBarCode10($barcode);
		}
		else
		{
			if (is_numeric($barcode))
			{
				if (strlen($barcode) == 12)
				{
					$barcode = OLM_ChkCode::DecryptCode($barcode);
					$ingk = intval(substr($barcode, 0,9));
					if ($ingk>800000000)
					{
						$ingk = $ingk - 800000000;
					}
					else 
					{
						if ($ingk > 500000000)
						{
							$ingk = $ingk - 500000000;
						}
						else
						{
							$ingk = $ingk + intval(substr($barcode, -3) - 1);
						}
					}
					$chkbarcode = $ingk;
				}
			}
		}
		return $chkbarcode;
	}
	
	/**	
	 * 解码
	 *$barcode
	 **/
	static function DecryptCode($barcode)
	{
		error_reporting(-9);
		$str9 = ''; $str3 = '';
		$str9 = substr($barcode, 0,9);
		$str3 = substr($barcode, -3);
		$str9 = OLM_ChkCode::strRightMove(substr("00000000". (round($str9) ^ round(str3 * 123)), -9), 8);
		$DecryptCode = substr($str9, 0,6).$str3.substr($str9, -3);
		
		$str9 = substr($DecryptCode, 0, 9);
		$str3 = substr($DecryptCode, -3);
		$str9 = OLM_ChkCode::strRightMove(substr("00000000". (round($str9) ^ round(str3 * 123)), -9), 8);
		$DecryptCode = substr($str9, 0,6).$str3.substr($str9, -3);
		
		$str9 = substr($DecryptCode, 0, 9);
		$str3 = substr($DecryptCode, -3);
		$DecryptCode = $str9 = OLM_ChkCode::strRightMove(substr("00000000". (round($str9) ^ round(str3 * 123)), -9), 8).$str3;
		
		return $DecryptCode;
	}
	
	static function strRightMove($strsrc, $ingnum)
	{
		for ($i=1;$i<$ingnum;$i++)
		{
			$strsrc = substr($strsrc, -1).substr($strsrc, 0,strlen($strsrc)-1);
		}
		$strRightMove = $strsrc;
		return $strRightMove;
	}
	
	static function ChkBarCode10($barcode)
	{
		$ingk = 0;
		if (is_numeric($barcode))
		{
			if (strlen($barcode) == 10)
			{
				$ingk = intval(substr($barcode,0,8));
				if ($ingk > 80000000)
				{
					$ingk = $ingk - 80000000;
				}
				else
				{
					if ($ingk > 50000000)
					{
						$ingk = $ingk -50000000;
					}
					else
					{
						$ingk = $ingk + intval(substr($barcode, -2) - 1);
					}
				}
			}
		}
		$chkbarcode = $ingk;
		return $chkbarcode;
	}
	
	static function ChkPackQty($barcode)
	{
		$barcode = OLM_ChkCode::DecryptCode($barcode);
		if (strlen($barcode)==10)
		{
			$ChkPackQty=substr($barcode, -2);
		}
		if (strlen($barcode)==12)
		{
			$ChkPackQty=substr($barcode,-3);
		}
		if (substr($barcode, 0,1)<5)
		{
			$ChkPackQty=1;
		}
		return $ChkPackQty;
	}
	
//	static function SpareNum()
//	{
//		$sparenum = OLM_ChkCode::GetValue('');
//	}
//	
//	static function GetValue($strSetID)
//	{
//		$result = Setting::find()->asArray()->getAll();
//	}
}
		