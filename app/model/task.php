<?php
class Task extends QDB_ActiveRecord_Abstract
{
	    /**
	 * 返回对象的定义
	 *
	 * @static
	 *
	 * @return array
	 */
	static function __define()
	{
		return array
		(
			// 指定该 ActiveRecord 要使用的行为插件
			'behaviors' => '',

			// 指定行为插件的配置
			'behaviors_settings' => array
			(
				# '插件名' => array('选项' => 设置),
			),

			// 用什么数据表保存对象
			'table_name' => 'task',

			// 指定数据表记录字段与对象属性之间的映射关系
			// 没有在此处指定的属性，QeePHP 会自动设置将属性映射为对象的可读写属性
			'props' => array
			(
				// 主键应该是只读，确保领域对象的“不变量”
				'id' => array('readonly' => true),

				/**
				 *  可以在此添加其他属性的设置
				 */
				# 'other_prop' => array('readonly' => true),

				/**
				 * 添加对象间的关联
				 */
				# 'other' => array('has_one' => 'Class'),

			),

			/**
			 * 允许使用 mass-assignment 方式赋值的属性
			 *
			 * 如果指定了 attr_accessible，则忽略 attr_protected 的设置。
			 */
			'attr_accessible' => '',

			/**
			 * 拒绝使用 mass-assignment 方式赋值的属性
			 */
			'attr_protected' => 'id',

			/**
			 * 指定在数据库中创建对象时，哪些属性的值不允许由外部提供
			 *
			 * 这里指定的属性会在创建记录时被过滤掉，从而让数据库自行填充值。
			 */
			'create_reject' => '',

			/**
			 * 指定更新数据库中的对象时，哪些属性的值不允许由外部提供
			 */
			'update_reject' => '',

			/**
			 * 指定在数据库中创建对象时，哪些属性的值由下面指定的内容进行覆盖
			 *
			 * 如果填充值为 self::AUTOFILL_TIMESTAMP 或 self::AUTOFILL_DATETIME，
			 * 则会根据属性的类型来自动填充当前时间（整数或字符串）。
			 *
			 * 如果填充值为一个数组，则假定为 callback 方法。
			 */
			'create_autofill' => array
			(
				# 属性名 => 填充值
				# 'is_locked' => 0,
			),

			/**
			 * 指定更新数据库中的对象时，哪些属性的值由下面指定的内容进行覆盖
			 *
			 * 填充值的指定规则同 create_autofill
			 */
			'update_autofill' => array
			(
			),

			/**
			 * 在保存对象时，会按照下面指定的验证规则进行验证。验证失败会抛出异常。
			 *
			 * 除了在保存时自动验证，还可以通过对象的 ::meta()->validate() 方法对数组数据进行验证。
			 *
			 * 如果需要添加一个自定义验证，应该写成
			 *
			 * 'title' => array(
			 *        array(array(__CLASS__, 'checkTitle'), '标题不能为空'),
			 * )
			 *
			 * 然后在该类中添加 checkTitle() 方法。函数原型如下：
			 *
			 * static function checkTitle($title)
			 *
			 * 该方法返回 true 表示通过验证。
			 */
			'validations' => array
			(
			),
		);
	}


/* ------------------ 以下是自动生成的代码，不能修改 ------------------ */

	/**
	 * 开启一个查询，查找符合条件的对象或对象集合
	 *
	 * @static
	 *
	 * @return QDB_Select
	 */
	static function find()
	{
		$args = func_get_args();
		return QDB_ActiveRecord_Meta::instance(__CLASS__)->findByArgs($args);
	}

	/**
	 * 返回当前 ActiveRecord 类的元数据对象
	 *
	 * @static
	 *
	 * @return QDB_ActiveRecord_Meta
	 */
	static function meta()
	{
		return QDB_ActiveRecord_Meta::instance(__CLASS__);
	}


/* ------------------ 以上是自动生成的代码，不能修改 ------------------ */
	/**
	 * 新建任务
	 * @param $task 任务详细
	 * @param $staus 任务状态
	 * @return 新建成功(失败)
	 */
	static function create($task, $status=0)
	{
		$exist_task = Task::find('name=?', $task['name'])->asArray()->getOne();
		if ($exist_task['id'])
		{
			return array('ack' => FAILURE, 'message' => 'task is exists!', 'task_id' => $exist_task['id']);
		}
		else
		{
			$new_task = new Task();
			$new_task->name = $task['name'];
			$new_task->model = $task['model'];
			$new_task->regex = $task['regex'];
			$new_task->seconds = $task['seconds'];
			$new_task->create_time = CURRENT_DATETIME;
			$new_task->status = $status;
			$new_task->save();
			return array('ack'=> SUCCESS, 'message' => '');
		}
	}
	
	static function search($condition, $cp, $ps)
	{
		$where = Task::buildSearchWhere($condition);
		$columns = array(
			'task.*',
		);
		if ($cp == 0 && $ps == 0)
		{
			$select = Task::find($where); 
			$select->setColumns($columns);
			$pagination = array();
		}
		else
		{
			$select = Task::find($where);
			$select->setColumns($columns)->limitPage($cp, $ps);
			$pagination = $select->getPagination();
		}
		$task = $select->asArray()->getAll();
		return array(
				'ack' => SUCCESS,
				'message' => '',
				'data' => $task,
				'pagination' => $pagination
		);
	}
	
	static function buildSearchWhere($condition)
	{
		$where = '1';
		if ( ! empty($condition['model']))
		{
			$where .= ' and task.model like \'%' . $condition['model'] . '%\'';
		}
		if ( ! empty($condition['status']))
		{
			$status_filter = '0';
			foreach (Q::ini('custom_flag/task_status') as $ts)
			{
				if (isset($condition['status'][$ts['value']]) && $condition['status'][$ts['value']] == $ts['value'])
				{
					$status_filter .= ' or task.status=' . $ts['value'];
				}
			}
			$where .= ' and (' . $status_filter . ')';
		}
		return $where;
	}
	
	/*
	 * 检测任务列表自动执行时间在一个时间段是否冲突
	 * @param $start_time 开始时间
	 * @param $end_time 结束时间 
	 */
	static function checkCronRepeat($start_time, $end_time)
	{
		$time = strtotime($start_time);
		$end_time = strtotime($end_time);
		$repeat = $repeat_time = array();
		$i = 1;
		$task = Task::find('status=?', Q::ini('custom_flag/task_status/opened/value'))->asArray()->getAll();
		while ($time <= $end_time)
		{
			$temp = array();
			foreach ($task as $t)
			{
 				if (preg_match($t['regex'], date('Y-m-d H:i w', $time)))
				{
					$temp[] = $t['name'];
				}
			}
			if (count($temp) >= 2)
			{
				sort($temp);
				$key = array_search($temp, $repeat, true);
				if ($key)
				{
					$repeat_time[$key][] = date('Y-m-d H:i w', $time);
				}
				else
				{
					$repeat[$i] = $temp;
					$repeat_time[$i][] = date('Y-m-d H:i w', $time);
					$i++;
				}
			}
			$time += 60;
		}
		$result = array();
		foreach ($repeat as $k => $r)
		{
			$result[$k]['name'] = $r;
			$result[$k]['repeat_time'] = $repeat_time[$k];
		}
		return array('ack' => SUCCESS, 'message' => '', 'repeat_task' => $result);
	}
}