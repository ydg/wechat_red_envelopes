<?php
class Customer extends QDB_ActiveRecord_Abstract
{
	/**
	 * 返回对象的定义
	 *
	 * @static
	 *
	 * @return array
	 */
	static function __define()
	{
		return array
		(
			// 指定该 ActiveRecord 要使用的行为插件
			'behaviors' => '',

			// 指定行为插件的配置
			'behaviors_settings' => array
			(
			    # '插件名' => array('选项' => 设置),
			),

			// 用什么数据表保存对象
			'table_name' => 'customer',

			// 指定数据表记录字段与对象属性之间的映射关系
			// 没有在此处指定的属性，QeePHP 会自动设置将属性映射为对象的可读写属性
			'props' => array
			(
				// 主键应该是只读，确保领域对象的“不变量”
				'id' => array('readonly' => true),

				/**
				 *  可以在此添加其他属性的设置
				 */
				# 'other_prop' => array('readonly' => true),

				/**
				 * 添加对象间的关联
				 */
				# 'other' => array('has_one' => 'Class'),

			),

			/**
			 * 允许使用 mass-assignment 方式赋值的属性
			 *
			 * 如果指定了 attr_accessible，则忽略 attr_protected 的设置。
			 */
			'attr_accessible' => '',

			/**
			 * 拒绝使用 mass-assignment 方式赋值的属性
			 */
			'attr_protected' => 'id',

			/**
			 * 指定在数据库中创建对象时，哪些属性的值不允许由外部提供
			 *
			 * 这里指定的属性会在创建记录时被过滤掉，从而让数据库自行填充值。
			 */
			'create_reject' => '',

			/**
			 * 指定更新数据库中的对象时，哪些属性的值不允许由外部提供
			 */
			'update_reject' => '',

			/**
			 * 指定在数据库中创建对象时，哪些属性的值由下面指定的内容进行覆盖
			 *
			 * 如果填充值为 self::AUTOFILL_TIMESTAMP 或 self::AUTOFILL_DATETIME，
			 * 则会根据属性的类型来自动填充当前时间（整数或字符串）。
			 *
			 * 如果填充值为一个数组，则假定为 callback 方法。
			 */
			'create_autofill' => array
			(
				# 属性名 => 填充值
				# 'is_locked' => 0,
			),

			/**
			 * 指定更新数据库中的对象时，哪些属性的值由下面指定的内容进行覆盖
			 *
			 * 填充值的指定规则同 create_autofill
			 */
			'update_autofill' => array
			(
			),

			/**
			 * 在保存对象时，会按照下面指定的验证规则进行验证。验证失败会抛出异常。
			 *
			 * 除了在保存时自动验证，还可以通过对象的 ::meta()->validate() 方法对数组数据进行验证。
			 *
			 * 如果需要添加一个自定义验证，应该写成
			 *
			 * 'title' => array(
			 *	array(array(__CLASS__, 'checkTitle'), '标题不能为空'),
			 * )
			 *
			 * 然后在该类中添加 checkTitle() 方法。函数原型如下：
			 *
			 * static function checkTitle($title)
			 *
			 * 该方法返回 true 表示通过验证。
			 */
			'validations' => array
			(
			),
		);
	}


/* ------------------ 以下是自动生成的代码，不能修改 ------------------ */

	/**
	 * 开启一个查询，查找符合条件的对象或对象集合
	 *
	 * @static
	 *
	 * @return QDB_Select
	 */
	static function find()
	{
		$args = func_get_args();
		return QDB_ActiveRecord_Meta::instance(__CLASS__)->findByArgs($args);
	}

	/**
	 * 返回当前 ActiveRecord 类的元数据对象
	 *
	 * @static
	 *
	 * @return QDB_ActiveRecord_Meta
	 */
	static function meta()
	{
		return QDB_ActiveRecord_Meta::instance(__CLASS__);
	}


/* ------------------ 以上是自动生成的代码，不能修改 ------------------ */
	
	static function create($customer)
	{
		$exist = Customer::find('c_no=? or c_name=?', $customer['number'],$customer['name'])->asArray()->getOne();
		if ($exist['id'])
		{
			return array('ack' => FAILURE, 'message' => '该代理已经存在！', 'product_id' => $exist['id']);
		}
		else
		{
			$user_id = User::find('name=?',$customer['number'])->asArray()->getOne();
			if ($user_id['id'])
			{
				return array('ack' => FAILURE, 'message' => '该用户已经存在！', 'product_id' => $user_id['id']);
			}
			$new= new Customer();
			$new->c_no = $customer['number'];
			$new->c_name = $customer['name'];
			$new->c_type = $customer['type'];
			$new->c_port = isset($customer['port']) ? $customer['port'] : 0;
			$new->c_contact = $customer['contact'];
			$new->c_phone =$customer['phone'];
			$new->c_fax =$customer['fax'];
			$new->c_province =$customer['province'];
			$new->c_city =$customer['city'];
			$new->c_addr =$customer['adrr'];
			$new->c_no_p = $customer['c_no_p']; //CURRENT_USER_ID;
			$new->c_bank =$customer['bank'];
			$new->c_bankid =$customer['bank_no'];
			$new->c_bankname =$customer['branch'];
			$new->c_bankuser =$customer['bank_name'];
			$new->c_pwd =$customer['password'];
			$new->parent_i =CURRENT_USER_ID;
			$new->save();
			return array('ack' => SUCCESS, 'message' => '');
		}
	}
	
	static function edit($customer)
	{
		$edit = Customer::find('id=?', $customer['id'])->getOne();
		if (!$edit['id'])
		{
			return array('ack' => FAILURE, 'message' => '该代理不存在！', 'custommer_id' => $edit['id']);
		}
		else
		{
			$edit->c_no = isset($customer['number']) ? $customer['number'] : $customer['user_name'];
			$edit->c_name = $customer['name'];
			$edit->c_type = $customer['type'];
			$edit->c_port = isset($customer['port']) ? $customer['port'] : 0;
			$edit->c_contact = $customer['contact'];
			$edit->c_phone =$customer['phone'];
			$edit->c_fax =$customer['fax'];
			$edit->c_province =$customer['province'];
			$edit->c_city =$customer['city'];
			$edit->c_addr =$customer['adrr'];
			$edit->c_no_p = $customer['c_no_p']; //CURRENT_USER_ID;
			$edit->c_bank =$customer['bank'];
			$edit->c_bankid =$customer['bank_no'];
			$edit->c_bankname =$customer['branch'];
			$edit->c_bankuser =$customer['bank_name'];
			$edit->c_pwd =$customer['password'];
			$edit->save();
			return array('ack'=> SUCCESS, 'message' => '');
		}
	}
	
	static function search($cp, $ps, $condition)
	{
		$where = self::buildSearchWhere($condition);
		$columns = array(
			'customer.*',
		);
		if ($cp == 0 && $ps == 0)
		{
			$select = Customer::find($where);
			$select->setColumns($columns);
			$pagination = array();
		}
		else
		{
			$select = Customer::find($where);
			$select->setColumns($columns)->limitPage($cp, $ps);
			$pagination = $select->getPagination();
		}
		$user = $select->asArray()->getAll();
		return array(
				'ack' => SUCCESS,
				'message' => '',
				'data' => $user,
				'pagination' => $pagination
		);
	}
	
	static function buildSearchWhere($condition)
	{
		$where = '1';
		if (! empty($condition['name']))
		{
			$where .= ' and customer.c_name like \'%' . $condition['name'] . '%\'';
		}
		if (! empty($condition['number']))
		{
			$where .= ' and customer.c_no=' . $condition['number'];
		}
		if (! empty($condition['belong']))
		{
			$where .= ' and customer.c_no_p=' . $condition['belong'];
		}
		return $where;
	}
	
	//很据当前用户id获取当前用户的下级信息
	//类似于User的获取下一级的方法
	static function getOffspringById($id, $field='', $level=0)
	{
		$user = Customer::find('parent_i=?', $id)->asArray()->getAll();
		if ($user)
		{
			$level++;
			$result = array();
			foreach ($user as $u)
			{
				$u['level'] = $level;
				if ($field)
				{
					$result[] = $u[$field];
				}
				else
				{
					$result[] = $u;
				}
				$ret = self::getOffspringById($u['id'], $field, $level);
				$result = array_merge($result, $ret);
			}
			return $result;
		}
		else
		{
			return array();
		}
	}
}