-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2016-04-27 15:58:17
-- 服务器版本： 5.5.28
-- PHP Version: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wsfwfch`
--

-- --------------------------------------------------------

--
-- 表的结构 `clearance_record_log`
--

CREATE TABLE IF NOT EXISTS `clearance_record_log` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `product_basic_id` int(11) NOT NULL COMMENT '产品ID',
  `quantity` int(11) NOT NULL COMMENT '库存',
  `amount` decimal(10,2) NOT NULL COMMENT '总金额',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `clearance_summary_log`
--

CREATE TABLE IF NOT EXISTS `clearance_summary_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `number_quantity` int(11) NOT NULL COMMENT 'SKU数',
  `total_actual_quantity` int(11) NOT NULL COMMENT '总实际数',
  `total_value` decimal(10,2) NOT NULL COMMENT '总货值',
  `status` tinyint(4) NOT NULL COMMENT '状态',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='清仓汇总日志' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `c_no` varchar(30) NOT NULL COMMENT '代理编号',
  `c_name` varchar(50) NOT NULL COMMENT '代理名字',
  `c_addr` varchar(100) DEFAULT NULL COMMENT '地址',
  `c_post` varchar(30) DEFAULT NULL,
  `c_phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `c_fax` varchar(30) DEFAULT NULL COMMENT '传真',
  `c_contact` varchar(50) DEFAULT NULL,
  `c_note` varchar(50) DEFAULT NULL,
  `c_province` varchar(30) DEFAULT NULL COMMENT '省份',
  `c_city` varchar(30) DEFAULT NULL COMMENT '城市',
  `c_albbname` varchar(30) DEFAULT NULL,
  `c_id` varchar(30) DEFAULT NULL COMMENT '相当于父类，控制上下级关系了，但是现在已全部规划到user表中',
  `c_weixin` varchar(50) DEFAULT NULL,
  `c_no_s` varchar(50) DEFAULT NULL COMMENT '控制上下级关系',
  `c_type` int(4) DEFAULT NULL COMMENT '代理等级',
  `c_bank` varchar(50) DEFAULT NULL,
  `c_bankid` varchar(50) DEFAULT NULL,
  `c_bankuser` varchar(50) DEFAULT NULL,
  `c_bankname` varchar(50) DEFAULT NULL,
  `c_nowpoint` float DEFAULT NULL COMMENT '关于权限控制暂时使用不上',
  `c_allpoint` float DEFAULT NULL COMMENT '关于权限控制暂时使用不上',
  `c_no_p` varchar(50) DEFAULT NULL COMMENT '关于权限控制暂时使用不上，上下级从属关系字段',
  `c_port` int(8) DEFAULT NULL,
  `c_wxid` varchar(50) DEFAULT NULL,
  `c_status` varchar(30) DEFAULT NULL,
  `c_qrurl` varchar(50) DEFAULT NULL,
  `c_wxhead` varchar(200) DEFAULT NULL,
  `c_pwd` varchar(50) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `parent_i` int(11) DEFAULT NULL COMMENT '父级ID',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='客户表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `user_id` int(11) NOT NULL COMMENT '发布人id',
  `create_time` datetime NOT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='公告' AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `news`
--

INSERT INTO `news` (`id`, `title`, `content`, `user_id`, `create_time`) VALUES
(1, '1212', '2112', 1, '2015-09-05 16:29:41'),
(2, '你好，你是谁', '你好这是我们的测试文件，我们会继续前进', 1, '2015-09-09 09:02:24'),
(3, '11', '112', 1, '2015-09-11 12:54:27');

-- --------------------------------------------------------

--
-- 表的结构 `news_unread`
--

CREATE TABLE IF NOT EXISTS `news_unread` (
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `news_id` int(11) NOT NULL COMMENT '公告id'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='公告未读';

--
-- 转存表中的数据 `news_unread`
--

INSERT INTO `news_unread` (`user_id`, `news_id`) VALUES
(10, 2),
(3, 1),
(4, 1),
(6, 1),
(5, 1),
(2, 1),
(9, 2),
(7, 2),
(8, 2),
(11, 2),
(10, 3),
(9, 3),
(7, 3),
(8, 3),
(11, 3);

-- --------------------------------------------------------

--
-- 表的结构 `permissions_actions`
--

CREATE TABLE IF NOT EXISTS `permissions_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roles_access_control` varchar(50) NOT NULL COMMENT '系统角色',
  `namespace` varchar(50) NOT NULL COMMENT '命名空间',
  `controller` varchar(50) NOT NULL COMMENT '控制器',
  `action` varchar(50) NOT NULL COMMENT '动作',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `desc` varchar(200) NOT NULL COMMENT '描述',
  `display_flag` tinyint(3) NOT NULL DEFAULT '2' COMMENT '显示标志',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `param` varchar(200) DEFAULT NULL COMMENT '参数',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='NCA列表' AUTO_INCREMENT=961 ;

--
-- 转存表中的数据 `permissions_actions`
--

INSERT INTO `permissions_actions` (`id`, `roles_access_control`, `namespace`, `controller`, `action`, `name`, `desc`, `display_flag`, `sort`, `param`) VALUES
(154, 'ACL_NULL', 'default', '', '', '默认', '', 2, 0, ''),
(155, 'ACL_NULL', 'default', 'default', '', 'default', '', 2, 0, ''),
(156, 'ACL_EVERYONE', 'default', 'default', 'index', 'Index', 'Index', 2, 0, ''),
(157, 'ACL_EVERYONE', 'default', 'default', 'login', '用户登入', '用户登入', 2, 0, ''),
(158, 'ACL_EVERYONE', 'default', 'default', 'logout', '用户登出', '用户登出', 2, 0, ''),
(159, 'ACL_NULL', 'setting', '', '', '设置', '设置', 1, 80, ''),
(160, 'ACL_NULL', 'setting', 'permissions', '', '权限管理', '', 1, 20, ''),
(161, 'ACL_NULL', 'setting', 'permissions', 'refreshnca', 'NCA刷新', 'NCA刷新', 1, 50, ''),
(162, 'ACL_NULL', 'setting', 'permissions', 'ncalist', 'NCA列表', 'NCA列表', 1, 10, ''),
(163, 'ACL_NULL', 'setting', 'permissions', 'ncasave', 'NCA编辑（保存）', 'NCA编辑（保存）', 2, 0, ''),
(164, 'ACL_NULL', 'setting', 'permissions', 'refreshaclfile', 'ACL刷新', 'ACL刷新', 1, 60, ''),
(762, 'ACL_NULL', 'setting', 'user', 'treesave', '树形分配（保存）', '树形分配（保存）', 2, 0, ''),
(169, 'ACL_NULL', 'setting', 'user', '', '用户管理', '', 1, 10, ''),
(170, 'ACL_HAS_ROLE', 'setting', 'user', 'passwordchange', '密码修改', '密码修改', 1, 40, ''),
(171, 'ACL_HAS_ROLE', 'setting', 'user', 'passwordchangesave', '密码修改（保存）', '密码修改（保存）', 2, 0, ''),
(172, 'ACL_NULL', 'setting', 'user', 'list', '列表', '列表', 1, 20, ''),
(173, 'ACL_NULL', 'setting', 'user', 'passwordreset', '密码重置', '密码重置', 2, 0, ''),
(174, 'ACL_NULL', 'setting', 'user', 'passwordresetsave', '密码重置（保存）', '密码重置（保存）', 2, 0, ''),
(175, 'ACL_NULL', 'setting', 'user', 'create', '创建', '创建', 2, 10, ''),
(176, 'ACL_NULL', 'setting', 'user', 'createsave', '创建（保存）', '创建（保存）', 2, 0, ''),
(761, 'ACL_HAS_ROLE', 'setting', 'permissions', 'refreshsession', '刷新session', '刷新session', 2, 0, ''),
(246, 'ACL_NULL', 'setting', 'basic', '', '基础设置', '', 1, 30, ''),
(814, 'ACL_NULL', 'setting', 'basic', 'index', '测试', '测试', 2, 0, ''),
(953, 'ACL_NULL', 'cust', 'main', 'inship', '', '', 2, NULL, NULL),
(952, 'ACL_NULL', 'cust', 'main', 'agentdetail', '代理商详情', '代理商详情', 2, NULL, NULL),
(823, 'ACL_NULL', 'setting', 'basic', 'companyinfo', '企业信息', '企业信息', 1, 50, ''),
(824, 'ACL_NULL', 'setting', 'basic', 'companyinfosave', '企业信息保存', '企业信息保存', 2, 0, ''),
(951, 'ACL_NULL', 'cust', 'main', 'agent', '代理管理', '代理管理', 2, NULL, NULL),
(950, 'ACL_NULL', 'cust', 'main', 'productlist', '商品分类', '商品分类', 2, NULL, NULL),
(949, 'ACL_NULL', 'cust', 'main', 'productdetail', '商品详情', '商品详情', 2, NULL, NULL),
(263, 'ACL_NULL', 'default', 'cron', '', 'cron', '', 2, 0, ''),
(325, 'ACL_NULL', 'setting', 'user', 'setoffdutycleanmapping', '设置离职&清空权限', '设置离职&清空权限', 2, 0, ''),
(765, 'ACL_NULL', 'setting', 'user', 'copy', '复制权限', '复制权限', 2, 0, ''),
(764, 'ACL_NULL', 'setting', 'roles', 'treesave', '树形结构（保存）', '树形结构（保存）', 2, NULL, NULL),
(763, 'ACL_NULL', 'setting', 'roles', 'delete', '删除', '删除', 2, NULL, NULL),
(468, 'ACL_NULL', 'default', 'public', '', 'public', '', 2, 0, ''),
(862, 'ACL_NULL', 'datamanage', 'product', 'createsave', '新建保存', '新建保存', 2, NULL, NULL),
(863, 'ACL_NULL', 'datamanage', 'product', 'detail', '产品详细', '产品详细', 2, NULL, NULL),
(864, 'ACL_NULL', 'datamanage', 'product', 'edit', '产品编辑', '产品编辑', 2, NULL, NULL),
(865, 'ACL_NULL', 'datamanage', 'product', 'editsave', '编辑保存', '编辑保存', 2, NULL, NULL),
(866, 'ACL_NULL', 'datamanage', 'product', 'delete', '删除产品', '删除产品', 2, NULL, NULL),
(867, 'ACL_NULL', 'datamanage', 'agent', 'province', '新建(代理资料)', '新建(代理资料)', 2, NULL, NULL),
(868, 'ACL_NULL', 'datamanage', 'agent', 'createsave', '代理资料保存', '代理资料保存', 2, NULL, NULL),
(869, 'ACL_NULL', 'datamanage', 'agent', 'edit', '代理资料编辑', '代理资料编辑', 2, NULL, NULL),
(870, 'ACL_NULL', 'datamanage', 'agent', 'editsave', '代理资料编辑（保存）', '代理资料编辑（保存）', 2, NULL, NULL),
(871, 'ACL_NULL', 'datamanage', 'agent', 'detail', '代理资料详细', '代理资料详细', 2, NULL, NULL),
(872, 'ACL_NULL', 'datamanage', 'agent', 'audit', '审核', '审核', 2, NULL, NULL),
(880, 'ACL_NULL', 'datamanage', 'agent', 'delete', '删除代理资料', '删除代理资料', 2, NULL, NULL),
(522, 'ACL_HAS_ROLE', 'setting', 'basic', 'newslist', '公告列表', '公告列表', 2, 0, ''),
(523, 'ACL_NULL', 'setting', 'basic', 'newscreate', '创建公告', '创建公告', 2, 0, ''),
(524, 'ACL_NULL', 'setting', 'basic', 'newscreatesave', '创建公告（保存）', '创建公告（保存）', 2, 0, ''),
(525, 'ACL_HAS_ROLE', 'setting', 'basic', 'newsdetail', '公告详细', '公告详细', 2, 0, ''),
(829, 'ACL_NULL', 'setting', 'user', 'headuser', '总部用户', '总部用户', 1, 50, ''),
(830, 'ACL_NULL', 'datamanage', '', '', '资料管理', '资料管理', 1, 10, ''),
(831, 'ACL_NULL', 'datamanage', 'product', '', '产品信息管理', '产品资料', 1, 10, ''),
(832, 'ACL_NULL', 'datamanage', 'product', 'create', '新建', '产品新建', 1, 10, ''),
(833, 'ACL_NULL', 'datamanage', 'product', 'list', '产品列表', '产品列表', 1, 20, ''),
(834, 'ACL_NULL', 'setting', 'user', 'headdelete', '总部用户删除', '总部用户删除', 2, 0, ''),
(960, 'ACL_NULL', 'cust', 'main', 'customer', '用户Ajax', '用户Ajax', 2, NULL, NULL),
(959, 'ACL_NULL', 'cust', 'main', 'outshipsaveajax', '', '', 2, NULL, NULL),
(958, 'ACL_NULL', 'cust', 'main', 'outshipajax', '出库Ajax', '出库Ajax', 2, NULL, NULL),
(957, 'ACL_NULL', 'cust', 'main', 'outshipdelajax', '出货删除Ajax', '出货删除Ajax', 2, NULL, NULL),
(956, 'ACL_NULL', 'cust', 'main', 'outshipdetail', '出库详细', '出库详细', 2, NULL, NULL),
(855, 'ACL_NULL', 'datamanage', 'agent', '', '代理管理', '代理资料', 1, 20, ''),
(856, 'ACL_NULL', 'datamanage', 'agent', 'create', '新建(代理资料)', '新建(代理资料)', 1, 10, ''),
(857, 'ACL_NULL', 'datamanage', 'agent', 'list', '代理资料列表', '代理资料列表', 1, 20, ''),
(559, 'ACL_EVERYONE', 'default', 'default', 'loginexternal', '外部登录', '外部登录', 2, 0, ''),
(570, 'ACL_EVERYONE', 'default', 'default', 'mobilelogin', '手机登录', '手机登录', 2, 0, ''),
(573, 'ACL_NULL', 'setting', 'task', '', '任务管理', '', 1, 50, ''),
(574, 'ACL_NULL', 'setting', 'task', 'create', '新增', '新增任务', 1, 10, ''),
(575, 'ACL_NULL', 'setting', 'task', 'createsave', '新增（保存）', '新增任务（保存）', 2, 0, ''),
(576, 'ACL_NULL', 'setting', 'task', 'edit', '编辑', '编辑任务', 2, 0, ''),
(577, 'ACL_NULL', 'setting', 'task', 'editsave', '编辑（保存）', '编辑任务（保存）', 2, 0, ''),
(578, 'ACL_NULL', 'setting', 'task', 'list', '列表', '任务列表', 1, 20, ''),
(579, 'ACL_NULL', 'setting', 'task', 'detail', '查看详细', '查看详细', 2, 0, ''),
(581, 'ACL_EVERYONE', 'default', 'cron', 'execute', '执行定期任务', '执行定期任务', 2, 0, ''),
(616, 'ACL_NULL', 'default', 'test', '', 'test', '', 2, 0, ''),
(828, 'ACL_NULL', 'default', 'public', 'productingzwarehouse', '查询广州仓产品信息', '查询广州仓产品信息', 2, NULL, NULL),
(766, 'ACL_NULL', 'setting', 'user', 'copysave', '复制权限（保存）', '复制权限（保存）', 2, 0, ''),
(948, 'ACL_NULL', 'cust', 'main', 'product', '商品', '商品', 2, NULL, NULL),
(947, 'ACL_NULL', 'cust', 'main', 'promotiondetail', '政策通告详细', '政策通告详细', 2, NULL, NULL),
(946, 'ACL_NULL', 'cust', 'main', 'promotion', '政策通告', '政策通告', 2, NULL, NULL),
(945, 'ACL_NULL', 'cust', 'main', 'trackingproduct', '产品追踪', '产品追踪', 2, NULL, NULL),
(944, 'ACL_NULL', 'cust', 'main', 'editpasswordsave', '修改密码保存', '修改密码保存', 2, NULL, NULL),
(943, 'ACL_NULL', 'cust', 'main', 'editpassword', '修改密码', '修改密码', 2, NULL, NULL),
(931, 'ACL_EVERYONE', 'default', 'default', 'moblogin', '手机登陆', '手机登陆', 2, 0, ''),
(932, 'ACL_NULL', 'default', 'public', 'login', '', '', 2, NULL, NULL),
(933, 'ACL_NULL', 'default', 'public', 'khdata', '', '', 2, NULL, NULL),
(934, 'ACL_NULL', 'default', 'public', 'cpdata', '', '', 2, NULL, NULL),
(935, 'ACL_NULL', 'default', 'public', 'loadkh', '', '', 2, NULL, NULL),
(936, 'ACL_NULL', 'default', 'public', 'loadcp', '', '', 2, NULL, NULL),
(937, 'ACL_NULL', 'default', 'public', 'filetodata', '', '', 2, NULL, NULL),
(942, 'ACL_NULL', 'cust', 'main', 'editinfosave', '个人信息保存', '个人信息保存', 2, NULL, NULL),
(686, 'ACL_NULL', 'default', 'test', 'index', '', '', 2, NULL, NULL),
(955, 'ACL_NULL', 'cust', 'main', 'outship', '出库管理', '出库管理', 2, NULL, NULL),
(954, 'ACL_NULL', 'cust', 'main', 'inshipdetail', '入库详细', '入库详细', 2, NULL, NULL),
(729, 'ACL_NULL', 'setting', 'basic', 'deletecache', '清空缓存', '清空缓存', 1, 60, ''),
(941, 'ACL_NULL', 'cust', 'main', 'editinfo', '修改个人信息', '修改个人信息', 2, NULL, NULL),
(940, 'ACL_NULL', 'cust', 'main', 'personcenter', '个人中心', '个人中心', 2, NULL, NULL),
(939, 'ACL_NULL', 'cust', 'main', '', 'main', '', 2, NULL, NULL),
(938, 'ACL_NULL', 'cust', '', '', 'cust', '', 2, NULL, NULL),
(749, 'ACL_NULL', 'setting', 'roles', '', '角色管理', '', 1, 15, ''),
(750, 'ACL_NULL', 'setting', 'roles', 'create', '新增', '新增', 1, 0, ''),
(751, 'ACL_NULL', 'setting', 'roles', 'createsave', '新增（保存）', '新增（保存）', 2, 0, ''),
(752, 'ACL_NULL', 'setting', 'roles', 'list', '列表', '列表', 1, 0, ''),
(753, 'ACL_NULL', 'setting', 'roles', 'allotaction', '分配action', '分配action', 2, 0, ''),
(754, 'ACL_NULL', 'setting', 'roles', 'allotactionsave', '分配action（保存）', '分配action（保存）', 2, 0, ''),
(755, 'ACL_NULL', 'setting', 'roles', 'allotuser', '分配用户', '分配用户', 2, 0, ''),
(756, 'ACL_NULL', 'setting', 'roles', 'allotusersave', '分配用户（保存）', '分配用户（保存）', 2, 0, ''),
(757, 'ACL_NULL', 'setting', 'user', 'tree', '树形列表', '树形列表', 1, 30, ''),
(758, 'ACL_NULL', 'setting', 'user', 'bind', '分配权限', '分配权限', 2, 0, ''),
(759, 'ACL_NULL', 'setting', 'user', 'bindsave', '分配权限（保存）', '分配权限（保存）', 2, 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `permissions_actions_user_mapping`
--

CREATE TABLE IF NOT EXISTS `permissions_actions_user_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用id',
  `permissions_actions_id` int(11) NOT NULL COMMENT 'NCAid',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `permissions_actions_id` (`permissions_actions_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='权限映射' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_no` varchar(30) NOT NULL,
  `p_name` varchar(50) NOT NULL,
  `p_point` int(11) DEFAULT '0',
  `p_price1` float DEFAULT '0',
  `p_price2` float DEFAULT '0',
  `p_price3` float DEFAULT '0',
  `p_price4` float DEFAULT '0',
  `p_price5` float DEFAULT '0',
  `p_price6` float DEFAULT '0',
  `p_price7` float DEFAULT '0',
  `p_price8` float DEFAULT '0',
  `p_spec` varchar(30) DEFAULT NULL,
  `p_pic1` varchar(50) DEFAULT NULL,
  `p_details` text,
  `p_price` float DEFAULT NULL,
  `p_num1` int(11) DEFAULT NULL,
  `p_num2` int(11) DEFAULT NULL,
  `p_num3` int(11) DEFAULT NULL,
  `p_num4` int(11) DEFAULT NULL,
  `p_num5` int(11) DEFAULT NULL,
  `p_num6` int(11) DEFAULT NULL,
  `p_num7` int(11) DEFAULT NULL,
  `p_num8` int(11) DEFAULT NULL,
  `p_num` int(11) DEFAULT NULL,
  `p_pic2` varchar(50) DEFAULT NULL,
  `p_pic3` varchar(50) DEFAULT NULL,
  `p_type` varchar(10) DEFAULT NULL,
  `p_whole` int(11) DEFAULT NULL,
  `p_total_count` int(11) DEFAULT NULL,
  `p_sold_count` int(11) DEFAULT NULL,
  `ma_c_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='产品表' AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `product`
--

INSERT INTO `product` (`id`, `p_no`, `p_name`, `p_point`, `p_price1`, `p_price2`, `p_price3`, `p_price4`, `p_price5`, `p_price6`, `p_price7`, `p_price8`, `p_spec`, `p_pic1`, `p_details`, `p_price`, `p_num1`, `p_num2`, `p_num3`, `p_num4`, `p_num5`, `p_num6`, `p_num7`, `p_num8`, `p_num`, `p_pic2`, `p_pic3`, `p_type`, `p_whole`, `p_total_count`, `p_sold_count`, `ma_c_id`) VALUES
(1, '1', '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, './productimg/xffw.jpg', '111111111qqqq222', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '80', NULL, NULL, NULL, 1),
(2, '2', '11212', 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, './productimg/back.png', '121qqqq······', 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '80', NULL, NULL, NULL, 2),
(4, '000003', '诺基亚手机', 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, './productimg/dly_sto_express.jpg', '这是诺基亚手机，你值得拥有哦', 1200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '50', NULL, NULL, NULL, 1),
(5, '000004', '1224', 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, './productimg/top2.jpg', '2', 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', NULL, NULL, NULL, NULL),
(6, '000005', '标签', 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, './productimg/er.png', '1111', 0.02, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '25*16', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `desc` text,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=242 ;

--
-- 转存表中的数据 `roles`
--

INSERT INTO `roles` (`id`, `parent_id`, `desc`, `sort`) VALUES
(1, 0, 'Admin', 0),
(175, 217, '设置-用户管理-密码修改', 0),
(176, 217, '设置-用户管理-列表', 0),
(177, 217, '设置-用户管理-创建', 0),
(178, 217, '设置-用户管理-列表[分配权限]', 0),
(241, 0, '一级代理', 0),
(216, 0, '设置', 0),
(217, 216, '设置-用户管理', 0),
(218, 216, '设置-基础设置', 0),
(219, 217, '设置-用户管理-列表[复制权限]', 0);

-- --------------------------------------------------------

--
-- 表的结构 `roles_actions_mapping`
--

CREATE TABLE IF NOT EXISTS `roles_actions_mapping` (
  `roles_id` int(11) NOT NULL,
  `permissions_actions_id` int(11) NOT NULL,
  PRIMARY KEY (`roles_id`,`permissions_actions_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `roles_actions_mapping`
--

INSERT INTO `roles_actions_mapping` (`roles_id`, `permissions_actions_id`) VALUES
(1, 156),
(1, 157),
(1, 158),
(1, 161),
(1, 162),
(1, 163),
(1, 164),
(1, 170),
(1, 171),
(1, 172),
(1, 173),
(1, 174),
(1, 175),
(1, 176),
(1, 325),
(1, 522),
(1, 523),
(1, 524),
(1, 525),
(1, 559),
(1, 570),
(1, 574),
(1, 575),
(1, 576),
(1, 577),
(1, 578),
(1, 579),
(1, 581),
(1, 686),
(1, 729),
(1, 750),
(1, 751),
(1, 752),
(1, 753),
(1, 754),
(1, 755),
(1, 756),
(1, 757),
(1, 758),
(1, 759),
(1, 761),
(1, 762),
(1, 763),
(1, 764),
(1, 765),
(1, 766),
(1, 814),
(1, 823),
(1, 824),
(1, 828),
(1, 829),
(1, 832),
(1, 833),
(1, 834),
(1, 856),
(1, 857),
(1, 862),
(1, 863),
(1, 864),
(1, 865),
(1, 866),
(1, 867),
(1, 868),
(1, 869),
(1, 870),
(1, 871),
(1, 872),
(1, 880),
(1, 931),
(1, 932),
(1, 933),
(1, 934),
(1, 935),
(1, 936),
(1, 937),
(1, 940),
(1, 941),
(1, 942),
(1, 943),
(1, 944),
(1, 945),
(1, 946),
(1, 947),
(1, 948),
(1, 949),
(1, 950),
(1, 951),
(1, 952),
(1, 953),
(1, 954),
(1, 955),
(1, 956),
(1, 957),
(1, 958),
(1, 959),
(1, 960),
(175, 170),
(175, 171),
(176, 172),
(177, 175),
(177, 176),
(178, 758),
(178, 759),
(218, 522),
(218, 523),
(218, 524),
(218, 525),
(218, 729),
(219, 765),
(219, 766),
(241, 156),
(241, 157),
(241, 158),
(241, 164),
(241, 170),
(241, 171),
(241, 172),
(241, 173),
(241, 174),
(241, 175),
(241, 176),
(241, 325),
(241, 522),
(241, 523),
(241, 524),
(241, 525),
(241, 559),
(241, 570),
(241, 581),
(241, 729),
(241, 752),
(241, 753),
(241, 754),
(241, 755),
(241, 756),
(241, 757),
(241, 758),
(241, 759),
(241, 762),
(241, 765),
(241, 766),
(241, 832),
(241, 833),
(241, 856),
(241, 857),
(241, 862),
(241, 863),
(241, 864),
(241, 865),
(241, 866),
(241, 867),
(241, 868),
(241, 869),
(241, 870),
(241, 871),
(241, 872),
(241, 880);

-- --------------------------------------------------------

--
-- 表的结构 `roles_users_mapping`
--

CREATE TABLE IF NOT EXISTS `roles_users_mapping` (
  `roles_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`roles_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `roles_users_mapping`
--

INSERT INTO `roles_users_mapping` (`roles_id`, `user_id`) VALUES
(1, 1),
(2, 2),
(2, 3),
(2, 4),
(3, 2),
(3, 3),
(3, 4),
(4, 2),
(4, 3),
(4, 4),
(5, 2),
(5, 3),
(5, 4),
(6, 2),
(6, 3),
(6, 4),
(7, 2),
(7, 3),
(7, 4),
(8, 2),
(8, 3),
(8, 4),
(9, 2),
(9, 3),
(9, 4),
(10, 2),
(10, 3),
(10, 4),
(11, 2),
(11, 3),
(11, 4),
(12, 2),
(12, 3),
(12, 4),
(13, 2),
(13, 3),
(13, 4),
(14, 2),
(14, 3),
(14, 4),
(15, 2),
(15, 3),
(15, 4),
(16, 2),
(16, 3),
(16, 4),
(17, 2),
(17, 3),
(17, 4),
(18, 2),
(18, 3),
(18, 4),
(19, 2),
(19, 3),
(19, 4),
(20, 2),
(20, 3),
(20, 4),
(21, 2),
(21, 3),
(21, 4),
(22, 2),
(22, 3),
(22, 4),
(23, 2),
(23, 3),
(23, 4),
(24, 2),
(24, 3),
(24, 4),
(25, 2),
(25, 3),
(25, 4),
(26, 2),
(26, 3),
(26, 4),
(27, 2),
(27, 3),
(27, 4),
(28, 2),
(28, 3),
(28, 4),
(29, 2),
(29, 3),
(29, 4),
(30, 2),
(30, 3),
(30, 4),
(31, 2),
(31, 3),
(31, 4),
(32, 2),
(32, 3),
(32, 4),
(33, 2),
(33, 3),
(33, 4),
(34, 2),
(34, 3),
(34, 4),
(35, 2),
(35, 3),
(35, 4),
(36, 2),
(36, 3),
(36, 4),
(37, 2),
(37, 3),
(37, 4),
(38, 2),
(38, 3),
(38, 4),
(39, 2),
(39, 3),
(39, 4),
(72, 2),
(72, 3),
(73, 2),
(73, 3),
(92, 2),
(92, 3),
(92, 4),
(93, 2),
(93, 3),
(93, 4),
(94, 2),
(94, 3),
(94, 4),
(95, 2),
(95, 3),
(95, 4),
(100, 2),
(100, 3),
(100, 4),
(101, 2),
(101, 3),
(101, 4),
(102, 2),
(102, 3),
(102, 4),
(103, 2),
(103, 3),
(103, 4),
(104, 2),
(104, 3),
(104, 4),
(105, 2),
(105, 3),
(105, 4),
(106, 2),
(106, 3),
(106, 4),
(107, 2),
(107, 3),
(107, 4),
(108, 2),
(108, 3),
(108, 4),
(109, 2),
(109, 3),
(109, 4),
(110, 2),
(110, 3),
(110, 4),
(111, 2),
(111, 3),
(111, 4),
(112, 2),
(112, 3),
(112, 4),
(113, 2),
(113, 3),
(113, 4),
(114, 2),
(114, 3),
(114, 4),
(115, 2),
(115, 3),
(115, 4),
(116, 2),
(116, 3),
(116, 4),
(117, 2),
(117, 3),
(117, 4),
(118, 2),
(118, 3),
(118, 4),
(119, 2),
(119, 3),
(119, 4),
(120, 2),
(120, 3),
(120, 4),
(121, 2),
(121, 3),
(121, 4),
(122, 2),
(122, 3),
(122, 4),
(123, 2),
(123, 3),
(123, 4),
(124, 2),
(124, 3),
(124, 4),
(125, 2),
(125, 3),
(125, 4),
(126, 2),
(126, 3),
(126, 4),
(127, 2),
(127, 3),
(127, 4),
(128, 2),
(128, 3),
(128, 4),
(129, 2),
(129, 3),
(129, 4),
(130, 2),
(130, 3),
(130, 4),
(131, 2),
(131, 3),
(131, 4),
(132, 2),
(132, 3),
(132, 4),
(133, 2),
(133, 3),
(133, 4),
(134, 2),
(134, 3),
(134, 4),
(135, 2),
(135, 3),
(135, 4),
(136, 2),
(136, 3),
(136, 4),
(137, 2),
(137, 3),
(137, 4),
(138, 2),
(138, 3),
(138, 4),
(139, 2),
(139, 3),
(139, 4),
(140, 2),
(140, 3),
(140, 4),
(141, 2),
(141, 3),
(141, 4),
(142, 2),
(142, 3),
(142, 4),
(143, 2),
(143, 3),
(143, 4),
(144, 2),
(144, 3),
(144, 4),
(145, 2),
(145, 3),
(145, 4),
(146, 2),
(146, 3),
(146, 4),
(147, 2),
(147, 3),
(147, 4),
(148, 2),
(148, 3),
(148, 4),
(149, 2),
(149, 3),
(149, 4),
(150, 2),
(150, 3),
(150, 4),
(151, 2),
(151, 3),
(151, 4),
(152, 2),
(152, 3),
(152, 4),
(153, 2),
(153, 3),
(153, 4),
(154, 2),
(154, 3),
(154, 4),
(155, 2),
(155, 3),
(155, 4),
(156, 2),
(156, 3),
(156, 4),
(157, 2),
(157, 3),
(157, 4),
(158, 2),
(158, 3),
(158, 4),
(160, 2),
(160, 3),
(160, 4),
(161, 2),
(161, 3),
(161, 4),
(162, 2),
(162, 3),
(162, 4),
(163, 2),
(163, 3),
(163, 4),
(164, 2),
(164, 3),
(164, 4),
(168, 2),
(168, 3),
(168, 4),
(169, 2),
(169, 3),
(169, 4),
(170, 2),
(170, 3),
(170, 4),
(171, 2),
(171, 3),
(171, 4),
(172, 2),
(172, 3),
(172, 4),
(173, 2),
(173, 3),
(173, 4),
(175, 2),
(175, 3),
(175, 4),
(178, 11),
(181, 2),
(181, 3),
(181, 4),
(182, 2),
(182, 3),
(182, 4),
(183, 2),
(183, 3),
(183, 4),
(184, 2),
(184, 3),
(185, 2),
(185, 3),
(185, 4),
(186, 2),
(186, 3),
(186, 4),
(187, 2),
(187, 3),
(187, 4),
(188, 2),
(188, 3),
(188, 4),
(189, 2),
(189, 3),
(189, 4),
(190, 2),
(190, 3),
(190, 4),
(191, 2),
(191, 3),
(191, 4),
(201, 2),
(201, 3),
(203, 2),
(203, 3),
(203, 4),
(204, 2),
(204, 3),
(204, 4),
(206, 2),
(206, 3),
(206, 4),
(207, 2),
(207, 3),
(207, 4),
(213, 2),
(213, 3),
(213, 4),
(214, 2),
(214, 3),
(214, 4),
(215, 2),
(215, 3),
(215, 4),
(218, 7),
(218, 8),
(218, 11),
(220, 2),
(220, 3),
(220, 4),
(221, 2),
(221, 3),
(221, 4),
(222, 2),
(222, 3),
(222, 4),
(223, 2),
(223, 3),
(223, 4),
(240, 4),
(241, 13);

-- --------------------------------------------------------

--
-- 表的结构 `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `company_name` varchar(200) DEFAULT NULL COMMENT '公司名',
  `company_url` varchar(500) DEFAULT NULL COMMENT '企业网址',
  `company_contact` varchar(500) DEFAULT NULL COMMENT '企业联系方式',
  `freeze_reply` varchar(400) DEFAULT NULL COMMENT '冻结回复',
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='基本设置' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `setting`
--

INSERT INTO `setting` (`id`, `company_name`, `company_url`, `company_contact`, `freeze_reply`, `create_time`) VALUES
(1, '深圳信诚防伪科技有限公司', 'http://www.xc-12315.net/', '1380038000', '你好，你查询的产品已被冻结，不是正品，谨防假冒！', '2015-09-11 08:37:51');

-- --------------------------------------------------------

--
-- 表的结构 `task`
--

CREATE TABLE IF NOT EXISTS `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '任务名称',
  `model` varchar(200) NOT NULL COMMENT '任务执行的model',
  `regex` varchar(500) NOT NULL,
  `seconds` int(11) NOT NULL COMMENT '执行允许秒数',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `status` tinyint(3) NOT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='定期执行任务列表' AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `task`
--

INSERT INTO `task` (`id`, `name`, `model`, `regex`, `seconds`, `create_time`, `status`) VALUES
(1, '更新系统图片索引', 'updateProductImgUrl', '/[0-9]{4}-[0-9]{2}-[0-9]{2}\\s(03|11):35/', 0, '2014-12-04 10:13:46', 1),
(2, '更新CK1订单追踪号和物流费', 'updateCK1SalesOrderTracking', '/[0-9]{4}-[0-9]{2}-[0-9]{2}\\s23:35/', 0, '2014-12-31 17:31:02', 1);

-- --------------------------------------------------------

--
-- 表的结构 `task_log`
--

CREATE TABLE IF NOT EXISTS `task_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL COMMENT '任务id',
  `ack` tinyint(3) NOT NULL COMMENT '执行结果',
  `message` text COMMENT '返回信息',
  `execute_time` decimal(10,4) DEFAULT NULL COMMENT '执行时长',
  `time` datetime NOT NULL COMMENT '执行时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='任务执行时间表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `upload_files`
--

CREATE TABLE IF NOT EXISTS `upload_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL COMMENT '文件名',
  `path` varchar(500) NOT NULL COMMENT '路径',
  `type` tinyint(3) NOT NULL COMMENT '类型',
  `time` datetime NOT NULL COMMENT '上传时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='上传文件表' AUTO_INCREMENT=52 ;

--
-- 转存表中的数据 `upload_files`
--

INSERT INTO `upload_files` (`id`, `name`, `path`, `type`, `time`) VALUES
(1, '光疗胶00639-00938 - 副本.xls', 'product_basic/1417340124-Gordon.xls', 3, '2014-11-30 17:35:24'),
(2, '光疗胶00639-00938 - 副本.xls', 'product_basic/1417340162-Gordon.xls', 3, '2014-11-30 17:36:02'),
(3, '光疗胶00639-00938 - 副本.xls', 'product_basic/1417340202-Gordon.xls', 3, '2014-11-30 17:36:42'),
(4, '产品基本信息表-20141127154441.xls', 'product_basic/1417340352-Gordon.xls', 3, '2014-11-30 17:39:12'),
(5, '产品基本信息表-20141127154441.xls', 'product_basic/1417340369-Gordon.xls', 3, '2014-11-30 17:39:29'),
(6, '产品基本信息表-20141127154441.xls', 'product_basic/1417340469-Gordon.xls', 3, '2014-11-30 17:41:09'),
(7, '产品基本信息表-20141127154441.xls', 'product_basic/1417340512-Gordon.xls', 3, '2014-11-30 17:41:52'),
(8, '产品基本信息表-20141127154441.xls', 'product_basic/1417340548-Gordon.xls', 3, '2014-11-30 17:42:28'),
(9, '产品基本信息表-20141127154441.xls', 'product_basic/1417340755-Gordon.xls', 3, '2014-11-30 17:45:55'),
(10, '光疗胶000639-000724 -NEW.xls', 'product_basic/1418719284-Gordon.xls', 3, '2014-12-16 16:41:24'),
(11, '美容工具000101-000119.xls', 'product_basic/1418719856-Gordon.xls', 3, '2014-12-16 16:50:56'),
(12, '手表000001-000059.xls', 'product_basic/1418719885-Gordon.xls', 3, '2014-12-16 16:51:25'),
(13, '手表001239-001584.xls', 'product_basic/1418719901-Gordon.xls', 3, '2014-12-16 16:51:41'),
(14, '手表001239-001584.xls', 'product_basic/1418719915-Gordon.xls', 3, '2014-12-16 16:51:55'),
(15, '手表001239-001584.xls', 'product_basic/1418719954-Gordon.xls', 3, '2014-12-16 16:52:34'),
(16, '手表001239-001584.xls', 'product_basic/1418719994-Gordon.xls', 3, '2014-12-16 16:53:14'),
(17, '手表001585-001979.xls', 'product_basic/1418720003-Gordon.xls', 3, '2014-12-16 16:53:23'),
(18, '手表001980-002103.xls', 'product_basic/1418720015-Gordon.xls', 3, '2014-12-16 16:53:35'),
(19, '手表001239-001584.xls', 'product_basic/1418720026-Gordon.xls', 3, '2014-12-16 16:53:46'),
(20, '手表001239-001584.xlsx', 'product_basic/1418720114-Gordon.xlsx', 3, '2014-12-16 16:55:14'),
(21, '手表001239-001584.xls', 'product_basic/1418720757-Gordon.xls', 3, '2014-12-16 17:05:57'),
(22, '手表001239-001584.xlsx', 'product_basic/1418720765-Gordon.xlsx', 3, '2014-12-16 17:06:05'),
(23, '产品基本信息表-20141216170710.xls', 'product_basic/1418720942-Gordon.xls', 3, '2014-12-16 17:09:02'),
(24, '002239-002346.xls', 'product_basic/1418720991-Gordon.xls', 3, '2014-12-16 17:09:51'),
(25, 'Albeni 000939-001018SKU.xls', 'product_basic/1418721119-Gordon.xls', 3, '2014-12-16 17:11:59'),
(26, 'auto产品基本信息表.xls', 'product_basic/1418721138-Gordon.xls', 3, '2014-12-16 17:12:18'),
(27, '002439-002538.xlsx', 'product_basic/1420431315-will.xlsx', 3, '2015-01-05 12:15:15'),
(28, '002439-002538.xlsx', 'product_basic/1420431319-will.xlsx', 3, '2015-01-05 12:15:19'),
(29, '002439-002538.xlsx', 'product_basic/1420431370-will.xlsx', 3, '2015-01-05 12:16:10'),
(30, '002439-002538.xlsx', 'product_basic/1420431383-will.xlsx', 3, '2015-01-05 12:16:23'),
(31, '002439-002538.xlsx', 'product_basic/1420431436-will.xlsx', 3, '2015-01-05 12:17:16'),
(32, '002439-002538.xlsx', 'product_basic/1420431561-will.xlsx', 3, '2015-01-05 12:19:21'),
(33, '产品基本信息表-20150105121959.xls', 'product_basic/1420432235-will.xls', 3, '2015-01-05 12:30:35'),
(34, '000001-000059.xls', 'product_basic/1420619801-will.xls', 3, '2015-01-07 16:36:41'),
(35, '000101-000119.xls', 'product_basic/1420619824-will.xls', 3, '2015-01-07 16:37:04'),
(36, '000301-000596.xls', 'product_basic/1420619857-will.xls', 3, '2015-01-07 16:37:37'),
(37, '000639-000724.xls', 'product_basic/1420619883-will.xls', 3, '2015-01-07 16:38:03'),
(38, '000639-000724.xls', 'product_basic/1420619888-will.xls', 3, '2015-01-07 16:38:08'),
(39, '000639-000724.xls', 'product_basic/1420619906-will.xls', 3, '2015-01-07 16:38:26'),
(40, '000939-001018.xls', 'product_basic/1420619947-will.xls', 3, '2015-01-07 16:39:07'),
(41, '001239-002103.xls', 'product_basic/1420620056-will.xls', 3, '2015-01-07 16:40:56'),
(42, '002239-002346.xls', 'product_basic/1420620085-will.xls', 3, '2015-01-07 16:41:25'),
(43, '002539-002565.xls', 'product_basic/1421051545-will.xls', 3, '2015-01-12 16:32:25'),
(44, '000060-000089.xls', 'product_basic/1425365200-will.xls', 3, '2015-03-03 14:46:40'),
(45, 'chiefs-000122-000150-nail.xls', 'product_basic/1426044483-chiefs.xls', 3, '2015-03-11 11:28:03'),
(46, '000600-000611(应急电源）.xls', 'product_basic/1426144549-will.xls', 3, '2015-03-12 15:15:49'),
(47, '产品基本信息表-20150310123741 - 副本.xls', 'product_basic/1426582802-chiefs.xls', 3, '2015-03-17 17:00:02'),
(48, '产品基本信息表-20150318140850.xls', 'product_basic/1426658987-will.xls', 3, '2015-03-18 14:09:47'),
(49, '产品基本信息表-20150320135710.xls', 'product_basic/1426831391-will.xls', 3, '2015-03-20 14:03:11'),
(50, '产品基本信息表-20150322110944.xls', 'product_basic/1427011121-will.xls', 3, '2015-03-22 15:58:41'),
(51, '002104-002128.xls', 'product_basic/1427706102-will.xls', 3, '2015-03-30 17:01:42');

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL COMMENT '名称',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `type` tinyint(3) NOT NULL COMMENT '类型',
  `registered_time` datetime NOT NULL COMMENT '注册时间',
  `login_count` int(11) DEFAULT NULL COMMENT '登录次数',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态',
  `key` varchar(50) DEFAULT NULL COMMENT '密钥',
  `is_agent` tinyint(4) NOT NULL COMMENT '是否代理',
  `rank_id` int(11) DEFAULT NULL COMMENT '代理等级',
  `realname` varchar(60) DEFAULT NULL,
  `weixin` varchar(200) DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `identityid` varchar(300) DEFAULT NULL,
  `address` text,
  `identity_img` varchar(500) DEFAULT NULL,
  `person_img` varchar(500) DEFAULT NULL,
  `touxiang_img` varchar(500) DEFAULT NULL,
  `level` varchar(20) DEFAULT NULL COMMENT '代理等级',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户表' AUTO_INCREMENT=16 ;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`id`, `parent_id`, `name`, `password`, `type`, `registered_time`, `login_count`, `last_login_time`, `status`, `key`, `is_agent`, `rank_id`, `realname`, `weixin`, `tel`, `phone`, `email`, `identityid`, `address`, `identity_img`, `person_img`, `touxiang_img`, `level`) VALUES
(1, 0, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 1, '2016-02-01 00:00:00', 199, '2016-04-27 14:51:05', 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 1, 'yezi', 'e10adc3949ba59abbe56e057f20f883e', 2, '2015-09-06 17:07:48', 4, '2016-01-12 08:56:44', 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 1, '212', 'b59c67bf196a4758191e42f76670ceba', 2, '2015-09-06 14:48:13', 0, '2015-09-06 14:48:13', 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 1, 'ft', '49af3b640275c9b552a5f3f3d96a6062', 2, '2015-09-06 09:22:43', 2, '2015-09-06 09:27:50', 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 7, 'test2', 'e10adc3949ba59abbe56e057f20f883e', 2, '2015-09-05 16:46:53', 0, '2015-09-05 16:46:53', 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 1, 'test', 'e10adc3949ba59abbe56e057f20f883e', 2, '2015-09-05 16:36:02', 0, '2015-09-05 16:36:02', 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 0, 'ydgall', 'e10adc3949ba59abbe56e057f20f883e', 2, '2015-09-18 11:47:47', 1, '2015-09-18 11:49:43', 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 1, '2321', 'c82fd0f1b2f4828937b8f5000883b27f', 2, '2016-04-27 15:16:22', 0, '2016-04-27 15:16:22', 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `user_department`
--

CREATE TABLE IF NOT EXISTS `user_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户部门表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `weixin`
--

CREATE TABLE IF NOT EXISTS `weixin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `us_id` int(11) NOT NULL,
  `appId` varchar(50) NOT NULL,
  `appSecret` varchar(50) NOT NULL,
  `mchId` varchar(20) DEFAULT NULL,
  `apiKey` varchar(50) DEFAULT NULL,
  `paySignKey` varchar(150) DEFAULT NULL,
  `access_token` varchar(150) NOT NULL,
  `access_time` datetime DEFAULT NULL,
  `jsapi_ticket` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信接口' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
