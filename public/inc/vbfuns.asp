<SCRIPT LANGUAGE=javascript RUNAT=Server>
function java_eval(str){return eval(str)}
function java_val(o,str){return o[str]}
</SCRIPT>
<%
Function urlDecode(strCode)
    Dim strChar,i
    For i = 1 To Len(strCode)
        strChar = Mid(strCode, i, 1)
        If strChar = "%" Then
            strHex = Mid(strCode, i + 1, 2)
            i = i + 2
            strChar = Mid(strCode, i + 1, 1)
            If strChar = "%" Then
                i = i + 1
                strHex = strHex & Mid(strCode, i + 1, 2)
                i = i + 2
            End If
            urlDecode = urlDecode & Chr("&H" & strHex)
        Else
            urlDecode = urlDecode & strChar
        End If
    Next
End Function

Function urlEncode(strCode)
    Dim i
    For i = 1 To Len(strCode)
        strHex = Right("0000" & Hex(Asc(Mid(strCode, i, 1))), 4)
        urlEncode = urlEncode & "%" & Left(strHex, 2) & "%" & Right(strHex, 2)
    Next
End Function

Function UnEscape_(str)
    Dim i
    Dim tempStr
    Dim Char
    tempStr = Split(str, "%")
    If UBound(tempStr) >= 0 Then
        Char = tempStr(0)
        For i = 1 To UBound(tempStr)
            If Left(tempStr(i), 1) = "u" Then
                Char = Char & ChrW("&H" & Mid(tempStr(i), 2, 4)) & Mid(tempStr(i), 6)
            Else
                Char = Char & ChrW("&H" & Mid(tempStr(i), 1, 2)) & Mid(tempStr(i), 3)
            End If
        Next
    End If
    UnEscape_ = Char
End Function

Function UeRequest(str)
    UeRequest=UnEscape(request(str))
End Function

Function SafeReq(ByVal strReq)
    strReq = Replace(strReq, "'", "")
    strReq = Replace(strReq, """", "")
    SafeReq = Replace(strReq, " ", "")
End Function

Function SafeSQL(strSql)
    SafeSQL = strSql
    SafeSQL = Replace(SafeSQL, "<", "&lt;")
    SafeSQL = Replace(SafeSQL, ">", "&gt;")
    SafeSQL = Replace(SafeSQL, "'", """")
    SafeSQL = Replace(SafeSQL, "*", "")
    SafeSQL = Replace(SafeSQL, "?", "")
    SafeSQL = Replace(SafeSQL, "#", "%")
    SafeSQL = Replace(SafeSQL, "select", "")
    SafeSQL = Replace(SafeSQL, "insert into", "")
    SafeSQL = Replace(SafeSQL, "delete", "")
    SafeSQL = Replace(SafeSQL, "update", "")
    SafeSQL = Replace(SafeSQL, "delete", "")
    SafeSQL = Replace(SafeSQL, "create", "")
    SafeSQL = Replace(SafeSQL, vbCrLf & vbCrLf, "</p><p>")
    SafeSQL = Replace(SafeSQL, vbCrLf, "<br>")
    SafeSQL = Replace(SafeSQL, " ", "??")
    
End Function

Function Val(ByVal Str)
    Dim intv
    Dim intc
    Dim symbol
    Dim i
    intv = 0
    Str = Str & ""
    Val = ""
    If (Left(Str, 1) = "-") Or (Left(Str, 1) = "+") Then
        symbol = Left(Str, 1)
        Str = Mid(Str, 2)
    End If
    If StrComp(Str, "true", 1) = 0 Then
        Val = -1
        Exit Function
    End If
    If StrComp(Str, "false", 1) = 0 Then
        Val = 0
        Exit Function
    End If
    For i = 1 To Len(Str)
        intc = Asc(Mid(Str, i, 1))
        If intc >= 48 And intc <= 57 Then
            Val = Val & Chr(intc)
        ElseIf intc = 46 Then
            If intv = 0 Then
                intv = 1
                Val = Val & Chr(intc)
            Else
                Exit For
            End If
        Else
            Exit For
        End If
    Next
    If Val = "" Then
        Val = 0
    Else
        Val = CDbl(symbol & Val)
    End If
End Function

Function IIf(expr, truepart, falsepart)
    If expr Then
        IIf = truepart
    Else
        IIf = falsepart
    End If
End Function

Function LocalIP()
    LocalIP=Request.ServerVariables("LOCAL_ADDR")
End Function

Function RemoteIP()
    RemoteIP=Request.ServerVariables("HTTP_X_FORWARDED_FOR")
    if RemoteIP="" then RemoteIP=Request.ServerVariables("REMOTE_ADDR")
End Function

Function Dir(ByVal fileName)
    Dim fs
    Set fs = CreateObject("Scripting.FileSystemObject")
    If fs.FileExists(fileName) Then Dir = fileName
    Set fs = Nothing
End Function

sub MsgBox(strMSG,Modal)
	Response.Charset="gb2312"
	Response.Write("<script>alert('"&strMSG&"');</script>")
	if Modal then Response.End()
End sub
sub Return(param,Value) 
	Response.ContentType= "text/html"
	Response.Charset="gb2312"
	Response.Write("<return><val>"&Value&"</val><param>"&param&"</param></return>")
	Response.End()
End sub
sub returnJSON(status,data) 
	Response.ContentType= "text/html"
	Response.Charset="gb2312"'
	Response.Write("{""status"":"""&status&""",""data"":"""&data&"""}")
	Response.End()
End sub
sub returnAJAX(success,msg)
	Response.Charset="gb2312"
	Response.Write("{success:"&iif(success,"true","false")&",msg:"""&msg&"""}")
	Response.End()
End sub
sub GotoUrl(strUrl,Modal) 
	Response.Write("<script>window.location='"&strUrl&"';</script>")
	if Modal then Response.End()
End sub
sub BackUrl(Modal) 
	Response.Write("<script>window.history.back();</script>")
	if Modal then Response.End()
End sub
Function post(url, data)
    Set xml = CreateObject("Msxml2.ServerXMLHTTP")
    xml.Open "POST", url, False, "", ""
    xml.setRequestHeader "CONTENT-TYPE", "application/x-www-form-urlencoded"
    xml.send (data)
    post = xml.responseText
    Set xml = Nothing
End Function
%>
