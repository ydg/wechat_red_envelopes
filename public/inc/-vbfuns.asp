<%
Function urlDecode(strCode)
    Dim strChar,i
    For i = 1 To Len(strCode)
        strChar = Mid(strCode, i, 1)
        If strChar = "%" Then
            strHex = Mid(strCode, i + 1, 2)
            i = i + 2
            strChar = Mid(strCode, i + 1, 1)
            If strChar = "%" Then
                i = i + 1
                strHex = strHex & Mid(strCode, i + 1, 2)
                i = i + 2
            End If
            urlDecode = urlDecode & Chr("&H" & strHex)
        Else
            urlDecode = urlDecode & strChar
        End If
    Next
End Function

Function urlEncode(strCode)
    Dim i
    For i = 1 To Len(strCode)
        strHex = Right("0000" & Hex(Asc(Mid(strCode, i, 1))), 4)
        urlEncode = urlEncode & "%" & Left(strHex, 2) & "%" & Right(strHex, 2)
    Next
End Function

Function UnEscape(str)
    Dim i
    Dim tempStr
    Dim Char
    tempStr = Split(str, "%")
    If UBound(tempStr) >= 0 Then
        Char = tempStr(0)
        For i = 1 To UBound(tempStr)
            If Left(tempStr(i), 1) = "u" Then
                Char = Char & ChrW("&H" & Mid(tempStr(i), 2, 4)) & Mid(tempStr(i), 6)
            Else
                Char = Char & ChrW("&H" & Mid(tempStr(i), 1, 2)) & Mid(tempStr(i), 3)
            End If
        Next
    End If
    UnEscape = Char
End Function

Function UeRequest(str)
    UeRequest=UnEscape(request(str))
End Function

Function SafeReq(ByVal strReq)
    strReq = Replace(strReq, "'", "")
    strReq = Replace(strReq, """", "")
    SafeReq = Replace(strReq, " ", "")
End Function

Function SafeSQL(strSql)
    SafeSQL = Replace(strSql, "<", "&lt;")
    SafeSQL = Replace(strSql, ">", "&gt;")
    SafeSQL = Replace(strSql, "'", """")
    SafeSQL = Replace(strSql, "*", "")
    SafeSQL = Replace(strSql, "?", "")
    SafeSQL = Replace(strSql, "select", "")
    SafeSQL = Replace(strSql, "insert into", "")
    SafeSQL = Replace(strSql, "delete", "")
    SafeSQL = Replace(strSql, "update", "")
    SafeSQL = Replace(strSql, "delete", "")
    SafeSQL = Replace(strSql, "create", "")
    SafeSQL = Replace(strSql, vbCrLf & vbCrLf, "</p><p>")
    SafeSQL = Replace(strSql, vbCrLf, "<br>")
    SafeSQL = Replace(strSql, " ", "??")
End Function

Function Val(ByVal Str)
    Dim intv
    Dim intc
    Dim i
    intv = 0
    Str=Str & ""
    Val = ""
    For i = 1 To Len(Str)
        intc = Asc(Mid(Str, i, 1))
        If intc >= 48 And intc <= 57 Then
            Val = Val & Chr(intc)
        ElseIf intc = 46 Then
            If intv = 0 Then
                intv = 1
                Val = Val & Chr(intc)
            Else
                Exit For
            End If
        Else
            Exit For
        End If
    Next
    If Val = "" Then 
        Val = 0
    else
        Val = CDbl(Val)
    end if
End Function
Function IIf(expr, truepart, falsepart)

    If expr Then
        IIf = truepart
    Else
        IIf = falsepart
    End If

End Function

Function LocalIP()
    LocalIP=Request.ServerVariables("LOCAL_ADDR")
End Function

Function RemoteIP()
    RemoteIP=Request.ServerVariables("HTTP_X_FORWARDED_FOR")
    if RemoteIP="" then RemoteIP=Request.ServerVariables("REMOTE_ADDR")
End Function

Function Dir(ByVal fileName)
    Dim fs
    Set fs = CreateObject("Scripting.FileSystemObject")
    If fs.FileExists(fileName) Then Dir = fileName
    Set fs = Nothing
End Function

sub MsgBox(strMSG,Modal) 
	Response.Write("<script>alert('"&strMSG&"');</script>")
	if Modal then Response.End()
End sub
sub Return(param,Value) 
	Response.ContentType= "text/html"
	Response.Charset="gb2312"'
	Response.Write("<return><param>"&param&"</param><val>"&Value&"</val></return>")
	Response.End()
End sub
sub GotoUrl(strUrl,Modal) 
	Response.Write("<script>window.location='"&strUrl&"';</script>")
	if Modal then Response.End()
End sub
sub BackUrl(Modal) 
	Response.Write("<script>window.history.back();</script>")
	if Modal then Response.End()
End sub
%>
