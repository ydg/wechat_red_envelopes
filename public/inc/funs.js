function $pz(n)
{
	var o=new Object
	if(typeof(n)=="object"){o.el=n}else{o.el=document.getElementById(n)}
	o.addClass=function(n){if(o.el)o.el.className=o.el.className+" "+n}	
	o.removeClass=function(n){if(o.el)o.el.className=o.el.className.replace(" "+n)}
	o.show=function(){if(o.el)o.el.style.display="block"}	
	o.hide=function(){if(o.el)o.el.style.display="none"}	
	return o
}
window.onload=function(){
	frm_submit.onchange=frm_submit.onkeyup=function(){
		var t=event.target
		var e=window.event
		if(event.srcElement)t=event.srcElement;
		do{
			if(t.nodeType==1){
				$pz(t).removeClass("error")
				break;
			}else t=t.parentNode
		}while(t!=this)
		return true
	}
}
function refreshQry(form,data,st){
	var oper;
	var ss;
	if(st){ss=st}else{ss=store};
	ss.baseParams={}
	if(data)for(var item in data){ss.baseParams[item]=data[item]}
	for(var i=0;i<form.elements.length;i++){ 
		if(form.elements[i].value!=""){
			oper=form.elements[i].getAttribute("DATA-OPER")
			if(oper){
				if(oper=="KTS"){ss.baseParams[form.elements[i].name]=oper+":%25"+form.elements[i].value+"%25"}
				else{ss.baseParams[form.elements[i].name]=oper+":"+form.elements[i].value}
			}
		}
	}
	store.reload();
}
function pzstore(params,field,dir,limit,url,fields){
	return new Ext.data.Store({
        remoteSort:true,
        baseParams:params ,
        sortInfo: {field:field, direction:dir},
        autoLoad:{params:{start:0, limit:limit}},
        proxy: new Ext.data.ScriptTagProxy({url:url}),
        reader: new Ext.data.JsonReader({
            root:'root',
            totalProperty:'totalCount',
			fields:fields
        })
    });
}

function getfields(col){
	var f=[];
	for(var i=0;i<col.length;i++){if(col[i].dataIndex&&!col[i].no)f[i]=col[i].dataIndex}
	return f;
}
function excel(){
	var str='excel=1&sort='+sort+'&dir='+dir
	for(var item in store.baseParams){str+='&'+item+'='+store.baseParams[item]}
	var header='';
	for(var i=0;i<columns.length;i++){if(columns[i].dataIndex&&!columns[i].no)header+=',"'+columns[i].dataIndex+'":"'+columns[i].header+'"'}
	str+='&header=({'+header.substr(1)+'})'
	window.open('qry_method.asp?'+str,'','height=1,width=1,top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no')
}

function checkform(form) 
{
	var range,el,lval,uval
	for(var i=0;i<form.elements.length;i++){ 
		el=form.elements[i];
		if(el.getAttribute("range")){
			range=el.getAttribute("range")
			lval=parseInt(range.split("-")[0]);uval=parseInt(range.split("-")[1])
			if(el.value.length==0&&lval>0){$pz(el).addClass("error");el.focus();return false}
			if(el.getAttribute("type")=="number"){if(parseInt(el.value)<lval||parseInt(el.value)>uval){$pz(el).addClass("error");el.focus();Ext.Msg.alert('警告',"数值不在取值范围！");return false}}
			else{if(el.value.length<lval||el.value.length>uval){$pz(el).addClass("error");el.focus();Ext.Msg.alert('警告',"请输内容或者内容太长！");return false}}
		}
	}
}
function loadform(form,record){
	for(var i=0;i<form.elements.length;i++){
		if (form.elements[i].name!=""){
			if(form.elements[i].type=="checkbox")form.elements[i].checked=getbool(record.get(form.elements[i].name));	
			else{form.elements[i].value=record.get(form.elements[i].name)}
		}
	}
}
function setmoney(val){return"￥"+val};
function setcolor(val,color){return "<span style='cursor:pointer;color:"+color+"'>" + val + "</span>"};
/////////////////////////
function SendPOST(strUrl,strPOST){
	var xml;
	if(window.ActiveXObject) {
		xml = new ActiveXObject('Microsoft.XMLHTTP');
	} else if(window.XMLHttpRequest) {
		xml = new XMLHttpRequest();
	}
	xml.open ("POST", strUrl, false, "", "");
	xml.setRequestHeader ("CONTENT-TYPE", "application/x-www-form-urlencoded");
	xml.send (strPOST);
	return xml.responseText
}
function POSTForm(theForm){
	var str="";
	for(var i=0;i<theForm.elements.length;i++){ 
		if (theForm.elements[i].name!=""){
			if (theForm.elements[i].type=="checkbox"){
				if (theForm.elements[i].checked==true){
					str=str+"&"+theForm.elements[i].name+"="+escape(theForm.elements[i].value);
				}
			}else{
					str=str+"&"+theForm.elements[i].name+"="+escape(theForm.elements[i].value);
			}
		}
	}
	str=str.substr(1);
	return SendPOST(theForm.action,str);
}
function POSTMsgbox(str){
	str=RECT(str,"<return>","</return>");
	if (str.length==0){alert("程序代码错误！\n"+str);return 0};
	alert(RECT(str,"<param>","</param>"));
	return parseInt(RECT(str,"<val>","</val>"));
}
function RECT(strSrc, strStart, strEnd){
	var s=0,e=0;
	if(strStart != ""){
		s=strSrc.indexOf(strStart,0);
		if(s==-1){return ""}else{s=s+strStart.length};
	}
	if(strEnd != ""){
		e = strSrc.indexOf(strEnd,s);
		if(e==-1){return ""};
	}else{
		e=strSrc.length
	}
	return strSrc.substring(s,e);
}