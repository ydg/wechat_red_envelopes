function pz_post(url,data,callback,type){
	var xml,str='';
	var datar
	if(data){for(var item in data){str+="&"+item+"="+escape(data[item])}}
	if(str.length){str=str.substr(1)}
	if(window.ActiveXObject){xml = new ActiveXObject('Microsoft.XMLHTTP')}
	else if(window.XMLHttpRequest){xml = new XMLHttpRequest()}
	xml.open ("POST", url, false, "", "");
	xml.setRequestHeader ("CONTENT-TYPE", "application/x-www-form-urlencoded");
	xml.send (str);
	if (type=="xml"){
		if(window.ActiveXObject){xml.responseXML.loadXML(xml.responseText)}
		datar=xml.responseXML
	}else if(type=="json"){
		str=xml.responseText
		if(str.substr(0,1)!="(")(str="("+str+")")
		datar=eval(str)
	}else{datar=xml.responseText}

	if(callback)callback(datar,true,xml)
	return true
}
function pz_postform(form,callback,type){
	var str=[];
	for(var i=0;i<form.elements.length;i++){ 
		if (form.elements[i].name!=""){
			if (form.elements[i].type=="checkbox"||form.elements[i].type=="radio"){if(form.elements[i].checked==true)str[form.elements[i].name]=form.elements[i].value;
			}else{	str[form.elements[i].name]=form.elements[i].value}
		}
	}
	return pz_post(form.action,str,callback,type);
}
function pz_box(config){
	//.id .time .mode .title .msg .yes .yesfun .nofun .no .el .align:left yesno
	var o=new Object
	var nnew=false;
	if(typeof(config)=="undefined")config={time:2000}
	if(config.id){o.el=document.getElementById(config.id)}else{o.el=document.createElement("div");document.body.appendChild(o.el);nnew=true}
	if(config.yesno)(config.yes="确定",config.no="取消")
	o.time=config.time	
	if(!config.mode)o.el.onclick=function(){if(event.target==this)o.hide()}
	o.el.className="pz-box-mode"
	var str="<div box class='pz-box'><div title class='pz-box-title'></div><div body class='pz-box-body'"
	if(config.align)str+="style='text-align:"+config.align+"'"
	str+="></div>"
	
	if(config.yes||config.no){
		str+="<div class='pz-css'>"
		if(config.no)str+="<div class='span6' style='float:right;'><button type='button' no>"+config.no+"</button></div>"
		if(config.yes)str+="<div class='span6'style='float:right;'><button type='button' yes class='red'>"+config.yes+"</button></div>"
		str+="</div>"
	}str+="</div>"
	o.el.innerHTML=str;
	if(config.el){o.body=document.getElementById(config.el);o.body.style.display="block"}
	o.show=function(msg){//msg .msg .el .body
		if(!msg)msg=Object;
		if(msg.el)config.el=msg.el;	if(msg.title)config.title=msg.title;if(msg.msg)config.msg=msg.msg
		var div=o.el.getElementsByTagName("div")
		for(var i=0;i<div.length;i++){
			if(div[i].hasAttribute("body")){
				if(typeof(msg)=="string"){
					div[i].innerHTML=msg
				}else{
					if(config.el){o.body=document.getElementById(config.el);o.elpar=o.body.parentNode;div[i].appendChild(o.body);o.body.style.display="block"}
					if(config.msg)div[i].innerHTML=config.msg
				}
			}
			if(div[i].hasAttribute("title")){if(config.title)div[i].innerHTML=config.title}
			if(div[i].hasAttribute("box"))var box=div[i];
		}o.el.style.display="block"
		if(box){box.style.top=(window.innerHeight/2)-(box.offsetHeight/2)+"px";}
		btn=o.el.getElementsByTagName("button")
		for(var i=0;i<btn.length;i++){
			if(btn[i].hasAttribute("no")){btn[i].onclick=function(){if(config.nofun){if(config.nofun())o.hide()}else{o.hide()}}}
			if(btn[i].hasAttribute("yes")){btn[i].onclick=function(){if(config.yesfun){if(config.yesfun())o.hide()}else{o.hide()}}}
		}if(o.time>0)setTimeout(o.hide,o.time);//2��
	}
	if(config.show)o.show();
	o.hide=function(){if(o.el)o.el.style.display="none";if(!config.keep)o.clear();if(config.hide)config.hide()}
	o.clear=function(){
		if(nnew){if(config.el){o.body.style.display="none";o.elpar.appendChild(o.body);}if(o.el){document.body.removeChild(o.el);delete o.el}}
	}
	return o;
}