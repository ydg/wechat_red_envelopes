<?php
$app_config = require(dirname(__FILE__) . '/../config/boot.php');
require $app_config['QEEPHP_DIR'] . '/library/q.php';
require $app_config['APP_DIR'] . '/myapp.php';
$ret = MyApp::instance($app_config);
if (isset($_GET['warehouse_code']) && $_GET['warehouse_code'])
{
	$warehouse_code = $_GET['warehouse_code'];
}
else
{
	$warehouse_code = 'GZ';
}
$shipping_method = Shipping_Method::getShippingMethodByWarehouse($warehouse_code);
foreach ($shipping_method as $k => $sm)
{
	$shipping_method[$k]['value'] = $sm['id'];
}
echo json_encode($shipping_method);
exit;