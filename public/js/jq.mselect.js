jQuery.fn.mselect = function(data, id, prefix, prompts, def, key_prefix) {
	var _data = data;
	var _prompts = prompts;
	var container = this;
	var _id = id;
	var _ids = [];
	var _default = def;
	var _defaults = [];
	var _prefix = prefix;
	var _key_prefix = key_prefix;

	this._updateSelect = function(data, id, prompt, defval) {
		var tmp = $('#'+id).attr('disabled','disabled');
		var html = '';
		if (prompt != null) {
			if (prompt != defval) {
				html += '<option value="' + prompt + '">' + prompt + '</option>';
			} else {
				html += '<option value="' + prompt + '" selected>' + prompt + '</option>';
			}
		}
		$.each(data, function(i,arr) {
			/*
			if (typeof i == 'number') {
				i = n;
			}
			*/
			if (_key_prefix != 'undefined' && _key_prefix)
			{
				i = i.replace(_key_prefix, '');
			}
			if (i != defval) {
				html += '<option value="' + i + '">' + arr['name'] + '</option>';
			} else {
				html += '<option value="' + i + '" selected>' + arr['name'] + '</option>';
			}
		});
		$('#'+id).html(html);
		tmp.removeAttr('disabled');
		//var first = $('#'+id+' option:first').val();
	};

	this._getValue = function(level) {
		if (level <= 0) {
			return [];
		}
		var obj = _data;
		for (var i = 0; ! jQuery.isEmptyObject(obj) && i < level; i++) {
			obj = obj[$('#'+_ids[i]).val()];
			if (! jQuery.isEmptyObject(obj))
			{
				obj = obj['children'];
			}
		}
		if (obj == null) {
			return [];
		}
		return obj;
	};
	
	this._getAllLevel = function(data) {
		var max_level=0;
		$.each(data, function(key, value){
			var new_level=0;
			if ( ! jQuery.isEmptyObject(value['children']))
			{
				new_level = container._getAllLevel(value['children'])+1;
			}
			else
			{
				new_level = 1;
			}
			if (max_level<new_level)
			{
				max_level = new_level;
			}
		});
		return max_level;
	};
	
	this._getDefaultValue = function(value, data) {
		for (var k in data)
		{
			var key = k;
			if (_key_prefix != 'undefined' && _key_prefix)
			{
				key = key.replace(_key_prefix, '');
			}
			if (key == value)
			{
				return [key];
			}
			else
			{
				if ( ! jQuery.isEmptyObject(data[k]['children']))
				{
					var ret = container._getDefaultValue(value ,data[k]['children']);
					if (ret != -1)
					{
						var result = [k];
						$.each(ret,function(i,r){
							result.push(r);
						});
						return result;
					}
				}
			}
		}
		return -1;
	};
	
	this._getSelectedValue = function(index) {
		for (var i=index; i>=0; i--)
		{
			var selected_value = $('#'+_ids[i]).val();
			if (selected_value)
			{
				return selected_value;
			}
		}
		return selected_value;
	};
	
	this._init = function() {
		for (var i = 0; i < _ids.length - 1; i++) {
			$('#'+_ids[i]).change(function() {
				var index = jQuery.inArray(this.id, _ids);
				var obj = container._getValue(index+1);
				$('#'+_id).val(container._getSelectedValue(index));
				container._updateSelect(obj, _ids[index+1], _prompts[index+1], _defaults[index+1]);
				$('#'+_ids[index+1]).change();
			});
		}
		$('#'+_ids[_ids.length-1]).change(function(){
			$('#'+_id).val(container._getSelectedValue(_ids.length-1));
		});
		container._updateSelect(_data, _ids[0], _prompts[0], _defaults[0]);
		$('#'+_ids[0]).change();
	};
	
	//获取联动层数
	var level = this._getAllLevel(_data);
	//组装select的html
	var _init_html = '';
	for (var i = 1; i <= level; i++)
	{
		_ids[i-1] = _prefix + i;
		_init_html += "<select id='" + _ids[i-1] + "'></select>";
	}
	$('#'+id).after(_init_html);
	//计算默认
	_defaults = this._getDefaultValue(_default, _data);
	//初始化
	this._init();
};
