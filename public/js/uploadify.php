<?php
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/

// Define a destination
// Relative to the root

if (!empty($_FILES))
{
	$file = $_FILES['Filedata'];
	$path = "../ebay_template/" . $_POST['path'];
	if ($file['error'] > 0)
	{
		switch ($file['error'])
		{
			  case 1:  echo 'File exceeded upload_max_filesize'; break;
		      case 2:  echo '不能超过800M'; break; 
		      case 3:  echo 'File only partially uploaded'; break;
		      case 4:  echo 'No file uploaded'; break;
		}
	}
	else
	{
		$fileTypes = array('gif', 'png', 'jpg', 'jpeg', 'txt');
		$fileParts = pathinfo($file['name']);
		if ( ! in_array($fileParts['extension'] , $fileTypes))
		{
		 	 echo '文件类型错误，请重新选择文件!<br>只允许gif,png,jpg,jpeg,txt类型的文件';
		}
		if(file_exists($path . '/' . $file['name']))
		{
			unlink($path . '/' . $file['name']);
		}
		$result = move_uploaded_file($file["tmp_name"], $path . '/' . $file['name']);
		if ($result)
		{
			echo '文件已成功上传！';
		}
	}
}
?>