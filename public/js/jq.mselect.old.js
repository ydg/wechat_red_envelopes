jQuery.fn.mselect = function(data, ids, prompts, defaults) {
	var _data = data;
	var _ids = ids;
	var _prompts = prompts;
	var _defaults = defaults;
	var container = this;

	this._updateSelect = function(data, id, prompt, defval) {
		var tmp = $('#'+id).attr('disabled','disabled');
		var html = '';
		if (prompt != null) {
			if (prompt != defval) {
				html += '<option value="' + prompt + '">' + prompt + '</option>';
			} else {
				html += '<option value="' + prompt + '" selected>' + prompt + '</option>';
			}
		}
		$.each(data, function(i,arr) {
			/*
			if (typeof i == 'number') {
				i = n;
			}
			*/
			if (i != defval) {
				html += '<option value="' + i + '">' + arr['name'] + '</option>';
			} else {
				html += '<option value="' + i + '" selected>' + arr['name'] + '</option>';
			}
		});
		$('#'+id).html(html);
		tmp.removeAttr('disabled');
		//var first = $('#'+id+' option:first').val();
	};

	this._getValue = function(level) {
		if (level <= 0) {
			return [];
		}
		var obj = _data;
		for (var i = 0; ! jQuery.isEmptyObject(obj) && i < level; i++) {
			obj = obj[$('#'+_ids[i]).attr('value')];
			if (! jQuery.isEmptyObject(obj))
			{
				obj = obj['children'];
			}
		}
		if (obj == null) {
			return [];
		}
		return obj;
	}

	this._init = function() {
		for (var i = 0; i < _ids.length - 1; i++) {
			$('#'+_ids[i]).change(function() {
				var index = jQuery.inArray(this.id, _ids);
				var obj = container._getValue(index+1);
				container._updateSelect(obj, _ids[index+1], _prompts[index+1], _defaults[index+1]);
				$('#'+_ids[index+1]).change();
			});
		}
		container._updateSelect(_data, _ids[0], _prompts[0], _defaults[0]);
		$('#'+_ids[0]).change();
	};

	this._init();
};
