<?php
$app_config = require(dirname(__FILE__) . '/../config/boot.php');
require $app_config['QEEPHP_DIR'] . '/library/q.php';
require $app_config['APP_DIR'] . '/myapp.php';
$ret = MyApp::instance($app_config);

$pack_user = array();
$user = User::find('parent_id=? or id=?', 55, 55)->asArray()->getAll();
foreach ($user as $u)
{
	$pack_user[] = array('id' => $u['id'], 'name' => $u['name']);
}
echo json_encode($pack_user);exit;