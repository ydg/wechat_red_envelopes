<?php
$app_config = require(dirname(__FILE__) . '/../config/boot.php');
require $app_config['QEEPHP_DIR'] . '/library/q.php';
require $app_config['APP_DIR'] . '/myapp.php';
$ret = MyApp::instance($app_config);
header("content-type:image/jpeg");
if (CURRENT_USER_ID)
{
	$path = Q::ini('custom_system/product_img_dir');
	$filename = $path . $_GET['s'];
	if (is_file($filename))
	{
		$wm = imagecreatefrompng('img/wm.png');
		if ($_SERVER['HTTP_HOST'] == '192.168.5.13')
		{
			$img = imagecreatefromjpeg($filename);
			imagecopy($img, $wm, 0, 0, 0, 0, 750, 750);
			imagejpeg($img);
		}
		else
		{
			$img = imagecreatefromjpeg($filename);
			imagecopy($img, $wm, 0, 0, 0, 0, 750, 750);
			imagejpeg($img,NULL,30);
		}
		imagedestroy($img);
		imagedestroy($wm);
	}
	else
	{
		echo "the image does not exist";
	}
}
else
{
	$img = file_get_contents("img/null.jpg");
	echo $img;
}