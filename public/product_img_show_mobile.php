<?php
$app_config = require(dirname(__FILE__) . '/../config/boot.php');
require $app_config['QEEPHP_DIR'] . '/library/q.php';
require $app_config['APP_DIR'] . '/myapp.php';
$ret = MyApp::instance($app_config);
?>
<!DOCTYPE html> 
<html> 
<head> 
	<title>产品图片</title> 
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?=Q::ini('custom_system/base_url')?>css/jq.mobile.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.js"></script>
	<script type="text/javascript" src="<?=Q::ini('custom_system/base_url')?>js/jq.mobile.js"></script>
</head>
<body>
<div data-role="header" data-theme="b">
	<h1><?=isset($_GET['number']) ? $_GET['number'] : '产品图片'?></h1>
</div>
<img style="width:100%;height:100%" src="<?=Q::ini('custom_system/product_img_url_prefix') . $_GET['s']?>" />
</body>
