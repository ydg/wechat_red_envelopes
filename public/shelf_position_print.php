<?php
if ($_POST)
{
	if (isset($_POST['shelf_position']) && $_POST['shelf_position'])
	{
		require_once '../lib/tcpdf/config/lang/eng.php';
		require_once '../lib/tcpdf/tcpdf.php';
		$pdf = new TCPDF('L', PDF_UNIT, array(80, 25), true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(1, 1, 1);
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
		$pdf->SetAutoPageBreak(true, 1);
		$pdf->SetLanguageArray($l);
		$pdf->setCellMargins(0, 0, 1, 0);
		$pdf->SetFont('freeserif', 'B', 40);
		$shelf_position = explode("\n", $_POST['shelf_position']);
		foreach ($shelf_position as $k => $sp)
		{
			$pdf->AddPage();
			$pdf->MultiCell(78, 22, $sp, 0, 'C', 0, 1);
			$pdf->write1DBarcode($sp, 'C93', 0, 17, 80, 5, 0.38, array('align' => 'C', 'text' => false, 'stretchtext' => 0, 'cellfitalign' => 'C'), 'C');
		}
		$pdf->Output('pdf.pdf');exit;
	}
	else
	{
		$message = '请输入打印架位的内容';
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Express--Quick--Query</title>
<link href="css/bootstrap.css" rel="stylesheet">
<script type="text/javascript" src="js/jq.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
</head>
<body>
<div class="container">
<form method="post">
<fieldset>
<legend>打印架位</legend>
<?if (isset($message)):?>
<div class="alert alert-error fade in">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>打印失败！</strong> <?=isset($message) ? $message : ''?>
</div>
<?endif;?>
<label>说明：可填入多个架位号，每条记录用回车分开</label>
<textarea rows="15" name="shelf_position"></textarea><br />
<input type="submit" class="btn" value="打印" />
</fieldset>
</form>
</div>
</body>
</html>