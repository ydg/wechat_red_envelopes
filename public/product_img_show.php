<?php
set_time_limit(5);
$app_config = require(dirname(__FILE__) . '/../config/boot.php');
require $app_config['QEEPHP_DIR'] . '/library/q.php';
require $app_config['APP_DIR'] . '/myapp.php';
$ret = MyApp::instance($app_config);
header("content-type:image/jpeg");
if (CURRENT_USER_ID)
{
	$img = file_get_contents(Q::ini('custom_system/product_img_remote_url_prefix') . $_GET['s']);
	echo $img;
}
else
{
	$img = file_get_contents("img/null.jpg");
	echo $img;
}