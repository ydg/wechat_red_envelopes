<?php
$app_config = require(dirname(__FILE__) . '/../config/boot.php');
require $app_config['QEEPHP_DIR'] . '/library/q.php';
require $app_config['APP_DIR'] . '/myapp.php';
$ret = MyApp::instance($app_config);

set_time_limit(1);
$public_path = Q::ini('custom_system/product_img_dir');
$number = (isset($_GET['number']) && $_GET['number']) ? $_GET['number'] : '';
if ($number)
{
	$img = array();
	if (is_dir($public_path . 'default'))
	{
		$path = 'default/products/';
		$img = `find $public_path$path$number -name '*.jpg' -printf %P"\n"|sort`;
		if ($img)
		{
			$img = explode("\n", $img);
			array_pop($img);
			$key = array_search($number . '.jpg', $img);
			if ($key !== False)
			{
				unset($img[$key]);
				array_unshift($img, $number . '.jpg');
			}
			foreach ($img as &$i)
			{
				$i = $path . $number . '/' . $i;
			}
		}
		else
		{
			$path = 'iwowcase/products/';
			$img = `find $public_path$path$number -name '*.jpg' -printf %P"\n"|sort`;
			if ($img)
			{
				$img = explode("\n", $img);
				array_pop($img);
				$key = array_search($number . '.jpg', $img);
				if ($key !== False)
				{
					unset($img[$key]);
					array_unshift($img, $number . '.jpg');
				}
				foreach ($img as &$i)
				{
					$i = $path . $number . '/' . $i;
				}
			}
			else
			{
				$img = array();
			}
		}
	}
	else
	{
		$img = array();
	}
}
else
{
	$img = array();
}
$new_img = array();
foreach ($img as $i)
{
	$first_pos = strrpos($i, '-');
	$last_pos = strrpos($i, '.');
	if ($first_pos)
	{
		$key = substr($i, $first_pos+1, $last_pos-$first_pos-1);
	}
	else
	{
		$key = 0;
	}
	$new_img[$key] = $i;
}
ksort($new_img);
echo json_encode(array_values($new_img));
?>