<?php
header("content-type:text/html; charset=utf-8");
$app_config = require(dirname(__FILE__) . '/../config/boot.php');
require $app_config['QEEPHP_DIR'] . '/library/q.php';
require $app_config['APP_DIR'] . '/myapp.php';
$ret = MyApp::instance($app_config);
$product = Product::find()->order('p_no asc')->asArray()->getAll();
$record = array();
foreach ($product as $p)
{
	$record[] = $p['p_no'].'#'.$p['p_name'];
}
dump(implode('||', $record));