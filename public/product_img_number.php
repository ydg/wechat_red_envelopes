<?php
$app_config = require(dirname(__FILE__) . '/../config/boot.php');
require $app_config['QEEPHP_DIR'] . '/library/q.php';
require $app_config['APP_DIR'] . '/myapp.php';
$ret = MyApp::instance($app_config);
header("content-type:image/jpeg");
if (isset($_GET['key']) && $_GET['key'] == Q::ini('custom_system/product_img_key'))
{
	$number = (isset($_GET['number']) && $_GET['number']) ? $_GET['number'] : '';
	if ($number)
	{
		$product = Product_Basic::find('number=?', $number)->asArray()->getOne();
		if ($product)
		{
			$imgs = json_decode($product['img']);
			if ($imgs)
			{
				$img = file_get_contents(Q::ini('custom_system/product_img_remote_url_prefix') . $imgs[0]);
				echo $img;
			}
		}
	}
}