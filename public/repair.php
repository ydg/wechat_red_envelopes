<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="image/x-icon" href="/box/favicon.ico" rel="icon"/>
<title>维护</title>
<style type="text/css">
* {
	margin: 0;
	padding: 0;
	font-family: '微软雅黑',Verdana,arial,tahoma,sans-serif;
}
body {
	color: #444444;
	font-size: 12px;
}
.container {
	margin: 80px auto;
	text-align: left;
	width: 455px;
}
.graymodel{ float:left; width:100%; margin:5px 0;}
.grayh, .grayf, .grayhl, .grayhr, .grayfl, .grayfr{ background:url(/box/img/rouGraybox.gif) }
.grayh{float:left; width:100%; background-position:0 -72px;  height:5px;}
.grayhl { float:left; width:5px; background-position:0 0;  height:5px;}
.grayhr { float:right; width:5px;  background-position:0 -36px;  height:5px;}
.grayhrc{float:right; margin-right:5px;height:28px; line-height:28px; text-align:right; cursor:pointer}
.grayf {width:100%; background-position:0 -180px; float:left; height:5px;}
.grayfl { float:left; width:5px; background-position:0 -108px; height:5px;}
.grayfr { float:right; width:5px; background-position:0 -144px;height:5px;}
.graybody { float:left;width:100%;height:100%;}
.graybodyin{border-left:1px solid #ccc;border-right:1px solid #ccc;height:100%;overflow:hidden; height: 250px;}
.hploginright{width:450px; }
.ssllogin{ width:100%; float:left;}
.ssllogin h1{ height:38px; line-height:36px;border-bottom:1px solid #ccc; margin-bottom:10px;font-size: 17px;font-weight: normal;color: #444;}
.ssllogin h1 span{ margin-left:10px;}
.ssllogin dl{ float:left;margin-top:30px;  padding-bottom:25px; width:100%;}
.ssllogin dt, .ssllogin dd{ float:left;}
.ssllogin dt{ width:120px; text-align:right; margin-right:10px;}
.ssllogin dd{ width:250px; margin-bottom:20px; vertical-align:middle}
.ssllogin dd input{ vertical-align:middle}
.ssllogin dd .logintext{border:1px solid #B0CED9;padding:3px;}
.hpbtnlogin {margin-left: 30px;width:112px; height:33px; padding-bottom:8px; padding-right:10px; border:none; color:#fff; font-size:13px; font-weight:bold; cursor:pointer;background:url(/box/img/loginbtn.png) no-repeat;}
.sslshadow{background:url(/box/img/shadow.png) no-repeat; width:463px; height:31px; float:left; margin-top:2px;}
</style>
</head>
<body>
<div class="container">
<div class="hploginright">
	<div class="graymodel">
		<div class="grayh">
			<div class="grayhl"></div>
			<div class="grayhr"></div>
		</div>
		<div class="graybody">
			<div class="graybodyin">
				<div class="ssllogin">
					<h1><span>XFv4 提示</span></h1>
					<div align="center" style="margin-top: 30px;font-size: 15px;">
					
					权限升级
					<br />如果有特殊需要，请联系<a target="_blank" href="http://sighttp.qq.com/msgrd?v=1&uin=2853321077&exe=qq&site=im.qq.com&menu=yes">XFFW</a>
					</div>
				</div>
			</div>
		</div>
		<div class="grayf">
			<div class="grayfl"></div><div class="grayfr"></div>
		</div>
	</div>
	<div class="sslshadow"></div>
</div>
</div>
</body>
</html>