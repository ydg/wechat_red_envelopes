<?php
$log_name = dirname(__FILE__) . '/../log/access/' . date('Y-m-d');
if (CURRENT_USER_ID)
{
	$user = CURRENT_USER_NAME;
}
else
{
	$user = '-';
}
$log_data = date('H:i:s') . ' ' . $user . ' ' . $_SERVER['REMOTE_ADDR'] . ' ' . $_SERVER['REQUEST_METHOD'] . ' ' . $_SERVER['REQUEST_URI'];
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$log_data .= ' ' . arrayToString($_POST);
}
`echo "$log_data" >> $log_name`;

function arrayToString($arr)
{
	$string = 'array(';
	foreach ($arr as $k => $a)
	{
		$string .= $k . '=>';
		if (is_array($a))
		{
			$string .= arrayToString($a) . ',';
		}
		else
		{
			$string .= $a . ',';
		}
	}
	return $string . ')';
}
